var initSelect2 = function() {
     $("select").select2({
          placeholder: "SELECT"
     });
}

var getBranches = function(branches, current_branch) {
     var branch_option = "";
     $(branches).each(function(b, branch) {
          var name = branch['name'];
          var address = branch['address'];
          var id = branch['id'];

          if(id == current_branch) {
               branch_option += "<option value='" + id + "' data-value='" + name + "' selected>" + id + " - " + name + "</option>";
          }else {
               branch_option += "<option value='" + id + "' data-value='" + name + "'>" + id + " - " + name + "</option>";
          }
     });

     $(branch_option).appendTo("#select-branch");

     $("#select-branch").on("change", function() {
          $("#field-branch").text($("#select2-select-branch-container").attr('title'));
     });
}

var getTypes = function(types, allBrands, allProducts) {
     var type_option = "<option value='null' selected disabled>SELECT</option>";
     var trigger = $("#new-type-toggle");
     var input_trigger = $("#input-type");
     var select_trigger = $("#select-type");
     var select2_dropdown = $(trigger).closest('label').siblings('span.select2');

     $(types).each(function(t, type) {
          var name = type['name'];
          var id = type['id'];

          type_option += "<option value='" + id + "'>" + name + "</option>";
     });

     $("#select-type").on("change", function() {
          $("#field-type").text($("#select2-select-type-container").attr('title'));
          $("#field-brand").text("");
          var brand_id = $(this).val()
          getBrands(allBrands, brand_id, allProducts);
          checkItemDetails();
     });

     $(type_option).appendTo("#select-type");
}

var getBrands = function(brands, type_id, allProducts) {
     var brand_option = "<option value='null' selected disabled>SELECT</option>";

     $("#select-brand").empty();
     $.map(brands, function(filtered_brand) {
          if (filtered_brand.type_fk == type_id) {
               $(filtered_brand).each(function(b, brand) {
                    var name = brand['name'];
                    var id = brand['id'];
                    brand_option += "<option value='" + id + "'>" + name + "</option>";
               });
          }
     });

     $(brand_option).appendTo("#select-brand");

     $("#select-brand").on("change", function() {
          $("#field-brand").text($("#select2-select-brand-container").attr('title'));
          var type_id = $("#select-type").val();
          var brand_id = $("#select-brand").val();
          getProducts(type_id, brand_id, allProducts);
          checkItemDetails();
     });
}

var getProducts = function(type_id, brand_id, allProducts) {
     var product_option = "<option value='null' selected disabled>SELECT</option>";

     $("#select-name").empty();
     $.map(allProducts, function(filtered_product) {
          if ((filtered_product.brand_fk == brand_id) && (filtered_product.type_fk == type_id)) {
               $(filtered_product).each(function(n, product) {
                    var name = product['name'];
                    var sku = product['sku'];
                    var id = product['id'];
                    product_option += "<option value='" + id + "'>" + name + "</option>";
               });
          }
     });

     $(product_option).appendTo("#select-name");

     $("#select-name").on("change", function() {
          $("#field-item-name").text($("#select2-select-name-container").attr('title'));
     });
}

var getNewType = function() {
     var trigger = $("#new-type-toggle");
     var input_trigger = $("#input-type");
     var select_trigger = $("#select-type");
     var select2_dropdown = $(trigger).closest('label').siblings('.input-group');
     trigger.on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val("");
          $(this).closest(".form-group").find("select").val("").trigger('change');
          toggleInputs(trigger, input_trigger, select_trigger, select2_dropdown);
     });
     input_trigger.on('change', function() {
          $("#field-type").text(input_trigger.val());
          checkItemDetails();
     });
     input_trigger.on('focusout', function() {
          $(this).siblings('select').find('option').each(function() {
               if($(this).text().toUpperCase() == input_trigger.val().toUpperCase()) {
                    $(this).parents('.form-group').find('a').trigger('click');
                    $(this).parents('select').val($(this).attr('value')).trigger("change");
               }
          });
     });
}

var getNewBrand = function() {
     var trigger = $("#new-brand-toggle");
     var input_trigger = $("#input-brand");
     var select_trigger = $("#select-brand");
     var select2_dropdown = $(trigger).closest('label').siblings('.input-group');
     trigger.on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val("");
          $(this).closest(".form-group").find("select").val("").trigger('change');
          toggleInputs(trigger, input_trigger, select_trigger, select2_dropdown);
     });
     input_trigger.on('change', function() {
          $("#field-brand").text(input_trigger.val());
          checkItemDetails();
     });
     input_trigger.on('focusout', function() {
          $(this).siblings('select').find('option').each(function() {
               if($(this).text().toUpperCase() == input_trigger.val().toUpperCase()) {
                    $(this).parents('.form-group').find('a').trigger('click');
                    $(this).parents('select').val($(this).attr('value')).trigger("change");
               }
          });
     });
}

var getNewName = function() {
     var trigger = $("#new-name-toggle");
     var input_trigger = $("#input-name");
     var select_trigger = $("#select-name");
     var select2_dropdown = $(trigger).closest('label').siblings('.input-group');
     trigger.on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val("");
          $(this).closest(".form-group").find("select").val("").trigger('change');
          toggleInputs(trigger, input_trigger, select_trigger, select2_dropdown);
     });
     input_trigger.on('change', function() {
          $("#field-item-name").text(input_trigger.val());
     });
     input_trigger.on('focusout', function() {
          $(this).siblings('select').find('option').each(function() {
               if($(this).text().toUpperCase() == input_trigger.val().toUpperCase()) {
                    $(this).parents('.form-group').find('a').trigger('click');
                    $(this).parents('select').val($(this).attr('value')).trigger("change");
               }
          });
     });
}

var toggleInputs = function(trigger, input_trigger, select_trigger, select2_dropdown) {
     if(trigger.hasClass('active')) {
          trigger.removeClass("active");
          trigger.text("Select");

          input_trigger.removeAttr("disabled");
          input_trigger.removeClass("d-none");
          select_trigger.attr("disabled", "disabled");
          select2_dropdown.hide();
     }else {
          trigger.addClass("active");
          trigger.text("Add");

          input_trigger.attr("disabled", "disabled");;
          input_trigger.addClass("d-none");
          select_trigger.removeAttr("disabled");
          select2_dropdown.show();
     }
}

var getDescription = function() {
     $("#input-description").on("change", function() {
          $("#field-description").text($(this).val());
     });
}

var getCost = function() {
     $('#input-cost').number(true, 2);
     $("#input-cost").on("keyup", function() {
          $("#field-cost").text("\u20B1 " + $.number($(this).val(),2));
          getTotal();
     });
}

var getQuantity = function() {
     $('#input-quantity').number(true);
     $("#input-quantity").on("keyup", function() {
          $("#field-quantity").text($(this).val());
          getTotal();
     });
}

var getPrice = function() {
     $('#input-price').number(true, 2);
     $("#input-price").on("keyup", function() {
          $("#field-price").text("\u20B1 " + $.number($(this).val(),2));
          getTotal();
     });
}

var getTotal = function() {
     var quantity = $("#input-quantity").val();
     var cost = $("#input-cost").val();
     var price = $("#input-price").val();

     var totalCost = $("#field-total-cost");
     var revenue = $("#field-revenue");

     totalCost.text("\u20B1 " + $.number(quantity * cost, 2));
     revenue.text("\u20B1 " + $.number(quantity * price, 2));
}

var validateForm = function(target) {
     $(target).validate({
          errorPlacement: function(error, element) {
               parent_container = element.parent("div");
               error_wrapper = "<div class='error-wrapper'></div>";
               $(error_wrapper).appendTo(element.parent("div"));
               error.appendTo(element.siblings(".error-wrapper"));
          },
          submitHandler: function(form) {
               $(target + " .confirm-modal").modal("show");
               $(target + " .confirm-modal .confirm").on("click", function() {
                    $("button[type='submit']").attr('disabled', 'disabled').find('span').text('PROCESSING');
                    form.submit();
               });
          }
     });
}

var initDatePicker = function() {
     $('.datepicker').datepicker({
         autoclose: true
     });
}

var getDates = function() {
     $("#date").on("change", function() {
          const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
          var transaction_date = $(this).val();

          var field_date = new Date(transaction_date);
          var field_month = field_date.getMonth() + 1;
          var field_date_text = months[field_date.getMonth()] + " " + field_date.getDate() + ", " + field_date.getFullYear();
          $("#field-date").text(field_date_text);
     });
}

var checkItemDetails = function() {
     var input_type = $("#input-type");
     var select_type = $("#select-type");

     var input_brand = $("#input-brand");
     var select_brand = $("#select-brand");

     var input_name = $("#input-name");
     var select_name = $("#select-name");

     if( (input_type.val() == "") && (select_type.val() == null) ) {
          input_brand.attr('readonly', 'readonly');
          select_brand.siblings(".select2").css("pointer-events", "none");
     } else {
          input_brand.removeAttr("readonly");
          select_brand.siblings(".select2").css("pointer-events", "auto");
     }

     if( (input_brand.val() == "") && (select_brand.val() == null) ) {
          input_name.attr('readonly', 'readonly');
          select_name.siblings(".select2").css("pointer-events", "none");
     } else {
          input_name.removeAttr("readonly");
          select_name.siblings(".select2").css("pointer-events", "auto");
     }
}
