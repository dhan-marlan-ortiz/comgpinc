var validateForm2= function() {
     'use strict';
     var forms = document.getElementsByClassName('needs-validation');
     
     var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {

               if (form.checkValidity() === true) {
                    $(this).find('button[type="submit"]').text('PROCESSING');
                    $(this).find('button[type="submit"]').attr('disabled', 'disabled');
               } else {
                    event.preventDefault();
                    event.stopPropagation();
                    $(this).find('.confirm-modal').modal('hide');
                    $(this).find('.form-modal').modal('show');
                    console.log($(this));
               }

               form.classList.add('was-validated');
          }, false);
     });
}

var voidSalesTransaction = function(base_url) {
     $('#void-sales-transaction-modal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var salesid = button.data('sales-id');
          var referenceid = button.data('reference-id');
          var modal = $(this);

          modal.find(".confirm").attr('href', base_url + "Transaction/voidSalesTransaction/" + salesid + "/" + referenceid);
     });
}

var updateStatus = function() {
     var trigger = $("#update-status-btn");
     var modal = $("#status-modal");
     var alertModal = $("#alert-modal");
     trigger.on("click", function (e) {
          e.preventDefault();
          var transactionId = $("#field-transactionID").text();
          var status = $("#field-status").text();
          if(status == "Sold") {
               alertModal.find(".message").text("Sold items cannot be updated.")
               alertModal.modal("show");
          }else {
               modal.find("input[name='transaction_id']").val(transactionId);
               modal.find("input[value='"+status+"']").attr("checked", "checked");
               modal.modal("show");
          }
     });
}

var updateHistoryRemarks = function() {
     var trigger = $(".update-history-remarks-btn");
     var modal = $("#update-history-remarks-modal");     
     
     trigger.on("click", function (e) {
          e.preventDefault();
          var data_id = $(this).attr('data-id');
          modal.closest("form").find("input[name='history_id']").val(data_id);
     });
}

var validateForm = function(target) {
     $(target).validate({
          errorPlacement: function(error, element) {
               parent_container = element.parent("div");
               error_wrapper = "<div class='error-wrapper'></div>";
               $(error_wrapper).appendTo(element.parent("div"));
               error.appendTo(element.siblings(".error-wrapper"));
          }
     });
}

var getRepurchasePayment = function(param) {
     $(param.charge).on("keyup", function(e) {
          $(param.due).val(parseFloat($(param.value).val()) + parseFloat($(param.fee).val()) + parseFloat($(param.charge).val()));
     });
     $(param.fee).on("keyup", function(e) {
          $(param.due).val(parseFloat($(param.value).val()) + parseFloat($(param.fee).val()) + parseFloat($(param.charge).val()));
     });
}

var getRenewalPayment = function(param) {
     $(param.charge).on("keyup", function(e) {
          $(param.due).val(parseFloat($(param.fee).val()) + parseFloat($(param.charge).val()));
     });
     $(param.fee).on("keyup", function(e) {
          $(param.due).val(parseFloat($(param.fee).val()) + parseFloat($(param.charge).val()));
     });
}

var initSelect2 = function() {
     $("select").select2({
          placeholder: "Select"
     });
}

var initDatePicker = function() {
     $('.datepicker').datepicker({
          autoclose: true
     });
}
