var initSlimScroll = function() {
}

var initDataTable = function() {
     // Bootstrap datepicker
     $('.date-range input').each(function() {
          $(this).datepicker({
               autoclose: true
          });
     });

     // Set up your table
     // var table = $('#my-table').DataTable();
     var table = $('#transaction-table').DataTable( {
          // "autoWidth": false,
          // "fnPreDrawCallback": function( oSettings ) {
               // viewTransaction();
          // },
          "order": [[ 2  , "desc" ]],
          "columnDefs": [{
               "targets": 'no-sort',
               "orderable": false
          }]
     });

     // Extend dataTables search
     $.fn.dataTable.ext.search.push(
          function(settings, data, dataIndex) {
               var min = $('#trans-min-date').val();
               var max = $('#trans-max-date').val();
               var createdAt = data[1] || 1; // Our date column in the table
               // console.log(min);
               // console.log(max);
               if (
                    min == "" || max == "" ||
                    (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
                    // moment.max(moment('2016-01-01'), moment('2016-02-01')).format()
               ) {
               
                    return true;
               }
               return false;
          }
     );

     $.fn.dataTable.ext.search.push(
          function(settings, data, dataIndex) {
               var min = $('#update-min-date').val();
               var max = $('#update-max-date').val();
               var updateAt = data[2] || 2; // Our date column in the table
               // console.log(min);
               // console.log(max);
               if (
                    min == "" || max == "" ||
                    (moment(updateAt).isSameOrAfter(min) && moment(updateAt).isSameOrBefore(max))
                    // moment.max(moment('2016-01-01'), moment('2016-02-01')).format()
               ) {
               
                    return true;
               }
               return false;
          }
     );

     // Re-draw the table when the a date range filter changes
     $('#entry-date input').change(function() {
          table.draw();
     });

     $('#update-date input').change(function() {
          table.draw();
     });

     $('#update-date-rb').change(function() {
          $("#entry-date").hide();
          $("#update-date").show();
          $('#update-min-date').val( $('#trans-min-date').val() ).trigger('change');
          $('#update-max-date').val( $('#trans-max-date').val() ).trigger('change');
          $('#entry-date input').val(null).trigger('change');
     });

     $('#entry-date-rb').change(function() {
          $("#entry-date").show();
          $("#update-date").hide();  
          $('#trans-min-date').val( $('#update-min-date').val() ).trigger('change');
          $('#trans-max-date').val( $('#update-max-date').val() ).trigger('change');
          $('#update-date input').val(null).trigger('change');
     });
     


     var filterBranch = $("#select-branch");
     filterBranch.on("change", function(e) {
          var branchColumn = 9;
          var filterBranchValue = filterBranch.val();
          if(filterBranchValue == "ALL") {
               filterBranchValue = "";
          }
          table.column(branchColumn).search(filterBranchValue).draw();
     });

     var filterCustomer = $("#select-customer");
     filterCustomer.on("change", function() {
          var customerColumn = 3;
          var filterCustomerValue = filterCustomer.val();
          if(filterCustomerValue == "ALL") {
               filterCustomerValue = "";
          }
          table.column(customerColumn).search(filterCustomerValue).draw();
     });

     var filterTransaction = $("#search-transaction");
     filterTransaction.on( 'keyup',function (e) {
          var transactionColumn = 8;
          var filtertransactionValue = filterTransaction.val();
          table.column(transactionColumn).search(filtertransactionValue).draw();
     });

     var filterStatus = $("#select-status");
     filterStatus.on("change", function(e) {
          var statusColumn = 0;
          var filterStatusValue = filterStatus.val();
          if(filterStatusValue == "ALL") {
               filterStatusValue = "";
          }
          table.column(statusColumn).search(filterStatusValue).draw();
     });
     $("#table-options").removeClass('d-none').addClass('d-inline-block');
}

var initSelect2 = function() {
     $("select").select2({
          placeholder: "Select"
     });
}

var getCustomers = function(customers) {
     var customer_option = "<option value='ALL' selected>ALL CUSTOMERS</option>";

     $(customers).each(function(c, customer) {
          var fullname = customer['first_name'] + " " + customer['middle_name'] + " " + customer['last_name'];
          var complete_address = customer['address'] + ", " + customer['city_name'] + ", " + customer['province'];
          var city_address = customer['city_name'];
          var id = customer['id'];
          var contact = customer['contact'];

          customer_option += "<option value='" + fullname + "'>" + fullname + " - " + contact + "</option>";
     });

     $(customer_option).appendTo("#select-customer");

}

var getBranches = function(branches) {
     var branch_option = "<option value='ALL' data-value='ALL' selected>ALL LOCATIONS</option>";

     $(branches).each(function(b, branch) {
          var name = branch['name'];
          var address = branch['address'];
          var id = branch['id'];

          branch_option += "<option value='" + id + "' data-value='" + name + "'>" + id + " - " + name + "</option>";
     });

     $(branch_option).appendTo("#select-branch");
}

var formatDate = function(dateToFormat) {
     const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
     date = new Date(dateToFormat);

     return months[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear();
}

var getDailyTransactions = function(branch_fk) {
     var dates_html = "";
     var target = $("#field-transactions");

     $.map(transactions, function(transaction, branch_id) {
          if (branch_id == branch_fk) {
               target.empty();
               $("#selected-branch").text(branch_id);
               $(transaction).each(function(ed, eachDate) {
                    var details = eachDate.details;
                    var dates = eachDate.dates;
                    var details_html = "<ul>";
                    $(details).each(function(d, details) {
                         var transaction_id = details['transaction_id'];
                         var customer = details['first_name'] + " " + details['middle_name'] + " " + details['last_name'];
                         details_html += "<li>" +
                              "<a href='#' " +
                              "data-id='" + transaction_id +
                              "'data-branch='" + details['branch_fk'] +
                              "'data-date='" + details['date'] +
                              "'data-status='" + details['status'] +
                              "'data-firstname='" + details['first_name'] +
                              "'data-middlename='" + details['middle_name'] +
                              "'data-lastname='" + details['last_name'] +
                              "'data-contact='" + details['contact'] +
                              "'data-gender='" + details['gender'] +
                              "'data-type='" + details['type_name'] +
                              "'data-brand='" + details['brand_name'] +
                              "'data-serial='" + details['serial'] +
                              "'data-duration='" + details['duration'] +
                              "'data-expiration='" + details['expiration'] +
                              "'data-value='" + details['value'] +
                              "'data-fee='" + details['fee'] +
                              "'data-amount='" + details['amount'] +
                              "'data-description='" + details['description'] +
                              "'data-appraiser='" + details['appraiser'] +
                              "'data-appraiser2='" + details['appraiser2'] +
                              "'class='transaction-id'>" + transaction_id +
                              "</a> - " + customer +
                              "</li>";
                    });
                    details_html += "</ul>"
                    dates_html += "<li>" + formatDate(dates) + details_html + "</li>";
               });
          }
     });
     $(dates_html).appendTo(target);

     viewTransaction();
     closeTransaction();
}

var getTransactions = function(transactions) {
     var trans_html = "";
     var table = "#transaction-table tbody";

     $(transactions).each(function(bt, trans) {
          var fullName = trans['fname'] + " " + trans['mname'] + " " + trans['lname'];
          var transactionDate = moment(trans['date']).format("MMM. D, YYYY");
          var date = trans['date'];
          var expirationDate = moment(trans['expiration']).format("MMM. D, YYYY");
          var product = trans['brand_name'] + " " + trans['product_name'];
          var itemType = trans['type_name'];
          var transactionID = trans['transaction_id'];
          var unitValue = $.number(trans['value'], 2);
          var transactionFee = $.number(trans['fee'], 2);
          var amountDue = $.number(trans['amount'], 2);
          var status = trans['status'];
          var address = trans['address'] + ", " + trans['city'] + ", " +  trans['province'];
          var branch = trans['branch_name'] + " (" + trans['branch_fk'] + ")";
          var base_url = $("#base-url").val();

          var category = trans['category'];
          var category_name = "";
          if(category == 1) {
               category_name = "Pawn";
          } else if(category == 2) {
               category_name = "Purchase";
               fullName = "";
          } else if(category == 3) {
               category_name = "Retail";
               fullName = "";
          }

          trans_html += "<tr " +
                              "  data-id='" + transactionID +
                              "' data-href='" + base_url + "Transaction?search=" + transactionID +
                              "' data-status='" + status +
                              "' data-category='" + category +
                         "' >" +
                              "<td class='cell-status'>" + status + "</td>" +
                              "<td class='cell-transaction-date' data-sort='" + date + "'>" + transactionDate + "</td>" +
                              "<td class='cell-full-name'>" + fullName + "</td>" +
                              "<td class='cell-item-type'>" + itemType + "</td>" +
                              "<td class='cell-product'>" + product + "</td>" +
                              "<td class='cell-unit-value column-shrinked'>" + unitValue + "</td>" +
                              "<td class='cell-transaction-fee column-shrinked'>" + transactionFee + "</td>" +
                              "<td class='cell-amount-due column-shrinked'>" + amountDue + "</td>" +
                              "<td class='cell-expiration-date column-shrinked'>" + expirationDate + "</td>" +
                              "<td class='cell-transaction-id'>" + transactionID + "</td>" +
                              "<td class='cell-branch'>" + branch + "</td>" +
                              "<td class='cell-branch'>" + category_name + "</td>" +
                         "</tr>";
     });

     $(trans_html).appendTo(table);
     viewTransaction();
}

var generateDataTable = function() {
     var table = $('#daily-transactions').DataTable();
}

var generateProfileLink = function(customer_fk) {
     var base_url = $("#base-url").val();
     var target = $("#view-customer-btn");
     var href =  base_url + "Customer/profile/" + customer_fk;
     target.attr('href', '');
     target.attr('href', href);
}

var viewTransaction = function() {
     var trigger = "#transaction-table tbody tr";
     var recordsContainer = $("#transaction-records");
     var detailsContainer = $("#transaction-details");

     $(trigger).on("click", function(e) {
          e.preventDefault();
          window.location.href = $(this).attr('data-href');
     });
}

var closeTransaction = function() {
     var trigger = "#transaction-details .close-btn";
     var recordsContainer = $("#transaction-records");
     var detailsContainer = $("#transaction-details");
     $(trigger).on("click", function(e) {
          e.preventDefault();
          recordsContainer.removeClass('d-none');
          detailsContainer.addClass('d-none');
     });
}

var toggleFilters = function() {
     var trigger = $("a[data-target='#filters']");
     var target = $('#filters');

     $('#filters').on('shown.bs.collapse', function() {
          // trigger.addClass("btn-light");
          // trigger.removeClass("btn-secondary");
          // trigger.removeClass("text-white");
          trigger.text("HIDE FILTERS");
     })

     $('#filters').on('hidden.bs.collapse', function() {
          // trigger.addClass("btn-secondary");
          // trigger.addClass("text-white");
          // trigger.removeClass("btn-light");
          trigger.text("SHOW FILTERS");
     })
}

var expandTable = function() {
     var columns = ".cell-item-type, .cell-unit-value, .cell-transaction-fee, .cell-amount-due, .cell-expiration-date, .cell-branch";
     var trigger = $("#expand-table-btn");

     var con = $("#table-options")
     con.addClass("d-inline-block");
     con.removeClass("d-none");
     // con.detach().appendTo("#table-toolbar");

     $(trigger).on("click", function(e) {
          e.preventDefault();
          if($(this).hasClass("shrinked")) {
               $(columns).removeClass("column-shrinked");
               $(this).removeClass("shrinked");
               trigger.text("SHRINK TABLE")

               trigger.addClass("btn-light");
               trigger.addClass("text-dark");
               trigger.removeClass("btn-secondary");
               trigger.removeClass("text-white");
          }else {
               $(columns).addClass("column-shrinked");
               $(this).addClass("shrinked");
               trigger.text("EXPAND TABLE");

               trigger.addClass("btn-secondary");
               trigger.addClass("text-white");
               trigger.removeClass("btn-light");
               trigger.removeClass("text-dark");
          }
     });
}

var updateStatus = function() {
     var trigger = $("#update-status-btn");
     var modal = $("#status-modal");
     var alertModal = $("#alert-modal");
     trigger.on("click", function (e) {
          e.preventDefault();
          var transactionId = $("#field-transactionID").text();
          var status = $("#field-status").text();
          if(status == "Sold") {
               alertModal.find(".message").text("Sold items cannot be updated.")
               alertModal.modal("show");
          }else {
               modal.find("input[name='transaction_id']").val(transactionId);
               modal.find("input[value='"+status+"']").attr("checked", "checked");
               modal.modal("show");
          }
     });
}

var showTable = function() {
     $(".loader").hide();
     $("#transaction-table").show();
}
