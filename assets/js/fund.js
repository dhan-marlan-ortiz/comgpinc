
var transferModal = function() {
     $('#transfer-btn').on('click', function (event) {
          var form = $("#transfer-form");
          form.get(0).reset();
          form.removeClass('was-validated');
     });
}

var updateUrl = function(base_url) {
     $('.nav-link').on('click', function (event) {
          var tab = $(this).attr("href").replace("#", "");
          var new_url = base_url + "Fund?tab=" + tab;
          window.history.pushState("data", "Title", new_url);

          if(tab == "record") {
               $("#filter-wrapper").fadeIn('fast');
          } else {
               $("#filter-wrapper").fadeOut('fast');
          }
     });
}

var cashModal = function() {
     $('#cashin-btn, #cashout-btn').on('click', function (event) {
          var title = $(this).attr('data-title');
          var tag = $(this).attr('data-tag')
          var form = $("#cash-form");
          var action = $("#cash-form").attr("data-action");

          form.attr("action", action + tag);
          form.get(0).reset();
          form.removeClass('was-validated');

          $(".modal-title").text(title);
     });
}

var initDatePicker = function() {
     $('.datepicker').datepicker({
          autoclose: true
     });
}

var initSelect2 = function() {
     $("select.form-control").select2({
          placeholder: "SELECT",
          closeOnSelect: true
     });
     $("#filter-location").select2({
          placeholder: "SELECT",
          closeOnSelect: true
     });
}

var numberFormat = function() {
     $('.float-format').number(true, 2);
     $('.int-format').number(true);
}

var formValidation = function() {
     'use strict';
     
     // Fetch all the forms we want to apply custom Bootstrap validation styles to
     var forms = document.getElementsByClassName('needs-validation');
     // Loop over them and prevent submission
     var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {

               if (form.checkValidity() === true) {
                    $('button[type="submit"]').text('PROCESSING');
                    $('button[type="submit"]').attr('disabled', 'disabled');
               } else {
                    $('.confirm-modal').modal('hide');
                    event.preventDefault();
                    event.stopPropagation();
                    
                    // $(this).find('.form-modal').modal('show');
               }

               form.classList.add('was-validated');
          }, false);
     });
}

var filterLocation = function() {
     var target = $("#filter-location");

     target.on("change", function(){
          target.closest("form").submit();
     });
}

var initDataTable = function(base_url, tab) {
     var table = $("#fund-table").DataTable( {
          "autoWidth": false,
          "columnDefs": [{
               "targets": 'no-sort',               
               "orderable": false
          }],
          /*"sDom": 'Rfrtlip', */
          "fnPreDrawCallback": function( oSettings ) {
               cashDetailsModal(base_url);
          }
     });

     table.on('order.dt search.dt', function() {
          table.column(0, {
               search: 'applied',
               order: 'applied'
          }).nodes().each(function(cell, i) {
               cell.innerHTML = i + 1;
          });
     }).draw();
     
     $(".dataTables_length").appendTo("#filter-length-wrapper");
     $(".dataTables_filter").appendTo("#filter-search-wrapper");

     if(tab == "record") {
          $("#filter-wrapper").fadeIn();
     }

     $("#fund-table").removeClass("d-none");
}

var cashDetailsModal = function (base_url) {
     $('#fund-table tbody tr').on('click', function (event) {
          var trigger = $(this);
          var target = $("#details-modal");
          var transaction = trigger.attr('data-transaction');
          var id = trigger.attr('data-id');
          var date = trigger.attr('data-date');
          var location = trigger.attr('data-origin');
          var destination = trigger.attr('data-destination');
          var user = trigger.attr('data-user');
          var tag = trigger.attr('data-tag');
          var amount = trigger.attr('data-amount');
          var description = trigger.attr('data-description');
          var acknowledged = trigger.attr('data-acknowledged');

          if(acknowledged == 0) {
               is_acknowledged = "NO";
          } else if(acknowledged == 1) {
               is_acknowledged = "YES";
          }

          $("#details-transaction").val(transaction);
          $("#details-date").val(date);

          if(trigger.hasClass('fund-cash')) {
               $("#details-location").closest(".col-6").show();
               $("#details-origin").closest(".col-6").hide();
               $("#details-destination").closest(".col-6").hide();
               $("#details-acknowledged").closest(".col-6").hide();
               $("#details-acknowledgedment-btn").hide();
          }else if(trigger.hasClass('fund-transfer')) {
               $("#details-location").closest(".col-6").hide();
               $("#details-origin").closest(".col-6").show();
               $("#details-destination").closest(".col-6").show();
               $("#details-acknowledged").closest(".col-6").show();
               if(acknowledged == 0) {
                    $("#details-acknowledgedment-btn").show();
               } else {
                    $("#details-acknowledgedment-btn").hide();
               }
          }

          $("#details-location").val(location);
          $("#details-origin").val(location);
          $("#details-destination").val(destination);
          $("#details-user").val(user);
          $("#details-tag").val(tag);
          $("#details-amount").val(amount);
          $("#details-description").val(description);
          $("#details-acknowledged").val(is_acknowledged);
          $("#details-acknowledgedment-btn").attr('href', base_url + 'Fund/acknowledgeFund/' + id);

          target.modal("show");
     });
}

var loader = function() {
     $(".loader").hide();
     $(".loader").next('.fade').addClass("show");
}