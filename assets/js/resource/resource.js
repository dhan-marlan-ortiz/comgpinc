var formValidation = function() {
     'use strict';
     var forms = document.getElementsByClassName('needs-validation');
     var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {

               if (form.checkValidity() === true) {
                    $(this).find('button[type="submit"]').text('PROCESSING');
                    $(this).find('button[type="submit"]').attr('disabled', 'disabled');
               } else {
                    event.preventDefault();
                    event.stopPropagation();
                    $(this).find('.confirm-modal').modal('hide');
                    $(this).find('.form-modal').modal('show');
               }

               form.classList.add('was-validated');
          }, false);
     });
}

var initDatePicker = function() {
     $('.datepicker').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
     });
}

var initSelect2 = function() {
     $(".select2").select2({
          autoclose: true
     });     
}

var clearFields = function(trigger, target) {
     $(trigger).on("click", function(e) {
          e.preventDefault();
          $(target).val(null).trigger('change');
          $(target).closest("form").removeClass('was-validated');
     });
}

var initDataTable = function(target) {
     var table = $(target).DataTable({
          "columnDefs": [{
               "searchable": false,
               "orderable": false,
               "targets": 0
          }],
          "order": [
               [1, 'asc']
          ],
          "columnDefs": [{
               "targets": 'no-sort',
               "searchable": false,
               "orderable": false
          }]
     });

     table.on('order.dt search.dt', function() {
          table.column(0, {
               search: 'applied',
               order: 'applied'
          }).nodes().each(function(cell, i) {
               cell.innerHTML = i + 1;
          });
     }).draw();
}

var numberFormat = function() {
     $('.float-format').number(true, 2);
     $('.int-format').number(true);
}