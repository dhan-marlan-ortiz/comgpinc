var validateForm2= function() {
     'use strict';
     var forms = document.getElementsByClassName('needs-validation');
     var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {

               if (form.checkValidity() === true) {
                    $(this).find('button[type="submit"]').text('Processing');
                    $(this).find('button[type="submit"]').attr('disabled', 'disabled');
               } else {
                    event.preventDefault();
                    event.stopPropagation();
                    $(this).find('.confirm-modal').modal('hide');
                    $(this).find('.form-modal').modal('show');
                    console.log($(this));
               }

               form.classList.add('was-validated');
          }, false);
     });
}

var initSlimScroll = function() {
     // $('.table-responsive').slimScroll({
     //      axis: 'y'
     // });
}

var initDataTable = function() {
     // Bootstrap datepicker
     $('.date-range input').each(function() {
          $(this).datepicker({
               autoclose: true
          });
     });

     // Set up your table
     // var table = $('#my-table').DataTable();
     var table = $('#transaction-table').DataTable( {
          "autoWidth": false,
          "order": [[ 5, "des" ]]
     });

     // Extend dataTables search
     $.fn.dataTable.ext.search.push(
          function(settings, data, dataIndex) {
               var min = $('#trans-min-date').val();
               var max = $('#trans-max-date').val();
               var createdAt = data[1] || 0; // Our date column in the table

               if (
                    (min == "" || max == "") ||
                    (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
               ) {
                    return true;
               }
               return false;
          }
     );

     // Re-draw the table when the a date range filter changes
     $('.date-range input').change(function() {
          table.draw();
     });

     var filterBranch = $("#select-branch");
     filterBranch.on("change", function(e) {
          var branchColumn = 6;
          var filterBranchValue = filterBranch.val();
          if(filterBranchValue == "ALL") {
               filterBranchValue = "";
          }
          table.column(branchColumn).search(filterBranchValue).draw();
     });

     var filterStatus = $("#select-status");
     filterStatus.on("change", function(e) {
          var statusColumn = 0;
          var filterStatusValue = filterStatus.val();
          if(filterStatusValue == "ALL") {
               filterStatusValue = "";
          }
          table.column(statusColumn).search(filterStatusValue).draw();
     });

     // $("#filter-toggle").detach().appendTo("#transaction-table_filter");
}

var initSelect2 = function() {
     $("select.select2").select2({
          placeholder: "Select"
     });
}

var getBranches = function(branches) {
     var branch_option = "<option value='ALL' data-value='ALL' selected>ALL LOCATIONS</option>";

     $(branches).each(function(b, branch) {
          var name = branch['name'];
          var address = branch['address'];
          var id = branch['id'];

          branch_option += "<option value='" + name + "' data-value='" + name + "'>" + name + "</option>";
     });

     $(branch_option).appendTo("#select-branch");
}

var formatDate = function(dateToFormat) {
     const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
     date = new Date(dateToFormat);

     return months[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear();
}

var getTransactions = function(transactions, base_url) {
     var trans_html = "";
     var table = "#transaction-table tbody";

     $(transactions).each(function(bt, trans) {
          var transactionDate = moment(trans['date_created']).format("MMM. D, YYYY");
          var product = trans['brand_name'] + " " + trans['product_name'] + " " + trans['description'];
          var stocks = trans['stocks'];
          var inventory = trans['stocks'];
          var quantity = trans['quantity'];
          var status = trans['sold'];
          var itemType = trans['type_name'];
          var transactionID = trans['transaction_id'];
          var branch = trans['branch_name'] + " (" + trans['branch_fk'] + ")";

          if(status == 1) {
               status = "<span class=''>Sold</span>";
          } else {
               status = "<span class=''>For Sale</span>";
          }

          trans_html += "<tr " +
                              "  data-id='" + transactionID +
                         "' >" +
                              "<td class='cell-product'>" + product + "</td>" +
                              "<td class='cell-status'>" + status + "</td>" +
                              "<td class='cell-item-type'>" + itemType + "</td>" +
                              "<td class='cell-transaction-id'>" + transactionID + "</td>" +
                              "<td class='cell-branch'>" + branch + "</td>" +
                              "<td class='cell-transaction-date'>" + transactionDate + "</td>" +
                         "</tr>";
     });
     $(trans_html).appendTo(table);
     viewTransaction(base_url);
}

var viewTransaction = function(base_url) {
     var trigger = "#transaction-table tbody tr";

     $(trigger).on("click", function(e) {
          e.preventDefault();
          window.location.href = base_url + "Purchase?search=" + $(this).attr('data-id');
     });
}

var closeTransaction = function() {
     var trigger = "#transaction-details .close-btn";
     var recordsContainer = $("#transaction-records");
     var detailsContainer = $("#transaction-details");
     $(trigger).on("click", function(e) {
          e.preventDefault();
          recordsContainer.removeClass('d-none');
          detailsContainer.addClass('d-none');
     });
}

var toggleFilters = function() {
     var trigger = $("a[data-target='#filters']");
     var target = $('#filters');

     $('#filters').on('shown.bs.collapse', function() {
          // trigger.addClass("btn-light");
          // trigger.removeClass("btn-secondary");
          // trigger.removeClass("text-white");
          trigger.text("HIDE FILTERS");
     })

     $('#filters').on('hidden.bs.collapse', function() {
          // trigger.addClass("btn-secondary");
          // trigger.addClass("text-white");
          // trigger.removeClass("btn-light");
          trigger.text("SHOW FILTERS");
     })
}

var showTable = function() {
     $(".loader").hide();
     $("#transaction-table").removeClass("fade");
}

var validateForm = function(target) {
     $(target).validate({
          errorPlacement: function(error, element) {
               parent_container = element.parent("div");
               error_wrapper = "<div class='error-wrapper'></div>";
               $(error_wrapper).appendTo(element.parent("div"));
               error.appendTo(element.siblings(".error-wrapper"));
               error.addClass("position-relative");
               error.addClass("mb-0");
          },
          submitHandler: function(form) {
               $(".modal").modal("hide");
               $("#sell-item-confirm").modal("show");
               $("#sell-item-confirm .confirm").on("click", function() {
                    form.submit();
               });
          }
     });
}

var initDatePicker = function() {
     $(".datepicker").datepicker({
          autoclose: true
     });
}

var sellItemAmount = function() {
     var qty = 0;
     var price = 0;
     var total = $("input[name='selling_total']");

     $("input[name='selling_price']").on("change", function(e) {
          e.preventDefault();
          qty = $("input[name='selling_quantity']");
          $(total).val(parseFloat(qty.val()) * parseFloat(price.val()));
     });

     $("input[name='selling_quantity']").on("keyup", function(e) {
          e.preventDefault();
          price = $("input[name='selling_price']");
          $(total).val("\u20B1 " + parseFloat(qty.val()) * parseFloat(price.val()));
     });
}
