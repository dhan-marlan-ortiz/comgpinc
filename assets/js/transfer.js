var validateForm = function() {
     'use strict';
     var forms = document.getElementsByClassName('needs-validation');
     var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {

               if (form.checkValidity() === true) {
                    $(this).find('button[type="submit"]').text('PROCESSING');
                    $(this).find('button[type="submit"]').attr('disabled', 'disabled');
               } else {
                    event.preventDefault();
                    event.stopPropagation();
                    $(this).find('.confirm-modal').modal('hide');
                    $(this).find('.form-modal').modal('show');
                    console.log($(this));
               }

               form.classList.add('was-validated');
          }, false);
     });
}

var initDataTable = function() {
     var table = $('.table').DataTable({
               "columnDefs": [{
               "searchable": false,
               "orderable": false,
               "targets": 'no-sort'
          }],
          "order": [
               [1, 'asc']
          ]
     });

     table.on('order.dt search.dt', function() {
          table.column(0, {
               search: 'applied',
               order: 'applied'
          }).nodes().each(function(cell, i) {
               cell.innerHTML = i + 1;
          });
     }).draw();
}

var initSelect2 = function() {
     $("select").select2({
          placeholder: "Select"
     });
     console.log('select2 test');
}

var initDatePicker = function() {
     $('.datepicker').datepicker({
          autoclose: true
     });

}

