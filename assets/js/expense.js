var loader = function() {
     $(".loader").hide();
     $(".loader").next('.fade').addClass("show");
}

var updateUrl = function(base_url) {
     $('.nav-link').on('click', function (event) {
          var tab = $(this).attr("href").replace(/[.#]/g, "");
          var new_url = base_url + "Expense?tab=" + tab;
          window.history.pushState("data", "Title", new_url);

          if(tab == "records") {
               $("#filter-wrapper").fadeIn('fast');
          } else {
               $("#filter-wrapper").fadeOut('fast');
          }
     });     
}

var getTotal = function() {
     var amounts = $("#expenses-table").DataTable().column(4).data();
     var sum = 0;
     $(amounts).each(function(a, amount) {
          sum += parseFloat(amount.replace(",", ""));
     });
     var total = $("#total-expenses").text($.number(sum, 2));
}

var getSum = function() {
     var table = document.getElementById('expenses-table');
     var sum = 0;
     try {
          for (var r = 1, n = table.rows.length; r < n-1; r++) {
               sum += parseFloat(table.rows[r].cells[4].innerHTML.replace(",", ""));
          }
          document.getElementById('total-expenses').innerHTML = "\u20B1 " + $.number(sum, 2);
     } catch (e) {
          document.getElementById('total-expenses').innerHTML = "";
     }
}

var getExpenses = function(expenses, role_fk) {
     var markup = "";
     var table = "#expenses-table";
     $(expenses).each(function(e, expense) {
          var id = expense['id'];
          var date = moment(expense['date']).format("D MMM YYYY");
          var type = expense['type'];
          var description = expense['description'];
          var amount = expense['amount'];
          var branch_name = $.trim(expense['branch_name']).toUpperCase();

          markup += "<tr>" +
                         "<td></td>" +
                         "<td data-sort='" + expense['date'] +"'>" + date + "</td>" +
                         "<td class='text-wrap'>" + type + "</td>" +
                         "<td class='text-wrap'>" + description + "</td>" +
                         "<td class='text-right text-monospace'>" + $.number(amount, 2) + "</td>" +
                         "<td>" + branch_name + "</td>";
          if(role_fk == 'ADMS') {
               markup += "<td class='text-center'>" + "<a href='' class='text-danger text-decoration-none' data-toggle='modal' data-target='#removeExpenseModal' data-id='" + id + "'><span class='ti-close'></i></a>" + "</td>";
          }else {
               markup += "<td class='text-center d-none'></td>";
          }

          markup += "</tr>";
     });

     $(markup).appendTo(table + " tbody");

     $('#removeExpenseModal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var dataid = button.data('id');
          var modal = $(this);
          var href = $("#confirm-remove-btn").attr('data-href');
          modal.find('#confirm-remove-btn').attr('href', href + dataid);
     })
}

var initDataTable = function(tab) {
     var table = $('.table').DataTable({
          "columnDefs": [{
               "targets": 'no-sort',
               "searchable": false,
               "orderable": false
          }],
          "bAutoWidth": false,
     });

     // Extend dataTables search
     $.fn.dataTable.ext.search.push(
          function(settings, data, dataIndex) {
               var min = $('#trans-min-date').val();
               var max = $('#trans-max-date').val();
               var createdAt = data[1] || 1; // Our date column in the table

               if ( (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max)) ) {
                    return true;
               }
               return false;
          }
     );

     // Re-draw the table when the a date range filter changes
     $('.date-range input').change(function() {
          table.draw();
          getSum();
     });

     table.on('order.dt search.dt', function() {
          table.column(0, {
               search: 'applied',
               order: 'applied'
          }).nodes().each(function(cell, i) {
               cell.innerHTML = i + 1;
          });
     }).draw();

     var filterLocation = $("#filter-location");
     filterLocation.on("change", function(e) {
          var locationColumn = 5;
          var filterLocationValue = $.trim(filterLocation.val()).toUpperCase();
          if(filterLocationValue == "ALL") {
               filterLocationValue = "";
          }
          table.column(locationColumn).search(filterLocationValue).draw();
          getSum();
     });

     var filterType = $("#filter-type");
     filterType.on("change", function(e) {
          var typeColumn = 2;
          var filterTypeValue = filterType.val();
          if(filterTypeValue == "ALL") {
               filterTypeValue = "";
          }
          table.column(typeColumn).search(filterTypeValue).draw();
          getSum();
     });

     // $("#expenses-table_filter").hide();
     // $("#filter-type-wrapper").show();
     // $("#filter-location-wrapper").show();
     // $("#filter-date-wrapper").show();
     
     if(tab == "records") {
          $("#filter-wrapper").fadeIn();
     }
     $("#expenses-table_length").appendTo("#filter-wrapper");
     $("#expenses-table_filter").appendTo("#filter-wrapper");
}

var validateForm = function() {
     'use strict';
     
     var forms = document.getElementsByClassName('needs-validation');
     // Loop over them and prevent submission
     var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
               if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    $(this).find('.confirm-modal').modal('hide');
               } else {
                    $(this).find('button[type="submit"]').text('PROCESSING');
                    $(this).find('button[type="submit"]').attr('disabled', 'disabled');
               }
               form.classList.add('was-validated');
          }, false);
     });
     
}

var initSelect2 = function() {
     $("select.form-control").select2();
}

var initDatePicker = function() {
     $('.datepicker').datepicker({
          autoclose: true
     });
}

var numberFormat = function() {
     $('.number-format').number(true, 2);
}
