var validateForm2= function() {
     'use strict';
     var forms = document.getElementsByClassName('needs-validation');
     var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {

               if (form.checkValidity() === true) {
                    $(this).find('button[type="submit"]').text('PROCESSING');
                    $(this).find('button[type="submit"]').attr('disabled', 'disabled');
               } else {
                    event.preventDefault();
                    event.stopPropagation();
                    $(this).find('.confirm-modal').modal('hide');
                    $(this).find('.form-modal').modal('show');
                    console.log($(this));
               }

               form.classList.add('was-validated');
          }, false);
     });
}

var initDataTable = function() {
     var product_table = $("#product-table").DataTable( {
          "autoWidth": false,
          "columnDefs": [{
               "targets": 'no-sort',
               "searchable": false,
               "orderable": false
          }],
     });

     product_table.on('order.dt search.dt', function() {
          product_table.column(0, {
               search: 'applied',
               order: 'applied'
          }).nodes().each(function(cell, i) {
               cell.innerHTML = i + 1;
          });
     }).draw();

     var filterLocation = $("#filter-location");
     filterLocation.on("change", function(e) {
          var branchColumn = 5;
          var filterLocationValue = filterLocation.val();
          product_table.column(branchColumn).search(filterLocationValue).draw();
     });

     $(".loader").hide();
     $("#product-table").removeClass("d-none");
}

/*
var initSelect2 = function() {
     $("select").select2({
          placeholder: "Select"
     });
} */

var initDatePicker = function() {
     $('.datepicker').datepicker({
          autoclose: true
     });
}

var initModal = function() {
     $('#remove-modal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var dataid = button.data('id');
          var modal = $(this);
          var href = $("#confirm-remove-btn").attr('data-href');
          modal.find('#confirm-remove-btn').attr('href', href + dataid);
     });

     $('#status-modal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var dataid = button.data('id');
          var modal = $(this);
          modal.find('input[name="id"]').attr('value', dataid);
     });
}

var getBranches = function(branches, current_branch) {
     var branch_option = "";
     $(branches).each(function(b, branch) {
          var name = branch['name'];
          var address = branch['address'];
          var id = branch['id'];

          if(id == current_branch) {
               branch_option += "<option value='" + id + "' data-value='" + name + "' selected>" + name.toUpperCase() + " - " + id.toUpperCase() + "</option>";
          }else {
               branch_option += "<option value='" + id + "' data-value='" + name + "'>" + name.toUpperCase() + " - " + id.toUpperCase() + "</option>";
          }
     });

     $(branch_option).appendTo("#select-branch");

     $("#select-branch").on("change", function() {
          $("#field-branch").text($("#select2-select-branch-container").attr('title'));
     });
}

var getTypes = function(types, allBrands, allProducts) {
     var type_option = "<option value='null' selected disabled>Select</option>";
     var trigger = $("#new-type-toggle");
     var input_trigger = $("#input-type");
     var select_trigger = $("#select-type");
     var select2_dropdown = $(trigger).closest('label').siblings('span.select2');

     $(types).each(function(t, type) {
          var name = type['name'];
          var id = type['id'];

          type_option += "<option value='" + id + "'>" + name + "</option>";
     });

     $("#select-type").on("change", function() {
          $("#field-type").text($("#select2-select-type-container").attr('title'));
          $("#field-brand").text("");
          var brand_id = $(this).val()
          getBrands(allBrands, brand_id, allProducts);
          checkItemDetails();
     });

     $(type_option).appendTo("#select-type");
}

var getBrands = function(brands, type_id, allProducts) {
     var brand_option = "<option value='null' selected disabled>Select</option>";

     $("#select-brand").empty();
     $.map(brands, function(filtered_brand) {
          if (filtered_brand.type_fk == type_id) {
               $(filtered_brand).each(function(b, brand) {
                    var name = brand['name'];
                    var id = brand['id'];
                    brand_option += "<option value='" + id + "'>" + name + "</option>";
               });
          }
     });

     $(brand_option).appendTo("#select-brand");

     $("#select-brand").on("change", function() {
          $("#field-brand").text($("#select2-select-brand-container").attr('title'));
          var type_id = $("#select-type").val();
          var brand_id = $("#select-brand").val();
          getProducts(type_id, brand_id, allProducts);
          checkItemDetails();
     });
}

var getProducts = function(type_id, brand_id, allProducts) {
     var product_option = "<option value='null' selected disabled>Select</option>";

     $("#select-name").empty();
     $.map(allProducts, function(filtered_product) {
          if ((filtered_product.brand_fk == brand_id) && (filtered_product.type_fk == type_id)) {
               $(filtered_product).each(function(n, product) {
                    var name = product['name'];
                    var sku = product['sku'];
                    var id = product['id'];
                    product_option += "<option value='" + id + "'>" + name + "</option>";
               });
          }
     });

     $(product_option).appendTo("#select-name");

     $("#select-name").on("change", function() {
          $("#field-item-name").text($("#select2-select-name-container").attr('title'));
     });
}

var getNewType = function() {
     var trigger = $("#new-type-toggle");
     var input_trigger = $("#input-type");
     var select_trigger = $("#select-type");
     var select2_dropdown = $(trigger).closest('label').siblings('span.select2');
     trigger.on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val("");
          $(this).closest(".form-group").find("select").val("").trigger('change');
          toggleInputs(trigger, input_trigger, select_trigger, select2_dropdown);
     });
     input_trigger.on('change', function() {
          $("#field-type").text(input_trigger.val());
          checkItemDetails();
     });
     input_trigger.on('focusout', function() {
          $(this).siblings('select').find('option').each(function() {
               if($(this).text().toUpperCase() == input_trigger.val().toUpperCase()) {
                    $(this).parents('.form-group').find('a').trigger('click');
                    $(this).parents('select').val($(this).attr('value')).trigger("change");
               }
          });
     });
}

var getNewBrand = function() {
     var trigger = $("#new-brand-toggle");
     var input_trigger = $("#input-brand");
     var select_trigger = $("#select-brand");
     var select2_dropdown = $(trigger).closest('label').siblings('span.select2');
     trigger.on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val("");
          $(this).closest(".form-group").find("select").val("").trigger('change');
          toggleInputs(trigger, input_trigger, select_trigger, select2_dropdown);
     });
     input_trigger.on('change', function() {
          $("#field-brand").text(input_trigger.val());
          checkItemDetails();
     });
     input_trigger.on('focusout', function() {
          $(this).siblings('select').find('option').each(function() {
               if($(this).text().toUpperCase() == input_trigger.val().toUpperCase()) {
                    $(this).parents('.form-group').find('a').trigger('click');
                    $(this).parents('select').val($(this).attr('value')).trigger("change");
               }
          });
     });
}

var getNewName = function() {
     var trigger = $("#new-name-toggle");
     var input_trigger = $("#input-name");
     var select_trigger = $("#select-name");
     var select2_dropdown = $(trigger).closest('label').siblings('span.select2');
     trigger.on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val("");
          $(this).closest(".form-group").find("select").val("").trigger('change');
          toggleInputs(trigger, input_trigger, select_trigger, select2_dropdown);
     });
     input_trigger.on('change', function() {
          $("#field-item-name").text(input_trigger.val());
     });
     input_trigger.on('focusout', function() {
          $(this).siblings('select').find('option').each(function() {
               if($(this).text().toUpperCase() == input_trigger.val().toUpperCase()) {
                    $(this).parents('.form-group').find('a').trigger('click');
                    $(this).parents('select').val($(this).attr('value')).trigger("change");
               }
          });
     });
}

var checkItemDetails = function() {
     var input_type = $("#input-type");
     var select_type = $("#select-type");

     var input_brand = $("#input-brand");
     var select_brand = $("#select-brand");

     var input_name = $("#input-name");
     var select_name = $("#select-name");

     if( (input_type.val() == "") && (select_type.val() == null) ) {
          input_brand.attr('readonly', 'readonly');
          select_brand.siblings(".select2").css("pointer-events", "none");
     } else {
          input_brand.removeAttr("readonly");
          select_brand.siblings(".select2").css("pointer-events", "auto");
     }

     if( (input_brand.val() == "") && (select_brand.val() == null) ) {
          input_name.attr('readonly', 'readonly');
          select_name.siblings(".select2").css("pointer-events", "none");
     } else {
          input_name.removeAttr("readonly");
          select_name.siblings(".select2").css("pointer-events", "auto");
     }
}

var toggleInputs = function(trigger, input_trigger, select_trigger, select2_dropdown) {
     if(trigger.hasClass('active')) {
          trigger.removeClass("active");
          trigger.text("Select");

          input_trigger.removeAttr("disabled");
          input_trigger.removeClass("d-none");
          select_trigger.attr("disabled", "disabled");
          select2_dropdown.hide();
     }else {
          trigger.addClass("active");
          trigger.text("Add");

          input_trigger.attr("disabled", "disabled");;
          input_trigger.addClass("d-none");
          select_trigger.removeAttr("disabled");
          select2_dropdown.show();
     }
}

var validateForm = function(target) {
     $(target).validate({
          errorPlacement: function(error, element) {
               parent_container = element.parent("div");
               error_wrapper = "<div class='error-wrapper'></div>";
               $(error_wrapper).appendTo(element.parent("div"));
               error.appendTo(element.siblings(".error-wrapper"));
          },
          submitHandler: function(form) {
               $(target + " .form-modal").modal("hide");
               $(target + " .confirm-modal").modal("show");
               $(target + " .confirm-modal .confirm").on("click", function() {
                    form.submit();
               });
          }
     });
}

var validateFormBS = function() {
     'use strict';
     window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('validate-addtocart');
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
               form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                         event.preventDefault();
                         event.stopPropagation();
                    }
                    form.classList.add('was-validated');
               }, false);
          });
     }, false);
}

var initSelect2 = function() {
     $("select.form-control").select2({
          autoclose: true
     });
     $("select.select2").select2({
          autoclose: true
     });
}

var numberFormat = function() {
     $('.number-format').number(true, 2);
     $('.int-format').number(true);
}

var getTotalPrices = function() {
     var qty =  $('#input-quantity');
     var buy =  $('#input-buy');
     var total_buy =  $('#total-buy');
     var retail =  $('#input-retail');
     var total_retail =  $('#total-retail');

     qty.on('keyup', function() {
          total_buy.val( '\u20B1 ' + $.number(parseInt(qty.val()) * parseInt(buy.val()), 2) );
          total_retail.val( '\u20B1 ' + $.number(parseInt(qty.val()) * parseInt(retail.val()), 2) );
     });

     buy.on('keyup', function() {
          total_buy.val( '\u20B1 ' + $.number(parseInt(qty.val()) * parseInt(buy.val()), 2) );
          total_retail.val( '\u20B1 ' + $.number(parseInt(qty.val()) * parseInt(retail.val()), 2) );
     });
}

var cartSettings = function() {
     table = $("#cart");
     submit_btn = $("#submit-cart-btn");
     var rows = table.find("tbody tr").length;

     if(rows > 0) {
          table.find("tfoot").fadeIn();
          submit_btn.attr("disabled", false);
     } else {
          table.find("tfoot").fadeOut();
          submit_btn.attr("disabled", "disabled");
     }
}

var addToCart = function() {
     $('#addtocart-modal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var product = button.data('product');
          var stock = button.data('stock');
          var unit = button.data('unit');
          var pk = button.data('pk');
          var id = button.data('id');
          var modal = $(this);
          var quantity = $("#addtocart-quantity");
          var amount = $("#addtocart-amount");
          var markup = "";

          modal.find('.modal-title').text(product);
          modal.find('#addtocart-quantity').attr('max', stock);
          modal.find('#addtocart-stock').text(stock);
          modal.find('#addtocart-unit').text(unit);
          modal.find('#addtocart-pk').val(pk);
          modal.find('#addtocart-id').val(id);

          $(".addtocart-validation")[0].reset();
          $(".addtocart-validation").removeClass('was-validated');
     });

     $('#inventory-modal').on('show.bs.modal', function (event) {
          $("#product-table").find(".addtocart-btn.added-to-cart").text("ADD TO CART");
          $("#product-table").find(".addtocart-btn.added-to-cart").removeClass("added-to-cart");

          $("#product-table").find("tbody tr").each(function() {
               var inventory_pk = $(this).find('.addtocart-btn');

               $("#cart").find("tbody tr").each(function(i) {
                    inventory_data_pk = inventory_pk.attr('data-pk');
                    cart_pk = $(this).find('input[name^="pk"]').attr('value');
                    if(inventory_data_pk == cart_pk) {
                         inventory_pk.text("ADDED");
                         inventory_pk.addClass("added-to-cart");
                    }
               });
          });
     });

     /* 
     'use strict';
     window.addEventListener('load', function() {
          var forms = document.getElementsByClassName('addtocart-validation');
          var validation = Array.prototype.filter.call(forms, function(form) {
               form.addEventListener('submit', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    if (form.checkValidity() === true) {

                         var pk = $("#addtocart-pk").val();
                         var id = $("#addtocart-id").val();
                         var product = $("#addtocart-modal .modal-title").text();
                         var quantity = $("#addtocart-quantity").val();
                         var amount = $("#addtocart-amount").val();
                         var unit = $("#addtocart-unit").text();
                         var target = $("#cart tbody");
                         var row = target.find("tr");

                         markup =   "<tr>" +
                                             "<td></td>" +
                                             "<td>" +
                                                  product +
                                                  "<input type='hidden' name='pk[]' value='" + pk + "'>" +
                                                  "<input type='hidden' name='id[]' value='" + id + "'>" +
                                             "</td>" +
                                             "<td class='text-center'>" +
                                                  $.number(quantity)  +
                                                  "<input type='hidden' name='quantity[]' value='" + quantity + "'>" +
                                             "</td>" +
                                             "<td class='text-center'>" + unit + "</td>" +
                                             "<td class='text-right'>" +
                                                  $.number(amount, 2) +
                                                  "<input type='hidden' name='amount[]' value='" + amount + "'>" +
                                             "</td>" +
                                             "<td class='text-right'>" + $.number(quantity * amount, 2) + "</td>" +
                                             "<td class='text-center'><a href='#' class='text-danger remove-row'><i class='ti-close'></i></a></td>" +
                                        "</tr>";
                         target.append(markup);

                         generateTableIndex("#cart");
                         getColumnSum("#cart", 4, 2, "\u20B1 ");
                         getColumnSum("#cart", 5, 2, "\u20B1 ");
                         getColumnSum("#cart", 2, 0);
                         removeRow();
                         cartSettings();

                         $("#addtocart-modal").modal('hide');
                    }
                    form.classList.add('was-validated');
               }, false);
          });
     }, false);
     */

     $("#addtocart-continue").on("click", function() {
          var pk = $("#addtocart-pk").val();
          var id = $("#addtocart-id").val();
          var product = $("#addtocart-modal .modal-title").text();
          var quantity = $("#addtocart-quantity").val();
          var amount = $("#addtocart-amount").val();
          var unit = $("#addtocart-unit").text();
          var target = $("#cart tbody");
          var row = target.find("tr");

          markup =   "<tr>" +
                              "<td></td>" +
                              "<td>" +
                                   product +
                                   "<input type='hidden' name='pk[]' value='" + pk + "'>" +
                                   "<input type='hidden' name='id[]' value='" + id + "'>" +
                              "</td>" +
                              "<td class='text-center'>" +
                                   $.number(quantity)  +
                                   "<input type='hidden' name='quantity[]' value='" + quantity + "'>" +
                              "</td>" +
                              "<td class='text-center'>" + unit + "</td>" +
                              "<td class='text-right'>" +
                                   $.number(amount, 2) +
                                   "<input type='hidden' name='amount[]' value='" + amount + "'>" +
                              "</td>" +
                              "<td class='text-right'>" + $.number(quantity * amount, 2) + "</td>" +
                              "<td class='text-center'><a href='#' class='text-decoration-none text-danger remove-row'>REMOVE</a></td>" +
                         "</tr>";
          target.append(markup);

          generateTableIndex("#cart");
          getColumnSum("#cart", 4, 2, "\u20B1 ");
          getColumnSum("#cart", 5, 2, "\u20B1 ");
          getColumnSum("#cart", 2, 0);
          removeRow();
          cartSettings();

          $("#addtocart-modal").modal('hide');
     });
}

var generateTableIndex = function(tableID) {
     table = $(tableID);
     table.find("tbody tr").each(function(i) {
          $(this).find("td").first().text(++i);
     });
}

var getColumnSum = function(tableID, col, point, prefix = "") {
     table = $(tableID);
     var sum = 0;

     table.find("tbody tr").each(function(i) {
          cell = $(this).find('td').eq(col).html();
          sum += parseFloat(cell.replace(",",""));
     });

     table.find("tfoot tr").first().find("th").eq(col).text(prefix + $.number(sum, point));
     return;
}

var removeRow = function() {
     $(".remove-row").on("click", function(e) {
          e.preventDefault();
          $(this).parents("tr").remove();
          generateTableIndex("#cart");
          getColumnSum("#cart", 4, 2, "\u20B1 ");
          getColumnSum("#cart", 5, 2, "\u20B1 ");
          getColumnSum("#cart", 2, 0);
          cartSettings();
     });
}
