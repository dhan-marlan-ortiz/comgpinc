var validateForm = function() {
     'use strict';
     // window.addEventListener('load', function() {

          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
               form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === true) {
                         $(this).find('button[type="submit"]').text('Processing');
                         $(this).find('button[type="submit"]').attr('disabled', 'disabled');                         
                         
                         if($("input.clicked").attr('name') != "export") {
                              $(".modal").modal('hide');                         
                              $("#generating-report-modal").modal('show');                         
                         }
                    } else {
                         event.preventDefault();
                         event.stopPropagation();
                         $(this).find('.confirm-modal').modal('hide');
                         $(this).find('.form-modal').modal('show');
                    }
                    form.classList.add('was-validated');
               }, false);
          });
     // }, false);
}

var initSelect2 = function() {
     $("select").select2({
          placeholder: "SELECT",
          closeOnSelect: false
     });
     $("select.close-on-click").select2({
          placeholder: "SELECT"
     });
}

var initDatePicker = function() {
     $('.datepicker').datepicker({
          autoclose: true
     });
}

var numberFormat = function() {
     $('.number-format').number(true, 2);
     $('.int-format').number(true);
}

var clearFields = function() {
     $(".clear-select").on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("select").val(null).trigger('change');
     });
     $(".clear-input").on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val(null).trigger('change');
     });
}

var selectAll = function() {
     $(".select-all").on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("select option").prop("selected", true).trigger("change");
     });
}

var loader = function() {
     $(".loader").hide();
     $(".has-loader").show();
}

var exportExcel = function() {
     $("input[name='export']").on("click", function() {
          $("#generating-report-modal").modal('hide');
     });
}

var clickedSubmit = function() {
     $("input[type='submit']").on("click", function() {
          $(this).addClass('clicked');
     });
}

var getSlimScroll = function(target, height) {
     $(target).slimScroll({
          height: height
     });
}

