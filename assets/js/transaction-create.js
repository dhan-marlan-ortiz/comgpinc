var initSelect2 = function() {
     $("select").select2({
          placeholder: "Select"

     });
}

var getCustomers = function(customers) {

     var customer_option = "<option value='null' selected disabled>Select</option>";

     $(customers).each(function(c, customer) {
          var fullname = customer['first_name'] + " " + customer['middle_name'] + " " + customer['last_name'];
          var complete_address = customer['address'] + ", " + customer['city_name'] + ", " + customer['province'];
          var city_address = customer['city_name'];
          var id = customer['id'];
          var contact = customer['contact'];

          customer_option += "<option value='" + id + "'>" + fullname + " - " + contact + "</option>";
     });

     $(customer_option).appendTo("#select-customer");

     $("#select-customer").on("change", function() {
          var value = $(this).val();
          $.map(customers, function(cust) {
               if (cust.id === value) {
                    $("#field-first-name").text(cust.first_name);
                    $("#field-middle-name").text(cust.middle_name);
                    $("#field-last-name").text(cust.last_name);
                    $("#field-contact").text(cust.contact);
                    $("#field-gender").text(cust.gender);
                    $("#field-id-presented").text(cust.identification_type);
                    $("#field-id-number").text(cust.identification_number);
                    $("#field-remarks").text(cust.remarks);
                    $("#field-address").text(cust.address + ", " + cust.city_name + ", " + cust.province);
               }
          });
     });

     $('.more-customer-info').on('shown.bs.collapse', function() {
          $("a[data-target='.more-customer-info']").text("Show less");
     })

     $('.more-customer-info').on('hidden.bs.collapse', function() {
          $("a[data-target='.more-customer-info']").text("Show more");
     })
}

var getBranches = function(branches, current_branch) {
     var branch_option = "";
     $(branches).each(function(b, branch) {
          var name = branch['name'];
          var address = branch['address'];
          var id = branch['id'];

          if(id == current_branch) {
               branch_option += "<option value='" + id + "' data-value='" + name + "' selected>" + id + " - " + name + "</option>";
          }else {
               branch_option += "<option value='" + id + "' data-value='" + name + "'>" + id + " - " + name + "</option>";
          }
     });

     $(branch_option).appendTo("#select-branch");

     $("#select-branch").on("change", function() {
          $("#field-branch").text($("#select2-select-branch-container").attr('title'));
     });
}

var getTypes = function(types, allBrands, allProducts) {
     var type_option = "<option value='null' selected disabled>Select</option>";
     var trigger = $("#new-type-toggle");
     var input_trigger = $("#input-type");
     var select_trigger = $("#select-type");
     var select2_dropdown = $(trigger).closest('label').siblings('span.select2');

     $(types).each(function(t, type) {
          var name = type['name'];
          var id = type['id'];

          type_option += "<option value='" + id + "'>" + name + "</option>";
     });

     $("#select-type").on("change", function() {
          $("#field-type").text($("#select2-select-type-container").attr('title'));
          $("#field-brand").text("");
          var brand_id = $(this).val()
          getBrands(allBrands, brand_id, allProducts);
          checkItemDetails();
     });

     $(type_option).appendTo("#select-type");
}

var getBrands = function(brands, type_id, allProducts) {
     var brand_option = "<option value='null' selected disabled>Select</option>";

     $("#select-brand").empty();
     $.map(brands, function(filtered_brand) {
          if (filtered_brand.type_fk == type_id) {
               $(filtered_brand).each(function(b, brand) {
                    var name = brand['name'];
                    var id = brand['id'];
                    brand_option += "<option value='" + id + "'>" + name + "</option>";
               });
          }
     });

     $(brand_option).appendTo("#select-brand");

     $("#select-brand").on("change", function() {
          $("#field-brand").text($("#select2-select-brand-container").attr('title'));
          var type_id = $("#select-type").val();
          var brand_id = $("#select-brand").val();
          getProducts(type_id, brand_id, allProducts);
          checkItemDetails();
     });
}

var getProducts = function(type_id, brand_id, allProducts) {
     var product_option = "<option value='null' selected disabled>Select</option>";

     $("#select-name").empty();
     $.map(allProducts, function(filtered_product) {
          if ((filtered_product.brand_fk == brand_id) && (filtered_product.type_fk == type_id)) {
               $(filtered_product).each(function(n, product) {
                    var name = product['name'];
                    var sku = product['sku'];
                    var id = product['id'];
                    product_option += "<option value='" + id + "'>" + name + "</option>";
               });
          }
     });

     $(product_option).appendTo("#select-name");

     $("#select-name").on("change", function() {
          $("#field-item-name").text($("#select2-select-name-container").attr('title'));
     });
}

var getNewType = function() {
     var trigger = $("#new-type-toggle");
     var input_trigger = $("#input-type");
     var select_trigger = $("#select-type");
     var select2_dropdown = $(trigger).closest('label').siblings('span.select2');
     trigger.on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val("");
          $(this).closest(".form-group").find("select").val("").trigger('change');
          toggleInputs(trigger, input_trigger, select_trigger, select2_dropdown);
     });
     input_trigger.on('change', function() {
          $("#field-type").text(input_trigger.val());
          checkItemDetails();
     });
     input_trigger.on('focusout', function() {
          $(this).siblings('select').find('option').each(function() {
               if($(this).text().toUpperCase() == input_trigger.val().toUpperCase()) {
                    $(this).parents('.form-group').find('a').trigger('click');
                    $(this).parents('select').val($(this).attr('value')).trigger("change");
               }
          });
     });
}

var getNewBrand = function() {
     var trigger = $("#new-brand-toggle");
     var input_trigger = $("#input-brand");
     var select_trigger = $("#select-brand");
     var select2_dropdown = $(trigger).closest('label').siblings('span.select2');
     trigger.on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val("");
          $(this).closest(".form-group").find("select").val("").trigger('change');
          toggleInputs(trigger, input_trigger, select_trigger, select2_dropdown);
     });
     input_trigger.on('change', function() {
          $("#field-brand").text(input_trigger.val());
          checkItemDetails();
     });
     input_trigger.on('focusout', function() {
          $(this).siblings('select').find('option').each(function() {
               if($(this).text().toUpperCase() == input_trigger.val().toUpperCase()) {
                    $(this).parents('.form-group').find('a').trigger('click');
                    $(this).parents('select').val($(this).attr('value')).trigger("change");
               }
          });
     });
}

var getNewName = function() {
     var trigger = $("#new-name-toggle");
     var input_trigger = $("#input-name");
     var select_trigger = $("#select-name");
     var select2_dropdown = $(trigger).closest('label').siblings('span.select2');
     trigger.on("click", function(e) {
          e.preventDefault();
          $(this).closest(".form-group").find("input").val("");
          $(this).closest(".form-group").find("select").val("").trigger('change');
          toggleInputs(trigger, input_trigger, select_trigger, select2_dropdown);
     });
     input_trigger.on('change', function() {
          $("#field-item-name").text(input_trigger.val());
     });
     input_trigger.on('focusout', function() {
          $(this).siblings('select').find('option').each(function() {
               if($(this).text().toUpperCase() == input_trigger.val().toUpperCase()) {
                    $(this).parents('.form-group').find('a').trigger('click');
                    $(this).parents('select').val($(this).attr('value')).trigger("change");
               }
          });
     });
}

var toggleInputs = function(trigger, input_trigger, select_trigger, select2_dropdown) {
     if(trigger.hasClass('active')) {
          trigger.removeClass("active");
          trigger.text("Select name");

          input_trigger.removeAttr("disabled");
          input_trigger.removeClass("d-none");
          select_trigger.attr("disabled", "disabled");
          select2_dropdown.hide();
     }else {
          trigger.addClass("active");
          trigger.text("Add new name");

          input_trigger.attr("disabled", "disabled");;
          input_trigger.addClass("d-none");
          select_trigger.removeAttr("disabled");
          select2_dropdown.show();
     }
}

var getSerial = function() {
     $("#input-serial").on("change", function() {
          $("#field-serial").text($(this).val());
     });
}

/*
var getDuration = function() {
     $("#select-duration").on("change", function() {
          const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
          var days = $(this).val();
          var expiration = new Date();
          expiration.setDate(expiration.getDate() + parseInt(days));
          formatted_text = months[expiration.getMonth()] + " " + expiration.getDate() + ", " + expiration.getFullYear();
          month = expiration.getMonth() + 1;
          formatted_db = expiration.getFullYear() + "-" + month + "-" + expiration.getDate();

          if (days) {
               $("#field-duration").text(days + " Days");
               $("#field-expiry").text(formatted_text);
               $("input[name='expiration']").attr('value', formatted_db);
          } else {
               $("#field-amount").text("");
               $("#field-fee").text("");
               $("#field-duration").text("");
               $("#field-expiry").text("");
          }

          getCharges();
     });
}
*/

var getDates = function() {
     var date = $('#date').datepicker({
          orientation: "bottom left",
          autoclose: true
     });

     $("#date").on("change", function() {
          const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
          var transaction_date = $(this).val();
          var expiration = new Date(transaction_date);


          expiration.setDate(expiration.getDate() + 30);
          var month = expiration.getMonth() + 1;

          formatted_text = months[expiration.getMonth()] + " " + expiration.getDate() + ", " + expiration.getFullYear();
          $("#field-expiry").text(formatted_text);

          var formatted_db = expiration.getFullYear() + "-" + month + "-" + expiration.getDate();
          $("input[name='expiration']").attr('value', formatted_db);

          var field_date = new Date(transaction_date);
          var field_month = field_date.getMonth() + 1;
          var field_date_text = months[field_date.getMonth()] + " " + field_date.getDate() + ", " + field_date.getFullYear();
          $("#field-date").text(field_date_text);

          getCharges();
     });

}

var getUnitValue = function() {
     $('#input-value').number(true, 2);
     $("#input-value").on("change", function() {
          var value = parseInt($(this).val());
          $("#field-value").text("\u20B1 " + $.number(value,2));

          var duration = parseInt($("#select-duration").val());
          var fee = 0;

          getCharges();
     });
}

var getCharges = function() {
     var value = parseInt($("#input-value").val());
     var fee = value * 0.1;
     var amount = value + fee;

     if (value > 0) {
          $("#field-value").text("\u20B1 " + $.number(value,2));
          $("#field-fee").text("\u20B1 " + $.number(fee,2));
          $("#field-amount").text("\u20B1 " + $.number(amount,2));
     } else {
          $("#field-value").text("");
          $("#field-fee").text("");
          $("#field-amount").text("");
     }

     $("input[name='fee']").attr('value', fee);
     $("input[name='amount']").attr('value', amount);
     $("input[name='value']").attr('value', value);
}

/*
var getCharges = function() {
     var value = parseInt($("#input-value").val());
     var duration = parseInt($("#select-duration").val());
     var fee = 0;
     if (duration == 15) {
          fee = value * 0.07;
     } else {
          fee = value * 0.1;
     }

     var amount = value + fee;

     if ((value > 0) && (duration > 0)) {
          $("#field-value").text("\u20B1 " + $.number(value,2));
          $("#field-fee").text("\u20B1 " + $.number(fee,2));
          $("#field-amount").text("\u20B1 " + $.number(amount,2));
     } else if (value > 0) {
          $("#field-value").text("\u20B1 " + $.number(value,2));
     } else {
          $("#field-value").text("");
          $("#field-fee").text("");
          $("#field-amount").text("");
     }

     $("input[name='fee']").attr('value', fee);
     $("input[name='amount']").attr('value', amount);
     $("input[name='value']").attr('value', value);
}
*/

var getAppraiser = function(appraisers = appraisersJSON) {
     var appraiser_option = "<option value='null' selected disabled>Select</option>";

     $(appraisers).each(function(a, appraiser) {
          var name = appraiser['name'];
          var id = appraiser['id'];

          appraiser_option += "<option value='" + name + "'>" + name + "</option>";
     });

     $(appraiser_option).appendTo("#select-appraiser");

     $("#select-appraiser").on("change", function() {
          $("#field-appraiser").text($("#select2-select-appraiser-container").attr('title'));
     });
}

var getAppraiser2 = function(appraisers = appraisersJSON) {
     var appraiser_option = "<option value='null' selected disabled>Select</option>";

     $(appraisers).each(function(a, appraiser) {
          var name = appraiser['name'];
          var id = appraiser['id'];

          appraiser_option += "<option value='" + name + "'>" + name + "</option>";
     });

     $(appraiser_option).appendTo("#select-appraiser2");

     $("#select-appraiser2").on("change", function() {
          $("#field-appraiser2").text($("#select2-select-appraiser2-container").attr('title'));
     });
}

var getDescription = function() {
     $("#input-description").on("change", function() {
          $("#field-description").text($(this).val());
     });
}

var validateForm = function(target) {

     $(".next-btn").on("click", function(e) {
          $(target).submit();
     });

     $(".back-btn").on("click", function(e) {
          $("#form-summary").fadeOut();
          $("#form-input").fadeIn();
     });

     $(".finish-btn").on("click", function(e) {
          $(target + " .confirm-modal").modal("show");
     });

     $(target).validate({
          errorPlacement: function(error, element) {
               parent_container = element.parent("div");
               error_wrapper = "<div class='error-wrapper'></div>";
               $(error_wrapper).appendTo(element.parent("div"));
               error.appendTo(element.siblings(".error-wrapper"));
          },
          submitHandler: function(form) {
               $("#form-input").fadeOut();
               $("#form-summary").fadeIn();
               // $(target + " .confirm-modal").modal("show");
               $(target + " .confirm-modal .confirm").on("click", function() {
                    form.submit();
                    $(this).attr('disabled', 'disabled');
                    $(this).text("PROCESSING");
               });
          }
     });
}

var checkItemDetails = function() {
     var input_type = $("#input-type");
     var select_type = $("#select-type");

     var input_brand = $("#input-brand");
     var select_brand = $("#select-brand");

     var input_name = $("#input-name");
     var select_name = $("#select-name");

     if( (input_type.val() == "") && (select_type.val() == null) ) {
          input_brand.attr('readonly', 'readonly');
          select_brand.siblings(".select2").css("pointer-events", "none");
     } else {
          input_brand.removeAttr("readonly");
          select_brand.siblings(".select2").css("pointer-events", "auto");
     }

     if( (input_brand.val() == "") && (select_brand.val() == null) ) {
          input_name.attr('readonly', 'readonly');
          select_name.siblings(".select2").css("pointer-events", "none");
     } else {
          input_name.removeAttr("readonly");
          select_name.siblings(".select2").css("pointer-events", "auto");
     }
}
