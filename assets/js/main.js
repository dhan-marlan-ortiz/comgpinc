function customers() {
	$('#customer-update-form').validate({
		rules: {
			firstname: 	{ required: true, maxlength: 30 },
			lastname: 	{ required: true, maxlength: 30 },
			middlename: 	{ maxlength: 30 },
			address: 		{ required: true, maxlength: 255 },
			city: 		{ required: true, maxlength: 4 },
			contact: 		{ required: true, maxlength: 13, type: "digits"},
			id_type: 		{ required: true, maxlength: 20 },
			id_number: 	{ required: true, maxlength: 20 },
			remarks: 		{ maxlength: 255 },
			gender: 		{ required: true, maxlength: 6 }
		},
		messages: {
			firstname: 	{ required: "First name is required", maxlength: "First name is too long" },
			lastname: 	{ required: "Last name is required", maxlength: "Last name is too long" },
			middlename: 	{ maxlength: "Middle name is too long" },
			address: 		{ required: "Address is required", maxlength: "Address is too long" },
			city: 		{ required: "Select city", maxlength: "City name too long" },
			contact: 		{ required: "Contact is required", maxlength: "Contact is too long", maxlength: "Invalid number" },
			id_type: 		{ required: "Presented ID is required", maxlength: "ID type too long" },
			id_number: 	{ required: "ID number is required", maxlength: "ID number too long" },
			remarks: 		{ maxlength: "Remarks too long" },
			gender: 		{ required: "Select gender", maxlength: "Gender too long" }
		}
	});

	/* clear form fields */
	$('.btn-clear-fields').on('click', function() {
		var fields = $(this).closest('form').find('input');
		fields.each(function(){
			$(this).val('');
		});
	});
}

var removeNovalidate = function() {
	//$("form[novalidate]").removeAttr('novalidate');
	console.log("form novalidate removed");
}

var getBrowser = function() {
        if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ){
            console.log('Opera');
		  removeNovalidate();
        }
	   else if(navigator.userAgent.indexOf("Edge") != -1 ){
            console.log('Edge');
		  removeNovalidate();
        }
        else if(navigator.userAgent.indexOf("Chrome") != -1 ){
            console.log('Chrome');
        }
        else if(navigator.userAgent.indexOf("Safari") != -1){
            console.log('Safari');
		  removeNovalidate();
        }
        else if(navigator.userAgent.indexOf("Firefox") != -1 ){
             console.log('Firefox');
		   removeNovalidate();
        }
        else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )){
          console.log('IE');
		removeNovalidate();
        }
        else{
           console.log('unknown');
		 removeNovalidate();
        }
}

var getScreenSize = function() {
	var width = $(window).width() + 17;
	if(width < 576) {
		console.log("w: " + width + "px (col-xs)");
	} else if((width >= 576) && (width < 768)) {
		console.log("w: " + width + "px (col-sm)");
	} else if((width >= 768) && (width < 992)) {
		console.log("w: " + width + "px (col-md)");
	} else if((width >= 992) && (width < 1200)) {
		console.log("w: " + width + "px (col-lg)");
	} else if(width >= 1200) {
		console.log("w: " + width + "px (col-xl)");
	}
}

// $(document).ready( function() {
// 	customers();
// 	getBrowser();
// 	$('a[data-toggle="popover"], button[data-toggle="popover"], label[data-toggle="popover"]').popover({
// 	     trigger: 'hover'
// 	});
// });

// $(window).resize(function(){
// 	 // code here
// });
