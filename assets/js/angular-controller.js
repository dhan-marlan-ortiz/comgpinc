// Module
var app = angular.module('myApp', []);

// Controller
app.controller('myController', function ($scope) {

  // Object
  $scope.friends = [{
      sno: 1,
      name: 'Yogesh Singh',
      age: 23,
      gender: 'Male'
    },
    {
      sno: 2,
      name: 'Sonarika Bhadoria',
      age: 23,
      gender: 'Female'
    },
    {
      sno: 3,
      name: 'Vishal Sahu',
      age: 24,
      gender: 'Male'
    },
    {
      sno: 4,
      name: 'Sanjay Singh',
      age: 22,
      gender: 'Male'
    }
  ];

  // column to sort
  $scope.column = 'sno';

  // sort ordering (Ascending or Descending). Set true for desending
  $scope.reverse = false;

  // called on header click
  $scope.sortColumn = function (col) {
    $scope.column = col;
    if ($scope.reverse) {
      $scope.reverse = false;
      $scope.reverseclass = 'arrow-up';
    } else {
      $scope.reverse = true;
      $scope.reverseclass = 'arrow-down';
    }
  };

  // remove and change class
  $scope.sortClass = function (col) {
    if ($scope.column == col) {
      if ($scope.reverse) {
        return 'arrow-down';
      } else {
        return 'arrow-up';
      }
    } else {
      return '';
    }
  }
});