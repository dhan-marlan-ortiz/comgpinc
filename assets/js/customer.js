var initSelect2 = function(target) {
     $(target).select2();
}

var initDataTable = function(tab) {
     var table = $("#customer-table").DataTable( {
          "autoWidth": false,
          "columnDefs": [{
               "targets": 'no-sort',               
               "orderable": false
          }],
     });

     table.on('order.dt search.dt', function() {
          table.column(0, {
               search: 'applied',
               order: 'applied'
          }).nodes().each(function(cell, i) {
               cell.innerHTML = i + 1;
          });
     }).draw();
     
     $(".dataTables_length").appendTo("#filter-length-wrapper");
     $(".dataTables_filter").appendTo("#filter-search-wrapper");

     if(tab == "records") {
          $("#filter-wrapper").fadeIn();
     }

     // $("#fund-table").removeClass("d-none");
}

var loader = function() {
     $(".loader").hide();
     $(".loader").next('div').fadeIn();
}

var updateUrl = function(base_url) {
     $('.main-panel .nav-link').on('click', function (event) {
          var tab = $(this).attr("href").replace("#", "");
          var new_url = base_url + "Customer?tab=" + tab;
          window.history.pushState("data", "Title", new_url);
          if(tab == "record") {
               $("#filter-wrapper").fadeIn('fast');
          } else {
               $("#filter-wrapper").fadeOut('fast');
          }
     });
}

var formValidation = function() {
     'use strict';
     
     // Fetch all the forms we want to apply custom Bootstrap validation styles to
     var forms = document.getElementsByClassName('needs-validation');
     // Loop over them and prevent submission
     var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {

               if (form.checkValidity() === true) {
                    $('button[type="submit"]').text('PROCESSING');
                    $('button[type="submit"]').attr('disabled', 'disabled');
               } else {
                    $('.confirm-modal').modal('hide');
                    event.preventDefault();
                    event.stopPropagation();
                    
                    // $(this).find('.form-modal').modal('show');
               }

               form.classList.add('was-validated');
          }, false);
     });
}

/*
var initDataTable = function(target) {
     $(target).DataTable({
          "fnPreDrawCallback": function( oSettings ) {
               clickRow(target);
          }
     });

     
}

var clickRow = function(target) {
     $(target + ' .customer-row').on('click', function() {
          var reference = $(this).attr('href');
          $(location).attr('href', reference);
     });
}

var validateForm = function(target) {
     $(target).validate({
          submitHandler: function(form) {
               $(target + " .confirm-modal").modal("show");
               $(target + " .confirm-modal .confirm").on("click", function() {
                    form.submit();
               });
          }
     });
}

*/