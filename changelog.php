<style media="screen">
     body {
          font-family: Arial, sans-serif;
          font-size: 12px;
     }

     table {
          border-collapse: collapse;
          margin-bottom: 30px;
     }
     table td,
     table th {
          border: solid 1px #444;
          font-family: Arial, sans-serif;
     }
     table td {
          font-size: 14px;
          padding: 5px 10px;
     }
     table td:first-child {
     	white-space: nowrap;
     	position: relative;
     	padding-left: 25px;
     }
     table th {
          font-size: 16px;
          text-align: left;
          padding: 8px 10px;
     }
     ul, ol {
          margin: 0;
          padding-left: 15px;
     }
     ul li, ol li{
     	margin-bottom: 5px;
     }
     mark.prod {
     	background: green;
     	color: #fff;
     }
     mark.stage {
     	background-color: orange;
     	color: #000;
     }

     mark.prod,
     mark.stage {
     	padding: 5px 10px;
     	margin: 2px;
     	display: inline-block;
     	border-radius: 10px;
     }
     table tr td:first-child:before {
     	content: "";
     	height: 12px;
     	width: 12px;
     	border-radius: 50%;
     	position: absolute;
     	left: 5px;
     	top: 50%;
     	transform: translateY(-50%);
     }
     table tr.stage td:first-child:before {
     	background-color: orange;
     }
     table tr.prod td:first-child:before {
     	background-color: green;
     }
</style>
<h2>Change Log</h2>
<p></p>
<h3>Updates</h3>
<table border="1">
     <thead>
          <tr>
               <th>Date</th>
               <th>Section</th>
               <th>Description</th>
          </tr>
     </thead>
     <tbody>
          <tr class="prod">
               <td>2019-12-03</td>
               <td>New Transaction Form</td>
               <td>
                    <ul>
                         <li>Added transaction date picker to allow input of past transactions</li>
                         <li>Removed time period dropdown options</li>
                         <li>Set a fixed time period of 30 days</li>
                         <li>Auto compute the due date (30 days after transaction date)</li>
                    </ul>
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-03</td>
               <td>Appraisers</td>
               <td>Now exclusive to admin only </td>
          </tr>
          <tr class="prod">
               <td>2019-12-03</td>
               <td>Item Settings</td>
               <td>Now exclusive to admin only </td>
          </tr>
          <tr class="prod">
               <td>2019-12-03</td>
               <td>Transaction ID</td>
               <td>Generate transaction ID based on transaction date instead on the date it was created</td>
          </tr>
          <tr class="prod">
               <td>2019-12-04</td>
               <td>Transaction Records Page</td>
               <td>Optimzed page for faster loading by transfering "Transaction Details" into another page.</td>
          </tr>
          <tr class="prod">
               <td>2019-12-04</td>
               <td>Transaction Details Page</td>
               <td>
          		<ul>
          			<li>Added "Sell Item" button that is only visible when the status is "For Sale". It allows user to input the selling price. Once submitted it will automatically update the status to "Sold" and remove the "Sell Item" and "Update Status" Button</li>
          			<li>Added "Sales Detail" section on items with "Sold" Status.</li>
          			<li>Removed "Sold" status on "Update Status" pop-up. It is automatically set selling the item.</li>
          		</ul>
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-05</td>
               <td>Transaction Details Page</td>
               <td>
          		Added "Update Sales" button exclusive for admin users. This allows admin to update the <em>Selling Price, Date Sold, Sold At, and Processed By</em> fields
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-10</td>
               <td>Transaction Details Page</td>
               <td>
                    <ul>
                         <li>Repurchase item</li>
                         <li>Renew Item</li>
                         <li>Transaction history</li>
                         <li>Customer payments</li>
                    </ul>
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-15</td>
               <td>Purchase Records</td>
               <td>
                    Show available stocks on status column
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-15</td>
               <td>New Purchase Form</td>
               <td>
                    Simplified layout
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-15</td>
               <td>Purchase Details</td>
               <td>
                    <ul>
                         <li>Simplified layout</li>
                         <li>Sell item section</li>
                         <li>Sales record section</li>
                         <li>Add sold item in transaction pages</li>
                    </ul>
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-16</td>
               <td>
                    Transaction Record and Purchase Record Page
               </td>
               <td>
                    Emloyee accounts can view transaction on their branch only
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-17</td>
               <td>Purchase Transaction</td>
               <td>
                    <ul>
                         <li>Void Transaction</li>
                         <li>History</li>
                    </ul>
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-18</td>
               <td>Purchase Details Page</td>
               <td> Return purchased item </td>
          </tr>
          <tr class="prod">
               <td>2019-12-19</td>
               <td>Purchase Details Page</td>
               <td> Void sales form purchased items </td>
          </tr>
          <tr class="prod">
               <td>2019-12-20</td>
               <td>Transacton Details Page</td>
               <td>
                    <ul>
                         <li>Void new transaction. Only applies to transaction that is not yet renewed, repurchased or sold</li>
                         <li>Item returns.</li>
                         <li>Void sold items</li>
                    </ul>
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-20</td>
               <td>Item type, brand, name > create/update</td>
               <td>
                    Prevent duplicates. Transform to uppercase
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-20</td>
               <td>New Purchase form </td>
               <td>
                    Set rules on inputs with prerequisites
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-27</td>
               <td>Transactions</td>
               <td>
                    <ul>
                         <li>Added an option to transfer items to main office or other branch. This option is avaible when the item is expired, for sale, for repair, and for disposal only</li>
                         <li>updated list of status</li>
                    </ul>
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-27</td>
               <td>Item Transfers</td>
               <td>
                    <ul>
                         <li>Added new page for inbound transfers. Once item is accepted it will be added to transactions</li>
                    </ul>
               </td>
          </tr>
          <tr class="prod">
               <td>2019-12-27</td>
               <td>Purchases</td>
               <td>
                    <ul>
                        <li>Removed quantity input on purchased items. Default quantity is set to 1</li>
                        <li>Quantity input will be avaible on retail items</li>
                    </ul>
               </td>
          </tr>
          <tr>
          	<td>
          		<b>Legends: </b>
          	</td>
          	<td colspan="2">
				<mark class="stage">Testing on stage.comgpinc.com</mark>
				<mark class="prod">Deployed live on comgpinc.com</mark>
          	</td>
          </tr>
     </tbody>
</table>
<h3>Bugs</h3>
<table >
     <thead>
          <tr>
               <th>Date</th>
               <th>Section</th>
               <th>Description</th>
               <th>Status</th>
          </tr>
     </thead>
     <tbody>
          <tr>
               <td>2019-12-03</td>
               <td>Item Settings</td>
               <td>Page not found after brand name update</td>
               <td>Fixed</td>
          </tr>
          <tr>
               <td>2019-12-03</td>
               <td>Item Settings</td>
               <td>Page not found after item type update</td>
               <td>Fixed</td>
          </tr>
          <tr>
               <td>2019-12-03</td>
               <td>Appraisers</td>
               <td>Page not found after update</td>
               <td>Fixed</td>
          </tr>
          <tr>
               <td>2019-12-03</td>
               <td>Location</td>
               <td>Page not found after update</td>
               <td>Fixed</td>
          </tr>
          <tr>
               <td>2019-12-03</td>
               <td>New Transaction Form</td>
               <td>Incorrect expiry date after form submission</td>
               <td>Fixed</td>
          </tr>
          <tr>
               <td>2019-12-17</td>
               <td>Transacion Detail Page > Update status</td>
               <td>No form validation</td>
               <td>Fixed</td>
          </tr>
     </tbody>
</table>

<h3>Temporary Notes</h3>
<table>
     <tr>
          <td>Transaction ID format</td>
          <td>Branch Code + Unique User Key + Transaction Type + Date Code (Year Month Day) + Transaction sequence number<br>
               Ex: URD 01 T JC3 001<b>
          </td>
     </tr>
     <tr>
          <td> Unique User Key </td>
          <td> This is the a unique primary key visible only in the database user table </td>
     </tr>
     <tr>
          <td>Transaction Type</td>
          <td>
               <ol>
                    <li>Pawn Transaction (T)</li>
                    <li>Purchased Item Transaction (P) </li>
                    <li>Retail Transaction (R)</li>
               </ol>
          </td>
     </tr>
     <tr>
          <td>Date Code</td>
          <td>
               <table style="margin: 0">
                    <tr>
                         <td><b>Numeric Date:</b> </td>
                         <td>01</td> <td>02</td> <td>03</td> <td>04</td> <td>05</td> <td>06</td> <td>07</td> <td>08</td> <td>09</td> <td>10</td> <td>11</td> <td>12</td> <td>13</td> <td>14</td> <td>15</td> <td>16</td> <td>17</td> <td>18</td> <td>19</td> <td>20</td> <td>21</td> <td>22</td> <td>23</td> <td>24</td> <td>25</td> <td>26</td> <td>27</td> <td>28</td> <td>29</td> <td>30</td> <td>31</td> <td>32</td> <td>33</td> <td>34</td> <td>35</td>
                    </tr>
                    <tr>
                         <td><b>Code:</b></td>
                         <td>1</td> <td>2</td> <td>3</td> <td>4</td> <td>5</td> <td>6</td> <td>7</td> <td>8</td> <td>9</td> <td>A</td> <td>B</td> <td>C</td> <td>D</td> <td>E</td> <td>F</td> <td>G</td> <td>H</td> <td>I</td> <td>J</td> <td>K</td> <td>L</td> <td>M</td> <td>N</td> <td>O</td> <td>P</td> <td>Q</td> <td>R</td> <td>S</td> <td>T</td> <td>U</td> <td>V</td> <td>W</td> <td>X</td> <td>Y</td> <td>Z</td>
                    </tr>
               </table>
          </td>
     </tr>
     <tr>
          <td>
               Transaction sequence number
          </td>
          <td>
               Order number of transaction in a particular day
          </td>
     </tr>
</table>
<p>Report bugs by sending an email to <a href="mailto:dhan.marlan.ortiz@gmail.com">dhan.marlan.ortiz@gmail.com</a></p>
<p>
     <?php echo "Last modified: " . date ("F d Y H:i:s.", getlastmod());  ?>
</p>
