<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Role extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Role_model');

		/* if not admin Clear session and redirect to login  */
		$role = ($this->session->userdata('role_fk'));
		if($role != "ADMS") {
			$this->session->set_flashdata('error', 'Unauthorize. Please login to continue.');
			header('Location: ' . base_url('Login'));
		}
	}
	public function index() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Roles";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);
		$data['roles'] = $this->Role_model->getRoles();
		if (null !== $this->input->post('add')) {
			$this->form_validation->set_rules('id', 'Code', 'required|trim|is_unique[roles.id]');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('error', 'Role already exists. Please try again.');
				header('Location: ' . base_url('Role'));
				exit;
			} else {
				$role =  $this->input->post();
				$values = array(
					'id' => $role['id'],
					'access_type' => $role['name']
				);
				$addRole = $this->Role_model->addRole($values);
				header('Location: ' . base_url('Role'));
				exit;
			}
		}
		$this->load->view('admin/role', $data);
		$this->load->view('admin/footer-admin');
	}
	public function update() {
		if (null !== $this->input->post('update')) {		
			$role = $this->input->post();
			$id = $role['current_id'];
			$values = array(
				'id' => $role['update_role_id'],
				'access_type' => $role['update_role_name']
			);
			$update_role = $this->Role_model->updateRole($id, $values);
			header('Location: ' . base_url('Role'));
		}
	}
}
