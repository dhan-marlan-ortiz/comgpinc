<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends CI_Controller {

	public function __construct() {
		 parent::__construct();
		 $this->load->library('session');
		 $this->load->model('Generic_model');
		 $this->load->model('Customer_model');
		 $this->load->model('Purchase_model');
		 $this->load->model('Transaction_model');
		 $this->load->model('User_model');
		 $this->load->model('Report_model');
		 date_default_timezone_set('Asia/Manila');

		 $role = $this->session->userdata('role_fk');
		 if(!$role) {
			$this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
		 }
	}

	public function index(){
		$branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Purchases";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);
		$data['branches'] = $this->Generic_model->getBranches();

		if(null !== $this->input->get('search')) {
			$transactionId = $this->input->get('search');
			$data['transaction'] = $this->Purchase_model->viewPurchase($transactionId);
			$data['sales'] = $this->Purchase_model->getSalesDetails($transactionId);
			$data['countQuantity'] = $this->Purchase_model->countSales($transactionId);
			$data['transfer'] = $this->Transaction_model->getTransfers($transactionId);
			$this->load->view('store/header-store', $data);
			$this->load->view('store/purchase-view', $data);
		}else {
			$data['transactions'] = $this->Purchase_model->getPurchase();

			$this->load->view('store/header-store', $data);
			$this->load->view('store/purchase', $data);
		}
		$this->load->view('store/footer-store');
	}

	public function report() {
		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Purchases";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
     	$data['branches'] = $this->Generic_model->getBranches();

		$location = $this->session->userdata('branch_fk');
     	$status = array('For sale', 'For repair', 'For transfer', 'For disposal', 'Sold', 'Disposed');

     	$date_start = date("Y-m-01");
     	$date_end = date("Y-m-d");

		if (null !== $this->input->post('location')) {
			$location = $this->input->post('location');
		}

     	if(null !== $this->input->post('status')) {
			$status = $this->input->post('status');
     	}

     	if(null !== $this->input->post('date_start')) {
     		$date_start = date("Y-m-d", strtotime($this->input->post('date_start')));
     	}

     	if(null !== $this->input->post('date_end')) {
     		$date_end = date("Y-m-d", strtotime($this->input->post('date_end')));			
     	}		

		$reports = $this->Purchase_model->getPurchasesReport($location, $status, $date_start, $date_end);

		if(null !== $this->input->post('export')) {			
 			$this->Report_model->exportToExcel($reports, "PURCHASE RECORD");
     	} 

     	$data['branch_info'] = $this->Generic_model->getBranchInfo($location);
		$data['date_start'] = $date_start;
		$data['date_end'] = $date_end;
 		$data['reports'] = $reports;	
 		$data['status'] = $status;	 		

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-purchased-items', $data);
		$this->load->view('admin/footer-admin');
	}

	public function updateStatus($transactionId) {
		$status = $this->input->post('status');
		$values = array('status' => $status);

		$this->Purchase_model->updateStatus($transactionId, $values);

		header("Location: " . base_url("Purchase?search=") . $transactionId);
	}


	public function create() {
		$branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Purchases";
     	$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
     	$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$data['branches'] = $this->Generic_model->getBranches();
		$data['products'] = $this->Generic_model->getProduct();
		$data['brands'] = $this->Generic_model->getItemBrand();
		$data['types'] = $this->Generic_model->getItemType();
		$data['branchInfo'] = $this->Generic_model->getBranchInfo($branch_code);

        if(null !== $this->input->post('date')) {
            $post = $this->input->post();

			$type_fk = "";
			$brand_fk = "";
			$product_fk = "";

			if(isset($post['type-new'])) {
				$type_name = $post['type-new'];
				$type_fk = $this->Generic_model->addItemType($type_name);
			}else {
				$type_fk = $post['type'];
			}

			if(isset($post['brand-new'])) {
				$brand_name = $post['brand-new'];
				$brand_fk = $this->Generic_model->addItemBrand($brand_name, $type_fk);
			}else {
				$brand_fk = $post['brand'];
			}

			if(isset($post['product-new'])) {
				$product_name = $post['product-new'];
				$product_fk = $this->Generic_model->addItemName($product_name, $brand_fk, $type_fk);
			}else {
				$product_fk = $post['product'];
			}

			$date = date("Y-m-d", strtotime($post['date']));
            $transaction_id = trim($this->Generic_model->generateTransactionId('purchases', $post['branch'], $date));
            $user_id = $this->session->userdata('id');
			// $quantity = floatval(str_replace(",", "", $post['quantity']));

			$values = array(
				'type_fk' => strtoupper($type_fk),
				'brand_fk' => strtoupper($brand_fk),
				'name_fk' => strtoupper($product_fk),
				// 'quantity' => $quantity,
				'date_created' => $date,
				'branch_fk' => $post['branch'],
				'user_fk' => $user_id,
				'transaction_id' => $transaction_id,
				'buy_cost' => floatval(str_replace(",", "", $post['cost'])),
				'description' => $post['description']
			);

            $create = $this->Purchase_model->create($values);

			$location = 'Location: ' . base_url('Purchase') . "?search=" . $create;
            header(trim($location));
			exit;
			
          }else {
               $this->load->view('store/header-store', $data);
               $this->load->view('store/purchase-create', $data);
               $this->load->view('store/footer-store');
          }
	}

	public function update() {
		$post = $this->input->post();
		$transaction_id = $post['transaction_id'];

		$match = array(
			'transaction_id' => $post['transaction_id']
		);
		$values = array(
			'stocks' => $post['stocks']
		);
		$updateStocks = $this->Purchase_model->updateStocks($match, $values);

		$location = 'Location: ' . base_url('Purchase') . "?search=" . $transaction_id;
		header(trim($location));
		exit;
	}

	public function createSales() {
		$post = $this->input->post();
		$transactionId = trim($post['transaction_id']);
		$sellingPrice = trim(str_replace(',', '', $post['selling_price']));
		$sellingDate = date("Y-m-d", strtotime($post['selling_date']));
		$branch = $this->session->userdata('branch_fk');
		$user = $this->session->userdata('id');

		$if_exist = $this->db->get_where('sales', array('transaction_id' => $transactionId))->row_array();
		if(null !== $if_exist) {
			$referenceId = $if_exist['reference_id'];
		} else {
			// $referenceId = trim($this->Generic_model->generateTransactionId('transactions', $branch, $sellingDate));
			$referenceId = $transactionId;
		}

		$values_sales = array('reference_id' => $referenceId,
						'transaction_id' => $transactionId,
						'selling_price' => $sellingPrice,
						'user_fk' => $user,
						'branch_fk' => $branch,
						'date_sold' => $sellingDate
					);

		$createSales = $this->Purchase_model->createSales($values_sales);

		if(null !== $createSales) {
			$purchaseStatusData = array('status' => 'Sold');
			$updatePurchaseStatus = $this->Purchase_model->updatePurchase($transactionId, $purchaseStatusData);
			if(null !== $if_exist) {
				$create = $this->Transaction_model->updateStatus($referenceId, 'Sold');
			} else {
				$values_transactions = array(
					'type_fk' => $post['type_fk'],
					'brand_fk' => $post['brand_fk'],
					'name_fk' => $post['name_fk'],
					'description' => trim($post['description']),
					'amount' => floatval(str_replace(",", "", $post['selling_price'])),
					'date' => $sellingDate,
					'status' => "Sold",
					'transaction_id' => $referenceId,
					'branch_fk' => $branch,
					'user_fk' => $user,
					'category' => 2
				);

				$create = $this->Transaction_model->create($values_transactions);
			}

			if(null !== $create) {
				$createLog = array('transaction_id' => $referenceId,
								'description' => "Item sold",
								'user_fk' => $user,
								'branch_fk' => $branch
							);
				$log = $this->Transaction_model->createLog($createLog);
			}
		}
		$redirect_url = trim("Location: " . base_url("Transaction?search=") . $referenceId);
		header($redirect_url);
		exit;
	}

	public function voidSales() {
		$post = $this->input->post();
		$transactionId = trim($post['sales_transaction_id']);
		$referenceId = trim($post['sales_reference_id']);
		$values = array('is_void' => 1);

		$updateSales = $this->Transaction_model->updateSales($referenceId, $values);

		if($updateSales > 0) {
			$updateStatus = $this->Purchase_model->updateStatus($transactionId, array('status' => 'For Sale'));

			$logData = array('transaction_id' => $referenceId,
						    'description' => "Void transaction",
						    'user_fk' => $this->session->userdata('id'),
						    'branch_fk' => $this->session->userdata('branch_fk'),
				    );
			$createLog = $this->Transaction_model->createLog($logData);

			$updateStatus = $this->Transaction_model->updateStatus($referenceId, "Void");
		}

		header("Location: " . base_url("Purchase?search=") . $transactionId);
	}

	public function voidPurchase() {
		$post = $this->input->post();

		$transactionId = trim($post['transaction_id']);
		$values = array('is_void' => 1, 'status' => 'Void');

		$voidCashout = $this->Transaction_model->voidCashout($transactionId);
		$updatePurchase = $this->Purchase_model->updatePurchase($transactionId, $values);

		header("Location: " . base_url("Purchase?search=") . $transactionId);
	}

	public function returnSales() {
		$post = $this->input->post();
		$transactionId = trim($post['transaction_id']);
		$referenceId = trim($post['reference_id']);
		$date = date("Y-m-d H:i:s", strtotime($post['return_date']));
		$remarks = $post['return_remarks'];
		$status = $post['return_status'];

		if($status == "For Sale") {
			$values = array('is_void' => 2, 'remarks' => $remarks);
		} else {
			$values = array('is_void' => 0, 'remarks' => $remarks);
		}

		$updateSales = $this->Transaction_model->updateSales($referenceId, $values);

		if($updateSales > 0) {
			$updateStatus = $this->Purchase_model->updateStatus($transactionId, array('status' => $status));

			$logData = array('transaction_id' => $referenceId,
						    'description' => "Item was returned. Update Status to " . $status,
						    'user_fk' => $this->session->userdata('id'),
						    'branch_fk' => $this->session->userdata('branch_fk'),
						    'date' => $date
				    );
			$createLog = $this->Transaction_model->createLog($logData);

			$updateStatus = $this->Transaction_model->updateStatus($referenceId, $status);
		}

		header("Location: " . base_url("Purchase?search=") . $transactionId);
	}

	public function createTransfer() {
		$post = $this->input->post();
		$transactionId = trim($post['transaction_id']);
		$status = "For Transfer";
		$origin = trim($post['transfer_origin']);
		$sender = $this->session->userdata('id');
		$destination = trim($post['transfer_destination']);
		$remarks = $post['transfer_remarks'];
		$date = date("Y-m-d H:i:s", strtotime($post['date']));

		$values = array('transaction_id' => $transactionId,
						'origin' => $origin,
						'destination' => $destination,
						'remarks' => $remarks,
						'date_created' => $date,
						'sender' => $sender);

		$createTransfer = $this->Transaction_model->createTransfer($values);
		$update_status = $this->db->where('transaction_id', $transactionId)->update('purchases', array('status' => 'For Transfer'));

		if($createTransfer > 0) {
			$logData = array('transaction_id' => $transactionId,
						    'description' => $status . " to " . $destination,
						    'user_fk' => $sender,
						    'branch_fk' => $origin
				    );

			$createLog = $this->Transaction_model->createLog($logData);
		}

		header("Location: " . base_url("Purchase?search=") . $transactionId);
		exit;
	}

	public function voidTransfer() {
		$transactionId = trim($this->input->post('transaction_id'));
		$status = $this->input->post('void_transfer_status');

		$voidTransfer = $this->Transaction_model->voidTransfer($transactionId);
		$update_status = $this->db->where('transaction_id', $transactionId)->update('purchases', array('status' => $status));

		if($voidTransfer > 0) {
			$logData = array('transaction_id' => $transactionId,
						    'description' => "Void transfer",
						    'user_fk' => $this->session->userdata('id'),
						    'branch_fk' => $this->session->userdata('branch_fk')
				    );

			$createLog = $this->Transaction_model->createLog($logData);
		}

		$location = 'Location: ' . base_url('Purchase') . "?search=" . $transactionId;
        	header(trim($location));
		exit;
	}
}
