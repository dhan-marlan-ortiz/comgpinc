<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {

	public function __construct() {
		parent::__construct();		
		date_default_timezone_set('Asia/Manila');

		$employee_id = $this->session->userdata('employee_id');
		// echo $employee_id;
		// die;
		if(isset($employee_id)) {
			header('Location: ' . base_url('Resource/myprofile'));
			exit();
		}
	}

	public function index() {
		if(null !== $this->input->post('login')) {
			$employee_id = $this->input->post('employee_id');
			$password = $this->input->post('password');

			/* XSS CHECK */
			$clean_employee_id = $this->security->xss_clean($employee_id);
			$clean_password = $this->security->xss_clean($password);

			if (strpos( $clean_employee_id,'[removed]') !== FALSE  || strpos($clean_password,'[removed]') !== FALSE) {

				$this->session->set_flashdata('error', 'Malicious input.');
				
				$data['additional_css'] = array('assets/css/resource/signin.css');
				$this->load->view('resource/header-resource', $data);
				$this->load->view('resource/portal');
				$this->load->view('resource/footer-resource');

			}else {

				/* FORM VALIDATION */
				$validation_rules = array(
					array(
						'field' => 'employee_id',
						'label' => 'Employee ID',
						'rules' => array(
							'required'
						),
						'errors' => array(
							'required' => '%s is required'
						)
					),
					array(
						'field' => 'password',
						'label' => 'Password',
						'rules' => array(
							'required'
						),
						'errors' => array(
							'required' => '%s is required'
						)
					)
				);

				$this->form_validation->set_rules($validation_rules);

				if ($this->form_validation->run() == FALSE) {

					$data['additional_css'] = array('assets/css/resource/signin.css');
					$this->load->view('resource/header-resource', $data);
					$this->load->view('resource/portal');
					$this->load->view('resource/footer-resource');

				} else {

					/* USER AUTHENTICATION */
					$authentication = $this->db->get_where('employees', 
												array('id' => $clean_employee_id, 'password' => $clean_password)
											)->row_array();

					if(isset($authentication)) {

						$this->session->set_userdata(array('employee_id'  => $authentication['id'] ));
						
						
						header('Location: ' . base_url('Resource/myprofile'));
						exit;

					}else {
						$this->session->set_flashdata('error', 'Invalid ID or password');
						$data['additional_css'] = array('assets/css/resource/signin.css');
						$this->load->view('resource/header-resource', $data);
						$this->load->view('resource/portal');
						$this->load->view('resource/footer-resource');
					}
					/* USER AUTHENTICATION end */
				}
				/* FORM VALIDATION end */
			}
			/* XSS TEST end */

		} else {
			$data['additional_css'] = array('assets/css/resource/signin.css');
			$this->load->view('resource/header-resource', $data);
			$this->load->view('resource/portal');
			$this->load->view('resource/footer-resource');
		}
		/* FORM SUBMIT end */
	}	
}
