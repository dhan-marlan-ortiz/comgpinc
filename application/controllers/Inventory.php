<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Report_model');

		$role = $this->session->userdata('role_fk');
		if(!$role) {
			$this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
		}
	}

	public function index() {
		$data['title'] = "Inventory";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		if(null !== $this->input->post('filter-location')) {
 		    $branch_fk = $this->input->post('filter-location');

	    	} else  {
			$branch_fk = $this->session->userdata('branch_fk');
		}

		$data['filterLocation'] = $branch_fk;

		$data['branches'] = $this->Generic_model->getBranches();
		$data['mybranch'] = $this->Generic_model->getBranchInfo($branch_fk);

 		$data['vault'] = $this->Report_model->getGeneralReport_vault(date('Y-m-d'), $branch_fk);
 		$data['vaultQty'] = $this->Report_model->getGeneralReport_vaultQty(date('Y-m-d'), $branch_fk);
 		
 		$data['shelf'] = $this->Report_model->getGeneralReport_shelf(date('Y-m-d'), $branch_fk); 		
 		$data['shelfQty'] = $this->Report_model->getGeneralReport_shelfQty(date('Y-m-d'), $branch_fk);

		$data['accessory'] = $this->Report_model->getGeneralReport_accessory(date('Y-m-d'), $branch_fk);
 		$data['accessoryQty'] = $this->Report_model->getGeneralReport_accessoryQty(date('Y-m-d'), $branch_fk);

		$this->load->view('store/header-store', $data);
		$this->load->view('store/navbar-store', $data);
		$this->load->view('store/vault', $data);
		$this->load->view('store/footer-store');
	}
}
