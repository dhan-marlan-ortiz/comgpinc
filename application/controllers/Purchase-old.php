<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends CI_Controller {

	public function __construct() {
		 parent::__construct();
		 $this->load->library('session');
		 $this->load->model('Generic_model');
		 $this->load->model('Customer_model');
		 $this->load->model('Purchase_model');
		 date_default_timezone_set('Asia/Manila');
		 
		 $role = $this->session->userdata('role_fk');
		 if(!$role) {
			$this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
		 }
	}

	public function index($id = null) {
		$branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Purchases";
		$data['products'] = $this->Generic_model->getProduct();
		$data['brands'] = $this->Generic_model->getItemBrand();
		$data['types'] = $this->Generic_model->getItemType();
		$data['branches'] = $this->Generic_model->getBranches();
		$data['branchInfo'] = $this->Generic_model->getBranchInfo($branch_code);

		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
          $data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

        $this->load->view('store/header-store', $data);

        if(null !== $this->input->get('search')) {
        	$id = $this->input->get('search');
        	$data['transaction'] = $this->Purchase_model->getTransaction($id);
        	$this->load->view('store/purchase-view', $data);
        }else {
        	$data['purchases'] = $this->Purchase_model->getPurchaseRecords();
        	$this->load->view('store/purchase', $data);
        }

        $this->load->view('store/footer-store');
	}

	public function create() {
		$data['title'] = "Purchases";
		$data['products'] = $this->Generic_model->getProduct();
		$data['brands'] = $this->Generic_model->getItemBrand();
		$data['types'] = $this->Generic_model->getItemType();
		$data['branches'] = $this->Generic_model->getBranches();

		if(null !== $this->input->post('submit')) {
			date_default_timezone_set('Asia/Manila');
			$transaction_id = "P".date("ymdHis");
			$user_fk = $this->session->userdata('id');

			$purchase = $this->input->post();
			$values = array();
			foreach ($purchase['product_fk'] as $key => $product) {
				$values[$key] = array(
					'transaction_id' => $transaction_id,
					'product_fk' => $product,
					'quantity' => $purchase['quantity'][$key],
					'amount' => $purchase['amount'][$key],
					'user_fk' => $user_fk,
					'branch_fk' => $purchase['branch_fk']
				);
			}
			$create = $this->Purchase_model->create($values);
			header('Location: ' . base_url('Purchase/create'));
		}

		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$this->load->view('store/header-store', $data);
		$this->load->view('store/purchase-create', $data);
		$this->load->view('store/footer-store');
	}
}
