<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ItemName extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// $this->load->model('ItemName_model');

		/* if not admin Clear session and redirect to login  */
		$role = ($this->session->userdata('role_fk'));
		if($role != "ADMS") {
			$this->session->set_flashdata('error', 'Session expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
			exit;
		}
	}
	public function index() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Item Types";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);

		$data['products'] = $this->Generic_model->getProduct();
		$data['brands'] = $this->Generic_model->getItemBrand();
		$data['types'] = $this->Generic_model->getItemType();

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		// $data['types'] = $this->ItemType_model->getTypes();

		$this->load->view('admin/item-name', $data);
		$this->load->view('admin/footer-admin');
	}

	public function create() {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		$post = $this->input->post();
		
		$values = array(
			'name' => $post['name'],
			'brand_fk' => $post['brand_fk'],
			'type_fk' => $post['type_fk']
		);

		if($post['id']) {
			if (!$this->db->where('id', $post['id'])->update('item_name', $values)) {
				$this->session->set_flashdata('error', "Failed. Please check inputs and try again");
			}else {
				$this->session->set_flashdata('success', 'Success. Item name has been successfully added');
			}
		} else {
			if (!$this->db->insert('item_name', $values)) {
				$this->session->set_flashdata('error', "Failed. Please check inputs and try again");
			}else {
				$this->session->set_flashdata('success', 'Success. Item name has been successfully added');
			}
		}

		header('Location: ' . base_url('ItemName'));
	}

	public function remove($id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		if (!$this->db->where('id', $id)->delete('item_name')) {
                  $this->session->set_flashdata('error', "Failed. Item name is already in use");
          }else {
               $this->session->set_flashdata('success', 'Success. Item name has been successfully deleted');
          }

		header('Location: ' . base_url('ItemName'));
	}
}
