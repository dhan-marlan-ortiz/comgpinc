<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Appraiser extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Appraiser_model');

		/* if not admin Clear session and redirect to login  */
		$role = ($this->session->userdata('role_fk'));
		if($role != "ADMS") {
			$this->session->set_flashdata('error', 'Session expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
			exit;
		}


	}
	public function index() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Appraisers";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);


		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$data['appraisers'] = $this->Appraiser_model->getAppraisers();

		if (null !== $this->input->post('add')) {
			$this->form_validation->set_rules('name', 'Name', 'required|trim|is_unique[appraisers.name]');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('error', 'Appraiser already exists. Please try again.');
				header('Location: ' . base_url('Appraiser'));
			}
			else {
				$type =  $this->input->post();

				$values = array(
					'name' => $type['name']
				);

				$addAppraiser = $this->Appraiser_model->addAppraiser($values);
				header('Location: ' . base_url('Appraiser'));
			}
		}

		$this->load->view('admin/appraiser', $data);
		$this->load->view('admin/footer-admin');
	}

	public function update() {
		if (null !== $this->input->post('update')) {
			$appraiser = $this->input->post();
			$id = $appraiser['current_id'];

			$values = array(
				'name' => $appraiser['update_appraiser_name']
			);
			$update_appraiser = $this->Appraiser_model->updateAppraiser($id, $values);

			header('Location: ' . base_url('Appraiser'));
		}
	}
}
