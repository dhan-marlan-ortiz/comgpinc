<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('session');
		$this->load->model('Expense_model');
		date_default_timezone_set('Asia/Manila');

		if(!$this->session->userdata('id')) {
			$this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
			exit();
		}
	}

	public function index() {
		$data['title'] = "Expenses";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);
		$data['types'] = $this->getType();
		$data['branches'] = $this->Generic_model->getBranches();
		$data['role_fk'] = $this->session->userdata('role_fk');

		if(null !== $this->input->get('tab')) {
	    	$data['tab'] = $this->input->get('tab');	
	    } else {
	    	$data['tab'] = "records";	
	    }

		if($this->session->userdata('role_fk') == "ADMS") {
			$data['expenses'] = $this->db->select('e.*, t.type, b.name as branch_name')->
			from('expenses as e')->
			where('is_void', 0)->
			join('expense_type as t', 't.id = e.type_fk')->
			join('branches as b', 'b.id = e.branch_fk')->
			order_by('e.date', 'DESC')->
			get()->
			result_array();
		} else {
			$data['expenses'] = $this->db->select('e.*, t.type, b.name as branch_name')->
			from('expenses as e')->
			where('is_void', 0)->
			where('branch_fk', $this->session->userdata('branch_fk'))->
			join('expense_type as t', 't.id = e.type_fk')->
			join('branches as b', 'b.id = e.branch_fk')->
			order_by('e.date', 'DESC')->
			get()->
			result_array();
		}

		$this->load->view('store/header-store', $data);
		$this->load->view('store/expense', $data);
		$this->load->view('store/footer-store');
	}

	public function addType() {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		if (!$this->db->insert('expense_type', array( 'type' => strtoupper($this->input->post('type_name'))))) {
			$error = $this->db->error();
			$this->session->set_flashdata('error', $error['message']);
		}else {
			$this->session->set_flashdata('success', 'Success. New expense type has been added');
		}

		// $this->session->set_flashdata('modal', 'expenseTypeModal');
		// header('Location: ' . base_url('Expense'));
		header('Location: ' . base_url('Expense?tab=types'));
	}

	public function addTransaction() {
		$post = $this->input->post();
		$type = strtoupper($post['type']);
		$description = strtoupper($post['description']);
		$date = date("Y-m-d", strtotime($post['date']));
		$amount = trim(str_replace(',', '', $post['amount']));
		$values = array(
			'type_fk' => $type,
			'description' => $description,
			'date' => $date,
			'amount' => $amount,
			'user_fk' => $this->session->userdata('id'),
			'branch_fk' => $this->session->userdata('branch_fk')
		);

		$this->db->insert('expenses', $values);

		if($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success', 'Success. Expence has been recorded');
		} else {
			$this->session->set_flashdata('error', 'Failed to add expence. Pleast try again.');
		}

		header('Location: ' . base_url('Expense?tab=new'));
	}

	public function getType() {
		$query = $this->db->order_by('type', 'DES')->get('expense_type');
		$result = $query->result_array();
		return $result;
	}

	public function removeType($id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		if (!$this->db->where('id', $id)->delete('expense_type')) {
			$this->session->set_flashdata('error', "Failed. Item is already in use");
		}else {
			$this->session->set_flashdata('success', 'Success. Item has been successfully deleted');
		}

		// $this->session->set_flashdata('modal', 'expenseTypeModal');
		header('Location: ' . base_url('Expense?tab=types'));
	}

	public function removeExpense($id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		if (!$this->db->where('id', $id)->delete('expenses')) {
			$this->session->set_flashdata('error', "Failed. An error occur");
		}else {
			$this->session->set_flashdata('success', 'Success. Transaction has been successfully deleted');
		}

		header('Location: ' . base_url('Expense'));
	}

}
