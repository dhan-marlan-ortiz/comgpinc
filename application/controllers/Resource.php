<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resource extends CI_Controller {

	public function __construct() {
		parent::__construct();		
		date_default_timezone_set('Asia/Manila');

		$employee_id = $this->session->userdata('employee_id');		
		if(!isset($employee_id)) {
			$this->session->set_flashdata('error', 'Please login to continue');
			header('Location: ' . base_url('Portal'));
			exit();
		}

		$this->load->model('Employee_model');
		$this->load->model('Admin_model');
		$this->load->model('Time_model');
	}	
	public function index() {		
		$data['branches'] = $this->Admin_model->getBranch();
		$data['profile'] = $this->Employee_model->getProfile($this->input->get('profile'));			
		$data['additional_css'] = array('assets/css/resource/dashboard.css');
		$this->load->view('resource/header-resource', $data);
		$this->load->view('resource/my-profile');
		$this->load->view('resource/footer-resource');
	}
	public function myprofile() {
		$data['branches'] = $this->Admin_model->getBranch();
		$data['profile'] = $this->Employee_model->getProfile($this->session->userdata('employee_id'));			
		$data['additional_css'] = array('assets/css/resource/dashboard.css');
		$this->load->view('resource/header-resource', $data);
		$this->load->view('resource/my-profile');
		$this->load->view('resource/footer-resource');
	}
	public function updateprofile($id) {
		$post = $this->input->post();
		$post = array_map('trim', $post);
		// $post = array_map('strtoupper', $post);
		$post['salary'] = trim(str_replace(',', '', $post['salary']));

		$this->db->db_debug = false;

		if($this->db->where('id', $id)->update('employees', $post)) {
			$this->session->set_flashdata('success', 'Profiles has been updated');
			header("location: " . base_url('Resource/myprofile'));
			exit();
		} else {
			$update_error = 'Failed to update profile';
			if(null !== $this->db->error() && $this->db->error()['code'] == 1062) {
				$update_error = 'Employee code already exist. Please enter a unique code';
			}

			$this->session->set_flashdata('error', $update_error);
			header("location: " . base_url('Resource/myprofile'));
			exit();
		}
	}
	public function timetracker() {		
		$employee_id = $this->session->userdata('employee_id');
		$data['profile'] = $this->Employee_model->getProfile($employee_id);					
		$date_hired = $data['profile']['date_hired'];
		$datetime_start = date('Y-m-d 00:00:00', strtotime($date_hired));
		$datetime_end = date('Y-m-d 23:59:59');
		$data['attendance'] = $this->Time_model->getAttendance($datetime_start, $datetime_end, $employee_id);
		$data['additional_css'] = array('assets/css/resource/dashboard.css');
		$data['profile'] = $this->Employee_model->getProfile($this->session->userdata('employee_id'));

		$this->load->view('resource/header-resource', $data);
		$this->load->view('resource/time-tracker');
		$this->load->view('resource/footer-resource');
	}
}
