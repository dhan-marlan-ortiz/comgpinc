<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct() {
		 parent::__construct();
		 $this->load->library('session');
		 $this->load->model('Generic_model');
		 $this->load->model('Customer_model');

		 $role = $this->session->userdata('role_fk');
		 if(!$role) {
			 $this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
			 header('Location: ' . base_url('Login'));
		 }
	}

	public function index() {
          $data['title'] = "Products";
		$data['products'] = $this->Generic_model->getProduct();
		$data['brands'] = $this->Generic_model->getItemBrand();
		$data['types'] = $this->Generic_model->getItemType();

          $data['navbar'] = $this->load->view('store/navbar-store', $data, true);
          $data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

          $this->load->view('store/header-store', $data);
          $this->load->view('store/product', $data);
          $this->load->view('store/footer-store');
	}
}
