<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Report_model');
		$this->load->model('Transaction_model');
		$this->load->model('User_model');
		$this->load->model('Retail_model');
		$this->load->model('Expense_model');
		$this->load->model('Fund_model');		
		$this->load->model('Time_model');		
		date_default_timezone_set('Asia/Manila');
		
		/* if not admin Clear session and redirect to login  */
		// $role = $this->session->userdata('role_fk');

		// if($role != "ADMS" && $role != "GST") {
		// 	$this->session->set_flashdata('error', 'Session expired. Please login to continue.');
			
		// 	header('Location: ' . base_url('Logout'));
		// 	exit;
		// }

		$data['updateStatus'] = $this->Generic_model->updateStatus();		
	}
	
	public function index() {
		
	    $role = $this->session->userdata('role_fk');
		$data['title'] = "Reports";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports', $data);
		$this->load->view('admin/footer-admin');
	}

	public function time() {
		$this->User_model->allowAdminOnly();

		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Time Tracker";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);     	
		

 		$date_start = (null !== $this->input->post('date_start') 
 						? date("Y-m-d 00:00:00", strtotime($this->input->post('date_start'))) 
 						: date("Y-m-01 00:00:00"));

 		$date_end = (null !== $this->input->post('date_end') 
 						? date("Y-m-d 23:59:59", strtotime($this->input->post('date_end')))
 						: date("Y-m-d 23:59:59"));

     	
		// $reports = $this->Expense_model->getExpenses($location, $date_start, $date_end);
		$reports = $attendance = $this->Time_model->getAttendance($date_start, $date_end);

		if(null !== $this->input->post('export')) {
 			$this->Report_model->exportToExcel($reports, "Time Tracker Report");
     	} 
     	
		$data['date_start'] = $date_start;
		$data['date_end'] = $date_end;
 		$data['reports'] = $reports;	 		

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-time', $data);
		$this->load->view('admin/footer-admin');
	}

	public function purchaseRecord() {
		$this->User_model->allowAdminOnly();

		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Purchases";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
     	$data['branches'] = $this->Generic_model->getBranches();

		$location = $this->session->userdata('branch_fk');
     	$status = array('For sale', 'For repair', 'For transfer', 'For disposal', 'Sold', 'Disposed');

     	$date_start = date("Y-m-01");
     	$date_end = date("Y-m-d");

		if (null !== $this->input->post('location')) {
			$location = $this->input->post('location');
		}

     	if(null !== $this->input->post('status')) {
			$status = $this->input->post('status');
     	}

     	if(null !== $this->input->post('date_start')) {
     		$date_start = date("Y-m-d", strtotime($this->input->post('date_start')));
     	}

     	if(null !== $this->input->post('date_end')) {
     		$date_end = date("Y-m-d", strtotime($this->input->post('date_end')));			
     	}		

     	$this->load->model('Purchase_model');
		$reports = $this->Purchase_model->getPurchasesReport($location, $status, $date_start, $date_end);

		if(null !== $this->input->post('export')) {			
 			$this->Report_model->exportToExcel($reports, "PURCHASE RECORD");
     	} 

     	$data['branch_info'] = $this->Generic_model->getBranchInfo($location);
		$data['date_start'] = $date_start;
		$data['date_end'] = $date_end;
 		$data['reports'] = $reports;	
 		$data['status'] = $status;	 		

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-purchased-items', $data);
		$this->load->view('admin/footer-admin');
	}

	public function shelf() {
		$this->User_model->allowAdminOnly();

		$role = $this->session->userdata('role_fk');		
		
		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Shelf";

		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);

		$data['branches'] = $this->Generic_model->getBranches();

		$location = $this->session->userdata('branch_fk');
		if (null !== $this->input->post('location')) {
			$location = $this->input->post('location');
		}

		$data['reports'] = $this->Report_model->getItemsOnShelf($location);

		if(null !== $this->input->post('export')) {
			$reports = $this->Report_model->getItemsOnShelf($this->input->post('location'));
			$this->Report_model->exportToExcel($reports, "SHELF ITEMS REPORT");	
		}

		$data['branch_info'] = $this->Generic_model->getBranchInfo($location);

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-shelf', $data);
		$this->load->view('admin/footer-admin');
	}

	public function fund() {
		$this->User_model->allowAdminOnly();

		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Funds";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
     	$data['branches'] = $this->Generic_model->getBranches();

		$location = (null !== $this->input->post('location') 
						? $this->input->post('location') 
						: $this->session->userdata('branch_fk'));

 		$date_start = (null !== $this->input->post('date_start') 
 						? date("Y-m-d", strtotime($this->input->post('date_start'))) 
 						: date("Y-m-01"));

 		$date_end = (null !== $this->input->post('date_end') 
 						? date("Y-m-d", strtotime($this->input->post('date_end')))
 						: date("Y-m-d"));

     	
		$reports = $this->Fund_model->getFundReports($location, $date_start, $date_end);

		if(null !== $this->input->post('export')) {
 			$this->Fund_model->exportExcel($reports);
     	} 

     	$data['branch_info'] = $this->Generic_model->getBranchInfo($location);
		$data['date_start'] = $date_start;
		$data['date_end'] = $date_end;
 		$data['reports'] = $reports;	 		

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-fund', $data);
		$this->load->view('admin/footer-admin');
	}

	public function journal() {
		

		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Cash Journal";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
		$data['branches'] = $this->Generic_model->getBranches();

		if( null !== $this->input->post('date_start') && null !== $this->input->post('date_end') ) {
            $date_start =  date("Y-m-d", strtotime($this->input->post('date_start')));
            $date_end =  date("Y-m-d", strtotime($this->input->post('date_end')));
        } else {
            $date_start = date("Y-m-01");
            $date_end = date("Y-m-d");
        }

        if(null !== $this->input->post('location') && $this->input->post('location') !== 'summary') {
            $branch_fk =  $this->input->post('location');
            $reports = $this->Report_model->getJournalReport($branch_fk, $date_start, $date_end);	
        	$data['branch_info'] = $this->Generic_model->getBranchInfo($branch_fk);
        } else {
            $reports = $this->Report_model->getJournalReportSummary($date_start, $date_end);
        	$data['branch_info'] = array('id' => 'summary', 'name' => 'SUMMARY (ALL LOCATIONS)');
        }

        if(null !== $this->input->post('export')) {			
			$this->Report_model->exportToExcel($reports, "JOURNAL REPORT");
		}
        
        $data['reports'] = $reports;

        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-journal', $data);
		$this->load->view('admin/footer-admin');
	}

	public function general() {
		$this->User_model->allowAdminOnly();
		
		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '>
								<small class='font-weight-medium'>Reports</small>
							</a> 
							<small class='text-muted'>/</small> 
							General";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
		$data['branches'] = $this->Generic_model->getBranches();

		if(null !== $this->input->post('date_end')) {
               $date_end =  date("Y-m-d", strtotime($this->input->post('date_end')));
        } else {
               $date_end = date("Y-m-d");
        }

        if(null !== $this->input->post('date_start')) {
               $date_start =  date("Y-m-d", strtotime($this->input->post('date_start')));
        } else {
               $date_start = $this->Report_model->getFirstFund();
        }

        if(null !== $this->input->post('location') && $this->input->post('location') !== 'summary') {
            $branch_fk =  $this->input->post('location');         	
        	$reports = $this->Report_model->getDailyGeneralReport($date_start, $date_end, $branch_fk);
        	$data['branch_info'] = $this->Generic_model->getBranchInfo($branch_fk);

        } else {
            $reports = $this->Report_model->getGeneralReport($date_end);	
            $date_start = $this->Report_model->getFirstFund();   
            $data['branch_info'] = array('id' => 'summary', 'name' => 'SUMMARY (ALL LOCATIONS)');
        }

        if(null !== $this->input->post('export')) {			
			$this->Report_model->exportToExcel($reports, "GENERAL REPORT");			
		}

		$data['reports'] = $reports; 
        $data['date_start'] = $date_start;
        $data['date_end'] = $date_end;

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-general', $data);
		$this->load->view('admin/footer-admin');
     }

	
	 public function expenses() {
		/*
		$data['title'] = "Reports / Expenses";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
     	$data['branches'] = $this->Generic_model->getBranches();
     	$data['types'] = $this->Expense_model->getType();

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		if(null !== $this->input->post('generate') || null !== $this->input->post('entries') || null !== $this->input->post('summary')) {
			if(null !== $this->input->post('summary')) {
				$data['reports'] = $this->Expense_model->getExpensesSummary();
				$data['report_type'] = $this->load->view('admin/reports-expenses-summary', $data, TRUE);
			} else {
				$data['reports'] = $this->Expense_model->getExpenses();
				$data['report_type'] = $this->load->view('admin/reports-expenses-entries', $data, TRUE);
			}
		} else if(null !== $this->input->post('export')) {
			if(null !== $this->input->post('export-summary')) {
				$reports = $this->Expense_model->getExpensesSummary();
				$this->Report_model->exportToExcel($reports, "EXPENSES SUMMARY");

				$data['reports'] = $this->Expense_model->getExpensesSummary();
				$data['report_type'] = $this->load->view('admin/reports-expenses-summary', $data, TRUE);
			} else {
				$reports = $this->Expense_model->getExpenses();
				$this->Report_model->exportToExcel($reports, "EXPENSES ENTRIES");

				$data['reports'] = $this->Expense_model->getExpenses();
				$data['report_type'] = $this->load->view('admin/reports-expenses-entries', $data, TRUE);
			}
		}

		$this->load->view('admin/reports-expenses', $data);
		$this->load->view('admin/footer-admin');
		*/
		$this->User_model->allowAdminOnly();

		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Expenses";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
     	$data['branches'] = $this->Generic_model->getBranches();

		$location = (null !== $this->input->post('location') 
						? $this->input->post('location') 
						: $this->session->userdata('branch_fk'));

 		$date_start = (null !== $this->input->post('date_start') 
 						? date("Y-m-d", strtotime($this->input->post('date_start'))) 
 						: date("Y-m-01"));

 		$date_end = (null !== $this->input->post('date_end') 
 						? date("Y-m-d", strtotime($this->input->post('date_end')))
 						: date("Y-m-d"));

     	
		$reports = $this->Expense_model->getExpenses($location, $date_start, $date_end);

		if(null !== $this->input->post('export')) {
 			$this->Report_model->exportToExcel($reports, "Expense Report");
     	} 

     	$data['branch_info'] = $this->Generic_model->getBranchInfo($location);
		$data['date_start'] = $date_start;
		$data['date_end'] = $date_end;
 		$data['reports'] = $reports;	 		

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-expenses', $data);
		$this->load->view('admin/footer-admin');

	}

    public function transactions() {
		$this->User_model->allowAdminOnly();

        $role = $this->session->userdata('role_fk');
		$data['title'] = "Reports";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);

          	$data['branches'] = $this->Generic_model->getBranches();

          	if(null !== $this->input->post('generate')) {
               	$data['transactions'] = $this->Report_model->getTransactions();
          	}

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-transactions', $data);
		$this->load->view('admin/footer-admin');
    }

	public function pawnRecord() {
		$this->User_model->allowAdminOnly();

		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Pawn Record";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
     	$data['branches'] = $this->Generic_model->getBranches();		

     	$location = $this->session->userdata('branch_fk');
     	$status = array('Due', 'Expired', 'Grace Period', 'In Contract', 'Freeze', 'For Sale', 'For Repair', 'For Transfer', 'For Disposal', 'Sold', 'Repurchased', 'Disposed');
     	$date_start = date("Y-m-01");
     	$date_end = date("Y-m-d");
     	$date_option = $this->input->post('date-range-option');

		if (null !== $this->input->post('location')) {
			$location = $this->input->post('location');
		}

     	if(null !== $this->input->post('status')) {
			$status = $this->input->post('status');
     	}

     	if(null !== $this->input->post('date_start')) {
     		$date_start = date("Y-m-d", strtotime($this->input->post('date_start')));
     	}

     	if(null !== $this->input->post('date_end')) {
     		$date_end = date("Y-m-d", strtotime($this->input->post('date_end')));			
		}
		
		$reports = $this->Report_model->getPawnRecord($location, $status, $date_start, $date_end, $date_option);
		
     	if(null !== $this->input->post('export')) {
 			$this->Report_model->exportToExcel($reports, "PAWN RECORD");
     	} 

		$data['branch_info'] = $this->Generic_model->getBranchInfo($location);
		$data['date_start'] = $date_start;
		$data['date_end'] = $date_end;
 		$data['reports'] = $reports;	
 		$data['status'] = $status;	

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-pawn-records', $data);
		$this->load->view('admin/footer-admin');
	}

	public function pawnRenewRepurchase() {
		$this->User_model->allowAdminOnly();

		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Renewal & repurchase payments";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
     	$data['branches'] = $this->Generic_model->getBranches();		

     	$location = $this->session->userdata('branch_fk');
     	$status = array('Due', 'Expired', 'Grace Period', 'In Contract', 'Freeze', 'For Sale', 'For Repair', 'For Transfer', 'For Disposal', 'Sold', 'Repurchased', 'Disposed');
     	$date_start = date("Y-m-01");
     	$date_end = date("Y-m-d");
     	$date_option = $this->input->post('date-range-option');

		if (null !== $this->input->post('location')) {
			$location = $this->input->post('location');
		}

     	if(null !== $this->input->post('status')) {
			$status = $this->input->post('status');
     	}

     	if(null !== $this->input->post('date_start')) {
     		$date_start = date("Y-m-d", strtotime($this->input->post('date_start')));
     	}

     	if(null !== $this->input->post('date_end')) {
     		$date_end = date("Y-m-d", strtotime($this->input->post('date_end')));			
		}
		
		$reports = $this->Report_model->getPawnRenewRepurchase($location, $status, $date_start, $date_end, $date_option);
		
     	if(null !== $this->input->post('export')) {
 			$this->Report_model->exportToExcel($reports, "PAWN RECORD");
     	} 

		$data['branch_info'] = $this->Generic_model->getBranchInfo($location);
		$data['date_start'] = $date_start;
		$data['date_end'] = $date_end;
 		$data['reports'] = $reports;	
 		$data['status'] = $status;	

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-pawn-renew-repurchase', $data);
		$this->load->view('admin/footer-admin');
	}

	public function exportPawnRecord($pawnRecord) {
		$this->User_model->allowAdminOnly();

		$this->load->library("excel");
		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);
		$borderStyle = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF999999'), ), ), );
		$borderStyleDark = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF000000'), ), ), );
		$borderStyleWhite = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FFFFFFFF'), ), ), );
		$fontStyle = array( 'font' => array( 'bold' => true, 'color' => array('argb' => 'FFFFFFFF')) );
		$table_header_row = 6;
		$row = 7;

		$total_appraisal_value = 0;
		$total_amount_due = 0;
		$total_income = 0;

		$file_name = "PAWN TRANSACTIONS REPORT - " . $this->input->post('location') . " - " . strtoupper(date("d M Y", strtotime($_POST['date_start']))) . " - " . strtoupper(date("d M Y", strtotime($_POST['date_end'])));
		$user_info = $this->User_model->getUsers($this->session->userdata('id'));

		$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "COMMUNITY GADGET PHONESHOP");
		$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $file_name);
		$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 3, "DATE GENERATED: " . strtoupper(date("F d, Y")));
		$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 4, "GENERATED BY: " . $user_info['first_name'] . " " . $user_info['first_name']);

		/* TABLE HEADER */
		$table_columns = array( " # ", " CODE ", " LOCATION ", " USER ", " CUSTOMER ", " CONTACT ", " ITEM TYPE ", " PRODUCT ", " SERIAL ", " APPRAISER ", " STATUS ", " ENTRY DATE ", " DUE DATE ", " VALUE ", " AMOUNT DUE ", " INCOME " );
		foreach($table_columns as $col => $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($col, $table_header_row, $field);
		}

		foreach(range('A','P') as $rowId => $columnID) {
			$object->getActiveSheet()->getStyle($columnID . $table_header_row)->applyFromArray($borderStyleWhite);
			$object->getActiveSheet()->getStyle($columnID . $table_header_row)->applyFromArray($fontStyle);
			$object->getActiveSheet()->getStyle($columnID . $table_header_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00000000');
		}
		$object->getActiveSheet()->getStyle('A6:P6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		/* TABLE HEADER END */

		/* TABLE BODY */
		foreach($pawnRecord as $p => $rec) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $row, ++$p);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $rec['transaction_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $rec['branch_fk']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $row, strtoupper($rec['user_fk']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $rec['first_name'] . " " . $rec['middle_name'] . " " . $rec['last_name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $rec['contact']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $rec['type']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $rec['brand'] . " " . $rec['name'] . ($rec['description'] ? " - " . $rec['description'] : ''));
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $rec['serial']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $rec['appraiser']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $row, strtoupper($rec['status']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $row, strtoupper(date("d M Y", strtotime($rec['date']))));
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $row, strtoupper(date("d M Y", strtotime($rec['expiration']))));
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $row, $rec['value']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $row, $rec['amount_due']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(15, $row, $rec['income']);

			// currency number
			$object->getActiveSheet()->getStyle('N' . $row . ':P' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

			// add cell border
			foreach(range('A','P') as $rowId => $columnID) {
				$object->getActiveSheet()->getStyle($columnID . $row)->applyFromArray($borderStyle);
			}

			$row++;
			$total_appraisal_value += $rec['value'];
			$total_amount_due += $rec['amount_due'];
			$total_income += $rec['income'];
		}
		/* TABLE BODY END */

		/* TABLE FOOTER */
		$object->getActiveSheet()->setCellValueByColumnAndRow(13, $row, $total_appraisal_value);
		$object->getActiveSheet()->setCellValueByColumnAndRow(14, $row, $total_amount_due);
		$object->getActiveSheet()->setCellValueByColumnAndRow(15, $row, $total_income);
		$object->getActiveSheet()->getStyle('N' . $row . ':P' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);


		foreach(range('A','P') as $rowId => $columnID) {
			$object->getActiveSheet()->getStyle($columnID . $row)->applyFromArray($borderStyleWhite);
			$object->getActiveSheet()->getStyle($columnID . $row)->applyFromArray($fontStyle);
			$object->getActiveSheet()->getStyle($columnID . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00000000');
		}
		$object->getActiveSheet()->getStyle('A' . $table_header_row . ':P' . $row)->applyFromArray($borderStyleDark);

		$object->getActiveSheet()->getStyle('A1:A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$object->getActiveSheet()->getStyle('I7:I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$object->getActiveSheet()->getStyle('N7' . ':N'. $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$object->getActiveSheet()->getStyle('N'. $row . ':P' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// $object->getActiveSheet()->getStyle('A6:A' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// auto width
		foreach(range('B','P') as $rowId => $columnID) {
			$object->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
		$object_writer->save('php://output');
	}

	public function retailSalesRecord() {
		$this->User_model->allowAdminOnly();

		$data['title'] = "Reports / Retail Sales Summary";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
     	$data['branches'] = $this->Generic_model->getBranches();

     	if(null !== $this->input->post('generate')) {
			$location = $this->input->post('location');
	          $date_start = date("Y-m-d", strtotime($this->input->post('date_start')));
	          $date_end = date("Y-m-d", strtotime($this->input->post('date_end')));

          	$data['retailSalesRecord'] = $this->Report_model->getRetailSalesRecord($location, $date_start, $date_end);

     	} else if(null !== $this->input->post('export')) {
			$location = $this->input->post('location');
	          $date_start = date("Y-m-d", strtotime($this->input->post('date_start')));
	          $date_end = date("Y-m-d", strtotime($this->input->post('date_end')));

			$report = $this->Report_model->getRetailSalesRecord($location, $date_start, $date_end);
			$exportReport = $this->exportRetailSalesRecord($report);
		}

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-retail-sales-summary', $data);
		$this->load->view('admin/footer-admin');
	}

	public function retailSalesTransaction() {
		$this->User_model->allowAdminOnly();

		$data['title'] = "Reports / Retail Sales Transactions";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
     	$data['branches'] = $this->Generic_model->getBranches();

     	if(null !== $this->input->post('generate')) {
			$location = $this->input->post('location');
	          $date_start = date("Y-m-d", strtotime($this->input->post('date_start')));
	          $date_end = date("Y-m-d", strtotime($this->input->post('date_end')));

          	$data['retailSalesTransaction'] = $this->Report_model->getRetailSalesTransaction($location, $date_start, $date_end);

     	} else if(null !== $this->input->post('export')) {
			$location = $this->input->post('location');
	          $date_start = date("Y-m-d", strtotime($this->input->post('date_start')));
	          $date_end = date("Y-m-d", strtotime($this->input->post('date_end')));

			$report = $this->Report_model->getRetailSalesTransaction($location, $date_start, $date_end);
			$exportReport = $this->exportRetailSalesTransaction($report);
		}

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-retail-sales-transaction', $data);
		$this->load->view('admin/footer-admin');
	}

	public function exportRetailSalesRecord($report) {
		$this->User_model->allowAdminOnly();

		$this->load->library("excel");
		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);
		$borderStyle = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF999999'), ), ), );
		$borderStyleDark = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF000000'), ), ), );
		$borderStyleWhite = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FFFFFFFF'), ), ), );
		$fontStyle = array( 'font' => array( 'bold' => true, 'color' => array('argb' => 'FFFFFFFF')) );
		$table_header_row = 6;
		$row = 7;

		$total_sales = 0;
		$total_refund = 0;
		$total_revenue = 0;

		$file_name = "RETAIL SALES SUMMARY - " . $this->input->post('location') . " - " . strtoupper(date("d M Y", strtotime($_POST['date_start']))) . " - " . strtoupper(date("d M Y", strtotime($_POST['date_end'])));
		$user_info = $this->User_model->getUsers($this->session->userdata('id'));

		$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "COMMUNITY GADGET PHONESHOP");
		$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $file_name);
		$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 3, "DATE GENERATED: " . strtoupper(date("F d, Y")));
		$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 4, "GENERATED BY: " . $user_info['first_name'] . " " . $user_info['first_name']);

		/* TABLE HEADER */
		$table_columns = array( " # ", " CODE ", " DATE ", " USER ", " LOCATION ", " SALES ", " REFUNDS ", " REVENUE ");
		foreach($table_columns as $col => $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($col, $table_header_row, $field);
		}

		foreach(range('A','H') as $rowId => $columnID) {
			$object->getActiveSheet()->getStyle($columnID . $table_header_row)->applyFromArray($borderStyleWhite);
			$object->getActiveSheet()->getStyle($columnID . $table_header_row)->applyFromArray($fontStyle);
			$object->getActiveSheet()->getStyle($columnID . $table_header_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00000000');
		}
		$object->getActiveSheet()->getStyle('A6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		/* TABLE HEADER END */

		/* TABLE BODY */
		foreach($report as $p => $rec) {
			$sales = $rec['sales'];
               $total_sales += $sales;
               $refund = $rec['refund'];
               $total_refund += $refund;
               $revenue = $sales - $refund;
               $total_revenue += $revenue;
               $transaction_id = $rec['transaction_id'];

			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $row, ++$p);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $rec['transaction_id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $row, strtoupper(date("d M Y", strtotime($rec['date']))));
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $row, strtoupper($rec['user_fk']));
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $rec['branch_fk']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $sales);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $refund);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $revenue);

			// currency number
			$object->getActiveSheet()->getStyle('F' . $row . ':H' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

			// add cell border
			foreach(range('A','H') as $rowId => $columnID) {
				$object->getActiveSheet()->getStyle($columnID . $row)->applyFromArray($borderStyle);
			}

			$row++;
		}
		/* TABLE BODY END */

		/* TABLE FOOTER */
		$object->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $total_sales);
		$object->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $total_refund);
		$object->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $total_revenue);
		$object->getActiveSheet()->getStyle('F' . $row . ':H' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);


		foreach(range('A','H') as $rowId => $columnID) {
			$object->getActiveSheet()->getStyle($columnID . $row)->applyFromArray($borderStyleWhite);
			$object->getActiveSheet()->getStyle($columnID . $row)->applyFromArray($fontStyle);
			$object->getActiveSheet()->getStyle($columnID . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00000000');
		}
		$object->getActiveSheet()->getStyle('A' . $table_header_row . ':H' . $row)->applyFromArray($borderStyleDark);
		$object->getActiveSheet()->getStyle('A7:A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

		// auto width
		foreach(range('B','H') as $rowId => $columnID) {
			$object->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
		$object_writer->save('php://output');
	}

	public function exportRetailSalesTransaction($report) {
		$this->User_model->allowAdminOnly();

		$this->load->library("excel");
		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);
		$excelObj = $object->getActiveSheet();
		$borderStyle = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF999999'), ), ), );
		$borderStyleDark = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF000000'), ), ), );
		$borderStyleWhite = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FFFFFFFF'), ), ), );
		$fontStyle = array( 'font' => array( 'bold' => true, 'color' => array('argb' => 'FFFFFFFF')) );
		$table_header_row = 6;
		$row = 7;
		$sum_selling_price = 0;
          $sum_quantity = 0;
          $sum_total_price = 0;
          $sum_returned_quantity = 0;
          $sum_refund_amount = 0;
          $sum_revenue = 0;


		$file_name = "RETAIL SALES TRANSACTIONS - " . $this->input->post('location') . " - " . strtoupper(date("d M Y", strtotime($_POST['date_start']))) . " - " . strtoupper(date("d M Y", strtotime($_POST['date_end'])));
		$user_info = $this->User_model->getUsers($this->session->userdata('id'));

		$writeTitle = $excelObj->setCellValueByColumnAndRow(0, 1, "COMMUNITY GADGET PHONESHOP");
		$writeTitle = $excelObj->setCellValueByColumnAndRow(0, 2, $file_name);
		$writeTitle = $excelObj->setCellValueByColumnAndRow(0, 3, "DATE GENERATED: " . strtoupper(date("F d, Y")));
		$writeTitle = $excelObj->setCellValueByColumnAndRow(0, 4, "GENERATED BY: " . $user_info['first_name'] . " " . $user_info['first_name']);

		/* TABLE HEADER */
		$table_columns = array(" # ", " CODE ", " DATE ", " USER ", " LOCATION ", " TYPE ", " PRODUCT ", " UNIT ", " QTY SOLD ", " UNIT PRICE ", " TOTAL PRICE ", " QTY RETURNED ", " REFUNDS ", " REVENUE ");
		foreach($table_columns as $col => $field) {
			$excelObj->setCellValueByColumnAndRow($col, $table_header_row, $field);
			$excelObj->getStyle(intToAlphabet($col) . $table_header_row)->applyFromArray($borderStyleWhite)->applyFromArray($fontStyle);
			$excelObj->getStyle(intToAlphabet($col) . $table_header_row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00000000');
			$excelObj->getStyle(intToAlphabet($col) . $table_header_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		/* TABLE HEADER end */

		/* TABLE BODY */
		foreach($report as $p => $rec) {
			$transaction_id = $rec['transaction_id'];
               $selling_price = $rec['selling_price'];
               $quantity = $rec['quantity'];
               $total_price = $selling_price * $quantity;
               $returned_quantity = (isset($rec['returned_quantity']) ? $rec['returned_quantity'] : 0);
               $refund_amount = (isset($rec['refund_amount']) ? $rec['refund_amount'] : 0);
               $revenue = $total_price - $refund_amount;

			$excelObj->setCellValueByColumnAndRow(0, $row, ++$p);
			$excelObj->setCellValueByColumnAndRow(1, $row, $transaction_id);
               $excelObj->setCellValueByColumnAndRow(2, $row, strtoupper( date('d M Y', strtotime($rec['date']))) );
               $excelObj->setCellValueByColumnAndRow(3, $row, strtoupper( $rec['user_fk']) );
               $excelObj->setCellValueByColumnAndRow(4, $row, strtoupper( $rec['branch_fk']) );
               $excelObj->setCellValueByColumnAndRow(5, $row, strtoupper( $rec['type']) );
               $excelObj->setCellValueByColumnAndRow(6, $row, strtoupper( $rec['brand'] . " " . $rec['name']) );
               $excelObj->setCellValueByColumnAndRow(7, $row, strtoupper( $rec['unit']) );
               $excelObj->setCellValueByColumnAndRow(8, $row, $quantity);
               $excelObj->setCellValueByColumnAndRow(9, $row, $selling_price);
               $excelObj->setCellValueByColumnAndRow(10, $row, $total_price);
               $excelObj->setCellValueByColumnAndRow(11, $row, $returned_quantity);
               $excelObj->setCellValueByColumnAndRow(12, $row, $refund_amount);
               $excelObj->setCellValueByColumnAndRow(13, $row, $revenue);

			// currency number
			$object->getActiveSheet()->getStyle(intToAlphabet(9) . $row . ':' . intToAlphabet(10) . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			$object->getActiveSheet()->getStyle(intToAlphabet(12) . $row . ':' . intToAlphabet(13) . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

			// Left alignt 1st column
			$excelObj->getStyle('A' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			// add cell border
			foreach(range('A',intToAlphabet(sizeof($table_columns) - 1)) as $rowId => $columnID) {
				$object->getActiveSheet()->getStyle($columnID . $row)->applyFromArray($borderStyle);
			}

			$sum_selling_price += $selling_price;
	          $sum_quantity += $quantity;
	          $sum_total_price += $total_price;
	          $sum_returned_quantity += $returned_quantity;
	          $sum_refund_amount += $refund_amount;
	          $sum_revenue += $revenue;
			$row++;
		}
		/* TABLE BODY end */

		/* TABLE FOOTER */
		$excelObj->setCellValueByColumnAndRow(8, $row, $sum_quantity);
		$excelObj->setCellValueByColumnAndRow(9, $row, $sum_selling_price);
		$excelObj->setCellValueByColumnAndRow(10, $row, $sum_total_price);
		$excelObj->setCellValueByColumnAndRow(11, $row, $sum_returned_quantity);
		$excelObj->setCellValueByColumnAndRow(12, $row, $sum_refund_amount);
		$excelObj->setCellValueByColumnAndRow(13, $row, $sum_revenue);
		$object->getActiveSheet()->getStyle(intToAlphabet(9) . $row . ':' . intToAlphabet(10) . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
		$object->getActiveSheet()->getStyle(intToAlphabet(12) . $row . ':' . intToAlphabet(13) . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

		foreach($table_columns as $col => $field) {
			$excelObj->getStyle(intToAlphabet($col) . $row)->applyFromArray($borderStyleWhite)->applyFromArray($fontStyle);
			$excelObj->getStyle(intToAlphabet($col) . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00000000');
			$excelObj->getStyle(intToAlphabet($col) . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		/* TABLE FOOTER end */

		// auto width
		foreach(range('B',intToAlphabet(sizeof($table_columns) - 1)) as $rowId => $columnID) {
			$object->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		// table outside border
		$object->getActiveSheet()->getStyle('A6:' . intToAlphabet(sizeof($table_columns) - 1) . $row)->applyFromArray($borderStyleDark);

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
		$object_writer->save('php://output');
	}

	public function exportExcel() {
		

		$this->load->model("excel_export_model");
		$this->load->library("excel");
		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

		$table_columns = array("Name", "Address", "Gender", "Designation", "Age");

		$column = 0;

		foreach($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}

		$employee_data = $this->excel_export_model->fetch_data();

		$excel_row = 2;

		foreach($employee_data as $row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->name);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->address);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->gender);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->designation);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->age);
			$excel_row++;
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Employee Data.xls"');
		$object_writer->save('php://output');
	}

}
