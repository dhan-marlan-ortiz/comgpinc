<?php 
class Upload extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	public function index() {
		$this->load->view('store/file_upload', array('msg' => ' ' ));
		// echo base_url();
		// echo 'Location: ' . base_url('Upload?error');
	}

	public function do_upload() {
		$upload_path = 'assets/images/customers';
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 2000; //2MB
		$config['max_width'] = 1024;
		$config['max_height'] = 768;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile')) {
			$msg = array('error' => $this->upload->display_errors());
			// header('Location: ' . base_url('Upload?error'));
			$this->load->view('store/file_upload', array('msg' => $msg ));
		} else {
			$msg = array('upload_data' => $this->upload->data());
			// header('Location: ' . base_url('Upload?success'));
			$this->load->view('store/file_upload', array('msg' => $msg ));
		}
	}
}
?>