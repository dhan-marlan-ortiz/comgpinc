<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Accounts_model');
		$this->load->model('Generic_model');
		date_default_timezone_set('Asia/Manila');

		$sess_role = $this->session->userdata('role_fk');
		if(isset($sess_role)) {
			if($sess_role == "ADMS") {
				/* if admin Redirect to Dashboard */
				header('Location: ' . base_url('Dashboard'));
			} else {
				/* else redirect Transaction page */
				header('Location: ' . base_url('Transaction'));
			}
		}
	}

	public function index() {
		if(null !== $this->input->post('login')) {
			$user_id = $this->input->post('user_id');
			$password = $this->input->post('password');

			/* XSS CHECK */
			$clean_user_id = $this->security->xss_clean($user_id);
			$clean_password = $this->security->xss_clean($password);

			if (strpos( $clean_user_id,'[removed]') !== FALSE  || strpos($clean_password,'[removed]') !== FALSE) {

				$this->session->set_flashdata('error', 'Malicious input.');
				$this->load->view('login');

			}else {

				/* FORM VALIDATION */
				$validation_rules = array(
					array(
						'field' => 'user_id',
						'label' => 'user ID',
						'rules' => array(
							'required'
						),
						'errors' => array(
							'required' => '%s is required.'
						)
					),
					array(
						'field' => 'password',
						'label' => 'password',
						'rules' => array(
							'required'
						),
						'errors' => array(
							'required' => '%s is required.'
						)
					)
				);

				$this->form_validation->set_rules($validation_rules);

				if ($this->form_validation->run() == FALSE) {

					$this->load->view('login');

				} else {

					/* USER AUTHENTICATION */
					$authentication = $this->Accounts_model->loginAuthentication($clean_user_id, $clean_password);

					if(isset($authentication)) {

						$role = $authentication['role_fk'];
						$active = $authentication['is_enabled'];

						if($active == 0) {
							$this->session->set_flashdata('error', 'Access denied. Please contact your system administrator.');
							$this->load->view('login');
						} else {
							$user = array(
								'first_name'  => $authentication['first_name'],
								'last_name'  => $authentication['last_name'],
								'role_fk'     => $role,
								'branch_fk'     => $authentication['branch_fk'],
								'id' => $authentication['id'],
								'uid' => $authentication['i']
							);

							$this->session->set_userdata($user);
							$this->session->unset_userdata('error');
							
							// update status of transactions
							$data['updateStatus'] = $this->Generic_model->updateStatus();

							$this->db->insert('visitors', array(
															'ip' => $_SERVER['REMOTE_ADDR'],
															'user' => $authentication['id']
														));
							
							if($role == "ADMS") {
								header('Location: ' . base_url('Dashboard'));
								exit;
							} else if($role == "EMP") {
								header('Location: ' . base_url('Transaction'));
								exit;
							} else if($role == "GST") {
								header('Location: ' . base_url('Report'));
								exit;
							} else {
								header('Location: ' . base_url('Logout'));
								exit;
							}

						}
						
					}else {
						$this->session->set_flashdata('error', 'Invalid username or password.');
						$this->load->view('login');
					}
					/* USER AUTHENTICATION end */
				}
				/* FORM VALIDATION end */
			}
			/* XSS TEST end */

		} else {
			$this->load->view('login');
		}
		/* FORM SUBMIT end */
	}
}
