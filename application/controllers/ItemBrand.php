<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ItemBrand extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Accounts_model');
		$this->load->model('Generic_model');
		$this->load->model('ItemBrand_model');

		/* if not admin Clear session and redirect to login  */
		$role = ($this->session->userdata('role_fk'));
		if($role != "ADMS") {
			$this->session->set_flashdata('error', 'Session expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
			exit;
		}
	}
	public function index() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Item Brands";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);


		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$data['brands'] = $this->ItemBrand_model->getBrands();
		$data['types'] = $this->ItemBrand_model->getTypes();

		if (null !== $this->input->post('add')) {
			$this->form_validation->set_rules('name', 'Name', 'required|trim');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('error', 'Failed. Please try again.');
				header('Location: ' . base_url('ItemBrand'));
			}
			else {
				$brand =  $this->input->post();

				$values = array(
					'name' => strtoupper($brand['name']),
					'type_fk' => $brand['type_fk']
				);

				$addBrand = $this->ItemBrand_model->addBrand($values);
				header('Location: ' . base_url('ItemBrand'));
			}
		}

		$this->load->view('admin/item-brand', $data);
		$this->load->view('admin/footer-admin');
	}

	public function update() {
		if (null !== $this->input->post('update')) {
			$brand = $this->input->post();
			$id = $brand['current_id'];

			$values = array(
				'name' => strtoupper($brand['update_brand_name']),
				'type_fk' => $brand['update_type']
			);
			$update_type = $this->ItemBrand_model->updateBrand($id, $values);

			header('Location: ' . base_url('ItemBrand'));
		}
	}

	public function remove($id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		if (!$this->db->where('id', $id)->delete('item_brand')) {
                  $this->session->set_flashdata('error', "Failed. Brand is already in use");
          }else {
               $this->session->set_flashdata('success', 'Success. Brand has been successfully deleted');
          }

		header('Location: ' . base_url('ItemBrand'));
	}

}
