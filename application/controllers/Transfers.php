<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transfers extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('session');
		$this->load->model('Generic_model');
		$this->load->model('Customer_model');
		$this->load->model('Transaction_model');
		$this->load->model('Retail_model');
		date_default_timezone_set('Asia/Manila');

		if(!$this->session->userdata('id')) {
			$this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
			exit();
		}
	}

	public function index(){
		$branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Item Transfer";
		$data['role_fk'] = $this->session->userdata('role_fk');
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);
		$data['transfers'] = $this->Transaction_model->getTransfers();
		$data['shipments'] = $this->Retail_model->getShipmentsInbound();

		$this->load->view('store/header-store', $data);
		$this->load->view('store/item-transfers', $data);

		$this->load->view('store/footer-store');
	}

	public function outbound(){
		$branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Item Transfer";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);
		$data['transfers'] = $this->Transaction_model->getTransfersOutbound();
		$data['shipments'] = $this->Retail_model->getShipmentsOutbound();

		$this->load->view('store/header-store', $data);
		$this->load->view('store/item-transfers-outbound', $data);

		$this->load->view('store/footer-store');
	}
}
