<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Accounts_model');
		$this->load->model('Generic_model');
		date_default_timezone_set('Asia/Manila');

		/* if not admin Clear session and redirect to login  */
		$role = ($this->session->userdata('role_fk'));
		if($role != "ADMS" && $role !== "GST") {
			$this->session->set_flashdata('error', 'Forbidden. Please login to continue.');
			header('Location: ' . base_url('Logout'));
		}
	}

	public function index(){
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Dashboard";

		if ($role == 'ADMS') {
			$data['title'] = "Dashboard";
			$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
			$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
			$data['countStatus'] = $this->Generic_model->count($this->input->get('branch'));
			$data['branchInfo'] = $this->Generic_model->getBranchInfo($this->input->get('branch'));
			$data['branches'] = $this->Generic_model->getBranches();
			$data['updateStatus'] = $this->Generic_model->updateStatus();

			if(isset($data['updateStatus']['is_updated']) && $data['updateStatus']['is_updated'] > 0) {
				$location = 'Location: ' . base_url('Dashboard');
				header(trim($location));
   	         		exit;
			}else {
				$this->load->view('admin/header-admin', $data);
				$this->load->view('admin/dashboard-admin', $data);
				$this->load->view('admin/footer-admin');
			}
		} else if($role == 'EMP') {
			$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
			$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

			$this->load->view('store/header-store', $data);
			$this->load->view('store/dashboard-store', $data);
			$this->load->view('store/footer-store');
		} else if($role == 'GST') {
			header('Location: ' . base_url('Report'));
		}
	}
}
