<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retail extends CI_Controller {

	public function __construct() {
		 parent::__construct();
		 $this->load->library('session');
		 $this->load->model('Customer_model');
		 $this->load->model('Purchase_model');
		 $this->load->model('Transaction_model');
		 $this->load->model('Retail_model');
		 $this->load->model('User_model');
		 date_default_timezone_set('Asia/Manila');

		 $role = $this->session->userdata('role_fk');
		 if(!$role) {
			$this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
		 }
	}

	public function pending() {
		$branch_fk = $this->session->userdata('branch_fk');
		$role_fk = $this->session->userdata('role_fk');
		$date = date('Y-m-d');
		$status = array('For Transfer', 'For Repair', 'For Disposal');
		$data['title'] = "Retails";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$data['branches'] = $this->Generic_model->getBranches();

		if($role_fk == 'ADMS') {
			$data['records'] = $this->Retail_model->getItemByStatus(null, $status);	
		} else {
			$data['records'] = $this->Retail_model->getItemByStatus($branch_fk, $status);	
		}
		
		$data['branchInfo'] = $this->Generic_model->getBranchInfo($branch_fk);

		$data['branch_fk'] = $branch_fk;
		$data['role_fk'] = $role_fk;
		$data['date'] = $date;

		$this->load->view('store/header-store', $data);
		$this->load->view('store/retail-pending', $data);
		$this->load->view('store/footer-store');	
	}

	public function disposedAndTransferred() {
		$branch_fk = $this->session->userdata('branch_fk');
		$role_fk = $this->session->userdata('role_fk');
		$date = date('Y-m-d');
		$status = array('Disposed', 'Transferred');
		$data['title'] = "Retails";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$data['branches'] = $this->Generic_model->getBranches();

		if($role_fk == 'ADMS') {
			$data['records'] = $this->Retail_model->getItemByStatus(null, $status);	
		} else {
			$data['records'] = $this->Retail_model->getItemByStatus($branch_fk, $status);	
		}
		
		$data['branchInfo'] = $this->Generic_model->getBranchInfo($branch_fk);

		$data['branch_fk'] = $branch_fk;
		$data['role_fk'] = $role_fk;
		$data['date'] = $date;

		$this->load->view('store/header-store', $data);
		$this->load->view('store/retail-disposed-transferred', $data);
		$this->load->view('store/footer-store');	
	}


	public function inventoryReport() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Reports";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);

		$data['branches'] = $this->Generic_model->getBranches();

		if(null !== $this->input->post('generate')) {
			$location = $this->input->post('location');
			$data['retails'] = $this->Retail_model->getInventory($location);
		}else if(null !== $this->input->post('export')) {
			$location = $this->input->post('location');
			$retails = $this->Retail_model->getInventory($location);
			$user_info = $this->User_model->getUsers($this->session->userdata('id'));

			$this->load->library("excel");
			$object = new PHPExcel();

			$object->setActiveSheetIndex(0);
			$borderStyle = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF999999'), ), ), );
			$borderStyleDark = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF000000'), ), ), );
			$borderStyleWhite = array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FFFFFFFF'), ), ), );
			$fontStyle = array( 'font' => array( 'bold' => true, 'color' => array('argb' => 'FFFFFFFF')) );
			$table_header_row = 6;
			$table_col = 6;
			$row = 7;

			/* header start */
			// populate text
			$table_columns = array("#", "LOCATION", "ITEM TYPE", "PRODUCT", "UNIT", "STOCKS");
			foreach($table_columns as $col => $field) {
				$object->getActiveSheet()
				->setCellValueByColumnAndRow($col, $table_header_row, $field);
			}
			// add style
			foreach(range('A','F') as $rowId => $columnID) {
				$object->getActiveSheet()
				->getStyle($columnID . $table_header_row)
				->applyFromArray($borderStyleWhite)
				->applyFromArray($fontStyle)
				->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setARGB('00000000');
			}
			$object->getActiveSheet()->getStyle("A" . $table_header_row . ":F" . $row)->applyFromArray($borderStyle);
			$object->getActiveSheet()->getStyle('A6:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			/* header end */

			/* body start */
			foreach($retails as $r => $retail) {
				$r++;
				$body_cell = array(
					$r,
					strtoupper($retail['branch_fk']),
					strtoupper($retail['type']),
					strtoupper($retail['brand'] . " "  . $retail['name']),
					strtoupper($retail['unit']),
					strtoupper($retail['stock'])
				);

				foreach ($body_cell as $bc => $cell) {
					$object->getActiveSheet()->setCellValueByColumnAndRow($bc, $row, $cell);
				}
				// add cell border
				foreach(range('A','F`') as $rowId => $columnID) {
					$object->getActiveSheet()->getStyle($columnID . $row)->applyFromArray($borderStyle);
				}
				$row++;
			}
			/* body end */

			// auto width
			foreach(range('B','F') as $rowId => $columnID) {
				$object->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			}

			$object->getActiveSheet()->getStyle('A6:A' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$object->getActiveSheet()->getStyle('F6:F' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$borderStyleDark 	= array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF000000'), ), ), );
			$object->getActiveSheet()->getStyle('A6:F' . --$row)->applyFromArray($borderStyleDark);


			$file_name = "RETAIL ITEM INVENTORY REPORT - " . $location;
			$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "COMMUNITY GADGET PHONESHOP");
			$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $file_name);
			$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 3, "DATE GENERATED: " . strtoupper(date("F d, Y")));
			$writeTitle = $object->getActiveSheet()->setCellValueByColumnAndRow(0, 4, "GENERATED BY: " . $user_info['first_name'] . " " . $user_info['first_name']);

			$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
			$object_writer->save('php://output');
			exit;
		}

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$this->load->view('admin/reports-retail-items-inventory', $data);
		$this->load->view('admin/footer-admin');
	}

	public function updateStatus() {

		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$post = $this->input->post();
		$registration_id = $post['id'];
		$status = $post['status'];

		$values = array('status' => $status);

		if (!$this->db->where('registration_id', $registration_id)->update('retails', $values)) {
			$this->session->set_flashdata('error', "Failed");
		}else {
			$this->session->set_flashdata('success', 'Success');
		}

		header('Location: ' . base_url('Retail/registration'));
	}

	public function updateItemStatus($redirect_page) {

		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$post = $this->input->post();
		$pk = $post['id'];
		$status = $post['status'];

		$values = array('status' => $status);

		if (!$this->db->where('pk', $pk)->update('retails', $values)) {
			$this->session->set_flashdata('error', "Failed");
		}else {
			$this->session->set_flashdata('success', 'Success');
		}

		header('Location: ' . base_url('Retail/') . $redirect_page);
	}

	public function acceptTransfer() {
		$id = $this->input->post('accept-id');
		$destination = $this->input->post('accept-destination');
		$status = $this->input->post('accept-status');
		$date = date("Y-m-d", strtotime($this->input->post('accept-date')));

		$user = $this->session->userdata('id');		
		$branch = $this->session->userdata('branch_fk');
		$registration_id = trim($this->Generic_model->generateTransactionId('retails', $branch, $date));

		$shipment = $this->db->get_where('shipments', array('transaction_id' => $id))->row_array();

		$values_shipment = array('receiver' => $user, 'is_received' => 1);

		if(isset($shipment['retail_id'])) {
			$this->db->where('pk', $shipment['retail_id'])->update('retails', array('status' => "Transferred"));
		} else {
			$updateFirstInItems = $this->Retail_model->getRetailFifo($shipment['type_fk'], $shipment['brand_fk'], $shipment['name_fk'], $shipment['unit_fk'], 'For Transfer', $shipment['quantity']);

			foreach ($updateFirstInItems as $fifo => $firstInItems) {
				$this->db->where('pk', $firstInItems['pk'])->update('retails', array('status' => "Transferred"));
			}
		}

		/* register as new item */
		$values_registration = array(
					'registration_id' => $registration_id,
					'date' => $date,
					'branch_fk' => $branch,
					'user_fk' => $user,
					'type_fk' => $shipment['type_fk'],
					'brand_fk' => $shipment['brand_fk'],
					'name_fk' => $shipment['name_fk'],
					'unit_fk' => $shipment['unit_fk'],
					'quantity' => 1,
					'buy' => $shipment['buy'],
					'status' => $status
				);

		$retail_item = array();
        for ($i=0; $i < $shipment['quantity']; $i++) {
			array_push($retail_item, $values_registration);
        }

		if (!$this->db->insert_batch('retails', $retail_item)) {
			$this->session->set_flashdata('error', "Failed");
		}else {
			$this->db->where('transaction_id', $id)->update('shipments', $values_shipment);
			$this->session->set_flashdata('success', 'Success');
		}

		header('Location: ' . base_url('Transfers'));
	}

	public function createShipping($redirect_page = null) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$post = $this->input->post();
		$date = date("Y-m-d", strtotime($post['transfer_date']));
		$origin = $post['transfer_origin'];
		$destination = $post['transfer_destination'];
		$type_fk = $post['transfer_type_fk'];
		$brand_fk = $post['transfer_brand_fk'];
		$name_fk = $post['transfer_name_fk'];
		$quantity = $post['transfer_quantity'];
		$unit_fk = $post['transfer_unit_fk'];
		$remarks = strtoupper($post['transfer_remarks']);
		$sender = $this->session->userdata('id');
		$buy_price = floatval(str_replace(",", "", $post['transfer_buy_price']));
		$transaction_id = trim($this->Generic_model->generateTransactionId('shipments', $origin, $date));

		$values = array( 'transaction_id' => $transaction_id,
				'type_fk' => $type_fk,
				'brand_fk' => $brand_fk,
				'name_fk' => $name_fk,
				'origin' => $origin,
				'destination' => $destination,
				'sender' => $sender,
				'unit_fk' => $unit_fk,
				'quantity' => $quantity,
				'buy' => $buy_price,
				'date' => $date,
				'remarks' => $remarks);

		if (!$this->db->insert('shipments', $values)) {
			$this->session->set_flashdata('error', "Failed");
		}else {			

			if(null !== $this->input->post('retail_id')) {
				$this->db->where('pk', $this->input->post('retail_id'))->update('retails', array('status' => "For Transfer"));
				$this->db->where('transaction_id', $transaction_id)->update('shipments', array('retail_id' => $this->input->post('retail_id')));
			} else {
				$updateFirstInItems = $this->Retail_model->getRetailFifo($type_fk, $brand_fk, $name_fk, $unit_fk, 'For Sale', $quantity);

				foreach ($updateFirstInItems as $fifo => $firstInItems) {
					$this->db->where('pk', $firstInItems['pk'])->update('retails', array('status' => "For Transfer"));
				}
			}
			
			$this->session->set_flashdata('success', 'Success');
		}

		if(!isset($redirect_page)) {
			$redirect_page = 'inventory';
		}

		header('Location: ' . base_url('Retail/') . $redirect_page);
	}

	public function voidReturn($transaction_id, $return_id, $ratail_id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$this->db->where('pk', $ratail_id)->update('retails', array('status' => 'Sold')); // update items to for returned
		$this->db->where('id', $return_id)->delete('retail_returns'); // delete return record

		if ($this->db->affected_rows() >= 1) {
			$this->session->set_flashdata('success', 'Success');
		}else {
			$this->session->set_flashdata('error', "Failed");

		}
		$location = 'Location: ' . base_url('Transaction?search=') . $transaction_id;
		header(trim($location));
	}

	public function sales() {
		$branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Retails";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$data['sales'] = $this->Retail_model->getSoldProducts();

		$this->load->view('store/header-store', $data);
		$this->load->view('store/retail-sales', $data);
		$this->load->view('store/footer-store');
	}

	public function returned() {
		$branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Retails";
		$data['role_fk'] = $this->session->userdata('role_fk');
		$data['branches'] = $this->Generic_model->getBranches();
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$data['returns'] = $this->Retail_model->getRetailReturns();

		$this->load->view('store/header-store', $data);
		$this->load->view('store/retail-return', $data);
		$this->load->view('store/footer-store');
	}

	public function return() {
		$post = $this->input->post();
		$reference_id = trim($post['reference_id']);

		// get registration ID
		$registration_id = $this->db->get_where('sales', array('reference_id' => $reference_id, 'sales_id' => $post['return_sales_id']))->row_array()['transaction_id']; 
		
		// use the registration id to get item info
		$retail = $this->db->get_where('retails', array('registration_id' => $registration_id))->row_array(); 
		
		// find items matcing item info
		$retail_items = $this->db->order_by('pk', 'DESC')->get_where('retails', array('date >=' => $retail['date'], 'name_fk' => $retail['name_fk'], 'unit_fk' => $retail['unit_fk'], 'status' => 'sold'))->row_array(); 
		
		// update items to returned
		$this->db->where('pk', $retail_items['pk'])->update('retails', array('status' => 'Returned')); 

		$values = array('transaction_id' => $reference_id,
			'sales_id' => $post['return_sales_id'],
			'retail_id' => $retail_items['pk'],
			'date' => date("Y-m-d", strtotime($post['return_date'])),
			'quantity' => $post['return_quantity'],
			'action' => $post['return_action'],
			'amount' => $post['return_amount'],
			'remarks' => $post['return_remarks'],
			'user_fk' => $this->session->userdata('id'),
			'branch_fk' => $this->session->userdata('branch_fk')
		);

		$retail_returns = $this->db->insert('retail_returns', $values);
		$retail_returns = $this->db->affected_rows();

		if($retail_returns > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}

		header("Location: " . base_url("Transaction?search=") . $reference_id);
	}

	public function updateReturnStatus() {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$post = $this->input->post();
		$id = $post['id'];
		$status = $post['status'];

		$values = array('status' => $status);

		if (!$this->db->where('pk', $id)->update('retails', $values)) {
			$this->session->set_flashdata('error', "Failed");
		}else {
			$this->session->set_flashdata('success', 'Success');
		}

		header('Location: ' . base_url('Retail/returned'));
	}

	public function voidReturnTransfer($shipment_id, $retail_id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		if (!$this->db->where('id', $shipment_id)->delete('shipments')) {
			$this->session->set_flashdata('error', "Failed");
		}else {
			$this->db->where('pk', $retail_id)->update('retails', array('status' => 'Returned'));
			$this->session->set_flashdata('success', 'Success');
		}

		header('Location: ' . base_url('Retail/returned'));
	}

	public function sell() {
        $branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Retails";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$data['branches'] = $this->Generic_model->getBranches();
		$data['products'] = $this->Generic_model->getProduct();
		$data['brands'] = $this->Generic_model->getItemBrand();
		$data['types'] = $this->Generic_model->getItemType();
		$data['branchInfo'] = $this->Generic_model->getBranchInfo($this->session->userdata('branch_fk'));
		$data['units'] = $this->Generic_model->getUnits();
		$data['retails'] = $this->Retail_model->getInventory($branch_code);

		$this->load->view('store/header-store', $data);
		$this->load->view('store/retail-sell', $data);
		$this->load->view('store/footer-store');
     }

     public function inventory() {
        $branch_fk = $this->session->userdata('branch_fk');
        $role_fk = $this->session->userdata('role_fk');
		$data['title'] = "Retails";
		$data['role_fk'] = $role_fk;
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$data['branches'] = $this->Generic_model->getBranches();
		$data['products'] = $this->Generic_model->getProduct();
		$data['brands'] = $this->Generic_model->getItemBrand();
		$data['types'] = $this->Generic_model->getItemType();
		$data['branchInfo'] = $this->Generic_model->getBranchInfo($this->session->userdata('branch_fk'));
		$data['units'] = $this->Generic_model->getUnits();

		if($role_fk == 'ADMS') {
			$retails = $this->Retail_model->getInventory();
		} else {
			$retails = $this->Retail_model->getInventory($branch_fk);
		}
		$data['retails'] = $retails;

		$this->load->view('store/header-store', $data);
		$this->load->view('store/retail-inventory', $data);
		$this->load->view('store/footer-store');
     }

     public function create() {
          $branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Retails";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);
		$this->load->view('store/header-store', $data);
		$this->load->view('store/retail-create', $data);
		$this->load->view('store/footer-store');
     }

	public function registration() {
		$branch_code = $this->session->userdata('branch_fk');
		$data['title'] = "Retails";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$data['branches'] = $this->Generic_model->getBranches();
		$data['products'] = $this->Generic_model->getProduct();
		$data['brands'] = $this->Generic_model->getItemBrand();
		$data['types'] = $this->Generic_model->getItemType();
		$data['branchInfo'] = $this->Generic_model->getBranchInfo($this->session->userdata('branch_fk'));
		$data['units'] = $this->Generic_model->getUnits();

		$data['retails'] = $this->Retail_model->getRegistered();

		$this->load->view('store/header-store', $data);
		$this->load->view('store/retail-registration', $data);
		$this->load->view('store/footer-store');
	}

	public function register() {
		$register = $this->Retail_model->register();
		header('Location: ' . base_url('Retail/registration'));
		exit();
	}

	public function addUnit() {
		$this->Generic_model->addUnit();
		$this->session->set_flashdata('modal', 'unit-modal');
		header('Location: ' . base_url('Retail/registration'));
		exit();
	}

	public function removeUnit($id) {
		$this->Generic_model->removeUnit($id);
		$this->session->set_flashdata('modal', 'unit-modal');
		header('Location: ' . base_url('Retail/registration'));
		exit();
	}

	public function removeRegistration($id) {
		$this->Retail_model->removeRegistration($id);
		header('Location: ' . base_url('Retail/registration'));
		exit();
	}

	public function removeOutbound($transaction_id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		
		$item_info = $this->db->select('type_fk, brand_fk, name_fk, unit_fk, quantity')
						->from('shipments')
						->where('transaction_id', $transaction_id)
						->get()->row_array();

		$updateFirstInItems = $this->Retail_model->getRetailFifo ($item_info['type_fk'], $item_info['brand_fk'], $item_info['name_fk'], $item_info['unit_fk'], 'For Transfer', $item_info['quantity']);

		foreach ($updateFirstInItems as $fifo => $firstInItems) {
			$this->db->where('pk', $firstInItems['pk'])->update('retails', array('status' => "For Sale"));
		}

		if (!$this->db->where('transaction_id', $transaction_id)->delete('shipments')) {
			$this->session->set_flashdata('error', "Failed");
		}else {
			$this->session->set_flashdata('success', 'Success');
		}

		header('Location: ' . base_url('Transfers/outbound'));
	}

	public function cartCheckout() {
		$post = $this->input->post();
		$user = $this->session->userdata('id');
		$branch = $post['location'];
		$reference_id = trim($this->Generic_model->generateTransactionId('transactions', $branch, date("Y-m-d", strtotime($post['date']))));

		/* array for sales table */
		foreach ($post['id'] as $i => $transaction_id) {
			$products[] = array('transaction_id' => $transaction_id,
							'reference_id' => $reference_id, // transaction id for transactions DB tbl
							'selling_price' => $post['amount'][$i],
							'date_sold' => date("Y-m-d H:i:s", strtotime($post['date'])),
							'user_fk' => $user,
							'branch_fk' => $branch,
							'quantity' => $post['quantity'][$i]);
		}

		$getFirstItem = $this->db->get_where('retails', array('registration_id' => $post['id'][0]))->row_array();

		/* array for transactions table */
		$transaction = array(
			'customer_fk' =>  1,
			'type_fk' => $getFirstItem['type_fk'],
			'brand_fk' => $getFirstItem['brand_fk'],
			'name_fk' => $getFirstItem['name_fk'],
			'date' => date("Y-m-d", strtotime($post['date'])),
			'status' => 'Sold',
			'amount' => $post['amount'][0],
			'transaction_id' => $reference_id,
			'branch_fk' => $branch,
			'user_fk' => $user,
			'category' => 3
		);

		/* record sales */
		$this->db->insert_batch('sales', $products);

		/* update indiviual items to sold */
		foreach ($products as $p => $prod) {
			$item_info = $this->db->select('type_fk, brand_fk, name_fk, unit_fk')
						->from('retails')
						->where('registration_id', $prod['transaction_id'])
						->get()->row_array();

			$updateFirstInItems = $this->db->select("pk")->from("retails")
								->where('type_fk', $item_info['type_fk'])
								->where('brand_fk', $item_info['brand_fk'])
								->where('name_fk', $item_info['name_fk'])
								->where('unit_fk', $item_info['unit_fk'])
								->where('status', 'For Sale')
								->limit($prod['quantity'])
								->order_by('date', 'ASC')
								->get()->result_array();

			foreach ($updateFirstInItems as $fifo => $firstInItems) {
				$this->db->where('pk', $firstInItems['pk'])->update('retails', array('status' => "Sold"));
			}
		}

		/* create record to transaction */
		$this->db->insert('transactions', $transaction);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}

		header("Location: " . base_url("Transaction?search=") . $reference_id);

	}

	public function void($id) {
		$values_sales = array('is_void' => 1);
		$values_transaction = array('is_void' => 1, 'status' => 'Void');
		$this->db->where('reference_id', $id)->update('sales', $values_sales); // update sales table status
		$this->db->where('transaction_id', $id)->update('transactions', $values_transaction); // update transaction table status

		$sales = $this->db->get_where('sales', array('reference_id' => $id))->result_array(); // get all sales row to be voided
		foreach ($sales as $s => $sale) {
			// loop in each item by quantity
			for ($i=0; $i < $sale['quantity']; $i++) {
				$retail_pk = $this->db->order_by('pk', 'ASC')->get_where('retails', array('registration_id' => $sale['transaction_id'], 'status' => 'sold'))->row_array()['pk']; // find specific item, FIFO
				$this->db->where('pk', $retail_pk)->update('retails', array('status' => 'For Sale')); // update item status
			}
		}

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success. Transaction has been voided.');
		} else {
			$this->session->set_flashdata('error', 'Failed. Failed to void transaction.');
		}

		header("Location: " . base_url("Transaction"));
	}
}
