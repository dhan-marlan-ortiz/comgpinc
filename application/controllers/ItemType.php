<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ItemType extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ItemType_model');

		/* if not admin Clear session and redirect to login  */
		$role = ($this->session->userdata('role_fk'));
		if($role != "ADMS") {
			$this->session->set_flashdata('error', 'Session expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
			exit;
		}
	}
	public function index() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Item Types";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$data['types'] = $this->ItemType_model->getTypes();

		if (null !== $this->input->post('add')) {
			$this->form_validation->set_rules('name', 'Name', 'required|trim|is_unique[item_type.name]');
			$this->form_validation->set_rules('code', 'Code', 'required|trim|is_unique[item_type.code]');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('error', 'Item type or code already exists');
			} else {
				$type =  $this->input->post();
				$values = array(
					'name' => strtoupper($type['name']),
					'code' => strtoupper($type['code'])
				);

				$addType = $this->ItemType_model->addType($values);
				header('Location: ' . base_url('ItemType'));
				exit;
			}
		}

		$this->load->view('admin/item-type', $data);
		$this->load->view('admin/footer-admin');
	}

	public function update() {
		
		if (null !== $this->input->post('update')) {
			$type = $this->input->post();
			$id = $type['current_id'];

			$values = array(
				'name' => strtoupper($type['update_type_name']),
				'code' => strtoupper($type['update_type_code'])
			);
			$update_type = $this->ItemType_model->updateType($id, $values);

		}
		header('Location: ' . base_url('ItemType'));
		exit;
	}

	public function remove($id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		if (!$this->db->where('id', $id)->delete('item_type')) {
                  $this->session->set_flashdata('error', "Failed. Type is already in use");
          }else {
               $this->session->set_flashdata('success', 'Success. Type has been successfully deleted');
          }

		header('Location: ' . base_url('ItemType'));
		exit;
	}

}
