<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Employee extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// $this->load->model('Employee_model');
		$this->load->model('Admin_model');
		$this->load->model('Time_model');
		$this->load->model('Employee_model');
		date_default_timezone_set('Asia/Manila');

		if($this->session->userdata('role_fk') != "ADMS") {
			$this->session->set_flashdata('error', 'Session expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
			exit;
		}
	}
	public function index() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Employees";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
		$data['branches'] = $this->Admin_model->getBranch();
		$data['default_location'] = $this->session->userdata('branch_fk');
		$data['employees'] = $this->db->select('e.*, b.name as location')->from('employees as e')->join('branches as b', 'b.id = e.location')->get()->result_array();
		$data['profile'] = "";

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);
		$this->load->view('admin/employee', $data);
		$this->load->view('admin/footer-admin');
	}

	public function upload() {
		$file = $this->input->post('photo');
		$id = $this->input->post('id');
		
		$config['upload_path'] = 'assets/images/employees';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 2000; //2MB
		$config['max_width'] = 1024;
		$config['max_height'] = 768;
		$config['overwrite'] = TRUE;
		$config['file_name'] = $id;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('photo')) {
			$upload_error = $this->upload->display_errors();
			$this->session->set_flashdata('upload_error', $upload_error);			
			// $this->session->set_flashdata('error', $upload_error);
		} else {
			$upload_data = $this->upload->data(); 
			$this->db->where('id', $id)->update('employees', array('photo' => $upload_data['file_name']));
			$this->session->set_flashdata('upload_data', $upload_data);			
			$this->session->set_flashdata('success', 'File has been successfully uploaded');
		}
		
		$redirect = trim("Location: " . base_url("Employee/form?profile=") . $id . "#photo-section");
		header($redirect);
	}

	public function form() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Employees";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
		$data['branches'] = $this->Admin_model->getBranch();
		$data['default_location'] = $this->session->userdata('branch_fk');
		$data['profile'] = "";

		if(null !== $this->input->post('id')) {
			$post = $this->input->post();
			$post = array_map('trim', $post);
			$post = array_map('strtoupper', $post);
			$post['salary'] = trim(str_replace(',', '', $post['salary']));

	        $this->form_validation->set_rules('id', 'Employee Code', 'is_unique[employees.id]', array('is_unique' => 'Employee code already exist. Please enter a unique code'));
	        if ($this->form_validation->run() == FALSE) {
	        	$this->session->set_flashdata('error', 'Form validation failed');
			} else {
				if($this->db->insert('employees', $post)) {
					$this->session->set_flashdata('success', 'New employee has been added');
					header("location: " . base_url('Employee/form?profile=') . $post['id']);
					exit();
				} else {
					$this->session->set_flashdata('error', 'Failed to add new employee');
					header("location: " . base_url('Employee/form'));
					exit();
				}
			}	        
		} else if($this->input->get('profile')) {
			$data['profile'] = $this->Employee_model->getProfile($this->input->get('profile'));			
			$data['default_location'] = $data['profile']['location'];
			$date_hired = $data['profile']['date_hired'];
			$employee_id = $data['profile']['id'];
			
			$datetime_start = date('Y-m-d 00:00:00', strtotime($date_hired));
			$datetime_end = date('Y-m-d 23:59:59');
			$data['attendance'] = $this->Time_model->getAttendance($datetime_start, $datetime_end, $employee_id);

		}

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);
		$this->load->view('admin/employee-form', $data);
		$this->load->view('admin/footer-admin');
	}

	public function update($id) {
		$post = $this->input->post();
		$post = array_map('trim', $post);
		// $post = array_map('strtoupper', $post);
		$post['salary'] = trim(str_replace(',', '', $post['salary']));

		$this->db->db_debug = false;

		if($this->db->where('id', $id)->update('employees', $post)) {
			$this->session->set_flashdata('success', 'Profiles has been updated');
			header("location: " . base_url('Employee/form?profile=') . $post['id']);
			exit();
		} else {
			$update_error = 'Failed to update profile';
			if(null !== $this->db->error() && $this->db->error()['code'] == 1062) {
				$update_error = 'Employee code already exist. Please enter a unique code';
			}

			$this->session->set_flashdata('error', $update_error);
			header("location: " . base_url('Employee/form?profile=') . $id);
			exit();
		}
	}

	public function create() {
		$post = $this->input->post();
		$post = array_map('trim', $post);
		$post = array_map('strtoupper', $post);

        $this->form_validation->set_rules('id', 'Employee Code', 'is_unique[employees.id]',
                array('is_unique' => 'Employee code already exist. Employee code must be unique')
        );
		if ($this->form_validation->run() == FALSE) {
            $this->load->view('myform');
        } else {
	        $this->load->view('formsuccess');
        }

		// $this->db->insert('employees', $post) 
		/*		
		if($this->db->insert('employees', $post)) {
			$this->session->set_flashdata('success', 'New employee has been added');
			header("location: " . base_url('Employee/') . $this->db->insert_id());
			exit();
		} else {
			$this->session->set_flashdata('error', 'Failed to add new employee');
			header("location: " . base_url('Employee'));
			exit();
		}
		*/
	}

	public function addTime() {
		
	}
}
