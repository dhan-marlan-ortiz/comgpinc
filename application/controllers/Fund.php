<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fund extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('Fund_model');
	    $this->load->model('User_model');

	    $role = $this->session->userdata('role_fk');
	    if(!$role) {
		   $this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
		   header('Location: ' . base_url('Login'));
	    }
	}

	public function acknowledgeFund($id) {
		$this->db->where('id', $id)->update('funds', array('is_acknowledged' => 1));

		$result = $this->db->affected_rows();

          if($result > 0) {
               $this->session->set_flashdata('success', 'Success');
          } else {
               $this->session->set_flashdata('error', 'Failed');
          }

          header('Location: ' . base_url('Fund'));
          exit;

	}

	public function index() {
	    $data['title'] = "Funds";
	    $data['navbar'] = $this->load->view('store/navbar-store', $data, true);
	    $data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

	    $role = $this->session->userdata('role_fk');
		$date_begin = null;
		$date_end = null;

	    if(null !== $this->input->post('branch')) {
	    	$branch_fk = $this->input->post('branch');	    	
	    } else {
	    	$branch_fk = $this->session->userdata('branch_fk');
	    }

	    if(null !== $this->input->get('tab')) {
	    	$data['tab'] = $this->input->get('tab');	
	    } else {
	    	$data['tab'] = "record";	
	    }

	    // if(null !== $this->input->post('date-filter-clear')) {	    
	    	// $data['funds'] = $this->Fund_model->getFund($branch_fk);
	    // } else 
	    if(null !== $this->input->post('date-filter')) {
	    	$date_begin = $this->input->post('date_begin');
			$date_end = $this->input->post('date_end');
	    	$data['funds'] = $this->Fund_model->getFund($branch_fk, $date_begin, $date_end);
	    } else {
	    	$data['funds'] = $this->Fund_model->getFund($branch_fk);
	    }

		// $data['funds'] = $this->Fund_model->getFund();
	    

	    /*
	    else if(null !== $this->input->post('filter-location') || null !== $this->input->post('date-filter-clear') ) {
		    $branch_fk = $this->input->post('filter-location');
			$_POST['date_begin'] = "";
			$_POST['date_end'] = "";
			$data['funds'] = $this->Fund_model->getFund($branch_fk);
		} else if(null !== $this->session->flashdata('filterLocation')) {
          	$branch_fk = $this->session->flashdata('filterLocation');

			$data['funds'] = $this->Fund_model->getFund($branch_fk);
     	} else {
			$data['funds'] = $this->Fund_model->getFund($branch_fk);
		}
		*/

		$data['sumCashIn'] = $this->Fund_model->sumCashIn($branch_fk);
		$data['sumCashOut'] = $this->Fund_model->sumCashOut($branch_fk);
		$data['sumTransferIn'] = $this->Fund_model->sumTransferIn($branch_fk);
		$data['sumTransferOut'] = $this->Fund_model->sumTransferOut($branch_fk);
		$data['branches'] = $this->Generic_model->getBranches();
	    $data['mybranch'] = $this->Generic_model->getBranchInfo($branch_fk);
		$data['filterLocation'] = $branch_fk;
		$data['date_begin'] =  $date_begin;
     	$data['date_end'] =  $date_end;

		$this->load->view('store/header-store', $data);
		$this->load->view('store/navbar-store', $data);
		$this->load->view('store/fund', $data);
		$this->load->view('store/footer-store');
	}

	public function insertFund($tag) {
		$this->Fund_model->insertFund($tag);
		header('Location: ' . base_url('Fund'));
	}

	public function transferFund() {
		$this->Fund_model->transferFund();
		header('Location: ' . base_url('Fund'));
	}

	public function void() {
		$this->Fund_model->voidFund();
		header('Location: ' . base_url('Fund'));
	}

	public function test() {
		print_pre($this->input->post());
	}
}
