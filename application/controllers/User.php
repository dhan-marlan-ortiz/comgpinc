<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Admin_model');
		date_default_timezone_set('Asia/Manila');

		/* if not admin Clear session and redirect to login  */
		$role = ($this->session->userdata('role_fk'));
		if($role != "ADMS") {
			$this->session->set_flashdata('error', 'Session expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
			exit;
		}
	}

	public function delete($id) {
		$this->User_model->deleteUser($id);
		header('Location: ' . base_url('User'));
	}

	public function index(){
		$data['title'] = "Users";

		$data['users'] = $this->User_model->getUsers();
		$data['roles'] = $this->Admin_model->getUserRole();
		$data['branches'] = $this->Admin_model->getBranch();

		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);

		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		if(null !== $this->input->post('add')) {
			$this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[users.id]');
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('error', 'Username already exists. Please try again.');
				header('Location: ' . base_url('User'));
			}
			else {
				$user =  $this->input->post();
				$values = array(
					'first_name' => strtoupper($user['firstname']),
					'last_name' => strtoupper($user['lastname']),
					'id' => $user['username'],
					'password' => password_hash($user['password'], PASSWORD_DEFAULT),
					'role_fk' => $user['role'],
					'branch_fk' => $user['branch']
				);
				$addUser = $this->User_model->addUser($values);
				header('Location: ' . base_url('User'));
			}
		}
		else {
			$this->load->view('admin/user', $data);
		}
		$this->load->view('admin/footer-admin');
	}

	public function update() {
		if (null !== $this->input->post('update')) {

			$user = $this->input->post();
			$id = $user['current_id'];
			$values = array(
				'first_name' => $user['update_firstname'],
				'last_name' => $user['update_lastname'],
				'id' => $user['update_username'],
				'role_fk' => $user['update_role'],
				'branch_fk' => $user['update_branch']
			);
			if ($user['update_username'] == $id) {
				$update_user = $this->User_model->updateUser($id, $values);
				header('Location: ' . base_url('User'));
				exit;
			} else {
				$this->form_validation->set_rules('update_username', 'Username', 'required|trim|is_unique[users.id]');
				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('error', 'Username already exists. Please try again.');
				}else {
					$update_user = $this->User_model->updateUser($id, $values);
				}
				header('Location: ' . base_url('User'));
				exit;
			}
		}
	}

	public function status($id, $status) {
		if($status == 1) {
			$status = array('is_enabled' => 0);
		} else {
			$status = array('is_enabled' => 1);
		}

		$this->User_model->deactivateUser($id, $status);
		header('Location: ' . base_url('User'));
	}

	public function newpassword() {
		$pass = $this->input->post();
		$id = $pass['password_id'];
		$newpassword = array('password' => password_hash($pass['update_password'], PASSWORD_DEFAULT));
		$update_pass = $this->User_model->updatePassword($id, $newpassword);
		header('Location: ' . base_url('User'));
		exit;
	}

	public function visitors() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Visitors";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);


		$this->load->view('admin/header-admin', $data);
		$this->load->view('admin/navbar-admin', $data);

		$data['results'] = $this->db->where('user !=', 'adm-ofc')->order_by('timestamp', 'DESC')->limit(100)->get('visitors')->result_array();	
		$data['connections'] = $this->db->where('user !=', 'adm-ofc')->get('visitors')->num_rows();	
		$data['first_connection'] = $this->db->where('user !=', 'adm-ofc')->order_by('timestamp', 'ASC')->get('visitors')->row_array()['timestamp'];	
		$data['ip'] = $this->db->distinct()->select('ip')->from('visitors')->where('user !=', 'adm-ofc')->get()->result_array();	
		$data['user'] = $this->db->distinct()->select('user')->from('visitors')->where('user !=', 'adm-ofc')->get()->result_array();	

		$this->load->view('admin/visitors', $data);
		$this->load->view('admin/footer-admin');
		
	}
}
