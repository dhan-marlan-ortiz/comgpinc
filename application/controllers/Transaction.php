<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction extends CI_Controller
{
     public function __construct()
     {
          parent::__construct();
          $this->load->library('session');
          $this->load->model('Generic_model');
          $this->load->model('Customer_model');
          $this->load->model('Transaction_model');
          $this->load->model('Purchase_model');
          date_default_timezone_set('Asia/Manila');
          if (!$this->session->userdata('id')) {
               $this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
               header('Location: ' . base_url('Login'));
               exit();
          }
     }
     public function index()
     {
          $branch_code     = $this->session->userdata('branch_fk');
          $user_role     = $this->session->userdata('role_fk');
          $data['title']   = "Transactions";
          $data['navbar']  = $this->load->view('store/navbar-store', $data, true);
          $data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

          if (null !== $this->input->get('search')) { // view
               $this->load->model('User_model');
               $this->load->model('Retail_model');
               $transactionId               = $this->input->get('search');
               $data['transaction']         = $this->Transaction_model->viewTransaction($transactionId);
               $data['sales']               = $this->Transaction_model->getSalesDetails($transactionId);
               $data['retail_returns']      = $this->Retail_model->getRetailReturns($transactionId);
               $data['transaction_returns'] = $this->Transaction_model->getReturn($transactionId);
               $data['logs']                = $this->Transaction_model->getLogs($transactionId);
               $data['payments']            = $this->Transaction_model->getPayments($transactionId);
               $data['retails']             = $this->Retail_model->getRetailtransaction($transactionId);
               $data['branches']            = $this->Generic_model->getBranches();
               $data['users']               = $this->User_model->getUsers();
               $data['category0_view']      = $this->load->view('store/transaction-view-cat0', $data, true);
               $data['category1_view']      = $this->load->view('store/transaction-view-cat1', $data, true);
               $data['category2_view']      = $this->load->view('store/transaction-view-cat2', $data, true);
               $data['category3_view']      = $this->load->view('store/transaction-view-cat3', $data, true);
               $this->load->view('store/header-store', $data);
               $this->load->view('store/transaction-view', $data);
          } else if (null !== $this->input->get('raw')) {
               if($this->input->get('raw')) {
                    $transactionId = $this->input->get('raw');
                    $data['transaction'] = $this->Transaction_model->viewTransaction($transactionId);
                    $data['logs'] = $this->Transaction_model->getLogs($transactionId);
                    $data['payments'] = $this->Transaction_model->getPayments($transactionId);
                    print_pre($data['payments']);
               } else {
                    $data['transactions'] = $this->Transaction_model->getAllTransactions($this->input->get('status'), $this->input->get('branch'));
                    print_pre($data['transactions']);
               }
               
               die;
          } else { // records

               if($user_role == 'ADMS') {
                    $data['item_notif'] = $this->Generic_model->count();
               } else {
                    $data['item_notif'] = $this->Generic_model->count($branch_code);
               }

               $data['clients']      = $this->Customer_model->getCustomers();
               $data['branches']     = $this->Generic_model->getBranches();
               $data['branchInfo']   = $this->Generic_model->getBranchInfo($branch_code);
               $data['products']     = $this->Generic_model->getProduct();
               $data['brands']       = $this->Generic_model->getItemBrand();
               $data['types']        = $this->Generic_model->getItemType();
               $data['products']     = $this->Generic_model->getProduct();
               $data['appraiser']    = $this->Generic_model->getAppraiser();
               $data['transactions'] = $this->Transaction_model->getAllTransactions($this->input->get('status'), $this->input->get('branch'));
               $this->load->view('store/header-store', $data);
               $this->load->view('store/transaction', $data);
          }


          $this->load->view('store/footer-store');
     }
     public function voidSalesTransaction($salesId, $transactionId)
     {
          $purchaseId = $this->Purchase_model->getPurchaseIdFromSales($transactionId);
          $voidSales = $this->db->where('sales_id', $salesId)->update('sales', array(
               'is_void' => 1
          ));

          if ($this->db->affected_rows() > 0) {
               if($purchaseId[5] == "P") {
                    $transactionStatusData = array('status' => 'Void');
                    $purchaseStatusData = array('status' => 'For Sale');
                    $updatePurchaseStatus = $this->db->where('transaction_id', $purchaseId)->update('purchases', $purchaseStatusData);
               } else {
                    $transactionStatusData = array('status' => 'For Sale');
               }
               $updateTransactionStatus = $this->db->where('transaction_id', $transactionId)->update('transactions', $transactionStatusData);


               $logData = array(
                    'transaction_id' => $transactionId,
                    'description' => "Void Sales. Updated status to for sale.",
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog = $this->Transaction_model->createLog($logData);
               $this->session->set_flashdata('success', 'Void success');
          } else {
               $this->session->set_flashdata('error', 'Void failed');
          }

          if($purchaseId[5] == "P") {
               header('Location: ' . base_url('Purchase?search=') . $purchaseId);
               exit;
          } else {
               header('Location: ' . base_url('Transaction?search=') . $transactionId);
               exit;
          }
     }
     public function returnSalesTransaction($transactionId, $salesId = null) {
		$post = $this->input->post();
		$date = date("Y-m-d", strtotime($post['return_date']));
		$status = $post['return_status'];
		$remarks = $post['return_remarks'];
          $purchaseId = $this->Purchase_model->getPurchaseIdFromSales($transactionId);
		$salesId = $this->db->get_where('sales', array('reference_id' => $transactionId, 'is_void' => 0))->row_array()['sales_id'];
          $action = "Cash Refund";
          $amount = trim(str_replace(',', '', $post['return_amount']));

		$returnSales = $this->db->where('sales_id', $salesId)->update('sales', array(
               'is_void' => 2,
               'remarks' => $remarks
          ));

		if ($this->db->affected_rows() > 0) {

               $returnData   = array(
                    'transaction_id' => $transactionId,
                    'sales_id' => $salesId,
                    'date' => $date,
                    'quantity' => 1,
                    'action' => $action,
                    'amount' => $amount,
                    'remarks' => $remarks,
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createReturn = $this->Transaction_model->createReturn($returnData);

               if($purchaseId[5] == "P") {
                    $transactionStatusData = array('status' => 'Void');
                    $purchaseStatusData = array('status' => $status);
                    $updatePurchaseStatus = $this->db->where('transaction_id', $purchaseId)->update('purchases', $purchaseStatusData);
                    $updateTransactionStatus = $this->db->where('transaction_id', $transactionId)->update('transactions', $transactionStatusData);
               } else {
                    $transactionStatusData = array('status' => $status);
                    $updateTransactionStatus = $this->db->where('transaction_id', $transactionId)->update('transactions', $transactionStatusData);
               }


               $logData = array(
                    'transaction_id' => $transactionId,
                    'description' => "Return Item. Updated status to " . $status . ".",
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog = $this->Transaction_model->createLog($logData);
               $this->session->set_flashdata('success', 'Return success');
          } else {
               $this->session->set_flashdata('error', 'Return failed');
          }

          if($purchaseId[5] == "P") {
               header('Location: ' . base_url('Purchase?search=') . $purchaseId);
               exit;
          } else {
               header('Location: ' . base_url('Transaction?search=') . $transactionId);
               exit;
          }
     }

     public function voidTransactionSalesReturn($transactionId, $return_id, $sales_id) {
          $status = "Sold";
          $revert_sales_info = $this->db->where('sales_id', $sales_id)->update('sales', array('is_void' => 0));
          $revert_tramsaction_info = $this->db->where('transaction_id', $transactionId)->update('transactions', array('status' => $status));
          $delete_return = $this->db->where('id', $return_id)->delete('transaction_returns');
          $logData = array(
               'transaction_id' => $transactionId,
               'description' => "Return void. Updated status to " . $status . ".",
               'user_fk' => $this->session->userdata('id'),
               'branch_fk' => $this->session->userdata('branch_fk')
          );
          $createLog = $this->Transaction_model->createLog($logData);
          $this->session->set_flashdata('success', 'Item return has been voided. Updated status to Sold');

          $location = 'Location: ' . base_url('Transaction') . "?search=" . $transactionId;
          header(trim($location));
          exit;
     }

     public function create() {
          $branch_code        = $this->session->userdata('branch_fk');
          $data['title']      = "Transactions";
          $data['navbar']     = $this->load->view('store/navbar-store', $data, true);
          $data['sidebar']    = $this->load->view('store/sidebar-store', $data, true);
          $data['customers']  = $this->Customer_model->getCustomers();
          $data['branches']   = $this->Generic_model->getBranches();
          $data['products']   = $this->Generic_model->getProduct();
          $data['brands']     = $this->Generic_model->getItemBrand();
          $data['types']      = $this->Generic_model->getItemType();
          $data['appraiser']  = $this->Generic_model->getAppraiser();
          $data['branchInfo'] = $this->Generic_model->getBranchInfo($branch_code);
          if (null !== $this->input->post('customer')) {
               $post       = $this->input->post();
               $type_fk    = "";
               $brand_fk   = "";
               $product_fk = "";
               if (isset($post['type-new'])) {
                    $type_name = $post['type-new'];
                    $type_fk   = $this->Generic_model->addItemType(strtoupper($type_name));
               } else {
                    $type_fk = $post['type'];
               }
               if (isset($post['brand-new'])) {
                    $brand_name = $post['brand-new'];
                    $brand_fk   = $this->Generic_model->addItemBrand(strtoupper($brand_name), $type_fk);
               } else {
                    $brand_fk = $post['brand'];
               }
               if (isset($post['product-new'])) {
                    $product_name = $post['product-new'];
                    $product_fk   = $this->Generic_model->addItemName(strtoupper($product_name), $brand_fk, $type_fk);
               } else {
                    $product_fk = $post['product'];
               }
               $transaction_id = trim($this->Generic_model->generateTransactionId('transactions', $post['branch'], date("Y-m-d", strtotime($post['date']))));
               $user_id        = $this->session->userdata('id');
               $appraiser2     = "None";
               if (isset($post['appraiser2'])) {
                    $appraiser2 = $post['appraiser2'];
               }
               $values = array(
                    'customer_fk' => $post['customer'],
                    'type_fk' => $type_fk,
                    'brand_fk' => $brand_fk,
                    'name_fk' => $product_fk,
                    'serial' => $post['serial'],
                    'duration' => $post['duration'],
                    'value' => $post['value'],
                    'description' => strtoupper($post['description']),
                    'appraiser' => strtoupper($post['appraiser']),
                    'appraiser2' => strtoupper($appraiser2),
                    'date' => date("Y-m-d", strtotime($post['date'])),
                    'expiration' => $post['expiration'],
                    'status' => $post['status'],
                    'fee' => $post['fee'],
                    'amount' => $post['amount'],
                    'transaction_id' => $transaction_id,
                    'branch_fk' => $post['branch'],
                    'user_fk' => $user_id
               );
               $create = $this->Transaction_model->create($values, 1);
               if (null !== $create) {
                    $createLog            = array(
                         'transaction_id' => $transaction_id,
                         'description' => "Initial transaction",
                         'user_fk' => $user_id,
                         'branch_fk' => $post['branch']
                    );
                    $log                  = $this->Transaction_model->createLog($createLog);
                    $data['updateStatus'] = $this->Generic_model->updateStatus();
               }
               $location = 'Location: ' . base_url('Transaction') . "?search=" . $create;
               header(trim($location));
               exit;
          } else {
               $this->load->view('store/header-store', $data);
               $this->load->view('store/transaction-create', $data);
               $this->load->view('store/footer-store');
          }
     }
     public function update($transactionId = null, $transactionStatus = null)
     {
          if (null !== $this->input->post('update_status')) {
               $post              = $this->input->post();
               $transactionId     = trim($post['transaction_id']);
               $transactionStatus = $post['transaction_status'];
               $updateStatus      = $this->Transaction_model->updateStatus($transactionId, $transactionStatus);
               $logData           = array(
                    'transaction_id' => $transactionId,
                    'description' => "Updated status to " . $transactionStatus,
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog         = $this->Transaction_model->createLog($logData);
               header("Location: " . base_url("Transaction?search=") . $transactionId);
               exit();
          }
     }
     public function voidTransaction()
     {
          $post          = $this->input->post();
          $transactionId = trim($post['transaction_id']);
          $updateStatus  = $this->Transaction_model->updateStatus($transactionId, "Void");
          $voidCashout = $this->Transaction_model->voidCashout($transactionId);
          if ($updateStatus > 0) {
               $logData   = array(
                    'transaction_id' => $transactionId,
                    'description' => "Void transaction",
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog = $this->Transaction_model->createLog($logData);
          }
          header("Location: " . base_url("Transaction?search=") . $transactionId);
          exit();
     }
     public function createSales()
     {
          $post              = $this->input->post();
          $transactionId     = trim($post['transaction_id']);
          $sellingPrice      = trim(str_replace(',', '', $post['selling_price']));
          $transactionStatus = "Sold";
          $date_sold         = date("Y-m-d", strtotime($post['date_sold']));
          $values            = array(
               'reference_id' => $transactionId,
               'selling_price' => $sellingPrice,
               'date_sold' => $date_sold,
               'user_fk' => $this->session->userdata('id'),
               'branch_fk' => $this->session->userdata('branch_fk')
          );
          $createSales       = $this->Transaction_model->createSales($values);
          // $updateSales = $this->Transaction_model->updateSales($transactionId, $values);
          if (isset($createSales)) {
               $logData   = array(
                    'transaction_id' => $transactionId,
                    'description' => "Item has been sold",
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog = $this->Transaction_model->createLog($logData);
          }
          header("Location: " . base_url("Transaction?search=") . $transactionId);
          exit();
     }
     public function updateSales()
     {
          $post              = $this->input->post();
          $transactionId     = trim($post['transaction_id']);
          $transactionStatus = "Sold";
          $values            = array(
               'selling_price' => trim(str_replace(',', '', $post['upSellPrc'])),
               'date_sold' => date("Y-m-d H:i:s", strtotime($post['upDateSld'])),
               'user_fk' => $post['upUser'],
               'branch_fk' => $post['upBrach'],
               'remarks' => $post['upRemarks']
          );
          $updateSales       = $this->Transaction_model->updateSales($transactionId, $values);
          if ($updateSales > 0) {
               $logData   = array(
                    'transaction_id' => $transactionId,
                    'description' => "Updated sales details",
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog = $this->Transaction_model->createLog($logData);
          }
          header("Location: " . base_url("Transaction?search=") . $transactionId);
          exit();
     }
     public function repurchase()
     {
          $post              = $this->input->post();
          $transactionId     = trim($post['transaction_id']);
          $expiration     = $post['expiration'];
          $transactionStatus = "Repurchased";
          $values            = array(
               'transaction_id' => $transactionId,
               'prev_due' => $expiration,
               'transaction_fee' => trim(str_replace(',', '', $post['repTransFee'])),
               'penalty_charge' => trim(str_replace(',', '', $post['repPenCharge'])),
               'status' => $transactionStatus,
               'date' => date("Y-m-d", strtotime($post['repDate'])),
               'user_fk' => $this->session->userdata('id'),
               'branch_fk' => $this->session->userdata('branch_fk')
          );
          $logData           = array(
               'transaction_id' => $transactionId,
               'description' => "Item has been repurchased",
               'user_fk' => $this->session->userdata('id'),
               'branch_fk' => $this->session->userdata('branch_fk')
          );
          $payment           = $this->Transaction_model->createPayment($values);
          if ($payment > 0) {
               $status    = $this->Transaction_model->updateStatus($transactionId, $transactionStatus);
               $createLog = $this->Transaction_model->createLog($logData);
          }
          header("Location: " . base_url("Transaction?search=") . $transactionId);
          exit;
     }
     public function renew()
     {
          $post              = $this->input->post();
          $transactionId     = trim($post['transaction_id']);
          $expiration     = $post['expiration'];
          $transactionStatus = "In Contract";
          $date              = date("Y-m-d", strtotime($post['renDate']));
          $due               = date("Y-m-d", strtotime($post['renDue']));

          $renew             = $this->Transaction_model->processRenew(array(
                                   'expiration' => $due,
                                   'transaction_id' => $transactionId,
                                   'status' => $transactionStatus
                              ));

          $payment           = $this->Transaction_model->createPayment(array(
                                   'transaction_id' => $transactionId,
                                   'transaction_fee' => trim(str_replace(',', '', $post['renTransFee'])),
                                   'penalty_charge' => trim(str_replace(',', '', $post['renPenCharge'])),
                                   'status' => "Renew",
                                   'user_fk' => $this->session->userdata('id'),
                                   'branch_fk' => $this->session->userdata('branch_fk'),
                                   'date' => $date,
                                   'prev_due' => $expiration,
                                   'next_due' => $due
                              ));

          if ($payment > 0) {
               $logData   = array(
                    'transaction_id' => $transactionId,
                    'description' => "Renew contract",
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog = $this->Transaction_model->createLog($logData);
          }
          header("Location: " . base_url("Transaction?search=") . $transactionId);
          exit;
     }
     public function returnTransaction()
     {
          $post          = $this->input->post();
          $transactionId = trim($post['transaction_id']);
          $salesId       = trim($post['sales_id']);
          $date          = date("Y-m-d", strtotime($post['return_date']));
          $remarks       = $post['return_remarks'];
          $status        = $post['return_status'];
          $action        = $post['return_action'];
          $amount        = $sellingPrice = trim(str_replace(',', '', $post['return_amount']));

          if ($status == "For Sale") {
               $values = array(
                    'is_void' => 2,
                    'remarks' => $remarks
               );
          } else if ($status == "For Repair") {
               $values = array(
                    'is_void' => 3,
                    'remarks' => $remarks
               );
          } else if ($status == "For Disposal") {
               $values = array(
                    'is_void' => 4,
                    'remarks' => $remarks
               );
          }
          $updateSales  = $this->Transaction_model->updateSales($transactionId, $values);
          $updateStatus = $this->Transaction_model->updateStatus($transactionId, $status);
          if ($updateSales > 0) {
               $returnData   = array(
                    'transaction_id' => $transactionId,
                    'sales_id' => $salesId,
                    'date' => $date,
                    'quantity' => 1,
                    'action' => $action,
                    'amount' => $amount,
                    'remarks' => $remarks,
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createReturn = $this->Transaction_model->createReturn($returnData);
               $logData      = array(
                    'transaction_id' => $transactionId,
                    'description' => "Item was returned. Update Status to " . $status,
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk'),
                    'date' => $date
               );
               $createLog    = $this->Transaction_model->createLog($logData);
          }
          header("Location: " . base_url("Transaction?search=") . $transactionId);
     }
     public function createTransfer()
     {
          $post           = $this->input->post();
          $transactionId  = trim($post['transaction_id']);
          $status         = "For Transfer";
          $origin         = trim($post['transfer_origin']);
          $sender         = $this->session->userdata('id');
          $destination    = trim($post['transfer_destination']);
          $remarks        = $post['transfer_remarks'];
          $date           = date("Y-m-d", strtotime($post['date']));
          $values         = array(
               'transaction_id' => $transactionId,
               'date_created' => $date,
               'origin' => $origin,
               'destination' => $destination,
               'remarks' => $remarks,
               'sender' => $sender
          );
          $createTransfer = $this->Transaction_model->createTransfer($values);
          if ($createTransfer > 0) {
               $logData      = array(
                    'transaction_id' => $transactionId,
                    'description' => $status . " to " . $destination,
                    'user_fk' => $sender,
                    'branch_fk' => $origin
               );
               $createLog    = $this->Transaction_model->createLog($logData);
               $updateStatus = $this->Transaction_model->updateStatus($transactionId, $status);
          }
          header("Location: " . base_url("Transaction?search=") . $transactionId);
          exit;
     }
     public function voidTransfer()
     {
          $transactionId = trim($this->input->post('transaction_id'));
          $status        = $this->input->post('void_transfer_status');
          $voidTransfer  = $this->Transaction_model->voidTransfer($transactionId);
          $updateStatus = $this->Transaction_model->updateStatus($transactionId, $status);
          if ($voidTransfer > 0) {
               $logData      = array(
                    'transaction_id' => $transactionId,
                    'description' => "Void transfer",
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog    = $this->Transaction_model->createLog($logData);
               
          }
          $location = 'Location: ' . base_url('Transaction') . "?search=" . $transactionId;
          header(trim($location));
          exit;
     }
     public function acceptTransfer()
     {

          $status = $this->input->post('accept-status');
          $date = $this->input->post('accept-date');
          $branch_fk      = trim($this->input->post('accept-destination'));
          $transactionId  = trim($this->input->post('accept-id'));
          $acceptTransfer = $this->Transaction_model->acceptTransfer($transactionId, $branch_fk, $date);
          $logData        = array(
               'transaction_id' => $transactionId,
               'description' => "Transfered to " . $branch_fk,
               'user_fk' => $this->session->userdata('id'),
               'branch_fk' => $this->session->userdata('branch_fk')
          );
          $createLog      = $this->Transaction_model->createLog($logData);
          if ($transactionId[5] == 'T') {
               $values            = array(
                    'branch_fk' => $branch_fk,
                    'status' => $status
               );
               $updateTransaction = $this->Transaction_model->updateTransaction($transactionId, $values);
          } else if ($transactionId[5] == 'P') {
               $values            = array(
                    'branch_fk' => $branch_fk,
                    'status' => $status
               );
               $updateTransaction = $this->Purchase_model->updatePurchase($transactionId, $values);
          }
          header('Location: ' . base_url('Transfers'));
          exit;
     }

     function voidLastPayment($transactionId) {
		$last_payment_id = $this->db->where('transaction_id', $transactionId)->order_by('id', 'DESC')->get('transaction_payments')->row_array()['id'];
		$last_log_id = $this->db->where('transaction_id', $transactionId)->order_by('id', 'DESC')->get('transaction_logs')->row_array()['id'];

          $prev_due = $this->db->select("prev_due")->where('id', $last_payment_id)->from('transaction_payments')->get()->row_array()['prev_due'];
          $this->db->where('transaction_id', $transactionId)->update('transactions', array('status' => 'In Contract', 'expiration' => trim($prev_due)));

          if ($this->db->affected_rows() > 0) {
               $this->db->where('id', $last_payment_id)->delete('transaction_payments');

               $logData = array(
                    'transaction_id' => $transactionId,
                    'description' => "Void last payment",
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog = $this->Transaction_model->createLog($logData);
               $this->session->set_flashdata('success', 'Void success');
          } else {
               $this->session->set_flashdata('error', 'Void failed');
          }


          header('Location: ' . base_url('Transaction?search=') . $transactionId);
          exit;
     }
     
     function updateHistoryRemarks($transactionId) {          
          
          $this->db
          ->where('id', $this->input->post('history_id'))
          ->update('transaction_logs', array('remarks' => $this->input->post('history_remarks')));

          header('Location: ' . base_url('Transaction?search=') . $transactionId . '#transaction-history');
          exit;
     }
     function updateAppraisalValue() {
          $post              = $this->input->post();
          $transactionId     = trim($post['transaction_id']);          
          $new_value     = $post['new_value'];
          $old_value     = $post['old_value'];
          
          $values            = array(
               'value' => trim(str_replace(',', '', $new_value))
          );

          $updateTransaction       = $this->Transaction_model->updateTransaction($transactionId, $values);
          $updateCashout           = $this->Transaction_model->updateCashout($transactionId, $values);

          if ($updateCashout > 0) {
               $logData   = array(
                    'transaction_id' => $transactionId,
                    'description' => "Updated appraisal value from &#8369;" . $old_value . " to &#8369;" . $new_value,
                    'user_fk' => $this->session->userdata('id'),
                    'branch_fk' => $this->session->userdata('branch_fk')
               );
               $createLog = $this->Transaction_model->createLog($logData);
          }
          header("Location: " . base_url("Transaction?search=") . $transactionId);
          exit();
     }
     
     function cashjournal() {
          $this->load->model('Report_model');

		$data['title'] = "<a href='".base_url('Report')."' class='text-decoration-none '><small class='font-weight-medium'>Reports</small></a><small class='text-muted'> / </small>Cash Journal";
		$data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
		$data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);
		$data['branches'] = $this->Generic_model->getBranches();

		if( null !== $this->input->post('date_start') && null !== $this->input->post('date_end') ) {
            $date_start =  date("Y-m-d", strtotime($this->input->post('date_start')));
            $date_end =  date("Y-m-d", strtotime($this->input->post('date_end')));
          } else {
               $date_start = date("Y-m-01");
               $date_end = date("Y-m-d");
          }

          if(null !== $this->input->post('location') && $this->input->post('location') !== 'summary') {
               $branch_fk =  $this->input->post('location');
               $reports = $this->Report_model->getJournalReport($branch_fk, $date_start, $date_end);	
               $data['branch_info'] = $this->Generic_model->getBranchInfo($branch_fk);
          } else {
               $reports = $this->Report_model->getJournalReportSummary($date_start, $date_end);
               $data['branch_info'] = array('id' => 'summary', 'name' => 'SUMMARY (ALL LOCATIONS)');
          }

          if(null !== $this->input->post('export')) {			
                    $this->Report_model->exportToExcel($reports, "JOURNAL REPORT");
               }
          
          $data['reports'] = $reports;

          $data['date_start'] = $date_start;
          $data['date_end'] = $date_end;

          $this->load->view('store/header-store', $data);
          $this->load->view('admin/reports-journal', $data);
          $this->load->view('store/footer-store');
	}

}
