<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct() {
		 parent::__construct();
		 $this->load->library('session');
		 $this->load->model('Customer_model');
		 date_default_timezone_set('Asia/Manila');

		 $role = $this->session->userdata('role_fk');
		 if(!$role) {
			 $this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
			 header('Location: ' . base_url('Login'));
		 }
	}

	public function upload() {
		$file = $this->input->post('photo');
		$customer_id = $this->input->post('customer-id');
		
		$config['upload_path'] = 'assets/images/customers';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 2000; //2MB
		$config['max_width'] = 1024;
		$config['max_height'] = 768;
		$config['overwrite'] = TRUE;
		$config['file_name'] = $customer_id;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('photo')) {
			$upload_error = $this->upload->display_errors();
			$this->session->set_flashdata('upload_error', $upload_error);			
			// $this->session->set_flashdata('error', $upload_error);
		} else {
			$upload_data = $this->upload->data(); 
			$this->db->where('id', $customer_id)->update('customers', array('photo' => $upload_data['file_name']));
			$this->session->set_flashdata('upload_data', $upload_data);			
			$this->session->set_flashdata('success', 'File has been successfully uploaded');
		}
		
		$redirect = trim("Location: " . base_url("Customer/profile/") . $customer_id . "#photo-section");
		header($redirect);
	}

	public function index() {
		$data['provinces'] = $this->Generic_model->getProvinces();
		$data['cities'] = $this->Generic_model->getCities();
		$data['customers'] = $this->Customer_model->getCustomers();
		$data['title'] = "Customers";

		if(null !== $this->input->get('tab')) {
	    	$data['tab'] = $this->input->get('tab');	
	    } else {
	    	$data['tab'] = "records";	
	    }

		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);
		$this->load->view('store/header-store', $data);
		$this->load->view('store/customer', $data);
		$this->load->view('store/footer-store');
	}

	public function create() {
		$data['provinces'] = $this->Generic_model->getProvinces();
		$data['cities'] = $this->Generic_model->getCities();
		$data['customers'] = $this->Customer_model->getCustomers();
		$data['title'] = "Customers";

		$customer =  $this->input->post();
		$values = array(
				'first_name' => trim($customer['firstname']),
				'middle_name' => trim($customer['middlename']),
				'last_name' => trim($customer['lastname']),
				'address' => trim($customer['address']),
				'city_fk' => trim($customer['city']),
				'contact' => trim($customer['contact']),
				'identification_type' => trim($customer['id_type']),
				'identification_number' => trim($customer['id_number']),
				'gender' => trim($customer['gender']),
				'remarks' => trim($customer['remarks'])
		);

		$addCustomer = $this->Customer_model->addCustomer($values);

		if($addCustomer > 0) {
			header('Location: ' . base_url('Customer/profile/') . $addCustomer);	
		} else {
			header('Location: ' . base_url('Customer?tab=new'));
		}
	}

	public function profile($id) {
		// view customer
		$data['provinces'] = $this->Generic_model->getProvinces();
		$data['cities'] = $this->Generic_model->getCities();
		$data['customers'] = $this->Customer_model->getCustomers($id);
		$data['transactions'] = $this->Customer_model->getTransactions($id);
		$data['title'] = "Customers";

		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);

		$this->load->view('store/header-store', $data);
		$this->load->view('store/customer-profile', $data);
		$this->load->view('store/footer-store');
	}

	public function update($id = null) {
		$data['title'] = "Customer";
		$data['provinces'] = $this->Generic_model->getProvinces();
		$data['cities'] = $this->Generic_model->getCities();

		$customer = $this->input->post();
		$id = $customer['id'];
		$values = array(
				'first_name' => trim($customer['firstname']),
				'middle_name' => trim($customer['middlename']),
				'last_name' => trim($customer['lastname']),
				'address' => trim($customer['address']),
				'city_fk' => trim($customer['city']),
				'contact' => trim($customer['contact']),
				'identification_type' => trim($customer['id_type']),
				'identification_number' => trim($customer['id_number']),
				'gender' => trim($customer['gender']),
				'remarks' => trim($customer['remarks'])
		);
		$updateCustomer = $this->Customer_model->updateCustomer($id, $values);
		header('Location: ' . base_url('Customer/profile/') . $id);

	}
}
