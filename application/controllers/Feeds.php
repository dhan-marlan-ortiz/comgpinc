<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Feeds extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		/* if not admin Clear session and redirect to login  */
		$role = ($this->session->userdata('role_fk'));
		if($role != "ADMS") {
			$this->session->set_flashdata('error', 'Session expired. Please login to continue.');
			// header('Location: ' . base_url('Login'));
			// exit;
		}
	}

	public function visitors() {
		$visitors = $this->db->get('visitors')->result_array();
		echo "<pre>";
		echo json_encode($visitors, JSON_PRETTY_PRINT);
		echo "</pre>";
	}

	public function server() {
		$server = array(
			'day' => date('l'),			
			'date' => date('F d, Y'),
			'time' => date("h:i:s A")
		);

		echo "<pre>";
		echo json_encode($server, JSON_PRETTY_PRINT);
		echo "</pre>";
	}
}
