<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TimeTracker extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$role = $this->session->userdata('role_fk');
		$this->load->model('Time_model');
		date_default_timezone_set('Asia/Manila');
		if(!$role) {
			$this->session->set_flashdata('error', 'Session Expired. Please login to continue.');
			header('Location: ' . base_url('Login'));
		}
	}
	public function index() {
		$role = $this->session->userdata('role_fk');
		$data['title'] = "Time Tracker";
		$data['navbar'] = $this->load->view('store/navbar-store', $data, true);
		$data['sidebar'] = $this->load->view('store/sidebar-store', $data, true);
		$data['employees'] = $this->db->get('employees')->result_array();
		$data['time_record'] = $this->db->get_where('time_tracker', array('date_time >=' => date('Y-m-d 00:00:00') ))->result_array();
		
		$datetime_start = date('Y-m-d 00:00:00');
		$datetime_end = date('Y-m-d 23:59:59');
		$attendance = $this->Time_model->getAttendance($datetime_start, $datetime_end);

		/*
		$attendance = $this->db->select("t.employee_id, CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) as employee_name, b.name as branch, DATE_FORMAT(t.date_time, '%M %d, %Y') as date, DATE_FORMAT(t.date_time, '%r') as time_in, 'time_out'")->from("time_tracker as t")
								->where('t.date_time >=', date('Y-m-d 00:00:00'))
								->where('t.description', 'time in')							
								->join('employees as e', 'e.id = t.employee_id')
								->join('branches as b', 'b.id = t.location')
								->get()->result_array();

		foreach ($attendance as $a_key => $a) {
			$attendance[$a_key]['time_out'] = $this->db->select('DATE_FORMAT(date_time, "%r") as date_time')->from('time_tracker')
														->where('employee_id', $a['employee_id'])
														->where('description', 'time out')
														->get()->row_array()['date_time'];
		}
		*/

		$data['attendance'] = $attendance;

		$this->load->view('store/header-store', $data);
		$this->load->view('store/navbar-store', $data);
		$this->load->view('store/time-tracker', $data);
		$this->load->view('store/footer-store');
	}

	public function register() {
		$post = $this->input->post();		
		$values = array(
					'employee_id' => $post['employee_id'],
					'description' => $post['description'],
					'location' => $this->session->userdata('branch_fk')
		);
		
		$register = $this->db->insert('time_tracker', $values);

		if($register > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}

		header('Location: ' . base_url('TimeTracker'));
		exit();
	}
}