<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function index() {
		$this->load->library('session');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('uid');
		$this->session->unset_userdata('first_name');
		$this->session->unset_userdata('last_name');
		$this->session->unset_userdata('role_fk');
		$this->session->unset_userdata('branch_fk');
          
		header('Location: ' . base_url('Login'));
	}

	public function resource() {			
		$this->session->unset_userdata('employee_id');				
		header('Location: ' . base_url('Portal'));
		exit();
	}
}
