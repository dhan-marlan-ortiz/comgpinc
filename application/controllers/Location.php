<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Location extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Location_model');
        
        /* if not admin Clear session and redirect to login  */
        $role = ($this->session->userdata('role_fk'));
        if($role != "ADMS") {
            $this->session->set_flashdata('error', 'Unauthorized Access. Your account is not authorized to access this section.');
            header('Location: ' . base_url('Logout'));
        }
    }
    public function index() {
        $role = $this->session->userdata('role_fk');
        $data['title'] = "Locations";
        $data['navbar'] = $this->load->view('admin/navbar-admin', $data, true);
        $data['sidebar'] = $this->load->view('admin/sidebar-admin', $data, true);


        $this->load->view('admin/header-admin', $data);
        $this->load->view('admin/navbar-admin', $data);

        $data['locations'] = $this->Location_model->getLocations();

        if (null !== $this->input->post('add')) {
            $this->form_validation->set_rules('id', 'Code', 'required|trim|is_unique[branches.id]');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', 'Location already exists. Please try again.');
                header('Location: ' . base_url('Location'));
            }
            else {
                $location =  $this->input->post();

                $values = array(
                    'id' => $location['id'],
                    'name' => $location['name'],
                    'address' => $location['address']
                );

                $addLocation = $this->Location_model->addLocation($values);
                header('Location: ' . base_url('Location'));
            }
        }

        $this->load->view('admin/location', $data);
        $this->load->view('admin/footer-admin');
    }

    public function update() {
        if (null !== $this->input->post('update')) {        
            $location = $this->input->post();
            $id = $location['current_id'];

            $values = array(
                'id' => $location['update_location_id'],
                'name' => $location['update_location_name'],
                'address' => $location['update_address_name']
            );
            $update_location = $this->Location_model->updateLocation($id, $values);

            header('Location: ' . base_url('Location'));
        }
    }
}
