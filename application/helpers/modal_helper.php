<?php
function report_location_status_daterange($title, $branch_info, $date_start, $date_end, $status, $branches, $has_result) {

$markup = "";
                         
$markup .=                    
          '
     <div class="row narrow-gutters">
          <div class="col-12 col-lg-6">
               <h4 class="font-weight-bold no-wrap mb-3">' . $title . '</h4>
               <p class="d-inline mr-4 no-wrap">
                    <i class="ti-location-pin font-weight-medium"></i>&ensp;' . $branch_info['name'] . '
               </p>
               <p class="d-inline no-wrap">
                    <i class="ti-calendar font-weight-medium"></i>&ensp;' . strtoupper(date("F d, Y", strtotime($date_start))) . ' &ndash; ' . strtoupper(date("F d, Y", strtotime($date_end))) . '
               </p>
               <div class="d-table mt-2">
                    <div class="d-table-cell">
                         <i class="ti-flag-alt font-weight-medium"></i>&ensp;
                    </div>
                    <div class="d-table-cell">
                         <ul class="list-inline list-inline-barred mb-0">';
                              foreach ($status as $s => $stat) {
                                   $markup .= '<li class="list-inline-item">' . $stat . '</li>';
                              }
$markup .=               '</ul>
                    </div>
               </div>
          </div>
          <div class="col-12 col-lg-6 text-lg-right no-wrap align-self-end">';

               $markup .= form_button(
                              array(
                                   'type' => 'button',
                                   'content' => 'GENERATE REPORT',
                                   'class' => 'btn btn-primary text-white mr-1 mt-2',
                                   'data-toggle' => 'modal',
                                   'data-target' => '#generate-report-modal'
                              ));

               if($has_result) {
                    $markup .= form_input(
                                   array(
                                        'type'  => 'submit',
                                        'name'  => 'export',
                                        'value' => 'Export to excel',
                                        'class' => 'btn btn-success mr-1 mt-2',
                                        'download' => 'download'
                              ));
                    }

               $markup .= anchor( base_url('Report'), 'Close', array(
                                   'class' => 'btn btn-light border mt-2')
                              );
$markup .=
          '</div>                    
     </div>                    
          <div class="modal fade" id="generate-report-modal" tabindex="-1" role="dialog">
               <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                              <h5 class="modal-title text-primary">GENERATE REPORT</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                         </div>
                         <div class="modal-body">
                              <div class="form-group mb-3">                                   
                                   <label for="" class="d-block">
                                        Location
                                   </label>
                                   <select class="form-control close-on-click" name="location" required>
                                        <option value="" disabled selected>SELECT</option>';
                                        foreach ($branches as $fb) {
                                        $markup .= "<option value='" . $fb['id'] . "' " . ($fb['id'] == $branch_info['id'] ? "selected" : "" ) . ">" .
                                                       $fb['name'] . " (" . $fb['id'] . ")" .
                                                  "</option>";
                                        }                                        
$markup .=                         '</select>
                              </div>                         
                              <div class="form-group mb-3">
                                   <label for="" class="d-block w-100">
                                        Status
                                        <a href="#" class="clear-select float-right text-decoration-none text-monospace text-small" title="Reset Field">
                                             CLEAR ALL
                                        </a>
                                        <span class="float-right mx-3 text-muted">|</span>
                                        <a href="#" class="select-all float-right text-decoration-none  text-monospace text-small" title="Reset Field">
                                             SELECT ALL
                                        </a>
                                   </label>
                                   <select class="form-control" multiple="multiple" name="status[]" required>
                                        <option value="Due" '. (in_array("Due", $status) ? "selected" : "" ) .' > DUE </option>
                                        <option value="Expired" '. (in_array("Expired", $status) ? "selected" : "" ) . '> EXPIRED </option>
                                        <option value="Grace Period" '. (in_array("Grace Period", $status) ? "selected" : "" ) . '> GRACE PERIOD </option>
                                        <option value="In Contract" '. (in_array("In Contract", $status) ? "selected" : "" ) . '> IN CONTRACT </option>
                                        <option value="Freeze" '. (in_array("Freeze", $status) ? "selected" : "" ) . '> FREEZE </option>
                                        <option value="For Sale" '. (in_array("For Sale", $status) ? "selected" : "" ) . '> FOR SALE </option>
                                        <option value="For Repair" '. (in_array("For Repair", $status) ? "selected" : "" ) . '> FOR REPAIR </option>
                                        <option value="For Transfer" '. (in_array("For Transfer", $status) ? "selected" : "" ) . '> FOR TRANSFER </option>
                                        <option value="For Disposal" '. (in_array("For Disposal", $status) ? "selected" : "" ) . '> FOR DISPOSAL </option>
                                        <option value="Sold" '. (in_array("Sold", $status) ? "selected" : "" ) . '> SOLD </option>
                                        <option value="Repurchased" '. (in_array("Repurchased", $status) ? "selected" : "" ) . '> REPURCHASED </option>
                                        <option value="Disposed" '. (in_array("Disposed", $status) ? "selected" : "" ) . '> DISPOSED </option>
                                   </select>
                              </div>
                         
                              <div class="form-group" id="date-range-filter">
                                   <label class="d-inline-block w-50">
                                        Date Range Filter
                                   </label>';

$markup .=                         '<div class="input-group w-100">
                                        <input type="text" placeholder="Start Date" class="form-control datepicker datepicker-left text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_start" value="' . date("F d Y", strtotime($date_start)) . '" required>
                                        <input type="text" placeholder="End Date" class="form-control has-loader datepicker datepicker-right text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_end" value="' .  date("F d Y", strtotime($date_end)) .'" required>
                                   </div>
                              </div>

                         </div>
                         <div class="modal-footer">
                              <a href="" class="btn btn-light border" data-dismiss="modal">Cancel</a>
                              <input type="submit" name="generate" value="generate" class="btn btn-primary">
                         </div>
                    </div>
               </div>
          </div>
                    ';

     return $markup;               
}

function report_date($title, $branch_info, $date_end, $has_result) {
     $mark_up = '
     <div class="row narrow-gutters">
          <div class="col-12 col-lg-6">
               <h4 class="font-weight-bold no-wrap mb-3">' . $title . '</h4>
               <p class="d-inline mr-4 no-wrap">
                    <i class="ti-calendar font-weight-medium"></i>&ensp;' . strtoupper(date("F d, Y", strtotime($date_end))) . '
               </p>      
          </div>
          <div class="col-12 col-lg-6 text-lg-right no-wrap">';
          $mark_up .= form_button(array(
                              'type' => 'button',
                              'content' => 'GENERATE REPORT',
                              'class' => 'btn btn-primary text-white mr-1 mt-2',
                              'data-toggle' => 'modal',
                              'data-target' => '#generate-report-modal'
                         ));

          if($has_result) {
               $mark_up .= form_input(array(
                              'type'  => 'submit',
                              'name'  => 'export',
                              'value' => 'Export to excel',
                              'class' => 'btn btn-success mr-1 mt-2',
                              'download' => 'download'
                         ));
               }

          $mark_up .= anchor( base_url('Report'), 'Close', array(
                              'class' => 'btn btn-light border mt-2')
                         );           
     $mark_up .= '     
          </div>
          <div class="modal fade" id="generate-report-modal" tabindex="-1" role="dialog">
               <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                              <h5 class="modal-title text-primary">GENERATE REPORT</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                         </div>
                         <div class="modal-body">
                              <div class="form-group">
                                   <label> Date </label>
                                   <input type="text" placeholder="Date" class="form-control datepicker text-uppercase font-12 w-100" data-date-format="MM dd, yyyy" name="date_end" value="' . date('F d Y', strtotime($date_end)) . '" required>
                              </div>                    
                         </div>
                         <div class="modal-footer">
                              <a href="" class="btn btn-light border col" data-dismiss="modal">Cancel</a>
                              <input type="submit" name="generate" value="generate" class="btn btn-primary col">
                         </div>
                    </div>
               </div>
          </div>
     </div>';

     return $mark_up;
}

function report_daterange($title, $date_start, $date_end, $has_result) {
     $mark_up = '
     <div class="row narrow-gutters">
          <div class="col-12 col-lg-6">
               <h4 class="font-weight-bold no-wrap mb-3">' . $title . '</h4>               
               <p class="d-inline mr-4 no-wrap">
                    <i class="ti-calendar font-weight-medium"></i>&ensp;' . strtoupper(date("F d, Y", strtotime($date_start))) . ' &ndash; ' . strtoupper(date("F d, Y", strtotime($date_end))) . '
               </p>               
          </div>       
          <div class="col-12 col-lg-6 text-lg-right no-wrap">';
               $mark_up .= form_button(array(
                                        'type' => 'button',
                                        'content' => 'GENERATE REPORT',
                                        'class' => 'btn btn-primary text-white mr-1 mt-2',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#generate-report-modal'
                                   ));

               if($has_result) {
                    $mark_up .= form_input(array(
                                   'type'  => 'submit',
                                   'name'  => 'export',
                                   'value' => 'Export to excel',
                                   'class' => 'btn btn-success mr-1 mt-2',
                                   'download' => 'download'
                              ));
                    }

               $mark_up .= anchor( base_url('Report'), 'Close', array(
                                   'class' => 'btn btn-light border mt-2')
                              );

     $mark_up .= '
          </div>              
     </div>
     <div class="modal fade" id="generate-report-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-primary">GENERATE REPORT</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">';
               $mark_up .= '<div class="form-group has-datepicker-label mb-3">
                              <label for="" class="d-block w-100">
                                   Date Range Filter
                                   <a href="#" class="clear-input float-right text-decoration-none ml-2" title="Reset Field">
                                        <small>Clear</small>
                                   </a>
                              </label>
                              <div class="input-group datepicker-group w-100">                              
                                                                      
                                   <div class="datepicker-item w-50">                              
                                        <input type="text" placeholder="Start Date" class="form-control datepicker datepicker-left text-uppercase font-12 w-100 border-right-0" data-date-format="MM dd, yyyy" name="date_start" value="' . date('F d Y', strtotime($date_start)) . '" required>
                                        <span class="datepicker-label">DATE START</span>
                                   </div>
                                   <div class="datepicker-item w-50">                              
                                        <input type="text" placeholder="End Date" class="form-control datepicker datepicker-right text-uppercase font-12 w-100" data-date-format="MM dd, yyyy" name="date_end" value="' . date('F d Y', strtotime($date_end  )) . '" required>
                                        <span class="datepicker-label">DATE END</span>
                                   </div>
                              </div>
                         </div>                    
                    </div>
                    <div class="modal-footer">
                         <a href="" class="btn btn-light border" data-dismiss="modal">Cancel</a>
                         <input type="submit" name="generate" value="generate" class="btn btn-primary">
                    </div>
               </div>
          </div>
     </div>';

     return $mark_up;
}

function report_location_date($title, $branch_info, $branches, $date_end, $has_result) {
     $mark_up = '
     <div class="row narrow-gutters">
          <div class="col-12 col-lg-6">
               <h4 class="font-weight-bold no-wrap mb-3">' . $title . '</h4>
               <p class="d-inline mr-4 no-wrap">
                    <i class="ti-location-pin font-weight-medium"></i>&ensp;' . $branch_info['name'] . '
               </p>
               <p class="d-inline mr-4 no-wrap">
                    <i class="ti-calendar font-weight-medium"></i>&ensp;' . strtoupper(date("F d, Y", strtotime($date_end))) . '
               </p>               
          </div>       
          <div class="col-12 col-lg-6 text-lg-right no-wrap">';

               $mark_up .= form_button(array(
                                        'type' => 'button',
                                        'content' => 'GENERATE REPORT',
                                        'class' => 'btn btn-primary text-white mr-1 mt-2',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#generate-report-modal'
                                   ));

               if($has_result) {
                    $mark_up .= form_input(array(
                                   'type'  => 'submit',
                                   'name'  => 'export',
                                   'value' => 'Export to excel',
                                   'class' => 'btn btn-success mr-1 mt-2',
                                   'download' => 'download'
                              ));
                    }

               $mark_up .= anchor( base_url('Report'), 'Close', array(
                                   'class' => 'btn btn-light border mt-2')
                              );

     $mark_up .= '
          </div>              
     </div>
     <div class="modal fade" id="generate-report-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-primary">GENERATE REPORT</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                              <div class="form-group mb-3">
                                   <label for="" class="d-block w-100">
                                        Location
                                   </label>
                                   <select class="form-control close-on-click" name="location" required>
                                        <option value="summary">SUMMARY (ALL LOCATION)</option>
                                   ';
                                   foreach ($branches as $fb) {
                                        $branch_code = $fb['id'];
                                        $branch_option = $fb['name'] . " (" . $branch_code . ")";
                                        if($branch_info['id'] == $branch_code) {
                                             $mark_up .= "<option value='" . $branch_code . "' selected>" . $branch_option . "</option>";
                                        } else {
                                             $mark_up .="<option value='" . $branch_code . "'>" . $branch_option . "</option>";
                                        }                              
                                   }
     $mark_up .=                   '</select>
                              </div>
                              <div class="form-group">
                                   <label> Date </label>
                                   <input type="text" placeholder="Date" class="form-control datepicker text-uppercase font-12 w-100" data-date-format="MM dd, yyyy" name="date_end" value="' . date('F d Y', strtotime($date_end)) . '" required>
                              </div>                    
                    </div>
                    <div class="modal-footer">
                         <a href="" class="btn btn-light border col" data-dismiss="modal">Cancel</a>
                         <input type="submit" name="generate" value="generate" class="btn btn-primary col">
                    </div>
               </div>
          </div>
     </div>';

     return $mark_up;
}

function report_location_daterange($title, $branch_info, $date_start, $date_end, $branches, $has_result, $has_summary) {


     
	$mark_up = '
	<div class="row narrow-gutters">
          <div class="col-12 col-lg-6">
               <h4 class="font-weight-bold no-wrap mb-3">' . $title . '</h4>
               <p class="d-inline mr-4 no-wrap">
                    <i class="ti-location-pin font-weight-medium"></i>&ensp;' . $branch_info['name'] . '
               </p>
               <p class="d-inline mr-4 no-wrap">
                    <i class="ti-calendar font-weight-medium"></i>&ensp;' . strtoupper(date("F d, Y", strtotime($date_start))) . ' &ndash; ' . strtoupper(date("F d, Y", strtotime($date_end))) . '
               </p>               
          </div>       
          <div class="col-12 col-lg-6 text-lg-right no-wrap">';

               $mark_up .= form_button(array(
                                        'type' => 'button',
                                        'content' => 'GENERATE REPORT',
                                        'class' => 'btn btn-primary text-white mr-1 mt-2',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#generate-report-modal'
                                   ));

               if($has_result) {
                    $mark_up .= form_input(array(
                                   'type'  => 'submit',
                                   'name'  => 'export',
                                   'value' => 'Export to excel',
                                   'class' => 'btn btn-success mr-1 mt-2',
                                   'download' => 'download'
                              ));
                    }

               $mark_up .= anchor( base_url('Report'), 'Close', array(
                                   'class' => 'btn btn-light border mt-2')
                              );

     $mark_up .= '
          </div>              
     </div>
	<div class="modal fade" id="generate-report-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-primary">GENERATE REPORT</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">';

     if($_SESSION['role_fk'] == 'ADMS') {
     $mark_up .=         '<div class="form-group mb-3">
                              <label for="" class="d-block w-100">
                                   Location
                                   <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                        <small>Clear</small>
                                   </a>
                              </label>
                              <select class="form-control close-on-click" name="location" required> ';
                              
                              if($has_summary) {
                                   $mark_up .=  '<option value="summary">SUMMARY (ALL LOCATION)</option>';
                              }

                              foreach ($branches as $fb) {
                                   $branch_code = $fb['id'];
                                   $branch_option = $fb['name'] . " (" . $branch_code . ")";
                                   if($branch_info['id'] == $branch_code) {
                                        $mark_up .= "<option value='" . $branch_code . "' selected>" . $branch_option . "</option>";
                                   } else {
                                        $mark_up .="<option value='" . $branch_code . "'>" . $branch_option . "</option>";
                                   }                              
                              }
     $mark_up .=              '</select>
                         </div>';
     } else {
          $mark_up .= '<input type="hidden" name="location" value="' . $_SESSION['branch_fk'] . '">';
     }
     

     $mark_up .=         '<div class="form-group has-datepicker-label mb-3">
                              <label for="" class="d-block w-100">
                                   Date Range Filter
                                   <a href="#" class="clear-input float-right text-decoration-none ml-2" title="Reset Field">
                                        <small>Clear</small>
                                   </a>
                              </label>
                              <div class="input-group datepicker-group w-100">                              
														
                                   <div class="datepicker-item w-50">                              
                                        <input type="text" placeholder="Start Date" class="form-control datepicker datepicker-left text-uppercase font-12 w-100 border-right-0" data-date-format="MM dd, yyyy" name="date_start" value="' . date('F d Y', strtotime($date_start)) . '" required>
                                        <span class="datepicker-label">DATE START</span>
                                   </div>
                                   <div class="datepicker-item w-50">                              
                                        <input type="text" placeholder="End Date" class="form-control datepicker datepicker-right text-uppercase font-12 w-100" data-date-format="MM dd, yyyy" name="date_end" value="' . date('F d Y', strtotime($date_end)) . '" required>
                                        <span class="datepicker-label">DATE END</span>
                                   </div>
                              </div>
                         </div>                    
                    </div>
                    <div class="modal-footer">
                         <a href="" class="btn btn-light border" data-dismiss="modal">Cancel</a>
                         <input type="submit" name="generate" value="generate" class="btn btn-primary">
                    </div>
               </div>
          </div>
     </div>';

     return $mark_up;
}

function generatingReportModal() {
	return '<div id="generating-report-modal" class="modal" tabindex="-1" role="dialog"  data-backdrop="static">
			     <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
			          <div class="modal-content bg-none border text-white">
			               <div class="modal-body">
			                    <table class="mx-auto my-3">
			                         <tr>
			                              <td>
			                                   <div class="spinner-border" role="status">
			                                        <span class="sr-only">Loading...</span>
			                                   </div>
			                              </td>
			                              <td>
			                                   <div>
			                                        <h4 class="m-0 p-2 font-weight-normal">Generating report...</h4>
			                                   </div>
			                              </td>
			                         </tr>
			                    </table>
			               </div>
			          </div>
			     </div>
			</div>';     
}

function loadingPageModal() {
	return '<div id="loading-page-modal" class="modal" tabindex="-1" role="dialog"  data-backdrop="static">
			     <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
			          <div class="modal-content bg-none border text-white">
			               <div class="modal-body">
			                    <table class="mx-auto my-3">
			                         <tr>
			                              <td>
			                                   <div class="spinner-border" role="status">
			                                        <span class="sr-only">Loading...</span>
			                                   </div>
			                              </td>
			                              <td>
			                                   <div>
			                                        <h4 class="m-0 p-2 font-weight-normal">Loading page...</h4>
			                                   </div>
			                              </td>
			                         </tr>
			                    </table>
			               </div>
			          </div>
			     </div>
			</div>';     
}



