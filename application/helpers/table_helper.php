<?php
     function table_open() {
          return "<table>";
     }
     function table_end() {
          return "</table>";
     }

     function tr_open() {
          return "<tr>";
     }
     function tr_close() {
          return "</tr>";
     }

     function td($text, $attr = null) {
          $class = "";
          if(isset($attr['class'])) {
               foreach ($attr['class'] as $key => $c) {
                    $class = $class . $c  . " " ;
               }               
          }
          return "<td class='" . $class . "'>" . $text . "</td>";
     }

     function th($text, $attr = null) {
          $class = "";
          if(isset($attr['class'])) {
               foreach ($attr['class'] as $key => $c) {
                    $class = $class . $c  . " " ;
               }               
          }
          return "<th class='" . $class . "'>" . $text . "</th>";
     }
