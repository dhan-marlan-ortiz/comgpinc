<?php

function tab_header($text, $style_class = null) {
     return '<h5 class="display-4 pb-3 '. $style_class .'"> <small class="font-weight-light text-twitter">' . $text . '</small> </h5>';
}

function admin_table($results, $options = null) {
$col_count = 0;
echo
     "<div class='table-responsive pb-0'>
          <table class='table pt-3' id='admin-table'>
               <thead>";
                         echo "<tr class='border-0 border-bottom-0 shadow-sm no-sort'>";
                         foreach ($results[0] as $th_key => $thead) {
                              echo "<th class='border-0 border-bottom-0 shadow-none text-left text-capitalize'>";
                              echo      "<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>";
                              echo           str_replace("_", " ", $th_key);
                              echo      "</span>";
                              echo "</th>";
                              $col_count++;
                         }
                         echo "</tr>";

echo                    
               "</thead>
                <tbody class='" . (isset($options['tbody_class']) ? $options['tbody_class'] : '') . "'>";
                         foreach ($results as $r => $result) {
                              echo tr_open();
                              foreach ($result as $key => $value) {
                                   echo td($value);                                   
                              }
                              echo tr_close();
                         }
echo                    
               "</tbody>
               <tfoot class='hidden-tfoot'>";
                         echo tr_open();
                         echo "<td colspan='" . $col_count . "'></td>";
                         echo tr_close();
echo
               "</tfoot>
          </table>
     </div>";
}

function report_table($reports, $sum_exempt, $currency_columns, $currency_counter = null, $item_counter = null) {
echo
     "<div class='table-responsive pb-0 mt-4 ' style='max-height: calc(100vh - 50px)'>
          <table class='table text-uppercase' id='report-table'>
               <thead>";
                         echo tr_open();
                         foreach ($reports[0] as $th_key => $thead) {
                              echo th(str_replace("_", " ", $th_key));
                         }
                         echo tr_close();
echo                    
               "</thead>
                <tbody class='text-monospace'>";
                         foreach ($reports as $r => $report) {
                              echo tr_open();
                              foreach ($report as $key => $value) {
                                   if($key == "date") {
                                        echo td(date("d M Y", strtotime($value)));
                                   } else if( in_array($key, $sum_exempt) ) {
                                        if($key == "description") {
                                             echo td($value, array('class'=> array('text-wrap')) );
                                        } else {
                                             echo td($value);                                             
                                        }                                        
                                   } else if(in_array($key, $currency_columns) ) {
                                        echo td(
                                             number_format(floatval($value), 2),
                                             array('class'=> array('text-right'))
                                        );
                                   } else if( isset($currency_counter) && in_array($key, $currency_counter) ) {
                                        echo td(
                                                  number_format(floatval($value), 2),
                                                  array('class'=> array('text-right'))
                                             );
                                   } else {
                                        echo td(
                                                  number_format(floatval($value)),
                                                  array('class'=> array('text-right'))
                                             );
                                   }                                             
                              }
                              echo tr_close();
                         }
echo                    
               "</tbody>
               <tfoot class='text-monospace'>";
                         echo tr_open();
                         foreach ($reports[0] as $tfoot_key => $tfoot) {
                              
                              if( in_array($tfoot_key, $sum_exempt) ) {
                                   echo th("");
                              } else {
                                   $sum = 0;      
                                   $last_item = "";
                                   $hidden_input_temp = "";
                                   foreach ($reports as $sr => $sum_report) {
                                        foreach ($sum_report as $sum_report_key => $sum_report_value) {
                                             if($tfoot_key == $sum_report_key) {
                                                  $sum += floatval($sum_report[$sum_report_key]);
                                                  $last_item = floatval($sum_report[$sum_report_key]);
                                                  if( isset($currency_counter) && in_array($tfoot_key, $currency_counter) ) {
                                                       $hidden_input = "<input type='hidden' name='currency_counter[]' value='" . $sum_report_key . "'>";
                                                       if($hidden_input != $hidden_input_temp) {
                                                            echo $hidden_input;
                                                       }
                                                       $hidden_input_temp = $hidden_input;
                                                  } else {
                                                       $hidden_input =  "<input type='hidden' name='sum_array[]' value='" . $sum_report_key . "'>";
                                                       if($hidden_input != $hidden_input_temp) {
                                                            echo $hidden_input;
                                                       }
                                                       $hidden_input_temp = $hidden_input;
                                                  }                                                  
                                             }
                                        }
                                   }

                                   if( isset($item_counter) && in_array($tfoot_key, $item_counter) ) {
                                        echo th(number_format($last_item),
                                               array( 'class' => array(($last_item >= 0 ? 'text-success' : 'text-danger') ) ) 
                                          );

                                   } else if( isset($currency_counter) && in_array($tfoot_key, $currency_counter) ) {
                                        echo th("<span class='font-weight-normal'>&#8369; </span> " . number_format($last_item, 2),
                                               array( 'class' => array(($last_item >= 0 ? 'text-success' : 'text-danger') ) ) 
                                          );

                                   } else if( in_array($tfoot_key, $currency_columns) ) {
                                        echo th("<span class='font-weight-normal'>&#8369; </span>" . number_format($sum, 2),
                                                  array( 'class' => array(($sum >= 0 ? 'text-success' : 'text-danger') ) ) 
                                             );
                                   } else {
                                        echo th(number_format($sum),
                                             array( 'class' => array(($sum >= 0 ? 'text-success' : 'text-danger') ) )
                                        );
                                   }                                      
                              }
                         }
                         echo tr_close();
echo
               "</tfoot>
          </table>
     </div>";
}

function result_not_found($redirect_link, $style_class = null) {
     $image_path = base_url('assets/svg/error-404.svg');
     echo "
     <div class='d-table w-100 h-75 $style_class'>
          <div class='d-table-cell align-middle text-center'>
               <div class='p-4'>
                    <img width='100px' height='auto' src=$image_path draggable='false'>
                    <h4 class='text-monospace font-weight-bold mb-4 mt-3 text-dark text-uppercase'>Result not found</h4>
                    <a href='$redirect_link' class='btn btn-secondary font-weight-bold'>Return</a>
               <div>
          </div>
     </div>";
}

function array_table($arr) {
     echo "<div class='table-responsive'>";
     echo      "<table class='table'>";
     echo           "<thead'>";
     echo                "<tr>";
                              foreach ($arr[0] as $key => $column) {
     echo                          "<th>$key</th>";
                              }
     echo                "</tr>";
     echo           "</thead'>";
     echo           "<tbody'>";
                         foreach ($arr as $row) {
     echo                     "<tr>";
                                   foreach ($row as $key => $column) {
     echo                               "<td>$column</td>";
                                   }
     echo                     "</tr>";
                         }    
     echo           "</tbody'>";
     echo      "</table>";
     echo "</div>";
}

function echo_f($value, $name = null) {
     if(null !== $name) {
          echo $name . ": " . $value . "<br>";
     } else {
          echo $name . "<br>";
     }
     return 0;
}

function sort_shelf($element1, $element2) {
     $datetime1 = strtotime($element1['entry_date']);
     $datetime2 = strtotime($element2['entry_date']);
     return $datetime1 - $datetime2;
}

function sort_journal($element1, $element2) {
     $datetime1 = strtotime($element1['date']);
     $datetime2 = strtotime($element2['date']);
     return $datetime1 - $datetime2;
}

function div_open($attr=null) {
     $class = "";
     if(isset($attr['class'])) {
          foreach ($attr['class'] as $key => $c) {
               $class = $class . $c  . " " ;
          }
     }

     return "<div class='" . $class . "'>";
}
function div_close() {
     return "</div>";
}

function reportFooterCTA($has_result = false) {
     $mark_up =  "<div class='row'><div class='col py-4 text-lg-right'>";
     $mark_up .= form_button(array(
                         'type' => 'button',
                         'content' => 'GENERATE REPORT',
                         'class' => 'btn btn-primary text-white mr-1 mt-2',
                         'data-toggle' => 'modal',
                         'data-target' => '#generate-report-modal'
                    ));

     if($has_result) {
          $mark_up .= form_input(array(
                         'type'  => 'submit',
                         'name'  => 'export',
                         'value' => 'Export to excel',
                         'class' => 'btn btn-success mr-1 mt-2',
                         'download' => 'download'
                    ));
          }

     $mark_up .= anchor( base_url('Report'), 'Close', array(
                         'class' => 'btn btn-light border mt-2')
                    );   
     $mark_up .= "</div></div>";

     return $mark_up;
}

function transaction_tab($active_nav, $item_notif = null) {
     echo "<h4 class='font-weight-bold mb-3'>Transactions</h4>";
     echo '<ul class="nav nav-tabs">
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "records" ? "active" : "") . ' " href="' . base_url('Transaction') . '">Records</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "create" ? "active" : "") . ' " href="' . base_url('Transaction/create') . '">New Transaction</a>
               </li>';
          if($active_nav == "view") {
          echo '<li class="nav-item">
                    <a class="nav-link active">' . $_GET['search'] . '</a>
               </li>';
          }
     echo '</ul>';
}

function purchase_tab($active_nav) {
     echo "<h4 class='font-weight-bold mb-3'>Purchases</h4>";
     echo '<ul class="nav nav-tabs">
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "records" ? "active" : "") . ' " href="' . base_url('Purchase') . '">Records</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "create" ? "active" : "") . ' " href="' . base_url('Purchase/create') . '">New Purchase</a>
               </li>';
               if($active_nav == "view") {
               echo '<li class="nav-item">
                         <a class="nav-link active">' . $_GET['search'] . '</a>
                    </li>';
               }
     echo '</ul>';
}

function item_settings_tab($active_nav) {
     echo "<h4 class='font-weight-bold mb-3'>Item Settings</h4>";
     echo '<ul class="nav nav-tabs">
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "type" ? "active" : "") . ' " href="' . base_url('ItemType') . '">Item Type</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "brand" ? "active" : "") . ' " href="' . base_url('ItemBrand') . '">Brand Name</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "product" ? "active" : "") . ' " href="' . base_url('ItemName') . '">Item Name</a>
               </li>';               
     echo '</ul>';
}

function admin_settings_tab($active_nav) {
     echo "<div class='row'>
               <div class='col-sm-4'>
                    <h4 class='font-weight-bold mb-3'>Admin Settings</h4>
               </div>
               <div class='col-sm-8'>
                    <div id='toolbar'>
                    </div>
               </div>
          </div>";
     echo '<ul class="nav nav-tabs">
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "User" ? "active" : "") . ' " href="' . base_url('User') . '">Users</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "Employee" ? "active" : "") . ' " href="' . base_url('Employee') . '">Employees</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "Location" ? "active" : "") . ' " href="' . base_url('Location') . '">Locations</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "Appraiser" ? "active" : "") . ' " href="' . base_url('Appraiser') . '">Appraisers</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link ' . ($active_nav == "Visitors" ? "active" : "") . ' " href="' . base_url('User/visitors') . '">Visitors</a>
               </li>';               

     echo '</ul>';
}


function retail_tab($active_nav) {
     echo '<ul class="nav nav-tabs">
               <li class="nav-item">
                    <a class="nav-link animated ' . ($active_nav == "inventory" ? "active slideInUp faster" : "fadeIn") . ' " href="' . base_url('Retail/inventory') . '">Products</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link animated ' . ($active_nav == "sell" ? "active slideInUp faster" : "fadeIn") . ' " href="' . base_url('Retail/sell') . '">Sell Product</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link animated ' . ($active_nav == "registration" ? "active slideInUp faster" : "fadeIn") . ' " href="' . base_url('Retail/registration') . '">Product Registration</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link animated ' . ($active_nav == "returned" ? "active slideInUp faster" : "fadeIn") . ' " href="' . base_url('Retail/returned') . '">Returned Items</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link animated ' . ($active_nav == "pending" ? "active slideInUp faster" : "fadeIn") . ' " href="' . base_url('Retail/pending') . '">Pending Items</a>
               </li>
               <li class="nav-item">
                    <a class="nav-link animated ' . ($active_nav == "disposedAndTransferred" ? "active slideInUp faster" : "fadeIn") . ' " href="' . base_url('Retail/disposedAndTransferred') . '">Disposed and Transferred</a>
               </li>
               ';
     echo '</ul>';
}

function transfer_tab($active_nav) {
     echo "<h4 class='font-weight-bold mb-3'>Item Transfer</h4>";
     echo "<div class='row no-gutters'>
               <div class='col-lg-6 align-self-end'>";
                    echo '<ul class="nav nav-tabs">
                              <li class="nav-item">
                                   <a class="nav-link ' . ($active_nav == "inbound" ? "active" : "") . ' " href="' . base_url('Transfers') . '">Inbound</a>
                              </li>
                              <li class="nav-item">
                                   <a class="nav-link ' . ($active_nav == "outbound" ? "active" : "") . ' " href="' . base_url('Transfers/outbound') . '">Outbound</a>
                              </li>';

                    echo '</ul>';
          echo "</div>";
          echo "<div class='col-lg-6' style='border-bottom: 1px solid #ebedf2'>";
          echo "<div id='table-toolbar' class='text-right'></div>";
          echo "</div>";
     echo "</div>";
}

function console_log($alert) {
     echo "<script>";
     echo "console.log('" . $alert . "')";
     echo "</script>";

     return;
}

function print_pre($array) {
     echo "<pre>";
     print_r($array);
     echo "</pre>";

     return;
}

function intToAlphabet($number) {
     $list = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
     return $list[$number];
}

function addGoogleTag() {
     $gtag = "";
     if(base_url() == "https://comgpinc.com/") {
          $gtag =  "
               <!-- Global site tag (gtag.js) - Google Analytics -->
               <script async src='https://www.googletagmanager.com/gtag/js?id=UA-158068065-1'></script>
               <script>
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', 'UA-158068065-1');
               </script>
          ";

     } else if(base_url() == "//dev.comgpinc.com/") {
          $gtag =  "
          <!-- Global site tag (gtag.js) - Google Analytics -->
          <script async src='https://www.googletagmanager.com/gtag/js?id=UA-158068065-2'></script>
          <script>
               window.dataLayer = window.dataLayer || [];
               function gtag(){dataLayer.push(arguments);}
               gtag('js', new Date());

               gtag('config', 'UA-158068065-2');
          </script>
          ";
          
     }

     return $gtag;
}

