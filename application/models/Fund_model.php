<?php
class Fund_model extends CI_Model {

     function getFundReports($location, $date_start, $date_end) {
          $this->db->select("*");
          $this->db->from("funds");

          $this->db->group_start();
               $this->db->or_where('origin', $location);
               $this->db->or_where('destination', $location);
          $this->db->group_end();

          $this->db->where('date >=', $date_start);
          $this->db->where('date <=', $date_end);

          $this->db->join('fund_tags', 'fund_tags.id = funds.tag_fk');
          $this->db->order_by('date', 'ASC');
          $query = $this->db->get();
          $result = $query->result_array();

          return $result;
     }

     function transferFund() {
          $db_debug = $this->db->db_debug;
          $this->db->db_debug = false;

          $post = $this->input->post();
          $origin = $post['origin'];
          $destination = $post['destination'];
          $description = strtoupper($post['description']);
          $user_fk = $this->session->userdata('id');
          $date = date("Y-m-d", strtotime($post['date']));
          $amount = floatval(str_replace(",", "", $post['amount']));
          $transactionId = $this->Generic_model->generateTransactionId('funds', $origin, $date);

          $values = array(
                         'transaction_id' => $transactionId,
                         'origin' => $origin,
                         'destination' => $destination,
                         'user_fk' => $user_fk,
                         'amount' => abs($amount),
                         'description' => $description,
                         'tag_fk' => 3,
                         'date' => $date
                    );

          if (!$this->db->insert('funds', $values)) {
               $this->session->set_flashdata('error', "Failed");
          }else {
               $this->session->set_flashdata('success', 'Success');
          }

          $this->session->set_flashdata('filterLocation', $origin);

          return;
     }

     function insertFund($tag) {
          $db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		$post = $this->input->post();
		$branch_fk = $post['location'];
		$description = strtoupper($post['description']);
		$user_fk = $this->session->userdata('id');
		$date = date("Y-m-d", strtotime($post['date']));
		$amount = floatval(str_replace(",", "", $post['amount']));
          $transactionId = $this->Generic_model->generateTransactionId('funds', $branch_fk, $date);


          if($tag == 1) { // cash in
               $amount = abs($amount);
          } else if($tag == 2 ) { // cash out
               $amount = abs($amount);
          }

          $values = array(
                         'transaction_id' => $transactionId,
                         'origin' => $branch_fk,
                         'destination' => $branch_fk,
                         'user_fk' => $user_fk,
                         'amount' => $amount,
                         'description' => $description,
                         'tag_fk' => $tag,
                         'date' => $date
                    );

          if (!$this->db->insert('funds', $values)) {
			$this->session->set_flashdata('error', "Failed");
		}else {
			$this->session->set_flashdata('success', 'Success');
		}

          $this->session->set_flashdata('filterLocation', $branch_fk);

          return;
     }

     function getFund($destination = null, $date_begin = null, $date_end = null) {
          $this->db->select("f.*, d.name as destination_name, o.name as origin_name, u.first_name, u.last_name, t.tag");
          $this->db->from("funds as f");

          if(null !== $destination) {
               $this->db->group_start()
                    ->where('f.origin', $destination)
                         ->or_group_start()
                              ->where('f.destination', $destination)
                         ->group_end()
                    ->group_end();
          }

          if(isset($date_begin) && isset($date_end)) {
               $this->db->where('f.date >=', date('Y-m-d', strtotime($date_begin)));
               $this->db->where('f.date <=', date('Y-m-d', strtotime($date_end)));
          }
          $this->db->join('branches as d', 'd.id = f.destination');
          $this->db->join('branches as o', 'o.id = f.origin');
          $this->db->join('users as u', 'u.id = f.user_fk');
          $this->db->join('fund_tags as t', 't.id = f.tag_fk');

          $this->db->order_by("f.date", 'ASC');
          $this->db->order_by("f.id", 'ASC');

          $query = $this->db->get();

          $result = $query->result_array();

          return $result;
     }

     function voidFund() {
          $db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		$post = $this->input->post();
          $transactionId = $post['details-transaction'];

          if (!$this->db->where('transaction_id', $transactionId)->delete('funds')) {
			$this->session->set_flashdata('error', "Failed" . $transactionId);
		}else {
			$this->session->set_flashdata('success', 'Success');
		}

          return;
     }

     function sumCashIn($branch_fk) {
          $match = array('destination' => $branch_fk,
                         'origin' => $branch_fk,
                         'tag_fk' => 1
                    );
          $query = $this->db->get_where('funds', $match);
          $results = $query->result_array();

          $cashin = 0;
          foreach ($results as $r => $result) {
               $cashin += $result['amount'];
          }

          return $cashin;
     }

     function sumCashOut($branch_fk) {
          $match = array('destination' => $branch_fk,
                         'origin' => $branch_fk,
                         'tag_fk' => 2
                    );
          $query = $this->db->get_where('funds', $match);
          $results = $query->result_array();

          $cashin = 0;
          foreach ($results as $r => $result) {
               $cashin += $result['amount'];
          }

          return $cashin;
     }

     function sumTransferIn($branch_fk) {
          $match = array('destination' => $branch_fk,
                         'tag_fk' => 3
                    );
          $query = $this->db->get_where('funds', $match);
          $results = $query->result_array();

          $transferIn = 0;
          foreach ($results as $r => $result) {
               $transferIn += $result['amount'];
          }

          return $transferIn;
     }

     function sumTransferOut($branch_fk) {
          $match = array('origin' => $branch_fk,
                         'tag_fk' => 3
                    );
          $query = $this->db->get_where('funds', $match);
          $results = $query->result_array();

          $transferOut = 0;
          foreach ($results as $r => $result) {
               $transferOut += $result['amount'];
          }

          return $transferOut;
     }

     function getfundSummary($branch_fk) {
          $fund_tags = $this->db->get('fund_tags')->result_array();

          foreach ($fund_tags as $f => $fund) {
               $match = array('origin' => $branch_fk,
                              'tag_fk' => $fund['id']
                         );

               $query = $this->db->get_where('funds', $match);
               $results = $query->result_array();

               $sum = 0;
               foreach ($results as $r => $result) {
                    $sum += $result['amount'];
               }

               $fund_tags[$f]['sum'] = $sum;
          }
          return $fund_tags;
     }

     function exportExcel($funds) {
		$this->load->library("excel");
		$object 		= new PHPExcel();
		$object->setActiveSheetIndex(0);
		$excelObj 		= $object->getActiveSheet();
		$user_info 		= $this->User_model->getUsers($this->session->userdata('id'));

		$row 			= 6;
		$total 			= 0;
		$cash_in 		= 0;
		$cash_out 		= 0;
		$transfer_in 	= 0;
		$transfer_out 	= 0;
		$location 		= $this->input->post('location');
		$date_start 	= strtoupper(date("d M Y", strtotime($_POST['date_start'])));
		$date_end 		= strtoupper(date("d M Y", strtotime($_POST['date_end'])));
		$file_name 		= "FUND REPORT - " . $location . " - " . $date_start . " - " . $date_end;


		/* styles variables */
		$borderStyle 		= array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF999999'), ), ), );
		$borderStyleDark 	= array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF000000'), ), ), );
		$borderStyleWhite 	= array( 'borders' => array( 'outline' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FFFFFFFF'), ), ), );
		$fontStyle 			= array( 'font' => array( 'bold' => true, 'color' => array('argb' => 'FFFFFFFF') ) );
		/* styles variables end */

		/* write page header */
		$excelObj->setCellValueByColumnAndRow(0, 1, "COMMUNITY GADGET PHONESHOP");
		$excelObj->setCellValueByColumnAndRow(0, 2, $file_name);
		$excelObj->setCellValueByColumnAndRow(0, 3, "DATE GENERATED: " . strtoupper(date("F d, Y")));
		$excelObj->setCellValueByColumnAndRow(0, 4, "GENERATED BY: " . $user_info['first_name'] . " " . $user_info['first_name']);
		/* write page header end */

		/* write and style table head */
		$table_head = array("#", "CODE", "DATE", "DESCRIPTION", "DEPOSITS", "WITHDRAWALS", "TRANSFER IN", "TRANSFER OUT", "FUND BALANCE");
		foreach($table_head as $th => $head) {
			$excelObj->setCellValueByColumnAndRow($th, $row, $head);
		}
		foreach(range('A','I') as $columnID) {
			$excelObj->getStyle($columnID . $row)->applyFromArray($borderStyleWhite);
			$excelObj->getStyle($columnID . $row)->applyFromArray($fontStyle);
			$object->getActiveSheet()->getStyle($columnID . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00000000');
		}
		$excelObj->getStyle('A' . $row . ':I' . $row)->applyFromArray($borderStyleDark);
		$excelObj->getStyle('A' . $row . ':I' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		/* write and style table head end */

		$row++;

		/* write and style table body */
		foreach($funds as $td => $data) {
			$date 			= strtoupper(date("d M Y", strtotime($data['date'])));
			$user_fk 		= strtoupper($data['user_fk']);
			$tag 			= strtoupper($data['tag']);
			$tag_fk 		= strtoupper($data['tag_fk']);
			$description 	= strtoupper($data['description']);
			$transaction_id = $data['transaction_id'];
			$amount 		= $data['amount'];
			$destination 	= $data['destination'];
			$origin 		= $data['origin'];

			$excelObj->setCellValueByColumnAndRow(0, $row, ++$td);
			$excelObj->setCellValueByColumnAndRow(1, $row, $transaction_id);
			$excelObj->setCellValueByColumnAndRow(2, $row, $date);
			// $excelObj->setCellValueByColumnAndRow(3, $row, $user_fk);
			// $excelObj->setCellValueByColumnAndRow(4, $row, $origin);
			// $excelObj->setCellValueByColumnAndRow(5, $row, $destination);
			$excelObj->setCellValueByColumnAndRow(3, $row, $description);

            if($tag_fk == 1) { // cash in
                 $excelObj->setCellValueByColumnAndRow(4, $row, $amount);
                 $cash_in += $amount;
                 $total += $amount;
            } else if($tag_fk == 2) { // cash out
                 $excelObj->setCellValueByColumnAndRow(5, $row, $amount);
                 $cash_out += $amount;
                 $total -= $amount;
            } else if($tag_fk == 3 && $location == $destination && $location != $origin) { // transfer in
				$excelObj->setCellValueByColumnAndRow(6, $row, $amount);

                 $total += $amount;
                 $transfer_in += $amount;
            } else if($tag_fk == 3 && $location == $origin && $location != $destination) { // transfer out
                 $excelObj->setCellValueByColumnAndRow(7, $row, $amount);
                 $transfer_out += $amount;
                 $total -= $amount;
            }

            $excelObj->setCellValueByColumnAndRow(8, $row, $total);


            foreach(range('A','I') as $columnID) {
				$excelObj->getStyle($columnID . $row)->applyFromArray($borderStyle);
			}
			$row++;
		}
		$excelObj->getStyle('A6'.':I' . $row)->applyFromArray($borderStyleDark);
		/* write and style table body end */

		/* write and style table foot */
		$excelObj->setCellValueByColumnAndRow(4, $row, $cash_in);
		$excelObj->setCellValueByColumnAndRow(5, $row, $cash_out);
		$excelObj->setCellValueByColumnAndRow(6, $row, $transfer_in);
		$excelObj->setCellValueByColumnAndRow(7, $row, $transfer_out);
		$excelObj->setCellValueByColumnAndRow(8, $row, $total);

		foreach(range('A','I') as $columnID) {
			$excelObj->getStyle($columnID . $row)->applyFromArray($borderStyleWhite);
			$excelObj->getStyle($columnID . $row)->applyFromArray($fontStyle);
			$object->getActiveSheet()->getStyle($columnID . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00000000');
		}
		$excelObj->getStyle('A'. $row .':I' . $row)->applyFromArray($borderStyleDark);
		/* write and style table foot end */

		/* Apply styles */
		$excelObj->getStyle('A1:A' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		// $excelObj->getStyle('H1:L' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$excelObj->getStyle('E4:I' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
		// $excelObj->getStyle('A6:L' . $row)->applyFromArray($borderStyleDark);
		foreach(range('B','I') as $rowId => $columnID) {
			$object->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}
		// $excelObj->getStyle('A' . $row . ':I' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// $excelObj->getStyle('A3:A' . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		/* Apply styles end */

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
		$object_writer->save('php://output');

		return;
	}
}
