<?php
class User_model extends CI_Model {

	function getUsers($id = null) {
		$this->db->select('users.*, users.id, first_name, last_name, role_fk, branch_fk, is_enabled, roles.access_type as role_type, branches.name as branch');
		$this->db->from('users');
		$this->db->where('is_visible', 1);
		$this->db->join('roles', 'users.role_fk = roles.id');
		$this->db->join('branches', 'users.branch_fk = branches.id');
		$this->db->order_by("first_name", "asc");

		if(null !== $id) {
			$this->db->where('users.id', $id);
			return $this->db->get()->row_array();
		} else {
			return $this->db->get()->result_array();
		}
	}

	function deleteUser($id) {
		$query = $this->db->where('id', $id)->update('users', array('is_visible' => 0));
		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'User has been successfully deleted.');
		} else {
			$this->session->set_flashdata('error', 'Failed to delete user. Please try again.');
		}
		return $result;
	}

	function addUser($values) {
		$query = $this->db->insert('users', $values);
		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'New User has been successfully added.');
		} else {
			$this->session->set_flashdata('error', 'Failed to add new User. Please try again.');
		}
		return $result;
	}

	function updateUser($id, $values) {

		$this->db->where('id', $id);
		$this->db->update('users', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'User has been successfully updated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update User. Pleast try again.');
		}
		return $result;
	}

	function activateUser($id, $status) {
		$this->db->where('id', $id);
		$this->db->update('users', $status);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'User information has been successfully activated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update User information. Pleast try again.');
		}
		return $result;
	}

	function deactivateUser($id, $status) {
		$this->db->where('id', $id);
		$this->db->update('users', $status);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'User information has been successfully deactivated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update User information. Pleast try again.');
		}
		return $result;
	}

	function updatePassword($id, $newpassword) {
		$this->db->where('id', $id);
		$this->db->update('users', $newpassword);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Password has been successfully updated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update User information. Pleast try again.');
		}
		return $result;
	}

	function allowAdminOnly() {
		$role = $this->session->userdata('role_fk');

		if($role != "ADMS" && $role != "GST") {
			$this->session->set_flashdata('error', 'Session expired. Please login to continue.');
			
			header('Location: ' . base_url('Logout'));
			exit;
		}
	}
}
