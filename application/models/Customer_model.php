<?php
class Customer_model extends CI_Model {

	function getCustomers($id = null) {
		$match = $this->input->get('search');
		$search_filter = $this->input->get('filter');

		$this->db->select('customers.*, cities.name as city_name, cities.province as province');
		$this->db->from('customers');
		$this->db->where('is_deleted', 0);
		$this->db->join('cities', 'customers.city_fk = cities.id');
		$this->db->order_by("customers.first_name", "asc");

		if(null !== $match && null !== $search_filter) {
			// name, contact
			if($search_filter == 'name') {
				$this->db->like('first_name', $match);
				$this->db->or_like('last_name', $match);
				$this->db->or_like('middle_name', $match);
			}else if ($search_filter == 'contact') {
				$this->db->like('contact', $match);
			}
			$query = $this->db->get();
			$result = $query->result_array();
		} else if($id) {
			$this->db->where('customers.id', $id);
			$query = $this->db->get();
			$result = $query->row_array();
		} else {
			$query = $this->db->get();
			$result = $query->result_array();
		}

		return $result;
     }

	function addCustomer($values) {
		$query = $this->db->insert('customers', $values);
		$result = $this->db->affected_rows();

		if($result > 0) {
			$result = $this->db->insert_id();
			$this->session->set_flashdata('success', 'New customer has been successfully added.');
		} else {
			$this->session->set_flashdata('error', 'Failed to add new customer. Please try again.');
		}
		return $result;
	}

	function updateCustomer($id, $values) {

		$this->db->where('id', $id);
		$this->db->update('customers', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Customer information has been successfully updated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update customer information. Pleast try again.');
		}
		return $result;
	}

	function getTransactions($customer_id) {
		$query = $this->db->select("trans.*, type.name as type, brand.name as brand, branch.id as branch_id, branch.name as branch_name, product.name as product")->from('transactions as trans')->where('customer_fk', $customer_id)->
				join('item_type as type', 'type.id = trans.type_fk')->
				join('item_brand as brand', 'brand.id = trans.brand_fk')->
				join('item_name as product', 'product.id = trans.name_fk')->
				join('branches as branch', 'branch.id = trans.branch_fk')->
				get();
		$result = $query->result_array();

		return $result;
	}

}
