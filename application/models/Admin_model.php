<?php
class Admin_model extends CI_Model {

	public function getBranch($id = null) {
		$query = $this->db->get('branches');
		$result = $query->result_array();

		return $result;
	}

	public function getUserRole($id = null) {
		$query = $this->db->get('roles');
		$result = $query->result_array();

		return $result;
	}
}
