<?php
class Report_model extends CI_Model
{

    // function getFundReport($location, $date_start, $date_end) {
          // CODE    DATE    USER    ORIGIN  DESTINATION DESCRIPTION CASH IN CASH OUT    TRANSFER IN TRANSFER OUT    FUND BALANCE
    // }
    
    function getDateRange($date_start, $date_end) {
        $begin = new DateTime( $date_start );
        $end   = new DateTime( $date_end );
        $date_array = array();

        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            array_push($date_array, $i->format("Y-m-d"));            
        }

        return $date_array;
    }

    function getItemsOnShelf($branch_fk) {
        $pawned = $this->db
                    ->select("t.transaction_id,
                            cat.category_name as category,
                            type.name as item_type,
                            CONCAT(brand.name, ' ', name.name) as product,
                            t.description as description,
                            t.serial as serial_number,
                            DATE_FORMAT(t.date, '%d %b %Y') as entry_date,
                            t.value")
                    ->from("transactions as t")
                    ->where('t.category', 1)->where('t.branch_fk', $branch_fk)->where('t.status', 'For Sale')
                    ->join('transaction_categories as cat', 'cat.id = t.category')
                    ->join('item_name as name', 'name.id = t.name_fk')
                    ->join('item_brand as brand', 'brand.id = name.brand_fk')
                    ->join('item_type as type', 'type.id = name.type_fk')
                    ->get()->result_array();

        $purchased = $this->db
                        ->select("p.transaction_id,
                            cat.category_name as category,
                            type.name as item_type,
                            CONCAT(brand.name, ' ', name.name) as product,
                            p.description as description,
                            p.is_void as serial_number,
                            DATE_FORMAT(p.date_created, '%d %b %Y') as entry_date,                            
                            p.buy_cost as value")
                        ->from("purchases as p")
                        ->where('p.branch_fk', $branch_fk)
                        ->where('p.status', 'For Sale')
                        ->where('p.is_void', 0)
                        ->join('transaction_categories as cat', 'cat.id = 2')
                        ->join('item_name as name', 'name.id = p.name_fk')
                        ->join('item_brand as brand', 'brand.id = name.brand_fk')
                        ->join('item_type as type', 'type.id = name.type_fk')
                        ->get()->result_array();

        $shelf = array_merge($pawned, $purchased); // merge 2 arrays

        usort($shelf, 'sort_shelf'); // sort merged arrays by date
        
        // add line number
        // foreach ($shelf as $key => $row) {
        //     array_unshift($shelf[$key], $key+1);
        // }

        return $shelf;
    }

    function exportToExcel($reports, $name) {
        $sum_array = array();
        $currency_counter = array();
        if (null !== $this ->input ->post('sum_array')) {
            $sum_array = $this ->input ->post('sum_array');
        }
        if (null !== $this ->input ->post('currency_counter')) {
            $currency_counter = $this ->input ->post('currency_counter');
        }
        
        $this->load->library("excel");
        $object             = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $excelObj           = $object->getActiveSheet();
        $borderStyle        = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF999999') , ) , ) , );
        $borderStyleDark    = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FF000000') , ) , ) , );
        $borderStyleWhite   = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => 'FFFFFFFF') , ) , ) , );
        $fontStyle          = array('font' => array('bold' => true, 'color' => array('argb' => 'FFFFFFFF') ) );
        $table_header_row   = 6;
        $row                = 7;

        if (isset($_POST['date_start'])) {
            $file_name = $name . " " . $this ->input ->post('location') . " " . strtoupper(date("d M Y", strtotime($_POST['date_start']))) . " - " . strtoupper(date("d M Y", strtotime($_POST['date_end'])));
        } else {
            $file_name = $name . " " . $this ->input ->post('location');
        }

        $user_info = $this->User_model->getUsers($this->session->userdata('id'));

        $writeTitle = $excelObj->setCellValueByColumnAndRow(0, 1, "COMMUNITY GADGET PHONESHOP");
        $writeTitle = $excelObj->setCellValueByColumnAndRow(0, 2, strtoupper($file_name));
        $writeTitle = $excelObj->setCellValueByColumnAndRow(0, 3, "DATE GENERATED: " . strtoupper(date("F d, Y")));
        $writeTitle = $excelObj->setCellValueByColumnAndRow(0, 4, "GENERATED BY: " . $user_info['first_name'] . " " . $user_info['first_name']);

        /* TABLE HEADER */
        $table_columns = array();
        foreach ($reports[0] as $a => $b) {
            array_push($table_columns, $a);
        }

        foreach ($table_columns as $col => $field) {
            $excelObj->setCellValueByColumnAndRow($col, $table_header_row, " " . str_replace("_", " ", strtoupper($field)) . " ");
            $excelObj->getStyle(intToAlphabet($col) . $table_header_row)->applyFromArray($borderStyleWhite)->applyFromArray($fontStyle);
            $excelObj->getStyle(intToAlphabet($col) . $table_header_row)->getFill() ->setFillType(PHPExcel_Style_Fill::FILL_SOLID) ->getStartColor() ->setARGB('00000000');
            $excelObj->getStyle(intToAlphabet($col) . $table_header_row)->getAlignment() ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        /* TABLE HEADER end */

        /* TABLE BODY */
        foreach ($reports as $r => $report) {

            for ($i = 0;$i < sizeof($table_columns);$i++) {
                if ($table_columns[$i] == "DATE" || $table_columns[$i] == "date") {
                    $excelObj->setCellValueByColumnAndRow($i, $row, strtoupper(date("d M Y", strtotime($report[$table_columns[$i]]))));
                } else {
                    $excelObj->setCellValueByColumnAndRow($i, $row, strtoupper($report[$table_columns[$i]]));
                }

                if ($table_columns[$i] == "AMOUNT" || in_array($table_columns[$i], $sum_array)) {
                    $excelObj->getStyle(intToAlphabet($i) . $row)->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                }
            }

            // Left alignt 1st column
            $excelObj->getStyle('A' . $row)->getAlignment() ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            // add cell border
            foreach (range('A', intToAlphabet(sizeof($table_columns) - 1)) as $rowId => $columnID) {
                $object->getActiveSheet() ->getStyle($columnID . $row)->applyFromArray($borderStyle);
            }

            $row++;
        }
        /* TABLE BODY end */

        /* TABLE FOOTER */
        foreach ($table_columns as $col => $field)
        {
            $excelObj->getStyle(intToAlphabet($col) . $row)->applyFromArray($borderStyleWhite)->applyFromArray($fontStyle);
            $excelObj->getStyle(intToAlphabet($col) . $row)->getFill() ->setFillType(PHPExcel_Style_Fill::FILL_SOLID) ->getStartColor() ->setARGB('00000000');
            $excelObj->getStyle(intToAlphabet($col) . $row)->getAlignment() ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $excelObj->getStyle(intToAlphabet($col) . $row)->getNumberFormat() ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $row_body_end = $row - 1;
            if ($field == "AMOUNT" || in_array($field, $sum_array)) {                
                $excelObj->setCellValue(intToAlphabet($col) . $row, '=SUM(' . intToAlphabet($col) . '7:' . intToAlphabet($col) . $row_body_end . ')');
            } else if(in_array($field, $currency_counter)) {
                $excelObj->setCellValue(intToAlphabet($col) . $row, '=SUM(' . intToAlphabet($col) . $row_body_end . ')');
            }
        }

        /* TABLE FOOTER end */

        // auto width
        foreach (range('B', intToAlphabet(sizeof($table_columns) - 1)) as $rowId => $columnID) {
            $object->getActiveSheet() ->getColumnDimension($columnID)->setAutoSize(true);
        }

        // table outside border
        $object->getActiveSheet() ->getStyle('A6:' . intToAlphabet(sizeof($table_columns) - 1) . $row)->applyFromArray($borderStyleDark);

        /* WRITE */
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . strtoupper($file_name) . '.xls"');
        $object_writer->save('php://output');
        /* WRITE end */

        $object_writer->disconnectWorksheets();
        $object_writer->garbageCollect();
        unset($object_writer);

        return;
    }

    function getGeneralReport_accessory($date_end, $branch_fk, $count = false) {
        $registered_items = $this->db->select_sum('buy')
                            ->from("retails")
                            ->where('branch_fk', $branch_fk)->where('date <=', $date_end) 
                            ->get()->row_array()['buy'];
        
        $registered_ctr = $this->db->get_where('retails', array('branch_fk' => $branch_fk, 'date <=' => $date_end))->num_rows();

        $sales = $this->db->select("s.reference_id, s.transaction_id, s.selling_price, s.quantity, s.sales_id")->from('transactions as t')
                    ->where('t.category', 3)
                    ->where('t.status', 'Sold')                    
                    ->where('t.date <=', $date_end)
                    ->where('t.branch_fk', $branch_fk)
                    ->where('s.is_void', 0)
                    ->join('sales as s', 's.reference_id = t.transaction_id')
                    ->get()->result_array();
        
        $total_sales_cost = 0;
        $total_sales_qty = 0;
        $total_returned_item_qty = 0;
        $total_returned_item_cost = 0;

        foreach ($sales as $s => $sale) {
            $sales_id = $sale['sales_id'];
            $sales_qty = $sale['quantity'];
            $sales_transaction_id = $sale['transaction_id'];
            $sales_reference_id = $sale['reference_id'];
            $sales_cost = $this->db->select("buy")->from("retails")->where('registration_id', $sales_transaction_id)->get()->row_array()['buy'];
            $total_sales_qty += $sales_qty;
            $total_sales_cost += $sales_cost * $sales_qty;
            
            $returned_items = $this->db->select("sales_id, quantity")->from("retail_returns")
                                ->where("transaction_id", $sales_reference_id)
                                ->where("date <=", $date_end)
                                ->where("sales_id", $sales_id)
                                ->get()->result_array();

            if(null !== $returned_items) {
                foreach ($returned_items as $rt => $returned) {
                    $return_sales_id = $returned['sales_id'];
                    $returned_qty = $returned['quantity'];
                    
                    $registration_id = $this->db->select("transaction_id")->from("sales")
                                        ->where('sales_id', $return_sales_id)
                                        ->get()->row_array()['transaction_id'];                    

                    $returned_item_cost = $this->db->select("buy")->from("retails")
                                        ->where('registration_id', $registration_id)
                                        ->get()->row_array()['buy'];

                    $total_returned_item_qty += $returned_qty;
                    $total_returned_item_cost += $returned_item_cost * $returned_qty;
                }
            }
        }

        if($count) {
            $transferred_ctr = $this->db->select("SUM(s.quantity) as transferred")->from("shipments as s")
                                    ->where('s.origin', $branch_fk)
                                    ->where('s.is_received', 1)
                                    ->where('s.date <=', $date_end)
                                    ->get()->row_array()['transferred'];

            $disposed_ctr = $this->db->select('buy')->from('retails')
                                    ->where('date_modified <=', $date_end)
                                    ->where('status', 'Disposed')
                                    ->where('branch_fk', $branch_fk)
                                    ->get()->num_rows();;

            $accessory = $registered_ctr + $total_returned_item_qty - $total_sales_qty - $transferred_ctr - $disposed_ctr;
        } else {
            $transferred_items = $this->db->select("SUM(s.quantity * s.buy) as transferred")->from("shipments as s")
                                    ->where('s.origin', $branch_fk)
                                    ->where('s.is_received', 1)
                                    ->where('s.date <=', $date_end)
                                    ->get()->row_array()['transferred'];

            $disposed_items = $this->db->select_sum('buy')->from('retails')
                                    ->where('date_modified <=', $date_end)
                                    ->where('status', 'Disposed')
                                    ->where('branch_fk', $branch_fk)
                                    ->get()->row_array()['buy'];

            $accessory = $registered_items + $total_returned_item_cost - $total_sales_cost - $transferred_items - $disposed_items;    
        }
        
        return $accessory;
    }

     function getGeneralReport_vault($date_end, $branch_fk) {
        return $this->db->select_sum("value")
            ->from("transactions")
            ->where('category', 1)
            ->where('branch_fk', $branch_fk)
            ->where('date <=', $date_end)
            ->group_start()
                ->or_where('status', 'In Contract')
                ->or_where('status', 'Grace Period')
                ->or_where('status', 'Due')
                ->or_where('status', 'Expired')
                ->or_where('status', 'Freeze')
                ->or_where('status', 'For Transfer')
                ->or_where('status', 'For Disposal')
            ->group_end()
            ->get()
            ->row_array() ['value'];
    }

    function getGeneralReport_vaultQty($date_end, $branch_fk) {
        return $this->db->select("id")
            ->from("transactions")
            ->where('category', 1)
            ->where('branch_fk', $branch_fk)
            ->where('date <=', $date_end)
            ->group_start()
                ->or_where('status', 'In Contract')
                ->or_where('status', 'Grace Period')
                ->or_where('status', 'Due')
                ->or_where('status', 'Expired')
                ->or_where('status', 'Freeze')
                ->or_where('status', 'For Transfer')
                ->or_where('status', 'For Disposal')
            ->group_end()
            ->get()
            ->num_rows();
    }

    function getGeneralReport_shelf($date_end, $branch_fk) {
        $shelf_t = $this->db->select_sum("value")
            ->from("transactions")
            ->where('category', 1)
            ->where('branch_fk', $branch_fk)
            ->where('status', 'For Sale')
            ->where('date <=', $date_end)
            ->get()
            ->row_array() ['value'];

        $shelf_p = $this->db->select_sum("buy_cost")
            ->from("purchases")
            ->where('branch_fk', $branch_fk)->where('date_created <=', $date_end)
            ->group_start()
                ->or_where('status', 'For Sale')
                ->or_where('status', 'For Transfer')
                ->or_where('status', 'For Disposal')
            ->group_end()
            ->get()
            ->row_array() ['buy_cost'];

        return $shelf_t + $shelf_p;
    }

    function getGeneralReport_shelfQty($date_end, $branch_fk) {
        $shelf_t_qty = $this->db->select("id")
            ->from("transactions")
            ->where('category', 1)
            ->where('branch_fk', $branch_fk)
            ->where('date <=', $date_end)
            ->where('status', 'For Sale')
            ->get()
            ->num_rows();

        $shelf_p_qty = $this->db->select("id")
            ->from("purchases")
            ->where('branch_fk', $branch_fk)
            ->where('date_created <=', $date_end)
            ->group_start()
                ->or_where('status', 'For Sale')
                ->or_where('status', 'For Repair')
                ->or_where('status', 'For Disposal')
                ->or_where('status', 'For Transafer')
            ->group_end()
            ->get()
            ->num_rows();

        return $shelf_t_qty + $shelf_p_qty;
    }

    function getGeneralReport_accessoryQty($date_end, $branch_fk) {
        $accessories = array();
        $accessories['registered_qty'] = intval($this->db->select_sum('quantity')
            ->from('retails')
            ->where('branch_fk', $branch_fk)            
            ->where('date <=', $date_end)
            ->get()
            ->row_array() ['quantity']);

        $accessories['disposed_qty'] = - abs(intval($this->db->select_sum('quantity')
            ->from('retails')
            ->where('branch_fk', $branch_fk)->where('status', "Disposed")             
            ->where('date <=', $date_end)
            ->get()
            ->row_array() ['quantity']));

        $accessories['shipped_qty'] = - abs(intval($this->db->select_sum('quantity')
            ->from('shipments')
            ->where('origin', $branch_fk)->where('is_received', 1)            
            ->where('date <=', $date_end)
            ->get()
            ->row_array() ['quantity']));

        $accessories['sales_qty'] = - abs(intval($this->db->select_sum('s.quantity')
            ->from('transactions as t')
            ->where('t.category', 3)
            ->where('t.status', "Sold")
            ->where('t.branch_fk', $branch_fk)->where('s.is_void', 0)            
            ->where('s.date_sold <=', $date_end)
            ->join('sales as s', 's.reference_id = t.transaction_id')
            ->get()
            ->row_array() ['quantity']));

        $accessories['refund_qty'] = $this->db->select_sum('rr.quantity')
            ->from('transactions as t')
            ->where('t.category', 3)
            ->where('t.status', "Sold")
            ->where('t.branch_fk', $branch_fk)->where('s.is_void', 0)
            ->where('rr.action', 'Cash Refund')            
            ->where('rr.date <=', $date_end)
            ->join('sales as s', 's.reference_id = t.transaction_id')
            ->join('retail_returns as rr', 'rr.transaction_id = s.reference_id')
            ->get()
            ->row_array() ['quantity'];

        return array_sum($accessories);
    }

    function getGeneralReport_cashin($date_end, $branch_fk) {
        $cash_in = array();
        $cash_in['deposits'] = $this->db->select_sum("amount")
            ->from("funds")
            ->where('date', $date_end)->where('destination', $branch_fk)->where('tag_fk', 1)
            ->get()
            ->row_array() ['amount'];

        $cash_in['transfer_in'] = $this->db->select_sum("amount")
            ->from("funds")
            ->where('date', $date_end)->where('origin !=', $branch_fk)->where('destination', $branch_fk)->where('tag_fk', 3)
            ->get()
            ->row_array() ['amount'];

        $cash_in['transaction_fee'] = $this->db->select_sum("transaction_fee")
            ->from("transaction_payments")
            ->where('date', $date_end)->where('branch_fk', $branch_fk)->get()
            ->row_array() ['transaction_fee'];

        $cash_in['penalty_charge'] = $this->db->select_sum("penalty_charge")
            ->from("transaction_payments")
            ->where('date', $date_end)->where('branch_fk', $branch_fk)->get()
            ->row_array() ['penalty_charge'];

        $cash_in['transaction_cashout'] = $this->db->select_sum("tc.value")
            ->from("transaction_payments as tp")            
            ->where('tp.date', $date_end)->where('tp.branch_fk', $branch_fk)
            ->where('tp.status', 'Repurchased')
            ->join('transaction_cashouts as tc', 'tc.transaction_id = tp.transaction_id')
            ->get()
            ->row_array() ['value'];

        $sales = $this->db->select("selling_price, quantity")
            ->from("sales")
            ->where('date_sold', $date_end)->where('branch_fk', $branch_fk)->where('is_void', 0)
            ->get()
            ->result_array();

        $cash_in['sales'] = 0;
        foreach ($sales as $s => $sale) {
            $cash_in['sales'] += $sale['selling_price'] * $sale['quantity'];
        }


        return array_sum($cash_in);
    }

    function getGeneralReport_cashout($date_end, $branch_fk) {
        $cash_out = array();

        $cash_out['withdrawals'] = $this->db->select_sum("amount")
            ->from("funds")
            ->where('date', $date_end)->where('destination', $branch_fk)->where('tag_fk', 2)
            ->get()
            ->row_array() ['amount'];

        $cash_out['transfer_out'] = $this->db->select_sum("amount")
            ->from("funds")
            ->where('date', $date_end)->where('origin', $branch_fk)->where('destination !=', $branch_fk)->where('tag_fk', 3)
            ->get()
            ->row_array() ['amount'];

        $cash_out['transaction_cashouts'] = $this->db->select_sum("value")
            ->from("transaction_cashouts")
            ->where('date', $date_end)->where('branch_fk', $branch_fk)->where('is_void', 0)
            ->get()
            ->row_array() ['value'];

        $cash_out['refund'] = $this->db->select_sum("amount")
            ->from("transaction_returns")
            ->where('date', $date_end)->where('branch_fk', $branch_fk)->where('is_void', 0)
            ->get()
            ->row_array() ['amount'];

        $cash_out['expenses'] = $this->db->select_sum("amount")
            ->from("expenses")
            ->where('date', $date_end)->where('branch_fk', $branch_fk)->where('is_void', 0)
            ->get()
            ->row_array() ['amount'];

        $cash_out['retail_register'] = $this->db->select_sum("buy")
            ->from("retails")
            ->where('date', $date_end)->where('branch_fk', $branch_fk)
            ->get()
            ->row_array() ['buy'];

        $cash_out['retail_returns'] = $this->db->select_sum("amount")
            ->from("retail_returns")
            ->where('date', $date_end)->where('branch_fk', $branch_fk)
            ->get()
            ->row_array() ['amount'];

        return array_sum($cash_out);
    }

    function getGeneralReport_cashBegin($date_end, $branch_fk) {
        $cash_in = array();

        $cash_in['deposits'] = $this->db->select_sum("amount")
            ->from("funds")
            ->where('date <', $date_end)->where('destination', $branch_fk)->where('tag_fk', 1)
            ->get()
            ->row_array() ['amount'];

        $cash_in['transfer_in'] = $this->db->select_sum("amount")
            ->from("funds")
            ->where('date <', $date_end)->where('origin !=', $branch_fk)->where('destination', $branch_fk)->where('tag_fk', 3)
            ->get()
            ->row_array() ['amount'];

        $cash_in['transaction_fee'] = $this->db->select_sum("transaction_fee")
            ->from("transaction_payments")
            ->where('date <', $date_end)->where('branch_fk', $branch_fk)->get()
            ->row_array() ['transaction_fee'];

        $cash_in['penalty_charge'] = $this->db->select_sum("penalty_charge")
            ->from("transaction_payments")
            ->where('date <', $date_end)->where('branch_fk', $branch_fk)->get()
            ->row_array() ['penalty_charge'];

        $cash_in['repurchase'] = $this->db->select_sum("tc.value")
            ->from("transaction_payments as tp")
            ->where('tp.date <', $date_end)->where('tp.branch_fk', $branch_fk)
            ->where('tp.status', 'Repurchased')
            ->join('transaction_cashouts as tc', 'tc.transaction_id = tp.transaction_id')
            ->get()
            ->row_array() ['value'];

        $sales = $this->db->select("selling_price, quantity")
            ->from("sales")
            ->where('date_sold <', $date_end)->where('branch_fk', $branch_fk)->where('is_void', 0)
            ->get()
            ->result_array();

        $cash_in['sales'] = 0;
        foreach ($sales as $s => $sale) {
            $cash_in['sales'] += $sale['selling_price'] * $sale['quantity'];
        }

        $cash_out = array();

        $cash_out['withdrawals'] = $this->db->select_sum("amount")
            ->from("funds")
            ->where('date <', $date_end)->where('destination', $branch_fk)->where('tag_fk', 2)
            ->get()
            ->row_array() ['amount'];

        $cash_out['transfer_out'] = $this->db->select_sum("amount")
            ->from("funds")
            ->where('date <', $date_end)->where('origin', $branch_fk)->where('destination !=', $branch_fk)->where('tag_fk', 3)
            ->get()
            ->row_array() ['amount'];

        $cash_out['transaction_cashouts'] = $this->db->select_sum("value")
            ->from("transaction_cashouts")
            ->where('date <', $date_end)->where('branch_fk', $branch_fk)->where('is_void', 0)
            ->get()
            ->row_array() ['value'];

        $cash_out['refund'] = $this->db->select_sum("amount")
            ->from("transaction_returns")
            ->where('date <', $date_end)->where('branch_fk', $branch_fk)->where('is_void', 0)
            ->get()
            ->row_array() ['amount'];

        $cash_out['expenses'] = $this->db->select_sum("amount")
            ->from("expenses")
            ->where('date <', $date_end)->where('branch_fk', $branch_fk)->where('is_void', 0)
            ->get()
            ->row_array() ['amount'];


        $cash_out['retail_registration'] = $this->db->select_sum("buy")
            ->from("retails")
            ->where('date <', $date_end)->where('branch_fk', $branch_fk)
            ->get()
            ->row_array() ['buy'];

        $cash_out['retail_returns'] = $this->db->select_sum("amount")
            ->from("retail_returns")
            ->where('date <', $date_end)->where('branch_fk', $branch_fk)
            ->get()
            ->row_array() ['amount'];

        return array_sum($cash_in) - array_sum($cash_out);

    }

    function getGeneralReport($date_end) {
        $this->db->select("b.id as branch_id, b.name as branch_name");
        $this->db->from("branches as b");

        $query = $this->db->get();

        $result = $query->result_array();

        foreach ($result as $i => $r) {
            $branch_fk = $r['branch_id'];
            $result[$i]['#'] = $i + 1;
            $result[$i]['branch'] = $r['branch_name'] . " (" . $branch_fk . ")";
            $result[$i]['vault'] = $this->getGeneralReport_vault($date_end, $branch_fk);
            $result[$i]['shelf'] = $this->getGeneralReport_shelf($date_end, $branch_fk);
            $result[$i]['accessory'] = $this->getGeneralReport_accessory($date_end, $branch_fk);
            $result[$i]['vault_qty'] = $this->getGeneralReport_vaultQty($date_end, $branch_fk);
            $result[$i]['shelf_qty'] = $this->getGeneralReport_shelfQty($date_end, $branch_fk);
            $result[$i]['accessory_qty'] = $this->getGeneralReport_accessory($date_end, $branch_fk, true);            
            $result[$i]['cash_begin'] = $this->getGeneralReport_cashBegin($date_end, $branch_fk);
            $result[$i]['cash_in'] = $this->getGeneralReport_cashin($date_end, $branch_fk);
            $result[$i]['cash_out'] = $this->getGeneralReport_cashout($date_end, $branch_fk);
            $result[$i]['cash_end'] = $result[$i]['cash_begin'] + $result[$i]['cash_in'] - $result[$i]['cash_out'];
            $result[$i]['actual_cash'] = 0;
            $result[$i]['cash_difference'] = $result[$i]['cash_end'] - $result[$i]['actual_cash'];
            $result[$i]['total_item_amount'] = $result[$i]['vault'] + $result[$i]['shelf'] + $result[$i]['accessory'];
            $result[$i]['total_qty'] = $result[$i]['vault_qty'] + $result[$i]['shelf_qty'] + $result[$i]['accessory_qty'];
            $result[$i]['grand_total'] = $result[$i]['vault'] + $result[$i]['shelf'] + $result[$i]['accessory'] + $result[$i]['cash_end'];

            unset($result[$i]['branch_id']);
            unset($result[$i]['branch_name']);
        }

        return $result;
    }
    function getFirstFund($branch_fk = null) {        
        if(null !== $branch_fk) {
            $result = $this->db->order_by('date', 'ASC')->get_where('funds', array('destination' => $branch_fk))->row_array()['date'];    
        } else {
            $result = $this->db->order_by('date', 'ASC')->get('funds')->row_array()['date'];    
        }
        
        if(!isset($result)) {
            $result = "2019-01-01";
        } 
        
        return $result;
    }

    function getDailyGeneralReport($date_start, $date_end, $branch_fk) {
        // $date_start = $this->getFirstFund();
        $begin = new DateTime($date_start);
        $end = new DateTime($date_end);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end->modify('+1 day'));
                
        $report = array();
        foreach ($period as $i => $date) {
            $date_f         = $date->format("Y-m-d");
            $vault          = $this->getGeneralReport_vault($date_f, $branch_fk);
            $shelf          = $this->getGeneralReport_shelf($date_f, $branch_fk);
            $accessory      = $this->getGeneralReport_accessory($date_f, $branch_fk);
            $vault_qty      = $this->getGeneralReport_vaultQty($date_f, $branch_fk);
            $shelf_qty      = $this->getGeneralReport_shelfQty($date_f, $branch_fk);
            $accessory_qty  = $this->getGeneralReport_accessory($date_f, $branch_fk, true);
            $cash_begin     = $this->getGeneralReport_cashBegin($date_f, $branch_fk);
            $cash_in        = $this->getGeneralReport_cashin($date_f, $branch_fk);
            $cash_out       = $this->getGeneralReport_cashout($date_f, $branch_fk);
            $actual_cash    = 0;
            $cash_end       = $cash_begin + $cash_in - $cash_out;

            $row = array(
                '#'                 => $i + 1,
                'date'              => $date_f,
                'vault'             => $vault,
                'shelf'             => $shelf,
                'accessory'         => $accessory,
                'vault_qty'         => $this->getGeneralReport_vaultQty($date_f, $branch_fk),
                'shelf_qty'         => $this->getGeneralReport_shelfQty($date_f, $branch_fk),
                'accessory_qty'     => $this->getGeneralReport_accessory($date_f, $branch_fk, true),            
                'cash_begin'        => $cash_begin,
                'cash_in'           => $cash_in,
                'cash_out'          => $cash_out,
                'cash_end'          => $cash_end,
                'actual_cash'       => $actual_cash,
                'cash_difference'   => $cash_end - $actual_cash,
                'total_item_amount' => $vault + $shelf + $accessory,
                'total_qty'         => $vault_qty + $shelf_qty + $accessory_qty,
                'grand_total'       => $vault + $shelf + $accessory + $cash_end
            );
            array_push($report, $row);
        }

        return $report;
    }

    function getJournalReportSummary($date_start, $date_end) {        
        $branches = $this->Generic_model->getBranches();
        $journal_summary = array();
        foreach ($branches as $b => $branch) {
            $journal =  $this->getJournalReport($branch['id'], $date_start, $date_end);
            $cash_in = array();
            $cash_out = array();
            foreach ($journal as $j => $jour) {
                array_push($cash_in, $jour['cash_in']);
                array_push($cash_out, $jour['cash_out']);
            }
            $summary = array(        
                '#' => ++$b,
                'location' => strtoupper($branch['name'] . ' (' . $branch['id'] . ')'),
                'date_range' => strtoupper( date('d M Y', strtotime($date_start)) . " - " . date('d M Y', strtotime($date_end)) ),
                'cash_in' => array_sum($cash_in),
                'cash_out' => array_sum($cash_out),
                'difference' => array_sum($cash_in) - array_sum($cash_out)
            );

            array_push($journal_summary, $summary);
        }

        return $journal_summary;
    }

    function getJournalReport($branch_fk, $date_start, $date_end) {    
        /* pawned items */
        $pawns = $this->db->select("'#' as '#', t.date, t.transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name, ' ', t.description) as particulars, CONCAT('Purchase') as description, tc.is_void as cash_in, tc.value as cash_out")
        		->from('transactions as t')
                ->where('t.date >=', $date_start)
                ->where('t.date <=', $date_end)
                ->where('tc.branch_fk', $branch_fk)
                ->where('t.category', 1)
                ->where('tc.is_void', 0)
                ->order_by('t.date')
                ->order_by('t.id')
                ->join('item_type as it', 'it.id = t.type_fk')
                ->join('item_brand as ib', 'ib.id = t.brand_fk')
                ->join('item_name as in', 'in.id = t.name_fk')
                ->join('transaction_cashouts as tc', 'tc.transaction_id = t.transaction_id')
                ->get()->result_array();

        $renew = $this->db->select("'#' as '#', tp.date, tp.transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name, ' ', t.description) as particulars, tp.status as description, CONCAT(tp.transaction_fee + tp.penalty_charge) as cash_in, CONCAT('0') as cash_out")
    			->from('transaction_payments as tp')
    			->where('tp.date >=', $date_start)
            	->where('tp.date <=', $date_end)
            	->where('tp.branch_fk', $branch_fk)
                ->where('tp.status', 'Renew')
            	->where('t.status !=', 'Void')
            	->order_by('tp.date')
            	->order_by('tp.id')
    			->join('transactions as t', 't.transaction_id = tp.transaction_id')
    			->join('item_type as it', 'it.id = t.type_fk')
            	->join('item_brand as ib', 'ib.id = t.brand_fk')
            	->join('item_name as in', 'in.id = t.name_fk')
            	->get()->result_array();

        $repurchased = $this->db->select("'#' as '#', tp.date, tp.transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name, ' ', t.description) as particulars, tp.status as description, CONCAT(tp.transaction_fee + tp.penalty_charge + t.value) as cash_in, CONCAT('0') as cash_out")
                ->from('transaction_payments as tp')
                ->where('tp.date >=', $date_start)
                ->where('tp.date <=', $date_end)
                ->where('tp.branch_fk', $branch_fk)
                ->where('tp.status', 'Repurchased')
                ->where('t.status !=', 'Void')
                ->order_by('tp.date')
                ->order_by('tp.id')
                ->join('transactions as t', 't.transaction_id = tp.transaction_id')
                ->join('item_type as it', 'it.id = t.type_fk')
                ->join('item_brand as ib', 'ib.id = t.brand_fk')
                ->join('item_name as in', 'in.id = t.name_fk')
                ->get()->result_array();

        $pawn_sold = $this->db->select("'#' as '#', s.date_sold as date, t.transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name, ' ', t.description) as particulars, CONCAT('Sold') as description, s.selling_price as cash_in, CONCAT('0') as cash_out")
                ->from('transactions as t')
                ->where('s.date_sold >=', $date_start)
                ->where('s.date_sold <=', $date_end)
                ->where('s.is_void !=', 1)
                ->where('t.branch_fk', $branch_fk)
                ->where('t.category !=', 3)
                // ->where('t.status', 'Sold')
                ->order_by('s.date_sold')
                ->order_by('s.sales_id')
                ->join('item_type as it', 'it.id = t.type_fk')
                ->join('item_brand as ib', 'ib.id = t.brand_fk')
                ->join('item_name as in', 'in.id = t.name_fk')
                ->join('sales as s', 's.reference_id = t.transaction_id')
                ->get()->result_array();

        $pawn_return = $this->db->select("'#' as '#', tr.date as date, t.transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name, ' ', t.description) as particulars, CONCAT('Cash Refund') as description, CONCAT('0') as cash_in, tr.amount as cash_out")
                ->from('transactions as t')
                ->where('tr.date >=', $date_start)->where('tr.date <=', $date_end)
                ->where('tr.is_void', 0)
                ->where('t.branch_fk', $branch_fk)
                ->where('t.category !=', 3)
                // ->where('t.status', 'Sold')
                ->order_by('tr.date')->order_by('tr.id')
                ->join('item_type as it', 'it.id = t.type_fk')->join('item_brand as ib', 'ib.id = t.brand_fk')->join('item_name as in', 'in.id = t.name_fk')
                ->join('transaction_returns as tr', 'tr.transaction_id = t.transaction_id')
                ->get()->result_array();
        /* pawned items end */

        /* funds */
        $cash_transfer_in = $this->db->select("'#' as '#', date, transaction_id, CONCAT('Funds') as type, description as particulars, CONCAT('Cash Transfer From ', origin) as description, amount as cash_in, CONCAT('0') as cash_out")
                            ->from('funds')
                            ->where('date >=', $date_start)->where('date <=', $date_end)  
                            ->where('tag_fk', 3)
                            // ->where('origin !=', $branch_fk)
                            ->where('destination', $branch_fk)
                            ->get()->result_array();

        $cash_transfer_out = $this->db->select("'#' as '#', date, transaction_id, CONCAT('Fund') as type, description as particulars, CONCAT('Cash Transfer To ', destination) as description, CONCAT('0') as cash_in, amount as cash_out")
                            ->from('funds')
                            ->where('date >=', $date_start)->where('date <=', $date_end)  
                            ->where('tag_fk', 3)
                            ->where('origin', $branch_fk)
                            // ->where('destination !=', $branch_fk)
                            ->get()->result_array();

        $cash_deposit = $this->db->select("'#' as '#', date, transaction_id, CONCAT('Fund') as type, description as particulars, CONCAT('Cash Deposit') as description, amount as cash_in, CONCAT('0') as cash_out")
                            ->from('funds')
                            ->where('date >=', $date_start)->where('date <=', $date_end)  
                            ->where('tag_fk', 1)
                            ->where('origin', $branch_fk)->where('destination', $branch_fk)
                            ->get()->result_array();

        $cash_withdrawal = $this->db->select("'#' as '#', date, transaction_id, CONCAT('Fund') as type, description as particulars, CONCAT('Cash withdrawal') as description, amount as cash_in, CONCAT('0') as cash_out")
                            ->from('funds')
                            ->where('date >=', $date_start)->where('date <=', $date_end)  
                            ->where('tag_fk', 2)
                            ->where('origin', $branch_fk)->where('destination', $branch_fk)
                            ->get()->result_array();

        /* funds end */

        /* expenses */
        $expense = $this->db->select("'#' as '#', e.date, CONCAT(e.branch_fk,'E', DATE_FORMAT(e.date, '%y%m%d'),e.id) as transaction_id, CONCAT('Expenses') as type, e.description as particulars, et.type as description, CONCAT('0') as cash_in, e.amount as cash_out")
                    ->from('expenses as e')
                    ->where('e.date >=', $date_start)->where('e.date <=', $date_end)
                    ->where('e.branch_fk', $branch_fk)
                    ->where('e.is_void', 0)
                    ->join('expense_type as et', 'et.id = e.type_fk')
                    ->get()->result_array();
        /* expenses end */

        /* purchase */
        $purchase = $this->db->select("'#' as '#', p.date_created as date, p.transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name, ' ', p.description) as particulars, CONCAT('Purchase') as description, tc.is_void as cash_in, tc.value as cash_out")
                ->from('purchases as p')
                ->where('p.date_created >=', $date_start)
                ->where('p.date_created <=', $date_end)
                ->where('tc.branch_fk', $branch_fk)
                ->where('p.is_void', 0)
                // ->where('tc.is_void', 0)
                ->order_by('p.date_created')
                ->order_by('p.id')
                ->join('item_type as it', 'it.id = p.type_fk')
                ->join('item_brand as ib', 'ib.id = p.brand_fk')
                ->join('item_name as in', 'in.id = p.name_fk')
                ->join('transaction_cashouts as tc', 'tc.transaction_id = p.transaction_id')
                ->get()->result_array();

        // $purchase_sold = $this->db->select("'#' as '#', s.date_sold as date, p.transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name, ' ', p.description) as particulars, CONCAT('Sold purchased item') as description, s.selling_price as cash_in, CONCAT('0') as cash_out")
        //         ->from('transactions as t')
        //         ->where('s.date_sold >=', $date_start)
        //         ->where('s.date_sold <=', $date_end)
        //         ->where('s.is_void !=', 1)
        //         ->where('t.branch_fk', $branch_fk)
        //         ->where('t.category', 1)
        //         // ->where('t.status', 'Sold')
        //         ->order_by('s.date_sold')
        //         ->order_by('s.sales_id')
        //         ->join('item_type as it', 'it.id = t.type_fk')
        //         ->join('item_brand as ib', 'ib.id = t.brand_fk')
        //         ->join('item_name as in', 'in.id = t.name_fk')
        //         ->join('sales as s', 's.reference_id = t.transaction_id')

        /* purchase end */

        /* retail */
        $retail_registration = $this->db->select("'#' as '#', r.date, r.registration_id as transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name) as particulars, CONCAT('PURCHASED ', SUM(r.quantity), ' PC(S) @ ', r.buy, ' per ', iu.unit) as description, CONCAT('0') as cash_in, SUM(r.buy) as cash_out")
                    ->from('retails as r')
                    ->where('r.date >=', $date_start)->where('r.date <=', $date_end)
                    ->where('r.branch_fk', $branch_fk)
                    ->join('item_type as it', 'it.id = r.type_fk')
                    ->join('item_brand as ib', 'ib.id = r.brand_fk')
                    ->join('item_name as in', 'in.id = r.name_fk')
                    ->join('item_unit as iu', 'iu.id = r.unit_fk')
                    ->group_by('r.registration_id')
                    ->order_by('r.date', 'ASC')->order_by('r.pk', 'ASC')
                    ->get()->result_array();

        $retail_sales = $this->db->select("'#' as '#', t.date, t.transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name) as particulars, CONCAT('SOLD ', s.quantity, ' PC(S) @ ', s.selling_price, ' per ', iu.unit) as description, CONCAT(s.selling_price * s.quantity) as cash_in, CONCAT('0') as cash_out")
                    ->from('transactions as t')
                    ->where('t.status', 'Sold')
                    ->where('t.category', 3)
                    ->where('s.is_void', 0)
                    ->where('t.branch_fk', $branch_fk)
                    ->join('sales as s', 's.reference_id = t.transaction_id')
                    ->join('retails as r', 'r.registration_id = s.transaction_id')
                    ->join('item_type as it', 'it.id = r.type_fk')
                    ->join('item_brand as ib', 'ib.id = r.brand_fk')
                    ->join('item_name as in', 'in.id = r.name_fk')
                    ->join('item_unit as iu', 'iu.id = r.unit_fk')
                    ->group_by('s.sales_id')
                    ->order_by('t.date', 'ASC')->order_by('s.sales_id', 'ASC')
                    ->get()->result_array();

        $retail_return = $this->db->distinct()->select("'#' as '#', rr.date, rr.transaction_id, it.name as type, CONCAT(ib.name, ' ', in.name) as particulars, CONCAT(rr.action, ' ', rr.quantity, ' ', iu.unit) as description, CONCAT('0') as cash_in, rr.amount as cash_out")
                    ->from('retail_returns as rr')
                    ->where('rr.branch_fk', $branch_fk)
                    ->join('sales as s', 's.reference_id = rr.transaction_id')
                    ->join('retails as r', 'r.registration_id = s.transaction_id')
                    ->join('item_type as it', 'it.id = r.type_fk')
                    ->join('item_brand as ib', 'ib.id = r.brand_fk')
                    ->join('item_name as in', 'in.id = r.name_fk')
                    ->join('item_unit as iu', 'iu.id = r.unit_fk')
                    // ->group_by('s.sales_id')
                    ->order_by('rr.date', 'ASC')->order_by('s.sales_id', 'ASC')
                    ->get()->result_array();

        /* retail end */


        $journal = array_merge($pawns, $renew, $repurchased, $pawn_sold, $pawn_return, $cash_transfer_in, $cash_transfer_out, $cash_deposit, $cash_withdrawal, $expense, $purchase, $retail_registration, $retail_sales, $retail_return);
        
        usort($journal, 'sort_journal');

        // add line number
        $balance = 0;
        foreach ($journal as $j => $jour) {
            $difference = $jour['cash_in'] - $jour['cash_out'];
            $balance += $difference;
            $journal[$j]['#'] = $j + 1;
            $journal[$j]['difference'] = $difference;
            $journal[$j]['balance'] = $balance;
        }

        return $journal;        
    }

    function getRetailSalesRecord($location, $date_start, $date_end) {

        $transactions = $this->db->select('transaction_id, branch_fk, user_fk, date')
            ->from('transactions')
            ->where('branch_fk', $location)->where('date >=', $date_start)->where('date <=', $date_end)->where('is_void', 0)
            ->where('category', 3)
            ->order_by('date')
            ->get()
            ->result_array();

        foreach ($transactions as $t => $trans)
        {
            $sales = $this
                ->db
                ->select('selling_price, quantity')
                ->from('sales')
                ->where('reference_id', $trans['transaction_id'])->where('is_void', 0)
                ->get()
                ->result_array();
            $total_sales = 0;
            foreach ($sales as $s => $sale)
            {
                $total_sales += $sale['selling_price'] * $sale['quantity'];
            }

            $transactions[$t]['sales'] = $total_sales;
            $transactions[$t]['refund'] = $this
                ->db
                ->select_sum('amount')
                ->from('retail_returns')
                ->where('transaction_id', $trans['transaction_id'])->get()
                ->row_array() ['amount'];
        }
        return $transactions;
    }

    function getRetailSalesTransaction($location, $date_start, $date_end) {
        $report = array();
        $transactions = $this->db->select('t.transaction_id, t.branch_fk, t.user_fk, t.date, s.selling_price, s.quantity, type.name as type, brand.name as brand, name.name, unit, s.sales_id')
            ->from('transactions as t')
            ->where('t.branch_fk', $location)->where('t.date >=', $date_start)->where('t.date <=', $date_end)->where('t.is_void', 0)
            ->where('t.category', 3)
            ->join('sales as s', 's.reference_id = t.transaction_id')
            ->join('retails as r', 'r.registration_id = s.transaction_id')
            ->join('item_type as type', 'type.id = r.type_fk')
            ->join('item_brand as brand', 'brand.id = r.brand_fk')
            ->join('item_name as name', 'name.id = r.name_fk')
            ->join('item_unit as unit', 'unit.id = r.unit_fk')
            ->order_by('t.date')
            ->get()
            ->result_array();

        foreach ($transactions as $t => $trans)
        {
            $returns = $this
                ->db
                ->where('sales_id', $trans['sales_id'])->get('retail_returns')
                ->result_array();
            if (null !== $returns)
            {
                foreach ($returns as $r => $ret)
                {
                    $trans['returned_quantity'] = $ret['quantity'];
                    $trans['refund_amount'] = $ret['amount'];
                }
            }
            array_push($report, $trans);
        }
        return $report;
    }

    function getSales($transactionId) {
        $this->db->select("*");
        $this->db->from("sales");
        $this->db->where('reference_id', $transactionId);
        $this->db->where('is_void !=', 1);
        $this->db->order_by('date_sold');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function getPayments($transactionId) {
        $this->db->select("*");
        $this->db->from("transaction_payments");
        $this->db->where('transaction_id', $transactionId);
        $this->db->order_by('date');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function getTransactions() {
        $location = $this
            ->input
            ->post('location');
        $category = $this
            ->input
            ->post('category');
        $status = $this
            ->input
            ->post('status');
        $date_start = date("Y-m-d", strtotime($this
            ->input
            ->post('date_start')));
        $date_end = date("Y-m-d", strtotime($this
            ->input
            ->post('date_end')));

        $this->db->select("t.transaction_id, t.description, t.branch_fk, t.serial, t.date, t.expiration, t.status, t.user_fk,
                    type.name as type, t.value, t.fee, t.amount, t.category,
                    brand.name as brand,
                    name.name as name,
                    c.last_name, c.middle_name, c.first_name, c.contact,
                    l.name as branch,
                    cat.category_name,
                    ");
        $this->db->from("transactions as t");

        // Location OR group
        $this->db->group_start();
        foreach ($location as $loc) {
            $this ->db ->or_where('t.branch_fk', $loc);
        }
        $this->db->group_end();

        // Category OR group
        $this->db->group_start();
        foreach ($category as $cat) {
            $this ->db ->or_where('t.category', $cat);
        }
        $this->db->group_end();

        // Status OR group
        $this->db->group_start();
        foreach ($status as $stat) {
            $this ->db ->or_where('t.status', $stat);
        }
        $this->db->group_end();

        $this->db->where('t.date >=', $date_start);
        $this->db->where('t.date <=', $date_end);

        $this->db->join('item_type as type', 'type.id = t.type_fk');
        $this->db->join('item_brand as brand', 'brand.id = t.brand_fk');
        $this->db->join('item_name as name', 'name.id = t.name_fk');
        $this->db->join('customers as c', 'c.id = t.customer_fk');
        $this->db->join('branches as l', 'l.id = t.branch_fk');
        $this->db->join('transaction_categories as cat', 'cat.id = t.category');

        $this->db->order_by('t.date');

        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    function getPawnRecord($location, $status, $date_start, $date_end, $date_option) {
        $this->db->select('"#" as "#", t.transaction_id, CONCAT(l.name, " (", t.branch_fk ,")" ) as location, 
            CONCAT(c.first_name, " ", c.middle_name, " ", c.last_name) as customer, c.contact, type.name as item_type, CONCAT(brand.name, " ", name.name) as item_name, t.serial, t.status, DATE_FORMAT(t.date, "%d %b %Y") as entry_date, DATE_FORMAT(t.updated_at, "%d %b %Y") as last_update, DATE_FORMAT(t.expiration, "%d %b %Y") as due_date, t.value, t.fee, t.expiration'); 
        $this->db->from('transactions as t');
        $this->db->group_start();
        
        foreach ($status as $stat) {
            $this->db->or_where('t.status', $stat);
        }

        $this->db->group_end(); 
        $this->db->where('t.branch_fk', $location);
        $this->db->where('t.category', 1);
            
        
        if($date_option == 'update') {
            $date_start_obj = new DateTime($date_start);
            $date_end_obj = new DateTime($date_end);             
            $this->db->where('t.updated_at >=', date_format($date_start_obj, 'Y-m-d 00:00:00'));
            $this->db->where('t.updated_at <=', date_format($date_end_obj, 'Y-m-d 23:59:59'));
        } else {
            $this->db->where('t.date >=', $date_start);
            $this->db->where('t.date <=', $date_end);
        }

        $this->db->join('item_type as type', 'type.id = t.type_fk');
        $this->db->join('item_brand as brand', 'brand.id = t.brand_fk');
        $this->db->join('item_name as name', 'name.id = t.name_fk');
        $this->db->join('customers as c', 'c.id = t.customer_fk');
        $this->db->join('branches as l', 'l.id = t.branch_fk');

        $this->db->order_by('t.date');

        $query = $this->db->get();

        $pawnRecord = $query->result_array();

        foreach ($pawnRecord as $pr => $rec) {
            $transaction_id = $rec['transaction_id'];
            $status = $rec['status'];
            $appraisal_value = $rec['value'];
            $transaction_fee = $rec['fee'];
            $today = date_create(date("Y-m-d"));
            $expiration = date_create($rec['expiration']);
            $overdue = intval(date_diff($expiration, $today)->format("%R%a"));
            // $last_transaction = $this->Transaction_model->getRecentLog($transaction_id)['last_transaction'];
                        
            $prev_penalty_charge = $this
                ->db
                ->select_sum('penalty_charge')
                ->from('transaction_payments')
                ->where('transaction_id', $transaction_id)->get()
                ->row_array() ['penalty_charge'];
            $prev_transaction_fee = $this
                ->db
                ->select_sum('transaction_fee')
                ->from('transaction_payments')
                ->where('transaction_id', $transaction_id)->get()
                ->row_array() ['transaction_fee'];
            $selling_price = $this
                ->db
                ->select('selling_price')
                ->from('sales')
                ->where('reference_id', $transaction_id)->where('is_void', 0)
                ->get()
                ->row_array();

            $selling_price = null !== $selling_price ? $selling_price['selling_price'] : 0;

            $prev_payments = $prev_penalty_charge + $prev_transaction_fee;

            if ($overdue > 0) {
                $penalty = ($appraisal_value * 0.01) * $overdue;
            } else {
                $penalty = 0;
            }

            if ($status == 'Repurchased') {
                $amount_due = 0;
                $income = $prev_payments;
                // $income = $appraisal_value + $prev_payments;
                
            } else if ($status == 'Sold') {
                $amount_due = 0;
                $income = $prev_payments;
                // $income = $selling_price + $prev_payments;
                
            } else if ($status == 'For Sale' || $status == 'For Repair' || $status == 'For Diposal' || $status == 'Disposed') {
                $amount_due = 0;
                $income = $prev_payments;                                            
            } else {
                $amount_due = $penalty + $transaction_fee;
                $income = $prev_payments;
            }
            // $pawnRecord[$pr]['last_transaction'] = $last_transaction;
            $pawnRecord[$pr]['#'] = $pr + 1;
            $pawnRecord[$pr]['amount_due'] = $amount_due;
            $pawnRecord[$pr]['income'] = $income;
            
            // $pawnRecord[$pr]['status'] = $status . " - " . $last_trnx[0];

            unset($pawnRecord[$pr]['fee']);
            unset($pawnRecord[$pr]['expiration']);
        }
        
        return $pawnRecord;
    }

    function getPawnRenewRepurchase($location, $status, $date_start, $date_end, $date_option) {

        $this->db->select('"#" as "#", t.transaction_id as trans_id,  CONCAT(c.first_name, " ", c.middle_name, " ", c.last_name) as customer, c.contact as contact, CONCAT(brand.name, " ", name.name) as item_name, t.serial as serial, DATE_FORMAT(pay.date, "%d %b %Y") as date, pay.status as transaction, t.value as loan_payment, pay.transaction_fee as trans_fee, pay.penalty_charge as penalty');
        $this->db->from('transactions as t');
        
        $this->db->group_start();
            foreach ($status as $stat) {
                $this->db->or_where('t.status', $stat);
            }
        $this->db->group_end(); 

        $this->db->where('t.branch_fk', $location);
        $this->db->where('t.category', 1);
        $this->db->where('t.is_void', 0);

        if($date_option == 'update') {
            $date_start_obj = new DateTime($date_start);
            $date_end_obj = new DateTime($date_end);             
            $this->db->where('t.updated_at >=', date_format($date_start_obj, 'Y-m-d 00:00:00'));
            $this->db->where('t.updated_at <=', date_format($date_end_obj, 'Y-m-d 23:59:59'));
        } else {
            $this->db->where('t.date >=', $date_start);
            $this->db->where('t.date <=', $date_end);
        }

        $this->db->join('item_type as type', 'type.id = t.type_fk');
        $this->db->join('item_brand as brand', 'brand.id = t.brand_fk');
        $this->db->join('item_name as name', 'name.id = t.name_fk');
        $this->db->join('customers as c', 'c.id = t.customer_fk');
        $this->db->join('branches as l', 'l.id = t.branch_fk');
        $this->db->join('transaction_payments as pay', 'pay.transaction_id = t.transaction_id');

        $this->db->order_by('t.transaction_id, pay.date');

        $query = $this->db->get();

        $pawnRecord = $query->result_array();

        $temp_total = 0;
        $prev_transaction_id = "";
        // print_pre($pawnRecord);
        // die;
        // $prev_sub_total = "";
        foreach ($pawnRecord as $pr => $rec) {
            $pawnRecord[$pr]['#'] = $pr + 1;
            $transaction_id = $rec['trans_id'];
            $sub_total = $rec['trans_fee'] + $rec['penalty'];
            $status = $rec['transaction'];
            $value = $rec['loan_payment'];

            if($status == 'Repurchased') {
                $sub_total += $value;  
            }

            $pawnRecord[$pr]['sub_total'] = $sub_total;
         
            if($transaction_id == $prev_transaction_id) {
                $temp_total += $sub_total;

                $pawnRecord[$pr]['revenue'] = $temp_total;

                $pawnRecord[$pr]['trans_id'] = "";
                $pawnRecord[$pr]['customer_name_and_contact'] = "";
                $pawnRecord[$pr]['item_name_and_serial'] = "";
            } else {
               
                $pawnRecord[$pr]['revenue'] = $sub_total;
                $temp_total = $sub_total;
            }

            if($status == 'Repurchased') {
                $pawnRecord[$pr]['profit'] = $temp_total - $value;
            } else {
                $pawnRecord[$pr]['loan_payment'] = 0;
                $pawnRecord[$pr]['profit'] = 0;
            }

            $prev_transaction_id = $transaction_id;
            // $prev_sub_total = $sub_total;
        }
        
        return $pawnRecord;
     
    }

}

