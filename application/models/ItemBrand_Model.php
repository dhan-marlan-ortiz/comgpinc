<?php
class ItemBrand_model extends CI_Model {

	function getTypes($id = null) {


		$this->db->select('item_type.*');
		$this->db->from('item_type');
		$this->db->where('is_visible', 1);

		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}

	function getBrands($id = null) {
		$this->db->select('item_brand.*, item_brand.id, item_brand.name as brand, item_type.name as type, type_fk, item_brand.is_visible');
		$this->db->from('item_brand');
		$this->db->join('item_type', 'item_brand.type_fk = item_type.id');
		$this->db->where('item_brand.is_visible', 1);
		$this->db->order_by('item_brand.id', 'desc');

		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}

	function addBrand ($values) {
		$query = $this->db->insert('item_brand', $values);
		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'New brand has been successfully registered.');
		} else {
			$this->session->set_flashdata('error', 'Failed to register new brand. Please try again.');
		}
		return $result;
	}

	function updateBrand ($id, $values) {

		$this->db->where('id', $id);
		$this->db->update('item_brand', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Brand has been successfully updated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update information. Pleast try again.');
		}
		return $result;
	}
}
?>