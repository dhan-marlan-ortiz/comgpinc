<?php
class Accounts_model extends CI_Model {

     function loginAuthentication($user_id, $password) {
          $query = $this->db->get_where('users', array('id'=>$user_id));
          $result = $query->row_array();
          
          if (null !== $result) {
               $verify = password_verify(trim($password), trim($result['password']));
               return $result;
          } else {
               return null;
          }
     }
}
