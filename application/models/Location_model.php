<?php
class Location_model extends CI_Model {

	function getLocations($id = null) {
		$query = $this->db->get('branches');
		$result = $query->result_array();

		return $result;
	}

	function addLocation($values) {
		$query = $this->db->insert('branches', $values);
		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'New Location has been successfully added.');
		} else {
			$this->session->set_flashdata('error', 'Failed to add new location. Please try again.');
		}
		return $result;
	}

	function updateLocation($id, $values) {

		$this->db->where('id', $id);
		$this->db->update('branches', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Location has been successfully updated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update location. Pleast try again.');
		}
		return $result;
	}

}
?>