<?php
class Expense_model extends CI_Model {

     function getType() {
          $query = $this->db->order_by('type', 'DES')->get('expense_type');
		$result = $query->result_array();
		return $result;
     }

     function getExpenses($location, $date_start, $date_end) {
          // $post = $this->input->post();
          // $location = $post['location'];
          // $date_start = date("Y-m-d H:i:s", strtotime($post['date_start']));
          // $date_end = date("Y-m-d H:i:s", strtotime($post['date_end']));

          return $this->db->select("e.id as #, l.name as LOCATION,  DATE_FORMAT(e.date, '%d %b %Y') as DATE, t.type as TYPE, e.description as DESCRIPTION, e.amount as AMOUNT")
               ->from('expenses as e')
               ->where('e.branch_fk', $location)
               ->where('e.is_void', 0)
               ->where('e.date >=', $date_start)
               ->where('e.date <=', $date_end)
               ->join('expense_type as t', 't.id = e.type_fk')
               ->join('branches as l', 'l.id = e.branch_fk')
               ->order_by('e.date', 'ASC')
               ->get()->result_array();
     }

     function getExpensesSummary() {
          $post = $this->input->post();
          $location = $post['location'];
          $date_start = date("Y-m-d H:i:s", strtotime($post['date_start']));
          $date_end = date("Y-m-d H:i:s", strtotime($post['date_end']));

          // return $this->db->select("e.id as #, branch_fk as LOCATION,  e.date as DATE, t.type as TYPE, e.description as DESCRIPTION, e.amount as AMOUNT")
          $reports =  $this->db->select("e.id as #, branch_fk as LOCATION,  e.date as DATE, e.description as DESCRIPTION, e.amount as AMOUNT, e.type_fk")
               ->from('expenses as e')
               ->where('e.branch_fk', $location)
               ->where('e.is_void', 0)
               ->where('e.date >=', $date_start)
               ->where('e.date <=', $date_end)
               // ->join('expense_type as t', 't.id = e.type_fk')
               ->order_by('e.date', 'ASC')
               ->get()->result_array();

          $total = 0;
          foreach ($reports as $r => $report) {
               $types = $this->db->order_by('type', 'ASC')->get("expense_type")->result_array();
               foreach ($types as $t => $type) {
                    if($report['type_fk'] == $type['id']) {
                         $reports[$r][$type['type']] = $report['AMOUNT'];
                         $total += $report['AMOUNT'];
                    } else {
                         $reports[$r][$type['type']] = "";
                    }
               }
               $reports[$r]['TOTAL'] = $total;
               // $reports[$r]['TOTAL'] = $report['AMOUNT'];
               unset($reports[$r]['AMOUNT']);
               unset($reports[$r]['type_fk']);
          }

          return $reports;
     }

}
