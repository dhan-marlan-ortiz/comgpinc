<?php
class Appraiser_model extends CI_Model {

	function getAppraisers($id = null) {


		$this->db->select('appraisers.*');
		$this->db->from('appraisers');
		$this->db->where('is_active', 1);
		$this->db->order_by("id", "desc");

		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}

	function addAppraiser ($values) {
		$query = $this->db->insert('appraisers', $values);
		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Appraiser has been successfully added.');
		} else {
			$this->session->set_flashdata('error', 'Failed to add new appraiser . Please try again.');
		}
		return $result;
	}

	function updateAppraiser ($id, $values) {

		$this->db->where('id', $id);
		$this->db->update('appraisers', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Appraiser has been successfully updated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update appraiser. Pleast try again.');
		}
		return $result;
	}
}
?>