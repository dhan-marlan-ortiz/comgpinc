<?php
class Time_model extends CI_Model {

	function getAttendance($datetime_start, $datetime_end, $employee_id = null) {
		$this->db->select("'#', t.employee_id, CONCAT(e.first_name, ' ', e.middle_name, ' ', e.last_name) as employee_name, b.name as branch, DATE_FORMAT(t.date_time, '%M %d, %Y') as date, DATE_FORMAT(t.date_time, '%r') as time_in, 'time_out'");
		$this->db->from("time_tracker as t");
		$this->db->where('t.date_time >=', $datetime_start);
		$this->db->where('t.date_time <=', $datetime_end);
		$this->db->where('t.description', 'time in');
		if(null !== $employee_id) {
			$this->db->where('t.employee_id', $employee_id);
		}
		$this->db->join('employees as e', 'e.id = t.employee_id');
		$this->db->join('branches as b', 'b.id = t.location');
		$attendance = $this->db->get()->result_array();

		
		foreach ($attendance as $a_key => $a) {
			$attendance[$a_key]['#'] = $a_key + 1;
			$date = date("Y-m-d", strtotime($a['date']));

			$getTimeOut = $this->db->select('date_time')
						->from('time_tracker')
						->where('employee_id', $a['employee_id'])
						->where('description', 'time out')
						->LIKE('date_time', $date, 'after')						
						->get()->row_array();
			
			if(null !== $getTimeOut) {
				$attendance[$a_key]['time_out'] = date("h:i:s A", strtotime($getTimeOut['date_time']));
			} else {
				$attendance[$a_key]['time_out'] = "-";
			}

			
		}
		return $attendance;
	}	
}
?>