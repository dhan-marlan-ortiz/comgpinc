<?php
class Retail_model extends CI_Model {

     function getItemByStatus($branch_fk = null, $status = null) {
          $this->db->select("r.*, l.name as branch, u.first_name, u.last_name, t.name as type, b.name as brand, n.name, m.unit");
          $this->db->from("retails as r");

          if(null !== $branch_fk) {
               $this->db->where('r.branch_fk', $branch_fk);
          }

          $this->db->join('branches as l', 'l.id = r.branch_fk');
          $this->db->join('users as u', 'u.id = r.user_fk');
          $this->db->join('item_type as t ', 't.id = r.type_fk');
          $this->db->join('item_brand as b', 'b.id = r.brand_fk');
          $this->db->join('item_name as n', 'n.id = r.name_fk');
          $this->db->join('item_unit as m', 'm.id = r.unit_fk');
          $this->db->group_start();
               foreach ($status as $s => $stat) {
                    $this->db->or_where('r.status', $stat);
               }
          $this->db->group_end();
          $this->db->order_by('r.date', 'DESC');
          $query = $this->db->get();

          $results = $query->result_array();

          return $results;
     }

     function getRetailFifo ($type_fk, $brand_fk, $name_fk, $unit_fk, $status, $quantity) {
          $query = $this->db->select("pk")->from("retails")
                    ->where('type_fk', $type_fk)
                    ->where('brand_fk', $brand_fk)
                    ->where('name_fk', $name_fk)
                    ->where('unit_fk', $unit_fk)
                    ->where('status', $status)
                    ->limit($quantity)
                    ->order_by('date', 'ASC')
                    ->get();

          return $query->result_array();
     }

     function getShipmentsOutbound () {
          $this->db->select('s.*, n.name, b.name as brand, t.name as type, u.unit, o.name as origin_name, d.name as destination_name');

          $this->db->from('shipments as s');
          $this->db->where('s.origin', $this->session->userdata('branch_fk'));
          $this->db->where('s.is_received', 0);

          $this->db->join('item_name as n', 'n.id = s.name_fk');
          $this->db->join('item_brand as b', 'b.id = n.brand_fk');
          $this->db->join('item_type as t', 't.id = n.type_fk');
          $this->db->join('item_unit as u', 'u.id = s.unit_fk');

          $this->db->join('branches as o', 'o.id = s.origin');
          $this->db->join('branches as d', 'd.id = s.destination');

          $query = $this->db->get();

          $result = $query->result_array();

          return $result;
     }

     function getShipmentsInbound () {
          $this->db->select('s.*, n.name, b.name as brand, t.name as type, u.unit, o.name as origin_name, d.name as destination_name');

          $this->db->from('shipments as s');
          $this->db->where('s.destination', $this->session->userdata('branch_fk'));
          $this->db->where('s.is_received', 0);

          $this->db->join('item_name as n', 'n.id = s.name_fk');
          $this->db->join('item_brand as b', 'b.id = n.brand_fk');
          $this->db->join('item_type as t', 't.id = n.type_fk');
          $this->db->join('item_unit as u', 'u.id = s.unit_fk');

          $this->db->join('branches as o', 'o.id = s.origin');
          $this->db->join('branches as d', 'd.id = s.destination');

          $query = $this->db->get();

          $result = $query->result_array();

          return $result;
     }

     function getRetailReturns($id = null) {
          $this->db->select("rr.id, rr.transaction_id, rr.date, rr.action, rr.remarks, rr.quantity, rr.amount,
                              t.name as type,
                              CONCAT(b.name, ' ', n.name) as product, 
                              r.status, r.pk as retail_id, r.type_fk, r.brand_fk, r.name_fk, r.unit_fk, r.buy,
                              b.name as brand,
                              n.name,
                              u.unit,  
                              p.first_name, p.last_name, 
                              l.name as branch,
                              CONCAT('0') as shipment_id"); 

          $this->db->from('retail_returns as rr');

          if(isset($id)) {
               $this->db->where('rr.transaction_id', $id);
          }

          if($this->session->userdata('role_fk') != 'ADMS') {
               $this->db->where('rr.branch_fk', $this->session->userdata('branch_fk'));
          }

          $this->db->join('sales as s', 's.sales_id = rr.sales_id');
          $this->db->join('retails as r', 'r.pk = rr.retail_id');

          $this->db->join('item_type as t', 't.id = r.type_fk');
          $this->db->join('item_brand as b', 'b.id = r.brand_fk');
          $this->db->join('item_name as n', 'n.id = r.name_fk');
          $this->db->join('item_unit as u', 'u.id = r.unit_fk');

          $this->db->join('branches as l', 'l.id = rr.branch_fk');
          $this->db->join('users as p', 'p.id = rr.user_fk');

          $query = $this->db->get();
          $result = $query->result_array();

          foreach ($result as $r => $res) {
               $result[$r]['shipment_id'] = $this->db->get_where('shipments', array('retail_id' => $res['retail_id']))->row_array()['id'];
          }

          return $result;
     }

     function getSoldProducts() {
          $this->db->select('s.*, t.name as type, b.name as brand, n.name as name, u.unit as unit'); $this->db->from('sales as s');
          $this->db->where('is_void', 0);

          if($this->session->userdata('role_fk') != 'ADMS') {
               $this->db->where('s.branch_fk', $this->session->userdata('branch_fk'));
          }

          $this->db->join('retails as r', 'r.registration_id = s.transaction_id');
          $this->db->join('item_type as t', 't.id = r.type_fk');
          $this->db->join('item_brand as b', 'b.id = r.brand_fk');
          $this->db->join('item_name as n', 'n.id = r.name_fk');
          $this->db->join('item_unit as u', 'u.id = r.unit_fk');

          $query = $this->db->get();

          return $query->result_array();

     }

     function register() {
		$post = $this->input->post();
          $quantity = trim(str_replace(',', '', $post['quantity']));
		$type_fk = "";
		$brand_fk = "";
		$product_fk = "";

		if(isset($post['type-new'])) {
			$type_name = $post['type-new'];
			$type_fk = $this->Generic_model->addItemType(strtoupper($type_name));
		}else {
			$type_fk = $post['type'];
		}

		if(isset($post['brand-new'])) {
			$brand_name = $post['brand-new'];
			$brand_fk = $this->Generic_model->addItemBrand(strtoupper($brand_name), $type_fk);
		}else {
			$brand_fk = $post['brand'];
		}

		if(isset($post['product-new'])) {
			$product_name = $post['product-new'];
			$product_fk = $this->Generic_model->addItemName(strtoupper($product_name), $brand_fk, $type_fk);
		}else {
			$product_fk = $post['product'];
		}

		$registration_id = trim($this->Generic_model->generateTransactionId('retails', $post['branch'], date("Y-m-d", strtotime($post['date']))));
		$user_id = $this->session->userdata('id');

		$values = array(
					'registration_id' => $registration_id,
					'date' => date("Y-m-d", strtotime($post['date'])),
					'branch_fk' => $post['branch'],
					'type_fk' => $type_fk,
					'brand_fk' => $brand_fk,
					'name_fk' => $product_fk,
					'user_fk' => $user_id,
					'unit_fk' => $post['unit'],
					'quantity' => 1,
                         'status' => "For Sale",
					'buy' => trim(str_replace(',', '', $post['buy'])),
				);

          $retail_item = array();
          for ($i=0; $i < $quantity; $i++) {
               array_push($retail_item, $values);
          }

          $this->db->insert_batch('retails', $retail_item);

          $result = $this->db->affected_rows();

		if ($result > 0) {
			$this->session->set_flashdata('success', 'Success. Item has been registered');
          }else {
			$this->session->set_flashdata('error', 'Failed. Please check and try again');
          }

		return $result;
	}

     function getRegistered() {
          $this->db->select("r.*, l.name as branch, u.first_name, u.last_name, t.name as type, b.name as brand, n.name, m.unit");
		$this->db->from("retails as r");

		if($this->session->userdata('role_fk') != 'ADMS') {
			$this->db->where('r.branch_fk', $this->session->userdata('branch_fk'));
		}

		$this->db->join('branches as l', 'l.id = r.branch_fk');
		$this->db->join('users as u', 'u.id = r.user_fk');
		$this->db->join('item_type as t ', 't.id = r.type_fk');
		$this->db->join('item_brand as b', 'b.id = r.brand_fk');
		$this->db->join('item_name as n', 'n.id = r.name_fk');
		$this->db->join('item_unit as m', 'm.id = r.unit_fk');
		$this->db->order_by('r.date', 'DESC');
		$this->db->group_by('r.registration_id');
		$query = $this->db->get();

          $results = $query->result_array();

          foreach ($results as $r => $result) {
               $results[$r]['quantity'] = $this->db->get_where('retails', array('registration_id' => $result['registration_id']))->num_rows();
               $results[$r]['available'] = $this->db->get_where('retails', array('registration_id' => $result['registration_id'], 'status' => "For Sale"))->num_rows();
               $results[$r]['returned'] = $this->db->get_where('retails', array('registration_id' => $result['registration_id'], 'status' => "Returned"))->num_rows();
               $results[$r]['sold'] = $this->db->get_where('retails', array('registration_id' => $result['registration_id'], 'status' => "Sold"))->num_rows();
               $results[$r]['transferred'] = $this->db->get_where('retails', array('registration_id' => $result['registration_id'], 'status' => "Transferred"))->num_rows();
               $results[$r]['shipments'] = $this->db->get_where('retails', array('registration_id' => $result['registration_id'], 'status' => "For Transfer"))->num_rows();
               $results[$r]['repair'] = $this->db->get_where('retails', array('registration_id' => $result['registration_id'], 'status' => "For Repair"))->num_rows();
               $results[$r]['disposal'] = $this->db->get_where('retails', array('registration_id' => $result['registration_id'], 'status' => "For Disposal"))->num_rows();
               $results[$r]['disposed'] = $this->db->get_where('retails', array('registration_id' => $result['registration_id'], 'status' => "Disposed"))->num_rows();
          }

          return $results;
     }

     function countSales($transactionId) {
          $query = $this->db->select("quantity")->
                    from('sales')->
                    where('transaction_id', $transactionId)->
                    where('is_void', 0)->
                    get();

          $result = $query->result_array();

          $quantity = 0;
		  foreach ($result as $r) {
				$quantity += $r['quantity'];
		   }


          return $quantity;
     }

	function sumQuantity($name_fk, $branch_fk, $unit_fk) {
		$match = array(
					'name_fk' => $name_fk,
					'branch_fk' => $branch_fk,
					'unit_fk' => $unit_fk
				);

		$query = $this->db->select("quantity")->
				from("retails")->
				where($match)->
				get();

		$result = $query->result_array();

		$quantity = 0;
		foreach ($result as $r) {
			$quantity += $r['quantity'];
		}

		return $quantity;

	}

	function sumReturn($name_fk, $branch_fk, $unit_fk) {
		$match = array(
					'r.name_fk' => $name_fk,
					'r.branch_fk' => $branch_fk,
					'r.unit_fk' => $unit_fk,
					'rr.action' => 'Cash Refund'
				);

		$this->db->select("rr.quantity");
		$this->db->from('retail_returns as rr');
		$this->db->join('sales as s', 's.sales_id = rr.sales_id');
		$this->db->join('retails as r', 'r.registration_id = s.transaction_id');
		$this->db->where($match);

		$query = $this->db->get();

		$result = $query->result_array();

		$quantity = 0;
		foreach ($result as $r) {
			$quantity += $r['quantity'];
		}

		return $quantity;

	}

     function sumShipments($name_fk, $branch_fk, $unit_fk) {
          $match = array(
					's.name_fk' => $name_fk,
					's.origin' => $branch_fk,
					's.unit_fk' => $unit_fk,
                         's.is_received' => 1
				);

          $this->db->select("s.quantity");
		$this->db->from('shipments as s');
		$this->db->where($match);

		$query = $this->db->get();

		$result = $query->result_array();

		$quantity = 0;
		foreach ($result as $r) {
			$quantity += $r['quantity'];
		}

		return $quantity;
     }

     function sumDisposed($name_fk, $branch_fk, $unit_fk) {
     	$match = array(
     					'name_fk' => $name_fk,
     					'branch_fk' => $branch_fk,
     					'unit_fk' => $unit_fk,
     					'status' => "Disposed");
     	$query = $this->db->get_where('retails', $match);
		$result = $query->result_array();

		$quantity = 0;
		foreach ($result as $r) {
			$quantity += $r['quantity'];
		}
		return $quantity;
     }

     function getInventoryOld($branch = null) {
          $this->db->select("r.*, l.name as branch, u.first_name, u.last_name, t.name as type, b.name as brand, n.name, m.unit");
          $this->db->from("retails as r");

          if($this->session->userdata('role_fk') != 'ADMS') {
               $this->db->where('r.branch_fk', $this->session->userdata('branch_fk'));
          }

          if(null !== $branch) {
               $this->db->where('r.branch_fk', $branch);
          }

          $this->db->join('branches as l', 'l.id = r.branch_fk');
          $this->db->join('users as u', 'u.id = r.user_fk');
          $this->db->join('item_type as t ', 't.id = r.type_fk');
          $this->db->join('item_brand as b', 'b.id = r.brand_fk');
          $this->db->join('item_name as n', 'n.id = r.name_fk');
          $this->db->join('item_unit as m', 'm.id = r.unit_fk');
          $this->db->order_by('n.name', 'ASC');
          $this->db->group_by(array('r.name_fk', 'r.unit_fk', 'r.branch_fk'));

          $inventory = $this->db->get()->result_array();

          // get stocks
          foreach ($inventory as $i => $item) {
               $name_fk = $item['name_fk'];
               $branch_fk = $item['branch_fk'];
               $unit_fk = $item['unit_fk'];
               $sales = $this->countSales($item['registration_id']);
               $quantity = $this->sumQuantity($name_fk, $branch_fk, $unit_fk);
               $retail_returns = $this->sumReturn($name_fk, $branch_fk, $unit_fk);
               $shipments = $this->sumShipments($name_fk, $branch_fk, $unit_fk);
               $disposed = $this->sumDisposed($name_fk, $branch_fk, $unit_fk);
               // $sold = $sales['quantity'];
               $inventory[$i]['quantity'] = $quantity;
               $inventory[$i]['stock'] = $quantity + $retail_returns - $sales - $shipments - $disposed;
          }

          return $inventory;
     }

     function getInventory($branch = null) {
          /* Get ALL ITEMS IN RETAIL TABLE START */
          $this->db->select("r.*, l.name as branch, u.first_name, u.last_name, t.name as type, b.name as brand, n.name, m.unit");
          $this->db->from("retails as r");
          if(null !== $branch) {
               $this->db->where('r.branch_fk', $branch);
          }
          $this->db->join('branches as l', 'l.id = r.branch_fk');
          $this->db->join('users as u', 'u.id = r.user_fk');
          $this->db->join('item_type as t ', 't.id = r.type_fk');
          $this->db->join('item_brand as b', 'b.id = r.brand_fk');
          $this->db->join('item_name as n', 'n.id = r.name_fk');
          $this->db->join('item_unit as m', 'm.id = r.unit_fk');
          $this->db->order_by('brand', 'ASC');
          $this->db->group_by(array('r.name_fk', 'r.unit_fk', 'r.branch_fk'));
          $inventory = $this->db->get()->result_array();
          /* Get ALL ITEMS IN RETAIL TABLE START end */

          // get stocks
          // print_pre($inventory);
          foreach ($inventory as $i => $item) {
               $name_fk = $item['name_fk'];
               $branch_fk = $item['branch_fk'];
               $unit_fk = $item['unit_fk'];
               $registration_id = $item['registration_id'];

               $for_sale = $this->db->get_where('retails', array('status' => 'For Sale', 'name_fk' => $name_fk, 'branch_fk' => $branch_fk, 'unit_fk' => $unit_fk))->num_rows();
               $for_repair = $this->db->get_where('retails', array('status' => 'For Repair', 'name_fk' => $name_fk, 'branch_fk' => $branch_fk, 'unit_fk' => $unit_fk))->num_rows();
               $for_transfer = $this->db->get_where('retails', array('status' => 'For Transfer', 'name_fk' => $name_fk, 'branch_fk' => $branch_fk, 'unit_fk' => $unit_fk))->num_rows();
               $for_disposal = $this->db->get_where('retails', array('status' => 'For Disposal', 'name_fk' => $name_fk, 'branch_fk' => $branch_fk, 'unit_fk' => $unit_fk))->num_rows();
               $sold = $this->db->get_where('retails', array('status' => 'Sold', 'name_fk' => $name_fk, 'branch_fk' => $branch_fk, 'unit_fk' => $unit_fk))->num_rows();
               $transferred = $this->db->get_where('retails', array('status' => 'Transferred', 'name_fk' => $name_fk, 'branch_fk' => $branch_fk, 'unit_fk' => $unit_fk))->num_rows();
               $disposed = $this->db->get_where('retails', array('status' => 'Disposed', 'name_fk' => $name_fk, 'branch_fk' => $branch_fk, 'unit_fk' => $unit_fk))->num_rows();
               $returned = $this->db->get_where('retails', array('status' => 'Returned', 'name_fk' => $name_fk, 'branch_fk' => $branch_fk, 'unit_fk' => $unit_fk))->num_rows();

               // $inventory[$i]['stock'] = $for_sale + $for_repair + $for_transfer + $for_disposal + $returned;
               $inventory[$i]['stock'] = $for_sale;
          }

          return $inventory;
     }

    function removeRegistration($registration_id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		if (!$this->db->where('registration_id', $registration_id)->delete('retails')) {
               $this->session->set_flashdata('error', "Failed. Product is already in use");
          }else {
               $this->session->set_flashdata('success', 'Success. Product has been successfully deleted');
          }
		return;
	}

	function getRetailtransaction($id = null) {
		$query = $this->db->select("s.*, item_brand.name as brand,  item_name.name as name, item_type.name as type, item_unit.unit")->
				from('sales as s')->
				where('s.reference_id', $id)->
				join('retails as r', 'r.registration_id = s.transaction_id')->
				join('item_brand', 'item_brand.id = r.brand_fk')->
				join('item_name', 'item_name.id = r.name_fk')->
				join('item_type', 'item_type.id = r.type_fk')->
				join('item_unit', 'item_unit.id = r.unit_fk')->
                    group_by('transaction_id')->
				get();

		$result = $query->result_array();
		return $result;
	}
}
