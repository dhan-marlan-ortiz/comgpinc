<?php
class ItemType_model extends CI_Model {

	function getTypes($id = null) {


		$this->db->select('item_type.*');
		$this->db->from('item_type');
		$this->db->where('is_visible', 1);
		$this->db->order_by("id", "desc");

		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}

	function addType ($values) {
		$query = $this->db->insert('item_type', $values);
		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Item type has been successfully added.');
		} else {
			$this->session->set_flashdata('error', 'Failed to add item type. Please try again.');
		}
		return $result;
	}

	function updateType ($id, $values) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
	
		if (!$this->db->where('id', $id)->update('item_type', $values)) {
			   $error = $this->db->error();
			   $this->session->set_flashdata('error', $error['message']);
		}else {
			$result = $this->db->affected_rows();
			if($result > 0) {
				$this->session->set_flashdata('success', 'Item information has been successfully updated.');
			} else {
				$this->session->set_flashdata('error', 'Failed to update item information. Pleast try again.');
			}
		}

		return;
	}
}
?>
