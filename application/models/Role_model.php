<?php
class Role_model extends CI_Model {
	function getRoles($id = null) {
		$query = $this->db->get('roles');
		$result = $query->result_array();
		return $result;
	}
	function addRole($values) {
		$query = $this->db->insert('roles', $values);
		$result = $this->db->affected_rows();
		if($result > 0) {
			$this->session->set_flashdata('success', 'Role has been successfully added.');
		} else {
			$this->session->set_flashdata('error', 'Failed to new add role. Please try again.');
		}
		return $result;
	}
	function updateRole($id, $values) {
		$this->db->where('id', $id);
		$this->db->update('roles', $values);
		$result = $this->db->affected_rows();
		if($result > 0) {
			$this->session->set_flashdata('success', 'Role has been successfully updated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update role. Pleast try again.');
		}
		return $result;
	}
}
?>