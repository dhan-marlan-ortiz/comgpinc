<?php
class Generic_model extends CI_Model {

	function upload($file, $config) {
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload($file)) {
			$message = array('error' => $this->upload->display_errors());			
		} else {
			$message = array('upload_data' => $this->upload->data());						
		}
		return $message;
	}

	function getProvinces() {
		$this->db->group_by('province');
		$query = $this->db->get('cities');
		$result = $query->result_array();
		return $result;
	}

    function getCities() {
     	$query = $this->db->get('cities');
     	$result = $query->result_array();

        	return $result;
     }

	function getBranches() {
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('branches');
     	$result = $query->result_array();

    	return $result;
	}

	function getBranchInfo($branch_fk) {
		$filter = array('id' => $branch_fk);
		$query = $this->db->get_where('branches', $filter);
		$result = $query->row_array();

		return $result;
	}

	function getItemType() {
		$this->db->order_by('name', 'asc');
		$query = $this->db->get_where('item_type', array('is_visible' => 1));
     	$result = $query->result_array();

        	return $result;
	}

	function addItemType($name) {
		$table = "item_type";
		$this->db->insert($table, array('name' => $name));

		return $this->db->insert_id();
	}

	function getItemBrand($id = null) {
		$this->db->order_by('name', 'asc');
		if(null !== $id) {
			$query = $this->db->get_where('item_brand', array('type_fk' => $id));
		} else {
			$query = $this->db->get_where('item_brand', array('is_visible' => 1));
		}
     	$result = $query->result_array();

        	return $result;
	}

	function addItemBrand($name, $item_fk) {
		$table = "item_brand";
		$this->db->insert($table, array(
								'name' => $name,
								'type_fk' => $item_fk
							));

		return $this->db->insert_id();
	}

	function getItemList() {
		$items = $this->getItemType();
		$item_list = array();
		foreach ($items as $i => $item):
			$id = $item['id'];
			$name = $item['name'];
			$brands = $this->getItemBrand($id);
			foreach ($brands as $b => $brand):
				$brand['id'];
				$item_list[$name][$b] = array(
					'brand_id'=> $brand['id'],
					'brand_name' => $brand['name'],
					'type_id'=> $id,
					'type_name'=> $name
				);
			endforeach;
		endforeach;
		return $item_list;
	}

	function getProduct() {
		$this->db->select('item_name.*, item_brand.name as brand, item_brand.type_fk as type_fk, item_type.name as type');
		$this->db->from('item_name');
		$this->db->where(array('item_name.is_visible' => 1));
		$this->db->join('item_brand', 'item_brand.id = item_name.brand_fk', 'left');
		$this->db->join('item_type', 'item_type.id = item_brand.type_fk', 'left');
		$this->db->order_by('item_name.name', 'asc');
		$query = $this->db->get();

		$result = $query->result_array();
		return $result;
	}

	function addItemName($name, $brand_fk, $item_fk) {
		$table = "item_name";
		$this->db->insert($table, array(
								'name' => $name,
								'brand_fk' => $brand_fk,
								'type_fk' => $item_fk
							));

		return $this->db->insert_id();
	}

	function getAppraiser($id = null) {
		$query = $this->db->get('appraisers');
		$result = $query->result_array();

		return $result;
	}

	function generateTransactionId($table, $branch, $date) {
		$date_map = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		$month = $date_map[date("m", strtotime($date))];
		$year = $date_map[date("y", strtotime($date))];
		$day = $date_map[date("d", strtotime($date))];

		$squence = 0;
		/*
		if($table == 'transactions') {
			$filters = array( 'branch_fk' => $branch, 'date' => $date);
			$squence = $this->db->where($filters)->count_all_results($table);
		} else if ($table == 'purchases') {
			$filters = array( 'branch_fk' => $branch);
			$squence = $this->db->where($filters)->like('date_created', $date, 'after')->count_all_results($table);
		} else if($table == 'retails') {
			$filters = array( 'branch_fk' => $branch, 'date' => $date);
			$squence = $this->db->where($filters)->count_all_results($table);
		} else if($table == 'shipments') {
			$filters = array( 'origin' => $branch, 'date' => $date);
			$squence = $this->db->where($filters)->count_all_results($table);
		} else if($table == 'funds') {
			$filters = array( 'origin' => $branch, 'date' => $date);
			$squence = $this->db->where($filters)->count_all_results($table);
		}
		*/

		if($table == 'transactions') {
			// $filters = array( 'branch_fk' => $branch, 'date' => $date);
			$squence = $this->db->where('date', $date)->count_all_results($table);
		} else if ($table == 'purchases') {
			// $filters = array( 'branch_fk' => $branch);
			$squence = $this->db->like('date_created', $date, 'after')->count_all_results($table);
		} else if($table == 'retails') {
			// $filters = array( 'branch_fk' => $branch, 'date' => $date);
			$squence = $this->db->where('date', $date)->count_all_results($table);
		} else if($table == 'shipments') {
			// $filters = array( 'origin' => $branch, 'date' => $date);
			$squence = $this->db->where('date', $date)->count_all_results($table);
		} else if($table == 'funds') {
			// $filters = array( 'origin' => $branch, 'date' => $date);
			$squence = $this->db->where('date', $date)->count_all_results($table);
		} else if($table == 'expenses') {
			$squence = $this->db->where('date', $date)->count_all_results($table);
		}


		$user_id = $this->session->userdata('uid');

		$transactionId = $branch.str_pad($user_id, 2,'0', STR_PAD_LEFT).strtoupper($table[0]).$year.$month.$day.str_pad($squence+1, 3,'0', STR_PAD_LEFT);
		// [Location][User ID][Transaction Type][year][month][day][sequence]

		return $transactionId;
	}

	function count($branch = null) {
		if(null !== $branch) {
			$count['due'] = $this->db->where(array('status' => 'Due', 'branch_fk' => $branch))->count_all_results('transactions');
			$count['expired'] = $this->db->where(array('status' => 'Expired', 'branch_fk' => $branch))->count_all_results('transactions');
			$count['grace'] = $this->db->where(array('status' => 'Grace Period', 'branch_fk' => $branch))->count_all_results('transactions');
			$count['freeze'] = $this->db->where(array('status' => 'Freeze', 'branch_fk' => $branch))->count_all_results('transactions');
			$count['repair'] = $this->db->where(array('status' => 'For Repair', 'branch_fk' => $branch))->count_all_results('transactions');
			$count['disposal'] = $this->db->where(array('status' => 'For Disposal', 'branch_fk' => $branch))->count_all_results('transactions');
			$count['transfer'] = $this->db->where(array('status' => 'For Transfer', 'branch_fk' => $branch))->count_all_results('transactions');
		} else {
			$count['due'] = $this->db->where('status', 'Due')->count_all_results('transactions');
			$count['expired'] = $this->db->where('status', 'Expired')->count_all_results('transactions');
			$count['grace'] = $this->db->where('status', 'Grace Period')->count_all_results('transactions');
			$count['freeze'] = $this->db->where('status', 'Freeze')->count_all_results('transactions');
			$count['repair'] = $this->db->where('status', 'For Repair')->count_all_results('transactions');
			$count['disposal'] = $this->db->where('status', 'For Disposal')->count_all_results('transactions');
			$count['transfer'] = $this->db->where('status', 'For Transfer')->count_all_results('transactions');
		}

		$count['transaction'] = $this->db->not_like('status', 'Void')->count_all_results('transactions');
		$count['purchase'] = $this->db->count_all_results('purchases');
		$count['customer'] = $this->db->where('id !=', 1)->count_all_results('customers');
		$count['location'] = $this->db->count_all_results('branches');
		// $count['contract'] = $this->db->where('status', 'In Contract')->count_all_results('transactions');
		// $count['sale'] = $this->db->where('status', 'For Sale')->count_all_results('transactions');
		// $count['sold'] = $this->db->where('status', 'Sold')->count_all_results('transactions');
		// $count['disposed'] = $this->db->where('status', 'Disposed')->count_all_results('transactions');
		// $count['purchase_qty'] = $this->db->select_sum('quantity')->get('purchases')->row()->quantity;
		// $count['purchase_stocks'] = $this->db->select_sum('stocks')->get('purchases')->row()->stocks;

		return $count;
	}

	function updateStatus() {
		$query = $this->db->select("id, status, expiration, updated_at")
				->from('transactions')
				->like('status', 'Contract', 'both')
				->or_like('status', 'Due', 'both')
				->or_like('status', 'Grace', 'both')
				->get();

		$getStatus = $query->result_array();

		$today = date_create(date("Y-m-d"));
		$toUpdate = array();

		foreach ($getStatus as $value) {
			$expiration = date_create($value['expiration']);
			$difference = intval(date_diff($expiration, $today)->format("%R%a"));
			$now = date("Y-m-d");
			$due = $value['expiration'];
			$id = $value['id'];
			$status = "";

			if($difference > 0 && $difference <= 7) {
				$status = "Grace Period";
			} else if ($difference > 7){
				$status = "Expired";
			} else if($difference <= 0 && $difference >= -7) {
				$status = "Due";
			}else {
				$status = "In Contract";
			}

			if($status != $value['expiration']) {
				$toUpdate[] = array('id' => $id, 'status' => $status);
			}
		}

		if($toUpdate) {
         	$this->db->update_batch('transactions', $toUpdate, 'id');
         	$data['is_updated'] = $this->db->affected_rows();
		}

		$data['getStatus'] = $getStatus;
		return $data;
	}

	function addUnit() {
          $db_debug = $this->db->db_debug;
          $this->db->db_debug = false;

          if (!$this->db->insert('item_unit', array( 'unit' => strtoupper($this->input->post('unit_name'))))) {
                  $error = $this->db->error();
                  $this->session->set_flashdata('error', $error['message']);
          }else {
               $this->session->set_flashdata('success', 'Success. New unit has been added');
          }

		return;
     }

	function getUnits() {
          $query = $this->db->order_by('unit', 'DES')->get('item_unit');
		$result = $query->result_array();
		return $result;
     }

	public function removeUnit($id) {
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		if (!$this->db->where('id', $id)->delete('item_unit')) {
                  $this->session->set_flashdata('error', "Failed. Unit is already in use");
          }else {
               $this->session->set_flashdata('success', 'Success. Unit has been successfully deleted');
          }

		return;
	}

}
