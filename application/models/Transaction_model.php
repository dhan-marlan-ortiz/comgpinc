<?php
class Transaction_model extends CI_Model {

	function getRetailSalesItem($transaction_id) {
		return $this->db->distinct()->select("t.name as type, CONCAT(b.name, ' ', n.name, ' (', s.quantity, 'x @ ' , s.selling_price, '/', u.unit, ')') as product")->from('sales as s')
		->where('s.reference_id', $transaction_id)
		->join('retails as r', 'r.registration_id = s.transaction_id')
		->join('item_type as t ', 't.id = r.type_fk')
		->join('item_brand as b', 'b.id = r.brand_fk')
		->join('item_name as n', 'n.id = r.name_fk')
		->join('item_unit as u', 'u.id = r.unit_fk')
		->get()
		->result_array();
	}

	function getRetailSalesAmount($transaction_id) {
		return $this->db->select('SUM(selling_price * quantity) as total_price')
				    ->from('sales')
				    ->where('reference_id', $transaction_id)
				    ->get()->row_array()['total_price'];
	}

	function createReturn($values) {
		$this->db->insert('transaction_returns', $values);
		return;
	}

	function getReturn($transactionId = null) {
		$query = $this->db->select('t.*, b.name as branch_name, u.first_name, u.last_name, s.is_void as sales_is_void')->
				from('transaction_returns as t')->
				where('t.transaction_id', $transactionId)->
				join('branches as b', 'b.id = t.branch_fk')->
				join('users as u', 'u.id = t.user_fk')->
				join('sales as s', 's.sales_id = t.sales_id')->
				get();

		$result = $query->result_array();

		return $result;
	}

	function create($values, $category = null) {
		
		if($category == 1) {
			$pawn_cashout = array( 'value' => $values['value'], 'date' => $values['date'], 'transaction_id' => $values['transaction_id'], 'branch_fk' => $values['branch_fk'], 'category' => 1 );
			$insert_pawn_cashout = $this->db->insert('transaction_cashouts', $pawn_cashout);
		}

		$insertNewTransaction = $this->db->insert('transactions', $values);
		$result = $this->db->affected_rows();

		/* get transaction ID of last input */
		$insert_id = $this->db->insert_id();
		$getTransactionId = $this->db->get_where('transactions', array('id' => $insert_id))->row_array();
		$transactionId = $getTransactionId['transaction_id'];

		if($result > 0) {
			$this->session->set_flashdata('success', 'New transaction has been successfully added.');
		} else {
			$this->session->set_flashdata('error', 'Failed to add new transaction. Please try again.');
		}
		return $transactionId;
	}

	function getTransactions() {
		$branches = $this->db->select('branch_fk')->from('transactions')->group_by('branch_fk')->get()->result_array();
		$transactions = array();
		foreach ($branches as $b => $branch) {
			$branch_id = $branch['branch_fk'];
			$transactions[$branch_id] = array();
			foreach ($transactions as $t => $trans) {
				$dates = $this->db->select('date')->from('transactions')->where('branch_fk', $branch_id)->group_by('date')->get()->result_array();
				foreach ($dates as $d => $date) {
					$eachDate = $date['date'];
					$details = $this->db->select('t.*, c.first_name, c.last_name, c.middle_name, c.contact, c.gender, type.name as type_name, brand.name as brand_name')
						->from('transactions as t')
						->where(array('t.date'=> $eachDate, 'branch_fk' => $branch_id))
						->join('customers as c', 't.customer_fk = c.id')
						->join('item_type as type', 'type.id = t.type_fk')
						->join('item_brand as brand', 'brand.id = t.brand_fk')
						->get()
						->result_array();
					$transactions[$branch_id][$d]['dates'] = $eachDate;
					$transactions[$branch_id][$d]['details'] = $details;
				}
			}
		}
		return $transactions;
	}

	function getAllTransactions($status = null, $branch = null) {
		$this->db->select("t.*,
					c.first_name as fname, c.last_name as lname, c.middle_name as mname, c.contact as contact, c.gender as gender, c.address as address, c.identification_type as idtype, c.identification_number as idnum, c.remarks as remarks,
					type.name as type_name, 
					brand.name as brand_name, 
					product.name as product_name, 
					cities.name as city, cities.province, 
					b.name as branch_name, 
					cat.category_name");
		$this->db->from("transactions as t");
		$this->db->where("t.status !=", 'Void');

		if($this->session->userdata('role_fk') != "ADMS") {
			$this->db->where("t.branch_fk", $this->session->userdata('branch_fk'));
		}

		if(isset($status) && isset($branch)){
			$this->db->where('t.branch_fk', $branch);
			$this->db->like('t.status', $status, 'both');
		} else if(isset($status)){
			$this->db->like('t.status', $status, 'both');
		} else if(isset($branch)) {
			$this->db->where('t.branch_fk', $branch);
		}

		$this->db->join('customers as c', 'c.id = t.customer_fk');
		$this->db->join('item_type as type', 'type.id = t.type_fk');
		$this->db->join('item_brand as brand', 'brand.id = t.brand_fk');
		$this->db->join('item_name as product', 'product.id = t.name_fk');
		$this->db->join('cities', 'cities.id = c.city_fk');
		$this->db->join('branches as b', 'b.id = t.branch_fk');
		$this->db->join('transaction_categories as cat', 'cat.id = t.category');
		// $this->db->join('transaction_payments as payment', 'payment.transaction_id = t.transaction_id');

		// $this->db->group_by('t.transaction_id');
		$this->db->order_by('t.updated_at', "DESC");
		$query = $this->db->get();

		
		$result = $query->result_array();
		
			foreach ($result as $r => $res) {
				$recentLog = $this->getRecentLog($res['transaction_id'])['last_transaction'];				
				$result[$r]['last_transaction'] = $recentLog;
			}
			
		return $result;
	}

	function getBranchTransactions($branch_fk) {
		$query = $this->db->
				select("t.*,
					c.first_name as fname, c.last_name as lname, c.middle_name as mname, c.contact as contact, c.gender as gender, c.address as address, c.identification_type as idtype, c.identification_number as idnum, c.remarks as remarks,
					type.name as type_name, brand.name as brand_name, product.name as product_name, cities.name as city, cities.province")->
				from("transactions as t")->
				where(array('branch_fk' => $branch_fk))->
				join('customers as c', 'c.id = t.customer_fk')->
				join('item_type as type', 'type.id = t.type_fk')->
				join('item_brand as brand', 'brand.id = t.brand_fk')->
				join('item_name as product', 'product.id = t.name_fk')->
				join('cities', 'cities.id = c.city_fk')->
				get();

		$result = $query->result_array();

		return $result;
	}

	function updateStatus($transactionId, $transactionStatus) {
		$value = array('status' => $transactionStatus);

		$this->db->where('transaction_id', $transactionId);
		$query = $this->db->update('transactions', $value);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Status has been successfully updated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update status. Pleast try again.');
		}

		return $result;
	}

	function voidCashout($transactionId) {
		return $this->db->where('transaction_id', $transactionId)->update('transaction_cashouts', array('is_void' => 1));
	}

	function viewTransaction($transactionId) {
		$query = $this->db->
				select("t.*,
					c.first_name as fname, c.last_name as lname, c.middle_name as mname, c.contact as contact, c.gender as gender, c.address as address, c.identification_type as idtype, c.identification_number as idnum, c.remarks as remarks,
					type.name as type_name, brand.name as brand_name, product.name as product_name, cities.name as city, cities.province, b.name as branch_name, u.first_name, u.last_name")->
				from("transactions as t")->
				where(array('transaction_id' => $transactionId))->
				join('customers as c', 'c.id = t.customer_fk')->
				join('item_type as type', 'type.id = t.type_fk')->
				join('item_brand as brand', 'brand.id = t.brand_fk')->
				join('item_name as product', 'product.id = t.name_fk')->
				join('cities', 'cities.id = c.city_fk')->
				join('branches as b', 'b.id = t.branch_fk')->
				join('users as u', 'u.id = t.user_fk')->
				get();

		$result = $query->row_array();

		return $result;
	}

	function updateSales($transactionId, $values) {

		$this->db->where('reference_id', $transactionId);
		$query = $this->db->update('sales', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}

		return $result;
	}

	function createSales($values) {
		$query = $this->db->insert('sales', $values);
		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->db->where('transaction_id', $values['reference_id']);
			$this->db->update('transactions', array('status' => 'Sold'));

			$this->session->set_flashdata('success', 'New sales transaction has been recorded.');
		} else {
			$this->session->set_flashdata('error', 'Failed. Pleast try again.');
		}

		return $result;
	}

	function getSalesDetails($transactionId) {
		$query = $this->db->
				select("s.*, b.name as branch_name, b.id as branch_id, u.first_name, u.last_name, u.id as user_id")->
				from("sales as s")->
				where('s.reference_id', trim($transactionId))->
				where('s.is_void', 0)->
				join('users as u', 'u.id = s.user_fk')->
				join('branches as b', 'b.id = s.branch_fk')->
				get();

		$result = $query->result_array();

		return $result;
	}

	function getLogs($transactionId) {
		$query = $this->db->select('l.*, u.last_name, u.first_name, b.name as branch_name')->
				from('transaction_logs as l')->
				where(array('l.transaction_id' => $transactionId))->
				join('users as u', 'u.id = l.user_fk')->
				join('branches as b', 'b.id = l.branch_fk')->
				order_by('l.id', 'ASC')->
				get();

		$result = $query->result_array();
		return $result;
	}
	
	function getRecentLog($transactionId) {
		$query = $this->db->select('l.description as last_transaction')->
				from('transaction_logs as l')->
				where(array('l.transaction_id' => $transactionId))->
				order_by('l.id', 'DESC')->
				get();

		$result = $query->row_array();
		return $result;
	}

	function createLog($values) {
		$query = $this->db->insert('transaction_logs', $values);
		$result = $this->db->affected_rows();

		return $result;
	}

	function createPayment($values) {
		$query = $this->db->insert('transaction_payments', $values);
		$result = $this->db->affected_rows();

		return $result;

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success. Payment has been recorded.');
		} else {
			$this->session->set_flashdata('error', 'Failed. Pleast try again.');
		}
	}

	function getPayments($transactionId) {
		$query = $this->db->select('p.*, u.last_name, u.first_name, b.name as branch_name')->
				from('transaction_payments as p')->
				where(array('p.transaction_id' => $transactionId))->
				join('users as u', 'u.id = p.user_fk')->
				join('branches as b', 'b.id = p.branch_fk')->
				get();

		$result = $query->result_array();
		return $result;
	}

	function processRenew($values) {
		$this->db->where('transaction_id', $values['transaction_id']);
		$query = $this->db->update('transactions', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success! Transaction has been renewed');
		} else {
			$this->session->set_flashdata('error', 'Failed. Pleast try again.');
		}
	}

	function updateTransaction($transactionId, $values) {
		$this->db->where('transaction_id', $transactionId);
		$this->db->update('transactions', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}
		return $result;
	}

	function updateCashout($transactionId, $values) {
		$this->db->where('transaction_id', $transactionId);
		$this->db->update('transaction_cashouts', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}
		return $result;
	}
	
	function createTransfer($values) {
		$query = $this->db->insert('transfers', $values);
		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}

		return $result;
	}

	function voidTransfer($transactionId) {
		$this->db->where('transaction_id', $transactionId);
		$this->db->update('transfers', array('is_void' => 1));

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}

		return $result;
	}

	function getTransfers($transactionId = null) {
		$this->db->from('transfers as transfers');
		$this->db->where('transfers.is_void', 0);
		$this->db->where('transfers.is_received', 0);

		if(null !== $transactionId) {
			$this->db->select('transfers.*');

			$this->db->where('transfers.transaction_id', trim($transactionId));

			if($transactionId[5] == 'T') {
				$this->db->join('transactions as transactions', 'transactions.transaction_id = transfers.transaction_id');
			} else if($transactionId[5] == 'P') {
				$this->db->join('purchases as transactions', 'transactions.transaction_id = transfers.transaction_id');
			}

			$this->db->join('item_brand as brand', 'brand.id = transactions.brand_fk');
			$this->db->join('item_name as item', 'item.id = transactions.name_fk');
		} else {
			$this->db->select('transfers.*,
								origin.name as origin_name, origin.id as origin_id,
								destination.name as destination_name, destination.id as destination_id,
								transactions.brand_fk, transactions.name_fk,
								transactions_brand.name as transactions_brand_name,
								transactions_item.name as transactions_item_name,
								transactions_type.name as transactions_type_name,
								transactions.category as transactions_category,
								purchases.category as purchases_category,
								purchases_brand.name as purchases_brand_name,
								purchases_item.name as purchases_item_name,
								purchases_type.name as purchases_type_name
							');

			$this->db->where('transfers.destination', $this->session->userdata('branch_fk'));

			$this->db->join('transactions as transactions', 'transactions.transaction_id = transfers.transaction_id', 'left');
			$this->db->join('item_brand as transactions_brand', 'transactions_brand.id = transactions.brand_fk', 'left');
			$this->db->join('item_name as transactions_item', 'transactions_item.id = transactions.name_fk', 'left');
			$this->db->join('item_type as transactions_type', 'transactions_type.id = transactions.type_fk', 'left');

			$this->db->join('purchases as purchases', 'purchases.transaction_id = transfers.transaction_id', 'left');
			$this->db->join('item_brand as purchases_brand', 'purchases_brand.id = purchases.brand_fk', 'left');
			$this->db->join('item_name as purchases_item', 'purchases_item.id = purchases.name_fk', 'left');
			$this->db->join('item_type as purchases_type', 'purchases_type.id = purchases.type_fk', 'left');
		}
		$this->db->join('branches as origin', 'origin.id = transfers.origin');
		$this->db->join('branches as destination', 'destination.id = transfers.destination');

		$query = $this->db->get();

		$result = $query->result_array();

		return $result;
	}

	function getTransfersOutbound($transactionId = null) {
		$this->db->from('transfers as transfers');
		$this->db->where('transfers.is_void', 0);
		$this->db->where('transfers.is_received', 0);

		if(null !== $transactionId) {
			$this->db->select('transfers.*');

			$this->db->where('transfers.transaction_id', trim($transactionId));

			if($transactionId[5] == 'T') {
				$this->db->join('transactions as transactions', 'transactions.transaction_id = transfers.transaction_id');
			} else if($transactionId[5] == 'P') {
				$this->db->join('purchases as transactions', 'transactions.transaction_id = transfers.transaction_id');
			}

			$this->db->join('item_brand as brand', 'brand.id = transactions.brand_fk');
			$this->db->join('item_name as item', 'item.id = transactions.name_fk');
		} else {
			$this->db->select('transfers.*,
								origin.name as origin_name, origin.id as origin_id,
								destination.name as destination_name, destination.id as destination_id,
								transactions.brand_fk, transactions.name_fk,
								transactions_brand.name as transactions_brand_name,
								transactions_item.name as transactions_item_name,
								transactions_type.name as transactions_type_name,
								transactions.category as transactions_category,
								purchases.category as purchases_category,
								purchases_brand.name as purchases_brand_name,
								purchases_item.name as purchases_item_name,
								purchases_type.name as purchases_type_name
							');

			$this->db->where('transfers.origin', $this->session->userdata('branch_fk'));

			$this->db->join('transactions as transactions', 'transactions.transaction_id = transfers.transaction_id', 'left');
			$this->db->join('item_brand as transactions_brand', 'transactions_brand.id = transactions.brand_fk', 'left');
			$this->db->join('item_name as transactions_item', 'transactions_item.id = transactions.name_fk', 'left');
			$this->db->join('item_type as transactions_type', 'transactions_type.id = transactions.type_fk', 'left');

			$this->db->join('purchases as purchases', 'purchases.transaction_id = transfers.transaction_id', 'left');
			$this->db->join('item_brand as purchases_brand', 'purchases_brand.id = purchases.brand_fk', 'left');
			$this->db->join('item_name as purchases_item', 'purchases_item.id = purchases.name_fk', 'left');
			$this->db->join('item_type as purchases_type', 'purchases_type.id = purchases.type_fk', 'left');
		}
		$this->db->join('branches as origin', 'origin.id = transfers.origin');
		$this->db->join('branches as destination', 'destination.id = transfers.destination');


		$query = $this->db->get();

		$result = $query->result_array();

		return $result;
	}


	function acceptTransfer($transactionId, $branch, $date) {
		$values = array('receiver' => $this->session->userdata('id'),
						'date_received' => date('Y-m-d', strtotime($date)),
						'is_received' => 1
					);
		$this->db->where('transaction_id', $transactionId);
		$this->db->where('is_void', 0);
		$this->db->update('transfers', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}
		return $result;
	}
}
