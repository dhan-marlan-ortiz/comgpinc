<?php
class Vault_model extends CI_Model {

     function sumPawns($select, $branch, $status = null) {
                    $this->db->select($select);
                    $this->db->from('transactions');
                    $this->db->where('branch_fk', $branch);
                    $this->db->where('is_void', 0);
                    $this->db->where('category', 1);

                    if(null !== $status) {
                         $this->db->where('status', $status);
                    }

          $query =  $this->db->get();
          $result = $query->result_array();

          $sum = 0;
          foreach ($result as $r) {
               $sum += $r[$select];
          }
          return $sum;
     }

     function sumPurchase($select, $branch, $status = null) {
          $this->db->select($select);
          $this->db->from('purchases');
          $this->db->where('branch_fk', $branch);
          $this->db->where('is_void', 0);
          $this->db->where('category', 2);

          if(null !== $status) {
               $this->db->where('status', $status);
          }

          $query =  $this->db->get();
          $result = $query->result_array();

          $sum = 0;
          foreach ($result as $r) {
               $sum += $r[$select];
          }
          return $sum;
     }

}
