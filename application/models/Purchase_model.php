<?php
class Purchase_model extends CI_Model {

	function getPurchasesReport($location, $status, $date_start, $date_end) {
          
          $this->db->select("'#', p.transaction_id, p.branch_fk, l.name as location, type.name as item_type, CONCAT(brand.name, ' ', name.name) as product, p.description, p.status, DATE_FORMAT(p.date_created, '%d %b %Y') as date_purchased, p.buy_cost");
          $this->db->from("purchases as p");


          // Location OR group
          /*
          $this->db->group_start();
          foreach ($location as $loc) {
               $this->db->or_where('p.branch_fk', $loc);
          } */
          $this->db->where('p.branch_fk', $location);

          // $this->db->group_end();

          // Status OR group
          $this->db->group_start();
          foreach ($status as $stat) {
               $this->db->or_where('p.status', $stat);
          }
          $this->db->group_end();

          $this->db->where('p.date_created >=', $date_start);
          $this->db->where('p.date_created <=', $date_end);
          // $this->db->where('s.is_void', 0);

          $this->db->join('item_type as type', 'type.id = p.type_fk');
          $this->db->join('item_brand as brand', 'brand.id = p.brand_fk');
          $this->db->join('item_name as name', 'name.id = p.name_fk');
          $this->db->join('branches as l', 'l.id = p.branch_fk');
          // $this->db->join('sales as s', 's.transaction_id = p.transaction_id', 'left');


          $this->db->order_by('p.date_created');

          $query = $this->db->get();
          $results = $query->result_array();

          foreach ($results as $r => $res) {
          	$match = array('transaction_id' => $res['transaction_id'], 'is_void' => 0);
          	$results[$r]['date_sold'] = date("d M Y", strtotime($this->db->get_where('sales', $match)->row_array()['date_sold']));
          	$results[$r]['selling_price'] = $this->db->get_where('sales', $match)->row_array()['selling_price'];          	
          	$results[$r]['#'] = $r + 1;

          	unset($results[$r]['branch_fk']);
          }

          return $results;
     }

	function getPurchaseIdFromSales($transactionId) {
		$query = $this->db->get_where('sales', array('reference_id' => $transactionId, 'is_void' => 0));;
		$result = $query->row_array()['transaction_id'];

		return $result;
	}

	function create($values) {
		$purchase_cashout = array( 'value' => $values['buy_cost'],
									'date' => $values['date_created'],
									'transaction_id' => $values['transaction_id'],
									'branch_fk' => $values['branch_fk'],
									'category' => 2 );
		$insert_cashout = $this->db->insert('transaction_cashouts', $purchase_cashout);

		$insertNewPurchase = $this->db->insert('purchases', $values);
		$result = $this->db->affected_rows();

		/* get transaction ID of last input */
		$insert_id = $this->db->insert_id();
		$getPurchaseId = $this->db->get_where('purchases', array('id' => $insert_id))->row_array();
		$purchaseId = $getPurchaseId['transaction_id'];

		if($result > 0) {
			$this->session->set_flashdata('success', 'New purchase has been successfully added.');
		} else {
			$this->session->set_flashdata('error', 'Failed to add new purchase. Please try again.');
		}
		return $purchaseId;
	}

	function viewPurchase($transactionId) {
		$query = $this->db->
				select("t.*,
					type.name as type_name,
					brand.name as brand_name,
					product.name as product_name,
					b.name as branch_name, b.id as branch_id,
					u.id as user_id, u.first_name as first_name, u.last_name as last_name
					")->
				from("purchases as t")->
				where(array('transaction_id' => $transactionId))->
				join('item_type as type', 'type.id = t.type_fk')->
				join('item_brand as brand', 'brand.id = t.brand_fk')->
				join('item_name as product', 'product.id = t.name_fk')->
				join('branches as b', 'b.id = t.branch_fk')->
				join('users as u', 'u.id = t.user_fk')->
				get();

		$result = $query->row_array();

		return $result;
	}

	function getQuantity($transactionId) {
		$query = $this->db->where('transaction_id', $transactionId)->count_all_results('purchases');
		return $query;
	}

	function getStocks($transactionId) {
		$filters = array('transaction_id' => $transactionId, 'is_available' => 1);
		$query = $this->db->get_where('purchases', $filters);

		return $query->num_rows();
	}

	function updateStocks($match, $values) {
		$this->db->where($match);
		$this->db->update('purchases', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Stocks has been successfully updated.');
		} else {
			$this->session->set_flashdata('error', 'Failed to update stocks. Pleast try again.');
		}
		return $result;
	}

	function updatePurchase($transactionId, $values) {
		$this->db->where('transaction_id', $transactionId);
		$this->db->update('purchases', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Success');
		} else {
			$this->session->set_flashdata('error', 'Failed');
		}
		return $result;
	}

	function getPurchase() {

		$this->db->select("t.status, CONCAT(DATE_FORMAT(date, '%Y-%m-%d')) as date, type.name as item_type, CONCAT(brand.name, ' ', product.name) as item_name, t.description as item_description, t.transaction_id, b.name as location");

		$this->db->from("purchases as t");
		$this->db->where("t.is_void !=", 1);
		if($this->session->userdata('role_fk') != "ADMS") {
			$this->db->where("t.branch_fk", $this->session->userdata('branch_fk'));
		}

		$this->db->join('item_type as type', 'type.id = t.type_fk');
		$this->db->join('item_brand as brand', 'brand.id = t.brand_fk');
		$this->db->join('item_name as product', 'product.id = t.name_fk');
		$this->db->join('branches as b', 'b.id = t.branch_fk');
		$this->db->join('users as u', 'u.id = t.user_fk');
		$query = $this->db->get();

		$results = $query->result_array();

		return $results;
	}

	function createSales($values) {
		$query = $this->db->insert('sales', $values);
		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'New sales transaction has been recorded.');
		} else {
			$this->session->set_flashdata('error', 'Failed. Pleast try again.');
		}

		return $result;
	}

	function updateStatus($transactionId, $values) {
		$this->db->where('transaction_id', $transactionId)->update('purchases', $values);

		$result = $this->db->affected_rows();

		if($result > 0) {
			$this->session->set_flashdata('success', 'Status update success');
		} else {
			$this->session->set_flashdata('error', 'Status update failed');
		}

		return;
	}

	function getSalesDetails($transactionId) {
		$query = $this->db->
		select("s.*, b.name as branch_name, b.id as branch_id, u.first_name, u.last_name, u.id as user_id")->
		from("sales as s")->
		where( array('s.transaction_id' => trim($transactionId), 's.is_void' => 0))->
		join('users as u', 'u.id = s.user_fk')->
		join('branches as b', 'b.id = s.branch_fk')->
		get();

		$result = $query->result_array();
		return $result;
	}

	function countSales($transactionId) {
		$this->db->select_sum('quantity');
		$this->db->where(array('reference_id' => $transactionId));
		$query = $this->db->get('sales');

		return $query->conn_id;
	}
}
