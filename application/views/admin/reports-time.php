<form class="needs-validation" action="<?php echo base_url('Report/time'); ?>" method="post" id="report-form" autocomplete="off" novalidate>
     
     <?php 
          echo report_daterange($title, $date_start, $date_end, ($reports ? true : false));
          // print_pre($reports);
          if($reports) {          		
               $sum_exempt = array("#", "employee_id", "employee_name", "branch", "date", "time_in", "time_out");
               report_table($reports, $sum_exempt, null);
               
               echo reportFooterCTA(true);
          } else {
               result_not_found(base_url('Report'), "bg-white border mt-5 border-radius-2");
          }
     ?>


     
</form>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          validateForm();
          clearFields();
          if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');          
          }     
          clickedSubmit();
     });
     $(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>
