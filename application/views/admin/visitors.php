<style>
     .dataTables_length, .dataTables_filter {
          float: right !important;
          margin-left: 12px;
          margin-bottom: 12px;
          display: inline-block;
     }
</style>

<?php echo admin_settings_tab('Visitors'); ?>
<div class="card border-top-0">
    <div class="card-body">                    
		<?php 
			echo tab_header("Visitors", "position-absolute");			
		?>
		<?php
			if($results) {
				$options = array('tbody_class' => "no-border");
				admin_table($results, $options);	
			}
		?>
		<br>
		<?php 
			echo tab_header("Summary");			
		?>
		<div class="d-inline-block mr-5 align-top">
			<p>
				<span class="font-weight-medium d-block mb-2">Connections:</span>
				<span class="text-monospace"> <?php echo $connections . " connections since " . date('m-d-Y', strtotime($first_connection)); ?> </span>
			</p>
		</div>
		
		<div class="d-inline-block mr-5 align-top">
			<p>
				<span class="font-weight-medium">Internet Protocols:</span>
				<ol class='text-monospace pl-4'>
				<?php 
					foreach ($ip as $p) {
						echo "<li>" . $p['ip'] . "</li>";
					}
				?>
				</ol> 
			</p>
		</div>

		<div class="d-inline-block mr-5 align-top">
			<p>
				<span class="font-weight-medium">Users:</span>
				<ol class='text-monospace pl-4'>
				<?php 
					foreach ($user as $u) {
						echo "<li>". $u['user'] . "</li>";
					}
				?> 
				</ol>
			</p>	
		</div>

	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/admin.js'); ?>"></script>
<script>
	$(document).ready( function() {
		initDataTable("#admin-table");	
		// moveToToolbar(".dataTables_length");
		// moveToToolbar(".dataTables_filter");	
     });
</script>