<?php
     $date = date('Y-m-d');
     $role_fk = $this->session->userdata('role_fk');
     $branch_fk = $this->session->userdata['branch_fk'];
?>

     <div class="row narrow-gutters mb-4">
          <div class="col-12 mb-2">
               <h4 class="font-weight-bold"><?php echo $title; ?> / Transaction Status</h4>
               <p class="text-muted"></p>
          </div>
     </div>
     <div class="card">
          <div class="card-body">
               <form class="needs-validation" action="<?php echo base_url('Report/pawnRecord'); ?>" method="post" novalidate id="report-form" autocomplete="off">
                    <div class="row narrow-gutters mb-2">
                         <div class="col-12 col-sm-4 col-md-4 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Location
                                        <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <select class="form-control has-loader" multiple="multiple" name="location[]" required>
                                        <?php
                                        foreach ($branches as $fb) {
                                             echo "<option value='" . $fb['id'] . "' " . (isset($_POST['location']) && in_array($fb['id'], $_POST['location']) ? "selected" : "" ) . ">" .
                                                       $fb['name'] . " (" . $fb['id'] . ")" .
                                                  "</option>";
                                             }
                                        ?>
                                   </select>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4 col-md-4 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Category
                                        <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <select class="form-control has-loader" multiple="multiple" name="category[]" required>
                                        <option value="1" <?php echo (isset($_POST['category']) && in_array("1", $_POST['category']) ? "selected" : "" );?> >PAWN</option>
                                        <option value="2" <?php echo (isset($_POST['category']) && in_array("2", $_POST['category']) ? "selected" : "" );?> >PURCHASE</option>
                                        <option value="3" <?php echo (isset($_POST['category']) && in_array("3", $_POST['category']) ? "selected" : "" );?> >RETAIL</option>
                                   </select>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4 col-md-4 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Status
                                        <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <select class="form-control has-loader" multiple="multiple" name="status[]" required>
                                        <option value="Due" <?php echo (isset($_POST['status']) && in_array("Due", $_POST['status']) ? "selected" : "" );?> >
                                             DUE
                                        </option>
                                        <option value="Expired" <?php echo (isset($_POST['status']) && in_array("Expired", $_POST['status']) ? "selected" : "" );?> >
                                             EXPIRED
                                        </option>
                                        <option value="Grace Period" <?php echo (isset($_POST['status']) && in_array("Grace Period", $_POST['status']) ? "selected" : "" );?> >
                                             GRACE PERIOD
                                        </option>
                                        <option value="In Contract" <?php echo (isset($_POST['status']) && in_array("In Contract", $_POST['status']) ? "selected" : "" );?> >
                                             IN CONTRACT
                                        </option>
                                        <option value="Freeze" <?php echo (isset($_POST['status']) && in_array("Freeze", $_POST['status']) ? "selected" : "" );?> >
                                             FREEZE
                                        </option>
                                        <option value="For Sale" <?php echo (isset($_POST['status']) && in_array("For Sale", $_POST['status']) ? "selected" : "" );?> >
                                             FOR SALE
                                        </option>
                                        <option value="For Repair" <?php echo (isset($_POST['status']) && in_array("For Repair", $_POST['status']) ? "selected" : "" );?> >
                                             FOR REPAIR
                                        </option>
                                        <option value="For Transfer" <?php echo (isset($_POST['status']) && in_array("For Transfer", $_POST['status']) ? "selected" : "" );?> >
                                             FOR TRANSFER
                                        </option>
                                        <option value="For Disposal" <?php echo (isset($_POST['status']) && in_array("For Disposal", $_POST['status']) ? "selected" : "" );?> >
                                             FOR DISPOSAL
                                        </option>
                                        <option value="Sold" <?php echo (isset($_POST['status']) && in_array("Sold", $_POST['status']) ? "selected" : "" );?> >
                                             SOLD
                                        </option>
                                        <option value="Repurchased" <?php echo (isset($_POST['status']) && in_array("Repurchased", $_POST['status']) ? "selected" : "" );?> >
                                             REPURCHASED
                                        </option>
                                        <option value="Disposed" <?php echo (isset($_POST['status']) && in_array("Disposed", $_POST['status']) ? "selected" : "" );?> >
                                             DISPOSED
                                        </option>
                                   </select>
                              </div>
                         </div>
                         <div class="col-12 col-sm-8 col-md-6 col-xl-4">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Date Range Filter
                                        <a href="#" class="clear-input float-right text-decoration-none " title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <div class="input-group">
                                        <input type="text" placeholder="Start Date" class="form-control has-loader datepicker datepicker-left text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_start" value="<?php echo (isset($_POST['date_start']) ? $_POST['date_start'] : "" );?>" required>
                                        <input type="text" placeholder="End Date" class="form-control has-loader datepicker datepicker-right text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_end" value="<?php echo (isset($_POST['date_end']) ? $_POST['date_end'] : "" );?>" required>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4 col-md-6 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="">&nbsp;</label>
                                   <input type="submit" name="generate" value="generate" class="btn btn-secondary btn-icon-text d-block text-white w-100" >
                              </div>
                         </div>
                    </div>
               </form>
          </div>
          <div class="card-body">
               <div class="table-responsive">
                    <table class="table table-bordered">
                         <thead>
                              <tr>
                                   <th>Code</th>
                                   <th>Location</th>
                                   <th>User</th>
                                   <th>Customer</th>
                                   <th>Contact</th>
                                   <th>Item Type</th>
                                   <th>Product</th>
                                   <th>Serial</th>
                                   <th>Category</th>
                                   <th>Status</th>
                                   <th>Entry Date</th>
                                   <th>Debit</th>
                                   <th>Credit</th>
                                   <th>Description</th>
                              </tr>
                         </thead>
                         <tbody>
                         <?php
                         $total_debit = 0;
                         $total_credit = 0;
                         if(isset($transactions)) {
                              foreach ($transactions as $t => $trans) {
                                   $appraisal_value = $trans['value'];
                                   $sold_amount = 0;
                                   $total_credit += $appraisal_value;

                                   $customer = $trans['last_name'] . ",&ensp;" . $trans['first_name'] . "&ensp;" . $trans['middle_name'];
                                   $contact = $trans['contact'];
                                   $branch = $trans['branch'] . "&ensp;(" . $trans['branch_fk'] . ")";
                                   $item_type = $trans['type'];
                                   $product = $trans['brand'] . " " . $trans['name'] . ($trans['description'] ? " - " . $trans['description'] : "");
                                   $category = $trans['category'];
                                   $transaction_id = $trans['transaction_id'];
                                   $serial = $trans['serial'];
                                   $category_name = $trans['category_name'];
                                   $status = $trans['status'];

                                   echo "<tr class='" . ($t % 2 == 0 ? 'bg-dim' : '')  . "' >";
                                   echo "    <td>
                                                  <a href='" . base_url('Transaction?search=') . $transaction_id . "' class='text-reset' >" .
                                                       $transaction_id .
                                                  "</a>
                                             </td>";
                                   echo "    <td>" . $trans['branch_fk'] . "</td>";
                                   echo "    <td>" . $trans['user_fk'] . "</td>";
                                   echo "    <td>" . $customer . "</td>";
                                   echo "    <td>" . $contact . "</td>";
                                   echo "    <td>" . $item_type . "</td>";
                                   echo "    <td>" . $product . "</td>";
                                   echo "    <td>" . $serial . "</td>";
                                   echo "    <td>" . $category_name . "</td>";
                                   echo "    <td>" . $status . "</td>";
                                   echo "    <td>" . date("d M Y", strtotime($trans['date']) ) . "</td>";


                                   // debit
                                   echo "    <td class='text-right'>";
                                                  if ($category != 1) { // purcase or retail
                                                       $sold_amount = $trans['amount'];
                                                       $total_debit += $sold_amount;
                                                       echo number_format($sold_amount, 2);
                                                  }
                                   echo "    </td>";
                                   // credit
                                   echo "    <td class='text-right'>";
                                                  if ($category == 1) {
                                                       echo number_format($appraisal_value, 2);
                                                  }
                                   echo      "</td>";

                                   echo "    <td>";
                                                  if ($category == 1) {
                                                       echo "Purchase Item";
                                                  } else {
                                                       echo "Sold Item";
                                                  }
                                   echo "    </td>";

                                   echo "</tr>";

                                   $payments = $this->Report_model->getPayments($transaction_id);
                                   if(isset($payments)) {
                                        foreach ($payments as $p => $pay) {
                                             $charge = $pay['penalty_charge'];
                                             $fee = $pay['transaction_fee'];
                                             $payment_type = $pay['status'];
                                             $payment_description = "";
                                             $payment_amount = 0;
                                             if($payment_type == "Renew") {
                                                  $payment_description = "Renewal Charges";
                                                  $payment_amount = $charge + $fee;
                                             } else if($payment_type == "Repurchased") {
                                                  $payment_description = "Repurchase Payment";
                                                  $payment_amount = $charge + $fee + $appraisal_value;
                                             }

                                             echo "<tr class='" . ($t % 2 == 0 ? 'bg-dim' : '')  . "' >";
                                             echo "    <td>" . $pay['transaction_id'] . "</td>";
                                             echo "    <td>" . $pay['branch_fk'] . "</td>";
                                             echo "    <td>" . $pay['user_fk'] . "</td>";
                                             echo "    <td>" . $customer . "</td>";
                                             echo "    <td>" . $contact . "</td>";
                                             echo "    <td>" . $item_type . "</td>";
                                             echo "    <td>" . $product . "</td>";
                                             echo "    <td>" . $serial . "</td>";
                                             echo "    <td>" . $category_name . "</td>";
                                             echo "    <td>" . $status . "</td>";
                                             echo "    <td>" . date("d M Y", strtotime($pay['date']) ) . "</td>";
                                             echo "    <td class='text-right'>" .  number_format($payment_amount, 2) . "</td>";
                                             echo "    <td></td>";
                                             echo "    <td>" . $payment_description . "</td>";
                                             echo "</tr>";

                                             $total_debit += $payment_amount;
                                        }
                                   }

                                   $sales = $this->Report_model->getSales($transaction_id);
                                   if(isset($sales)) {
                                        foreach ($sales as $s => $sale) {
                                             $selling_price = $sale['selling_price'];
                                             $sales_description = "Sold Item";
                                             echo "<tr class='" . ($t % 2 == 0 ? 'bg-dim' : '')  . "' >";
                                             echo "    <td>" . $sale['reference_id'] . "</td>";
                                             echo "    <td>" . $sale['branch_fk'] . "</td>";
                                             echo "    <td>" . $sale['user_fk'] . "</td>";
                                             echo "    <td>" . $customer . "</td>";
                                             echo "    <td>" . $contact . "</td>";
                                             echo "    <td>" . $item_type . "</td>";
                                             echo "    <td>" . $product . "</td>";
                                             echo "    <td>" . $serial . "</td>";
                                             echo "    <td>" . $category_name . "</td>";
                                             echo "    <td>" . $status . "</td>";
                                             echo "    <td>" . date("d M Y", strtotime($sale['date_sold']) ) . "</td>";
                                             echo "    <td class='text-right'>" .  number_format($selling_price, 2) . "</td>";
                                             echo "    <td></td>";
                                             echo "    <td>" . $sales_description . "</td>";
                                             echo "</tr>";

                                             $total_debit += $selling_price;
                                        }
                                   }
                              }
                         }

                         ?>
                         </tbody>
                         <tfoot class="border-top-double">
                              <tr>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td>
                                        <?php echo number_format($total_debit, 2); ?>
                                   </td>
                                   <td>
                                        <?php echo number_format($total_credit, 2); ?>
                                   </td>
                                   <td></td>
                              </tr>
                         </tfoot>
                    </table>
               </div>

          </div>
     </div>


<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          validateForm();
          clearFields();
     });
     $(window).on('load', function() {
          loader();
     });
</script>
