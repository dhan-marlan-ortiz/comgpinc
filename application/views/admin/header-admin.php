<!DOCTYPE html>
<html lang="en">

<head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

     <meta name="description" content="Community Gadget Phoneshop">
     <meta name="keywords" content="Gadget Pawenshop, Community Gadget Phoneshop">
     <meta name="author" content="Dhan Marlan Ortiz">

     <?php
          echo addGoogleTag();
          // if(base_url() == "//localhost/comgpinc/") {
               // echo "<title>PCM IT Solutions & Services</title>";
               // echo "<link rel='shortcut icon' href='" . base_url('assets/images/pcmall.ico') . "' class='mr-2' alt='logo' />";
               // echo "<script src='//localhost:35729/livereload.js'></script>";
          // }else {
               echo "<title>Community Gadget Phoneshop</title>";
               echo "<link rel='shortcut icon' href='" . base_url('assets/images/favicon.ico') . "' class='mr-2' alt='logo' />";
          // }
     ?>

     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/jquery.dataTables.min.css">

     <!-- plugins:css -->
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>vendors/ti-icons/css/themify-icons.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>vendors/base/vendor.bundle.base.css">
     <!-- End plugin css for this page -->

     <!-- inject:css -->
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/selectize.default.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/style.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/select2.min.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/main.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/dataTable-overwrite.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/select2-overwrite.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/bootstrap-datepicker3.min.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/jquery.mCustomScrollbar.min.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/animate.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/transaction.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/purchase.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/customer.css">
     <!-- <link rel="stylesheet" href="<?php //echo base_url('assets/') ?>css/admin.css"> -->
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/dashboard.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/report.css">
     <!-- endinject -->

     <script src="<?php echo base_url('assets/') ?>js/jquery-3.4.1.min.js"></script>
</head>

<body>

<!-- container scroller -->
<div class="container-scroller">
     <div class="container-fluid page-body-wrapper">

          <?php echo $sidebar; ?>

          <div class="main-panel">
               <div class="content-wrapper">
