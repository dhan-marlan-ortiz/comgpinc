<form class="needs-validation" action="<?php echo base_url('Report/expenses'); ?>" method="post" id="report-form" autocomplete="off" novalidate>
     
     <?php 
          echo report_location_daterange($title, $branch_info, $date_start, $date_end, $branches, ($reports ? true : false), $has_summary = false);
          // print_pre($reports);
          if($reports) {
               $sum_exempt = array("#", "DATE", "LOCATION", "TYPE", "DESCRIPTION");
               $currency_columns = array('amount');

               report_table($reports, $sum_exempt, $currency_columns);                    
               
               echo reportFooterCTA(true);
          } else {
               result_not_found(base_url('Report'), "bg-white border mt-5 border-radius-2");
          }
     ?>


     
</form>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          validateForm();
          clearFields();
          if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');          
          }     
          clickedSubmit();
     });
     $(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>
