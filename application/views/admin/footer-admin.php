          </div>
     </div>
</div>
<footer class="footer">
     <div class="d-sm-flex justify-content-center justify-content-sm-between">
          <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 Comunity Gadget Phoneshop. All rights reserved.</span>

          
          <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">
               Page rendered in {elapsed_time} seconds.
               <!-- Information System <i class="ti-desktop text-primary ml-1"></i> -->
          </span>
     </div>
</footer>

</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="<?php echo base_url('assets/') ?>vendors/base/vendor.bundle.base.js"></script>
<!-- endinject -->

<!-- Plugin js for this page-->
<script src="<?php echo base_url('assets/') ?>vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->

<!-- inject:js -->
<script src="<?php echo base_url('assets/') ?>js/off-canvas.js"></script>
<script src="<?php echo base_url('assets/') ?>js/hoverable-collapse.js"></script>
<script src="<?php echo base_url('assets/') ?>js/template.js"></script>
<script src="<?php echo base_url('assets/') ?>js/todolist.js"></script>
<!-- endinject -->

<!-- Custom js for this page-->
<script src="<?php echo base_url('assets/') ?>js/dashboard.js"></script>

<script src="<?php echo base_url('assets/') ?>js/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/selectize.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/select2.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/jquery-number.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/jquery.mCustomScrollbar.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/moment.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/notify.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/main.js"></script>
<!-- End custom js for this page-->

<?php
     echo $navbar;
     if(null !== $this->session->flashdata('success')) {
          echo '<script type="text/javascript">
                    $.notify("'.$this->session->flashdata('success').'", "success");
               </script>';
     }else if(null !== $this->session->flashdata('error')) {
          echo '<script type="text/javascript">
                    $.notify("'.$this->session->flashdata('error').'", "error");
               </script>';
     }

     if(null !== $this->session->flashdata('modal')) {
          echo $modal = $this->session->flashdata('modal');
          echo "<script>$(function() { $('#".$modal."').modal('show'); })</script>";
     }

     
     echo loadingPageModal();
     echo generatingReportModal();     
?>

</body>
</html>
