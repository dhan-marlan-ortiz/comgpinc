<style>
     .dataTables_wrapper .dataTables_length {
          float: right;
          margin-left: 12px;
          margin-bottom: 12px;
     }
</style>

<?php echo item_settings_tab('product'); ?>

<div class="card border-top-0">
	<div class="card-body">
		<div class="row">			
			<div class="col-md-8 pb-5">
				<div id="type-list-tab">
		      		<?php echo tab_header("Item Name", "position-absolute"); ?>
		      		<div class="table-responsive pb-0">
		      			<table class="table pt-3" id="product-table">
		      				<thead>
		      					<tr class="shadow-sm">
	                                 <th class="border-0 border-bottom-0 bg-light shadow-none no-sort">
	                                         <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>#</span>
	                                    </th>                                             
	                                    <th class="border-0 border-bottom-0 bg-light shadow-none text-left">
	                                         <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Name</span>
	                                    </th>
	                                    <th class="border-0 border-bottom-0 bg-light shadow-none text-left">
	                                         <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Brand</span>
	                                    </th>                                                
	                                    <th class="border-0 border-bottom-0 bg-light shadow-none text-left">
	                                         <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Type</span>
	                                    </th>                                                
	                                    <th class="border-0 border-bottom-0 bg-light shadow-none no-sort">
	                                         <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>&nbsp;</span>
	                                    </th>
	                               </tr>		      					
		      				</thead>
		      				<tbody class="no-border narrow-height">
		      					<?php
		      					foreach ($products as $p => $product) {
		      						echo "<tr>
		      								<td class='text-center'></td>
		      								<td>" . $product['name'] . "</td>
		      								<td>" . $product['brand'] . "</td>
		      								<td>" . $product['type'] . "</td>
		      								<td class='text-right'>		      									
	      										<a href='#' class='btn btn-twitter btn-sm text-monospace updateButton' title='update' data-id='" . $product['id'] . "'  data-name='" . $product['name'] . "' data-brand='" . $product['brand_fk'] . "' data-type='" . $product['type_fk'] . "'>
	      											<small class='monospace'>UPDATE</small>
	      										</a>	      									
	      										<a class='btn btn-danger btn-sm text-monospace ml-1' title='Remove' href='" . base_url('ItemName/remove/') . $product['id'] . "'>
	      											<small class='monospace'>DELETE</small>
	      										</a>			      									
		      								</td>
		      							</tr>";
	      						}
		      					?>
		      				</tbody>
		      				<tfoot class="hidden-footer">
                                  <tr>
                                       <th colspan="5"></th>
                                  </tr>
                             </tfoot>
		      			</table>
		      		</div>               
		      	</div>		
			</div>
			<div class="col-md-4">
				<div id="appraiser-create-tab">
					<?php echo tab_header("New Name"); ?>
		           	<form class="needs-validation" action="<?php echo base_url('ItemName/create'); ?>" method="post" autocomplete="off" id="create-form" novalidate>
		           		<input id="id" type="hidden" name="id" value="">		           		
		           		<div class="form-group mb-3">
		           			<label>Item Type <span class="text-danger">*</span></label>
		           			<select id="select-type" class="form-control text-uppercase" name="type_fk" required></select>
		           			<div class="valid-feedback"> Looks good! </div>
		           			<div class="invalid-feedback">Select item type.</div>
		           		</div>
		           		<div class="form-group mb-3">
		           			<label>Brand Name <span class="text-danger">*</span></label>
		           			<select id="select-brand" class="form-control text-uppercase" name="brand_fk" required></select>
		           			<div class="valid-feedback"> Looks good! </div>
		           			<div class="invalid-feedback">Select brand name. Item type is a prerequisite</div>
		           		</div>
		           		<div class="form-group mb-3">
		           			<label>Item Name <span class="text-danger">*</span></label>
		           			<input id="input-name" type="text" class="form-control text-uppercase" name="name" placeholder="Enter new item name" max-maxlength="100" required>
		           			<div class="valid-feedback"> Looks good! </div>
		           			<div class="invalid-feedback">Enter a unique item name. Item Brand is a prerequisite</div>
		           		</div>
		           		<p class="card-description text-small">
		           			<span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
		           		</p>
		           		<div class="form-group row narrow-gutters form-btns mb-2">
		           			<div class="col">
		           				<button type="button" id="reset" class="btn btn-secondary btn-block text-uppercase text-nowrap mb-2">Reset</button>
		           			</div>
		           			<div class="col">
		           				<button type="submit" id="submit" class="btn btn-primary btn-block text-uppercase text-nowrap mb-2">Save</button>
		           			</div>
		           		</div>
		           	</form>               
		      	</div>		
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
     var typesJSON = <?php echo json_encode($types); ?>;
     var brandsJSON = <?php echo json_encode($brands); ?>;
     var productsJSON = <?php echo json_encode($products); ?>;
</script>

<script type="text/javascript">
	var getTypes = function(types, allBrands, allProducts) {
		var type_option = "<option value='' selected disabled>SELECT</option>";

		$(types).each(function(t, type) {
			var name = type['name'];
			var id = type['id'];
			type_option += "<option value='" + id + "'>" + name.toUpperCase() + "</option>";
		});

		$("#select-type").on("change", function() {
			var brand_id = $(this).val()
			getBrands(allBrands, brand_id, allProducts);
			checkItemDetails();
		});

		$(type_option).appendTo("#select-type");
	}
	var getBrands = function(brands, type_id, allProducts) {
		var brand_option = "<option value='' selected disabled>SELECT</option>";

		$("#select-brand").empty();
		$.map(brands, function(filtered_brand) {
			if (filtered_brand.type_fk == type_id) {
				$(filtered_brand).each(function(b, brand) {
					var name = brand['name'];
					var id = brand['id'];
					brand_option += "<option value='" + id + "'>" + name.toUpperCase() + "</option>";
				});
			}
		});

		$(brand_option).appendTo("#select-brand");

		$("#select-brand").on("change", function() {
			$("#field-brand").text($("#select2-select-brand-container").attr('title'));
			var type_id = $("#select-type").val();
			var brand_id = $("#select-brand").val();
			checkItemDetails();
		});
	}
     var initSelect2 = function() {
          $("select").select2({
               placeholder: "SELECT"
          });
     }
     var initDataTable = function() {
          var table = $('#product-table').DataTable({
               "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 'no-sort'
               }],
               "order": [
                    [1, 'asc']
               ],
			"autoWidth": false
          });

          table.on('order.dt search.dt', function() {
               table.column(0, {
                    search: 'applied',
                    order: 'applied'
               }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
               });
          }).draw();
     }
     var checkItemDetails = function() {
          var select_type = $("#select-type");
          var select_brand = $("#select-brand");
          var input_name = $("#input-name");

          if(select_type.val() == null) {
               select_brand.siblings(".select2").css("pointer-events", "none");
          } else {
               select_brand.siblings(".select2").css("pointer-events", "auto");
          }

          if(select_brand.val() == null) {
               input_name.attr('readonly', 'readonly');
          } else {
               input_name.removeAttr("readonly");
          }
     }
	var validateForm = function() {
		'use strict';
		var forms = document.getElementsByClassName('needs-validation');
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				var is_duplicate = 0;
                    var input_name = $.trim($("#input-name").val().toUpperCase());
				var select_type = $.trim($("#select-type").val());
				var select_brand = $.trim($("#select-brand").val());

				$.map(productsJSON, function(product) {
					var type_fk =  $.trim(product.type_fk)
					var brand_fk =  $.trim(product.brand_fk)
					var name =  $.trim(product.name).toUpperCase();
					if(type_fk == select_type && brand_fk == select_brand && name == input_name) {
						$.notify("Item name " + input_name + " with the same type and brand already exists", "warn");
                              $("#input-name").val("").attr("placeholder", input_name);
                              is_duplicate = 1;
					}

				});

				if (form.checkValidity() === true && is_duplicate == 0) {
					$(this).find('button[type="submit"]').text('Processing');
					$(this).find('button[type="submit"]').attr('disabled', 'disabled');
				} else {
					event.preventDefault();
					event.stopPropagation();
				}

				form.classList.add('was-validated');
			}, false);
		});		
	}
	var update = function() {
		trigger = $(".updateButton");
		reset = $("#reset");
		submit = $("#submit");

		trigger.on("click", function(e) {
			// e.preventDefault();
			var id = $(this).attr('data-id');
			var type = $(this).attr('data-type');
			var brand = $(this).attr('data-brand');
			var name = $(this).attr('data-name');

			$("#id").val(id);
			$("#select-type").val(type).trigger("change");
			$("#select-brand").val(brand).trigger("change");
			$("#input-name").attr('readonly', false).val(name);
			$(".needs-validation").removeClass('was-validated');
			$("#input-name").attr("placeholder", "ENTER NEW ITEM NAME");
			$("#reset").text("Cancel Update");
			$("#submit").text("Save Changes");
			console.log("click");
		});

		reset.on("click", function(e) {
			// e.preventDefault();
			var id = $(this).attr('data-id');
			var type = $(this).attr('data-type');
			var brand = $(this).attr('data-brand');
			var name = $(this).attr('data-name');

			$("#id").val("");
			$("#select-type").val("").trigger("change");
			$("#select-brand").val("").trigger("change");
			$("#input-name").attr('readonly', 'disabled').val("");
			$("#input-name").attr("placeholder", "ENTER NEW ITEM NAME");
			$(".needs-validation").removeClass('was-validated');
			$("#reset").text("Reset");
			$("#submit").text("Save");
		});

	}
	$(function() {
		initSelect2();
		initDataTable();
		getTypes(typesJSON, brandsJSON, productsJSON);
		checkItemDetails();
		validateForm();
		console.log(productsJSON);
	});
	update();
</script>
