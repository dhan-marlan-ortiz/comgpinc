<style>
     .dataTables_wrapper .dataTables_length {
          float: right;
          margin-left: 12px;
          margin-bottom: 12px;
     }
</style>

<?php echo admin_settings_tab('Location'); ?>

<div class="card border-top-0">
	<div class="card-body">
		<div class="row">			
			<div class="col-md-8 pb-5 pb-md-0">
				<!-- locations table -->
				<div class="" id="location-list-tab">			
					<?php echo tab_header("Locations", "position-absolute"); ?>
					<div class="table-responsive pb-0">
						<table class="table pt-3" id="location-table">
							<thead>
								<tr class="shadow-sm">
									<th class="border-0 border-bottom-0 shadow-none no-sort">
										<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>#</span>
									</th>
									<th class="border-0 border-bottom-0 shadow-none text-left">
										<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Code&emsp;</span>
									</th>
									<th class="border-0 border-bottom-0 shadow-none text-left">
										<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Name&emsp;</span>
									</th>
									<th class="border-0 border-bottom-0 shadow-none text-left">
										<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Address&emsp;</span>
									</th>
									<th class="border-0 border-bottom-0 shadow-none no-sort">
										<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'></span>
									</th>
								</tr>
							</thead>
							<tbody class="no-border narrow-height">
								<?php
								foreach ($locations as $location) {
									echo "
									<tr>
										<td class='text-center'></td>
										<td>" . $location['id'] . "</td>
										<td>" . $location['name'] . "</td>
										<td>" . $location['address'] . "</td>
										<td class='text-right'>
											<a href='#' class='btn btn-twitter btn-sm text-monospace updateButton' data-id='" . $location['id'] . "' data-name='" . $location['name'] . "' data-address='" . $location['address'] . "'>
													UPDATE
												</a>
											
										</td>
									</tr>";
								}
								?>
							</tbody>
							<tfoot class="hidden-footer">
                                <tr>
                                     <th colspan="5"></th>
                                </tr>
                           </tfoot>
						</table>
					</div>			
				</div>
			</div>

			<div class="col-md-4">
				<!-- Create location form -->
				<div class="" id="location-create-tab">			
					<?php echo tab_header("New Locations"); ?>
					<form method="post" action="<?php echo base_url('Location'); ?>" class="location-create-form">
						<div class="row narrow-gutters">
							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Code <span class="text-danger">*</span></label>
									<input class="form-control" placeholder="Location Code" type="text" name="id" id="id">
								</div>
							</div>
							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Name <span class="text-danger">*</span></label>
									<input class="form-control" placeholder="Location Name" type="text" name="name" id="name">
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<label>Address <span class="text-danger">*</span></label>
									<input class="form-control" placeholder="Complete Address" type="text" name="address" id="address">
								</div>
							</div>
						</div>
						<p class="card-description">
							<span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
						</p>
						<div class="form-group row form-btns narrow-gutters">
							<div class="col-6">
								<button type="button" class="col btn btn-light border text-uppercase w-100 btn-clear-fields">Clear</button>
							</div>
							<div class="col-6">
								<button type="button" class="col btn btn-primary text-uppercase w-100 create-location-btn">Save</button>
							</div>
						</div>
						<div id="createModal" class="modal fade confirm-modal" role="dialog">
			                    <div class="modal-dialog modal-sm" role="document">
			                         <div class="modal-content">
			                              <div class="modal-header">
			                                   <h5 class="modal-title">Confirmation</h5>
			                                   <button type="button" class="close" data-dismiss="modal">
			                                        <span aria-hidden="true">&times;</span>
			                                   </button>
			                              </div>
			                              <div class="modal-body">
			                                   <p>Are you sure you want to register new location?</p>
			                              </div>
			                              <div class="modal-footer">
			                                   <button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
			                                   <button type="submit" class="col btn btn-primary confirm" name="add">CONFIRM</button>
			                              </div>
			                         </div>
			                    </div>
			               </div>
					</form>			
				</div>
				<!-- Create location form end -->

				<!-- Update location form -->
				<div class="hidden" id="location-update-tab">			
					<?php echo tab_header("Update Locations"); ?>
					<form method="post" action="<?php echo base_url('Location/update'); ?>" id="location-update-form">
						<input type="hidden" name="current_id" id="current_id">
						<div class="row narrow-gutters">
							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Code <span class="text-danger">*</span></label>
									<input class="form-control" placeholder="Location Code" type="text" name="update_location_id" id="update_location_id">
									<input class="form-control" placeholder="" type="hidden" id="check_location_id">
								</div>
							</div>
							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Name <span class="text-danger">*</span></label>
									<input class="form-control" placeholder="Location Name" type="text" name="update_location_name" id="update_location_name">
									<input class="form-control" placeholder="" type="hidden" id="check_location_name">
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<label>Address <span class="text-danger">*</span></label>
									<input class="form-control" placeholder="Complete Address" type="text" name="update_address_name" id="update_address_name">
									<input class="form-control" placeholder="" type="hidden" id="check_address_name">
								</div>
							</div>
						</div>
						<p class="card-description">
							<span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
						</p>
						<div class="form-group row narrow-gutters form-btns">
							<div class="col-6">
								<button type="button" class="btn btn-light border text-uppercase w-100 btn-cancel">Cancel</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-primary mr-2 text-uppercase w-100 confirm-update">Confirm</button>
							</div>
						</div>
						<div id="updateModal" class="modal fade confirm-modal" role="dialog">
							<div class="modal-dialog modal-sm" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Confirmation</h5>
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<p>Are you sure you want to update?</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
										<button type="submit" class="col btn btn-primary confirm" name="update">CONFIRM</button>
									</div>
								</div>
							</div>
						</div>
					</form>			
				</div>
				<!-- Update location form end -->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		 var table = $('#location-table').DataTable({
			"columnDefs": [{
				"searchable": false,
				"orderable": false,
				"targets": 'no-sort'
			}],
			"order": [
				[1, 'asc']
			]
		});

		table.on('order.dt search.dt', function() {
			table.column(0, {
				search: 'applied',
				order: 'applied'
			}).nodes().each(function(cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
	});

	$(".create-location-btn").on('click', function() {
		$(".location-create-form").validate({
			rules: {
				id : { required: true, maxlength: 20 },
				name : { required: true, maxlength: 30 },
				address : { required: true, maxlength: 255 }
			}
		});
		if ($('.location-create-form').valid()) {
			console.log($('.location-create-form').valid());
			$("#createModal").modal('toggle');
			var id = $("#id").val();
			var name = $("#name").val();
			var address = $("#address").val();

			$("#id_preview").html("CODE: <b>" + id + "</b>");
			$("#name_preview").html("NAME: <b>" + name + "</b>");
			$("#address_preview").html("ADDRESS: <b>" + address + "</b>");
		}
		else {
			console.log('not validate');
		}
	});

	$(".btn-clear-fields").on('click', function() {
		$(".location-create-form").validate().resetForm();
	});

	$(".updateButton").on('click', function() {
		var id = $(this).attr("data-id");
		var name = $(this).attr("data-name");
		var address = $(this).attr("data-address");

		$("#location-create-tab").addClass('hidden');
		$("#location-update-tab").removeClass('hidden');

		$(".updateButton").prop("disabled", true);
		$(this).closest('tr').addClass('highlighted');

		$("#update_location_id").val(id);
		$("#update_location_name").val(name);
		$("#update_address_name").val(address);

		$("#check_location_id").val(id);
		$("#check_location_name").val(name);
		$("#check_address_name").val(address);

		$("#current_id").val(id);
	});

	$(".confirm-update").on('click', function() {
		$("#location-update-form").validate({
			rules: {
				update_location_id : { required: true, maxlength: 20 },
				update_location_name : { required: true, maxlength: 30 },
				update_location_name : { required: true, maxlength: 255 }
			}
		});
		if ($('#location-update-form').valid()) {
			var checkID = $("#check_location_id").val();
			var checkName = $("#check_location_name").val();
			var checkAddress = $("#check_address_name").val();

			var inputID = $("#update_location_id").val();
			var inputName = $("#update_location_name").val();
			var inputAddress = $("#update_address_name").val();

			if ((checkID == inputID) && (checkName == inputName) && (checkAddress == inputAddress)) {
				$.notify("No changes were made", "warn");
			}
			else {
				$("#updateModal").modal('toggle');
			}
		}		
	});

	$(".btn-cancel").on('click', function() {
		$("#location-update-tab").addClass('hidden');
		$("#location-create-tab").removeClass('hidden');
		$(".user-row").removeClass('highlighted');
		$(".updateButton").prop("disabled", false);
		var fields = $(this).closest('form').find('input');
		fields.each(function(){
			$(this).val('');
		});
	});

</script>
