<?php
     $user = $this->session->userdata('first_name') . " " . $this->session->userdata('last_name');
     $location = $this->Generic_model->getBranchInfo($this->session->userdata('branch_fk'));
 ?>
<!-- navbar start -->
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
     <div class="text-center navbar-brand-wrapper d-flex align-items-center">
          <a class="navbar-brand brand-logo" href="<?php echo base_url('Dashboard/') ?>">
               <?php
                    // if(base_url() == "//localhost/comgpinc/") {
                         // echo "<img src='" .base_url('assets/images/pcmHeader.png') ."' class='mr-2' alt='logo' />";
                    // }else {
                         echo "<img src='" .base_url('assets/images/logo.png') ."' class='mr-2' alt='logo' />";
                    // }
               ?>
          </a>
          <a class="navbar-brand brand-logo-mini" href="<?php echo base_url('Dashboard/') ?>"><img src="<?php echo base_url('assets/') ?>images/logo-mini.png" alt="logo" /></a>
     </div>
     <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">

          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
               <span class="ti-view-list"></span>
          </button>

          <ul class="navbar-nav navbar-nav-right">
               <!-- 
               <li class="nav-item nav-profile dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                         <span class="text-primary">
                              <?php echo strtolower($user); ?>
                         </span>
                         <img src="<?php echo base_url('assets/') ?>images/faces/face29.png" alt="profile" />
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                         <a class="dropdown-item">
                              <i class="ti-location-pin"></i>
                              <?php echo $location['name'] . " (" . $location['id'] . ")"; ?>
                         </a>
                         <div class="dropdown-divider"></div>
                         <a class="dropdown-item" href="<?php echo base_url('Logout'); ?>">
                              <i class="ti-power-off text-primary"></i> Logout
                         </a>
                    </div>
               </li>
                -->
               <li class="nav-item mx-2">
                    <a class="nav-link d-none d-sm-block " href="#">                         
                         <i class="ti-user">&nbsp;</i><small class='no-wrap'><?php echo $user; ?></small>
                    </a>
               </li>
               <li class="nav-item mx-2">
                    <a class="nav-link d-none d-sm-block " href="#">                         
                         <i class="ti-location-pin">&nbsp;</i><small class='no-wrap'><?php echo $location['name']; ?></small>
                    </a>
               </li>
               <li class="nav-item mx-2">
                    <a class="d-none d-sm-block nav-link text-primary" href="<?php echo base_url('Logout'); ?>">                         
                         <i class="ti-power-off">&nbsp;</i><small class='no-wrap'>SIGN OUT</small>
                    </a>
               </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
               <span class="ti-view-list"></span>
          </button>
     </div>
</nav>
<!-- navbar end-->
