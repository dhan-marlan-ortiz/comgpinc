<div class="table-responsive pb-0 pt-3">
     <table class="table" id="report-table">
          <thead>
               <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Location</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">Type</th>
                    <th class="text-center">Description</th>
                    <th class="text-center text-right">Amount</th>
               </tr>
          </thead>
          <tbody>
               <?php
                    $total = 0;
                    foreach ($reports as $r => $rec):
                         $total += $rec['AMOUNT'];
                         ?>
                    <tr>
                         <td> <?php echo ++$r; ?> </td>
                         <td> <?php echo $rec['LOCATION']; ?> </td>
                         <td> <?php echo date("d M Y", strtotime($rec['DATE'])); ?> </td>
                         <td> <?php echo $rec['TYPE']; ?> </td>
                         <td> <?php echo $rec['DESCRIPTION']; ?> </td>
                         <td class="text-right"><?php echo number_format($rec['AMOUNT'], 2); ?> </td>
                    </tr>
               <?php endforeach; ?>
          </tbody>
          <tfoot">
               <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th> &#8369; <?php echo number_format($total, 2); ?> </th>
               </tr>
          </tfoot>
     </table>
</div>
