<?php
	$role = $this->session->userdata('role_fk');
?>

<div class="row narrow-gutters mb-4">
	<div class="col-12 mb-2">
		<h4 class="font-weight-bold">
			<?php echo $title; ?>
		</h4>
		<p class="text-muted"></p>
	</div>
</div>
<div id="report-dashboard" class="row">

	<form class="needs-validation d-block form-inline" action="<?php echo base_url('Report/journal'); ?>" method="post" id="report-form" autocomplete="off" novalidate>
	<?php 
		$branch_info['name'] = $this->session->userdata('branch_fk');
		$branch_info['id'] = $this->session->userdata('branch_fk');

		$date_start = date("Y-m-01");
		$date_end = date("Y-m-d");
		$branches = array( array('id' => $branch_info['id'], 'name' => $branch_info['name']) );		
		
		echo "<div style='position: absolute; right: -100%; bottom: -100%; height: 0; width: 0;'>";
		echo report_location_daterange($title, $branch_info, $date_start, $date_end, $branches, false, false ); 
		echo "</div>";
	?>
	</form>
	
	<?php if($role == 'EMP') { ?>

	<!-- Cash Journal -->
	<div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="#" data-toggle="modal" data-target="#generate-report-modal"  class="report-tile__inner   delay-1s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-graph-up" viewBox="0 0 16 16">
						<path fill-rule="evenodd"
							d="M0 0h1v15h15v1H0V0zm10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4.9l-3.613 4.417a.5.5 0 0 1-.74.037L7.06 6.767l-3.656 5.027a.5.5 0 0 1-.808-.588l4-5.5a.5.5 0 0 1 .758-.06l2.609 2.61L13.445 4H10.5a.5.5 0 0 1-.5-.5z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Cash Journal </h5>
				</div>
			</a>
		</div>
     </div>

	 <?php 
	} else {
	 ?>
	
	<!-- Cash Journal -->
	<div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/journal'); ?>" class="report-tile__inner   delay-1s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-graph-up" viewBox="0 0 16 16">
						<path fill-rule="evenodd"
							d="M0 0h1v15h15v1H0V0zm10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4.9l-3.613 4.417a.5.5 0 0 1-.74.037L7.06 6.767l-3.656 5.027a.5.5 0 0 1-.808-.588l4-5.5a.5.5 0 0 1 .758-.06l2.609 2.61L13.445 4H10.5a.5.5 0 0 1-.5-.5z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Cash Journal </h5>
				</div>
			</a>
		</div>
     </div>

	
	
	<!-- General Report -->
	<div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/general'); ?>" class="report-tile__inner  ">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-bar-chart-line" viewBox="0 0 16 16">
						<path
							d="M11 2a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v12h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3h1V7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7h1V2zm1 12h2V2h-2v12zm-3 0V7H7v7h2zm-5 0v-3H2v3h2z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> General Report </h5>
				</div>
			</a>
		</div>
	</div>

	 <!-- Pawned Item -->
     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/pawnRecord'); ?>" class="report-tile__inner   delay-2s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-phone" viewBox="0 0 16 16">
						<path
							d="M11 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h6zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z" />
						<path d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Pawned Items </h5>
					<p class="">
						Status, Entry date, Last update, Due date, Appraisal value, Amount due, Total income
					</p>
				</div>
			</a>
		</div>
     </div>

	 <!-- Renewal & Repurchase Payments -->
     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/pawnRenewRepurchase'); ?>" class="report-tile__inner   delay-1s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-cash-stack" viewBox="0 0 16 16">
						<path d="M1 3a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1H1zm7 8a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" />
						<path
							d="M0 5a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V5zm3 0a2 2 0 0 1-2 2v4a2 2 0 0 1 2 2h10a2 2 0 0 1 2-2V7a2 2 0 0 1-2-2H3z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class="">Renewal &amp; Repurchase Payments</h5>
					<p class="">
						Transaction fee, Penalty Charge, Revenue, Profit
					</p>
				</div>
			</a>
		</div>
     </div>

	 <!-- Purchased Items  -->
     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/purchaseRecord'); ?>" class="report-tile__inner   delay-2s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
						<path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Purchased Items </h5>
				</div>
			</a>
		</div>
     </div>

     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/shelf'); ?>" class="report-tile__inner   delay-3s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-columns" viewBox="0 0 16 16">
						<path
							d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V2zm8.5 0v8H15V2H8.5zm0 9v3H15v-3H8.5zm-1-9H1v3h6.5V2zM1 14h6.5V6H1v8z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Shelf Items </h5>
				</div>
			</a>
		</div>
     </div>
	 
	 <!-- Retail Items Inventory  -->
     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Retail/inventoryReport'); ?>" class="report-tile__inner   delay-2s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-bag-check" viewBox="0 0 16 16">
						<path fill-rule="evenodd"
							d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
						<path
							d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Retail Items Inventory </h5>
				</div>
			</a>
		</div>
     </div>

	 <!-- Retail Sales Transaction -->
     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/retailSalesTransaction'); ?>" class="report-tile__inner   delay-3s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-bag-check" viewBox="0 0 16 16">
						<path fill-rule="evenodd"
							d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
						<path
							d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Retail Sales Transaction </h5>
				</div>
			</a>
		</div>
     </div>

	 <!-- Retail Sales Summary -->
     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/retailSalesRecord'); ?>" class="report-tile__inner   delay-4s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-bag-check" viewBox="0 0 16 16">
						<path fill-rule="evenodd"
							d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
						<path
							d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Retail Sales Summary </h5>
				</div>
			</a>
		</div>
     </div>

	 <!-- Fund transactions -->
     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/fund'); ?>" class="report-tile__inner   delay-3s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-wallet" viewBox="0 0 16 16">
						<path
							d="M0 3a2 2 0 0 1 2-2h13.5a.5.5 0 0 1 0 1H15v2a1 1 0 0 1 1 1v8.5a1.5 1.5 0 0 1-1.5 1.5h-12A2.5 2.5 0 0 1 0 12.5V3zm1 1.732V12.5A1.5 1.5 0 0 0 2.5 14h12a.5.5 0 0 0 .5-.5V5H2a1.99 1.99 0 0 1-1-.268zM1 3a1 1 0 0 0 1 1h12V2H2a1 1 0 0 0-1 1z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Fund transactions </h5>
				</div>
			</a>
		</div>
     </div>

	 <!-- Expense Transactions -->
     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/expenses'); ?>" class="report-tile__inner   delay-4s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-receipt-cutoff" viewBox="0 0 16 16">
						<path
							d="M3 4.5a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 1 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zM11.5 4a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1h-1zm0 2a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1h-1zm0 2a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1h-1zm0 2a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1h-1zm0 2a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1h-1z" />
						<path
							d="M2.354.646a.5.5 0 0 0-.801.13l-.5 1A.5.5 0 0 0 1 2v13H.5a.5.5 0 0 0 0 1h15a.5.5 0 0 0 0-1H15V2a.5.5 0 0 0-.053-.224l-.5-1a.5.5 0 0 0-.8-.13L13 1.293l-.646-.647a.5.5 0 0 0-.708 0L11 1.293l-.646-.647a.5.5 0 0 0-.708 0L9 1.293 8.354.646a.5.5 0 0 0-.708 0L7 1.293 6.354.646a.5.5 0 0 0-.708 0L5 1.293 4.354.646a.5.5 0 0 0-.708 0L3 1.293 2.354.646zm-.217 1.198l.51.51a.5.5 0 0 0 .707 0L4 1.707l.646.647a.5.5 0 0 0 .708 0L6 1.707l.646.647a.5.5 0 0 0 .708 0L8 1.707l.646.647a.5.5 0 0 0 .708 0L10 1.707l.646.647a.5.5 0 0 0 .708 0L12 1.707l.646.647a.5.5 0 0 0 .708 0l.509-.51.137.274V15H2V2.118l.137-.274z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Expense Transactions </h5>
				</div>
			</a>
		</div>
     </div>

	 <!-- Time Tracker -->
     <div class="col-12 col-md-6 col-lg-6 col-xl-4">
		<div class="report-tile__container">
			<a href="<?php echo base_url('Report/time'); ?>" class="report-tile__inner   delay-5s">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
						class="bi bi-clock-history" viewBox="0 0 16 16">
						<path
							d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z" />
						<path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z" />
						<path
							d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z" />
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Time Tracker </h5>
				</div>
			</a>
		</div>
     </div> 

	 <?php } ?>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
     var page_loaded = 0;
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          validateForm();
          clearFields();
          if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');          
          }     
          clickedSubmit();
     });
     $(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>