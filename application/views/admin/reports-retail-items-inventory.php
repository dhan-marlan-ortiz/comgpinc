<?php
     $date = date('Y-m-d');
     $role_fk = $this->session->userdata('role_fk');
     $branch_fk = $this->session->userdata['branch_fk'];
?>

     <div class="row narrow-gutters mb-4">
          <div class="col-12 mb-2">
               <h4 class="font-weight-bold"><?php echo $title; ?> / Retail Items Inventory</h4>
               <p class="text-muted"></p>
          </div>
     </div>
     <div class="card">
          <form class="needs-validation" action="<?php echo base_url('Retail/inventoryReport'); ?>" method="post" novalidate id="report-form" autocomplete="off">
          <div class="card-body pb-0">
               <div class="row narrow-gutters mb-2">
                    <div class="col-12 col-sm-8 col-md-4 col-lg-3 col-xl-2">
                         <div class="form-group mb-0">
                              <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                              <label for="" class="d-block">
                                   Location
                                   <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                        <small class="ti-reload"></small>
                                   </a>
                              </label>
                              <select class="form-control has-loader close-on-click" name="location" required>
                                   <option value="" disabled selected>SELECT</option>
                                   <?php
                                   foreach ($branches as $fb) {
                                        echo "<option value='" . $fb['id'] . "' " . (isset($_POST['location']) && $_POST['location'] == $fb['id'] ? "selected" : "" ) . ">" .
                                                  $fb['name'] . " (" . $fb['id'] . ")" .
                                             "</option>";
                                        }
                                   ?>
                              </select>
                         </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2">
                         <div class="form-group">
                              <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                              <label for="">&nbsp;</label>
                              <input type="submit" name="generate" value="generate" class="btn btn-secondary btn-icon-text d-block text-white w-100" >
                         </div>
                    </div>
               </div>
          </div>
          <?php if(isset($retails)) { ?>
          <div class="card-body">
               <div class="table-responsive  pb-0">
                    <table class="table table-bordered">
                         <thead>
                              <tr class="text-center">
                                   <th>#</th>
                                   <th>Location</th>
                                   <th>Item Type</th>
                                   <th>Product</th>
                                   <th>Unit</th>
                                   <th>Stocks</th>                                   
                              </tr>
                         </thead>
                         <tbody class="border-bottom">
                              <?php
                              foreach ($retails as $r => $retail) {
                                   $stock = $retail['stock'];
                                   echo "<tr>";
                                   echo      "<td class='text-center'>" . ++$r .  "</td>";
                                   echo      "<td>" . $retail['branch_fk'] .  "</td>";
                                   echo      "<td>" . $retail['type'] .  "</td>";
                                   echo      "<td>" . $retail['brand'] . " "  . $retail['name'] . "&ensp;" . $retail['description'] .  "</td>";
                                   echo      "<td>" . $retail['unit'] .  "</td>";
                                   echo      "<td  class='text-center'>" . number_format($stock) .  "</td>";
                                   echo "</tr>";
                              }
                              ?>
                         </tbody>
                    </table>
               </div>
          </div>
          <?php
          echo '<div class="card-body bg-light">';
          echo form_input(array(
                    'type'  => 'submit',
                    'name'  => 'export',
                    'value' => 'Export',
                    'class' => 'btn btn-secondary text-white'
               ));
          echo '</div>';
          }
          ?>
          </form>
     </div>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
$(document).ready( function() {
     initDatePicker();
     initSelect2();
     numberFormat();
     validateForm();
     clearFields();
});
$(window).on('load', function() {
     loader();
});
</script>
