<style>
     .dataTables_wrapper .dataTables_length {
          float: right;
          margin-left: 12px;
          margin-bottom: 12px;
     }
</style>

<?php echo item_settings_tab('brand'); ?>

<div class="card border-top-0">
	<div class="card-body">
		<div class="row">			
			<div class="col-md-8 pb-5">
				<div id="type-list-tab">			
					<?php echo tab_header("Item Brands", "position-absolute"); ?>
					<div class="table-responsive pb-0">
						<table class="table pt-3" id="brand-table">
							<thead>
								<tr class="shadow-sm">
	                                 <th class="border-0 border-bottom-0 bg-light shadow-none no-sort">
	                                         <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>#</span>
	                                    </th>                                             
	                                    <th class="border-0 border-bottom-0 bg-light shadow-none text-left">
	                                         <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Brand</span>
	                                    </th>
	                                    <th class="border-0 border-bottom-0 bg-light shadow-none text-left">
	                                         <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Type</span>
	                                    </th>                                                
	                                    <th class="border-0 border-bottom-0 bg-light shadow-none no-sort">
	                                         <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>&nbsp;</span>
	                                    </th>
	                               </tr>
							</thead>
							<tbody class="no-border narrow-height">
								<?php
								foreach ($brands as $brand) {
									echo "
									<tr>
										<td class='text-center'></td>
										<td>" . $brand['brand'] . "</td>
										<td data-typeid='" . $brand['type_fk'] . "'>
											" . $brand['type'] . "
										</td>
										<td class='text-right'>											
											<a href='#' class='btn btn-twitter btn-sm text-monospace updateButton' title='update' data-id='" . $brand['id'] . "' data-name='" . $brand['brand'] . "' data-type='" . $brand['type_fk'] . "'>
                                		       	<small>UPDATE</small>
                                  			</a>
                                          	<a class='btn btn-danger btn-sm text-monospace ml-1' title='Remove' href='" . base_url('ItemBrand/remove/') . $brand['id'] . "'>
                                               <small>DELETE</small>
                                          	</a>
										</td>
									</tr>";
								}
								?>
							</tbody>
							<tfoot class="hidden-footer">
                                  <tr>
                                       <th colspan="4"></th>
                                  </tr>
                             </tfoot>
						</table>
					</div>			
				</div>
			</div>
			<div class="col-md-4">
				<!-- Create Item Brand form -->
				<div id="brand-create-tab">
					<?php echo tab_header("New Brand"); ?>
					<form method="post" action="<?php echo base_url('ItemBrand'); ?>" class="brand-create-form" autocomplete="off">
						<div class="row narrow-gutters">
							<div class="col-12">
								<div class="form-group selectize-group">
									<label>Type <span class="text-danger">*</span></label>
									<select id="brand-create-select-type" name="type_fk" placeholder="Type or Select Item Type">
										<?php
										foreach ($types as $type) {
											echo "<option value='" . $type['id'] . "'>" . $type['name'] . "</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<label>Brand <span class="text-danger">*</span></label>
									<input class="form-control text-uppercase" placeholder="Item Brand" type="text" name="name" id="name" required>
								</div>
							</div>
						</div>
						<p class="card-description">
							<span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
						</p>
						<div class="form-group row narrow-gutters form-btns">
							<div class="col-6">
								<button type="button" class="btn btn-secondary btn-clear-fields btn-block">CLEAR</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-primary create-brand-btn btn-block">SAVE</button>
							</div>
						</div>
						<div id="createModal" class="modal fade confirm-modal" role="dialog">
			                    <div class="modal-dialog modal-sm" role="document">
			                         <div class="modal-content">
			                              <div class="modal-header">
			                                   <h5 class="modal-title">Confirmation</h5>
			                                   <button type="button" class="close" data-dismiss="modal">
			                                        <span aria-hidden="true">&times;</span>
			                                   </button>
			                              </div>
			                              <div class="modal-body">
			                                   <p>Are you sure you want to register?</p>
			                              </div>
			                              <div class="modal-footer">
			                                   <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
			                                   <button type="submit" class="col btn btn-primary confirm" name="add">CONFIRM</button>
			                              </div>
			                         </div>
			                    </div>
			               </div>
					</form>			
				</div>
				<!-- Create Item Brand form end -->

				<!-- Update Item Brand form -->
				<div class="hidden" id="brand-update-tab">
					<?php echo tab_header("Update"); ?>
					<form method="post" action="<?php echo base_url('ItemBrand/update'); ?>" id="brand-update-form" autocomplete="off">
						<input type="hidden" name="current_id" id="current_id">
						<div class="row narrow-gutters">
							<div class="col-12">
								<div class="form-group selectize-group">
									<label>Type <span class="text-danger">*</span></label>
									<select id="update_type" name="update_type" placeholder="Type or Select Type">
										<?php
										foreach ($types as $type) {
											echo "<option value='" . $type['id'] . "'>" . $type['name'] . "</option>";
										}
										?>
									</select>
									<input type="hidden" class="check-type">
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<label>Brand <span class="text-danger">*</span></label>
									<input class="form-control text-uppercase" placeholder="Item Brand" type="text" name="update_brand_name" id="update_brand_name" required>
									<input class="form-control" placeholder="" type="hidden" id="check_brand_name">
								</div>
							</div>
						</div>
						<p class="card-description">
							<span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
						</p>
						<div class="form-group narrow-gutters row form-btns">
							<div class="col-6">
								<button type="button" class="btn btn-secondary btn-cancel btn-block text-uppercase">Cancel update</button>
							</div>
							<div class="col-6">
								<button type="button" class="btn btn-primary mr-2  btn-block text-uppercase confirm-update">Save changes</button>
							</div>
						</div>
						<div id="updateModal" class="modal fade confirm-modal" role="dialog">
			                    <div class="modal-dialog modal-sm" role="document">
			                         <div class="modal-content">
			                              <div class="modal-header">
			                                   <h5 class="modal-title">Confirmation</h5>
			                                   <button type="button" class="close" data-dismiss="modal">
			                                        <span aria-hidden="true">&times;</span>
			                                   </button>
			                              </div>
			                              <div class="modal-body">
			                                   <p>Are you sure you want to update?</p>
			                              </div>
			                              <div class="modal-footer">
			                                   <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
			                                   <button type="submit" class="col btn btn-primary confirm" name="update">CONFIRM</button>
			                              </div>
			                         </div>
			                    </div>
			               </div>
					</form>			
				</div>					
				<!-- Update Item Brand form end -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var brandsJSON = <?php echo json_encode($brands); ?>;
</script>

<script type="text/javascript">
	$(document).ready(function(){
		var initSelect2 =  $("form select").select2({
			placeholder: "SELECT"
		});

        var table = $('#brand-table').DataTable({
            "columnDefs": [{
                 "searchable": false,
                 "orderable": false,
                 "targets": 'no-sort'
            }],
            "order": [
                 [1, 'asc']
            ]
       });

       table.on('order.dt search.dt', function() {
            table.column(0, {
                 search: 'applied',
                 order: 'applied'
            }).nodes().each(function(cell, i) {
                 cell.innerHTML = i + 1;
            });
       }).draw();
          
	});

	$(".create-brand-btn").on('click', function() {
		$(".brand-create-form").validate({
			rules: {
				name : { required: true, maxlength: 50 }
			}
		});

		if ($('.brand-create-form').valid()) {
			var c_inputName = $.trim($("#name").val().toUpperCase());
			var c_inputType = $("#brand-create-select-type").val();
			var c_is_duplicate = 0;

			$.map(brandsJSON, function(brand) {
				console.log(brand.type_fk + " " + $.trim(brand.name.toUpperCase()));
				if((brand.type_fk == c_inputType) && ($.trim(brand.name.toUpperCase()) == c_inputName)) {
					c_is_duplicate = 1;
				}
		     });

			if(c_is_duplicate == 1) {
				$.notify(c_inputName + " with the same type already exist", "warn");
			} else {
				$("#createModal").modal('toggle');
			}

		}
	});

	$(".updateButton").on('click', function() {
		var id = $(this).attr("data-id");
		var name = $(this).attr("data-name");
		var type = $(this).attr("data-type");

		$("#brand-create-tab").addClass('hidden');
		$("#brand-update-tab").removeClass('hidden');

		$(".updateButton").prop("disabled", true);
		$(this).closest('tr').addClass('highlighted');

		$("#update_brand_name").val(name);
		$("#update_type").val(type).trigger('change.select2');

		$("#check_brand_name").val(name);
		$(".check-type").val(type);

		$("#current_id").val(id);
	});

	$(".confirm-update").on('click', function() {
		$("#brand-update-form").validate({
			rules: {
				update_brand_name : { required: true, maxlength: 50 }
			}
		});

		if ($('#brand-update-form').valid()) {
			var inputName = $.trim($("#update_brand_name").val().toUpperCase());
			var inputType = $("#update_type").val();
			var is_duplicate = 0;

			$.map(brandsJSON, function(brand) {
				console.log(brand.type_fk + " " + $.trim(brand.name.toUpperCase()));
				if((brand.type_fk == inputType) && ($.trim(brand.name.toUpperCase()) == inputName)) {
					is_duplicate = 1;
				}
		     });

			if(is_duplicate == 1) {
				$.notify(inputName + " with the same type already exist", "warn");
			} else {
				$("#updateModal").modal('toggle');
			}

		}
	});

	$(".btn-cancel").on('click', function() {
		$("#brand-update-tab").addClass('hidden');
		$("#brand-create-tab").removeClass('hidden');
		$(".user-row").removeClass('highlighted');
		$(".updateButton").prop("disabled", false);
		var fields = $(this).closest('form').find('input');
		fields.each(function(){
			$(this).val('');
		});
	});

	$(".btn-clear-fields").on('click', function() {
		$(".brand-create-form").validate().resetForm();
	});

</script>
