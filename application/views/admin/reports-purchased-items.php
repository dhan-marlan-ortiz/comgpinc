<form class="needs-validation" action="<?php echo base_url('Report/purchaseRecord'); ?>" method="post" id="report-form" autocomplete="off" novalidate>     
     <div class="row narrow-gutters">
          <div class="col-12 col-lg-6">
               <h4 class="font-weight-bold no-wrap mb-3"><?php echo $title; ?></h4>
               <p class="d-inline mr-4 no-wrap">
                    <i class="ti-location-pin font-weight-medium"></i>&ensp;<?php echo $branch_info['name']; ?>
               </p>
               <p class="d-inline no-wrap">
                    <i class="ti-calendar font-weight-medium"></i>&ensp;<?php echo strtoupper(date("F d, Y", strtotime($date_start))) . ' &ndash; ' . strtoupper(date("F d, Y", strtotime($date_end))); ?>
               </p>
               <div class="d-table mt-2">
                    <div class="d-table-cell">
                         <i class="ti-flag-alt font-weight-medium"></i>&ensp;
                    </div>
                    <div class="d-table-cell">
                         <ul class="list-inline list-inline-barred mb-0">
                              <?php
                              foreach ($status as $s => $stat) {
                                   echo  '<li class="list-inline-item">' . $stat . '</li>';
                              }
                              ?>
                         </ul>
                    </div>
               </div>
          </div>
          <div class="col-12 col-lg-6 text-lg-right no-wrap align-self-end">
          <?php
               echo form_button(
                              array(
                                   'type' => 'button',
                                   'content' => 'GENERATE REPORT',
                                   'class' => 'btn btn-primary text-white mr-1 mt-2',
                                   'data-toggle' => 'modal',
                                   'data-target' => '#generate-report-modal'
                              ));

               if($reports) {
                    echo form_input(
                                   array(
                                        'type'  => 'submit',
                                        'name'  => 'export',
                                        'value' => 'Export to excel',
                                        'class' => 'btn btn-success mr-1 mt-2',
                                        'download' => 'download'
                              ));
               }

               echo anchor( base_url('Report'), 'Close', array(
                         'class' => 'btn btn-light border mt-2')
                    );
          ?>
          </div>                    
     </div>    

     <div class="modal fade" id="generate-report-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-primary">GENERATE REPORT</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">                    
                         <div class="form-group">
                              <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                              <label for="" class="d-block">
                                   Location
                                   <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                        <small class="ti-reload"></small>
                                   </a>
                              </label>
                              <select class="form-control has-loader close-on-click" name="location" required>
                                   <option value="" disabled selected>SELECT</option>
                                   <?php
                                   foreach ($branches as $fb) {
                                        echo "<option value='" . $fb['id'] . "' " . (null !== $_POST['location'] && $fb['id'] == $_POST['location'] ? "selected" : "" ) . ">" .
                                                  $fb['name'] . " (" . $fb['id'] . ")" .
                                             "</option>";
                                   }
                                   ?>
                              </select>
                         </div>                         
                         <div class="form-group">
                              <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                              <label for="" class="d-block">
                                   Status
                                   <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                        <small class="ti-reload"></small>
                                   </a>
                                   <span class="float-right mx-2 text-muted">|</span>
                                   <a href="#" class="select-all float-right text-decoration-none" title="Reset Field">
                                        <small>All</small>
                                   </a>
                              </label>
                              <select class="form-control has-loader" multiple="multiple" name="status[]" required>
                                   <option value="For Sale" <?php echo (isset($_POST['status']) && in_array("For Sale", $_POST['status']) ? "selected" : "" );?> >
                                        FOR SALE
                                   </option>
                                   <option value="For Repair" <?php echo (isset($_POST['status']) && in_array("For Repair", $_POST['status']) ? "selected" : "" );?> >
                                        FOR REPAIR
                                   </option>
                                   <option value="For Transfer" <?php echo (isset($_POST['status']) && in_array("For Transfer", $_POST['status']) ? "selected" : "" );?> >
                                        FOR TRANSFER
                                   </option>
                                   <option value="For Disposal" <?php echo (isset($_POST['status']) && in_array("For Disposal", $_POST['status']) ? "selected" : "" );?> >
                                        FOR DISPOSAL
                                   </option>
                                   <option value="Sold" <?php echo (isset($_POST['status']) && in_array("Sold", $_POST['status']) ? "selected" : "" );?> >
                                        SOLD
                                   </option>
                                   <option value="Disposed" <?php echo (isset($_POST['status']) && in_array("Disposed", $_POST['status']) ? "selected" : "" );?> >
                                        DISPOSED
                                   </option>
                              </select>
                         </div>                         
                         <div class="form-group">
                              <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                              <label for="" class="d-block">
                                   Date Range Filter
                                   <a href="#" class="clear-input float-right text-decoration-none " title="Reset Field">
                                        <small class="ti-reload"></small>
                                   </a>                                        
                              </label>
                              <div class="input-group">
                                   <input type="text" placeholder="Start Date" class="form-control has-loader datepicker datepicker-left text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_start" value="<?php echo (isset($_POST['date_start']) ? $_POST['date_start'] : "" );?>" required>
                                   <input type="text" placeholder="End Date" class="form-control has-loader datepicker datepicker-right text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_end" value="<?php echo (isset($_POST['date_end']) ? $_POST['date_end'] : "" );?>" required>
                              </div>
                         </div>                                                       
                    </div>
                    <div class="modal-footer">
                         <a href="" class="btn btn-light border" data-dismiss="modal">Cancel</a>
                         <input type="submit" name="generate" value="generate" class="btn btn-primary">
                    </div>
               </div>                    
          </div>
     </div>
     
     <?php if($reports) { 
          $sum_exempt = array("#", "transaction_id", "location", "item_type", "product", "description", "status", "date_purchased", "date_sold");
          $currency_columns = array("selling_price", "buy_cost");               
          report_table($reports, $sum_exempt, $currency_columns);
          echo reportFooterCTA(true);
     } else {
          result_not_found(base_url('Report'), "mt-5");
     }
     ?>
     
</form>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
$(document).ready( function() {
     initDatePicker();
     initSelect2();
     numberFormat();
     validateForm();
     clearFields();
     selectAll();
     if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');          
          }     
     clickedSubmit();
});
$(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>
