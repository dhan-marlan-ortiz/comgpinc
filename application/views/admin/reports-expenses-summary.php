<input type="hidden" name="export-summary" value="export-summary">
<div class="table-responsive pb-0 pt-3">
     <table class="table" #report-table>
          <thead class="text-center">
               <?php
                    if($reports) {
                         echo tr_open();
                         foreach ($reports[0] as $th_key => $thead) {
                              echo th($th_key);
                         }
                         echo tr_close();
                    }
               ?>
          </thead>
          <tbody>
               <?php
                    if($reports) {
                         foreach ($reports as $r => $report) {
                              echo tr_open();
                              foreach ($report as $key => $value) {
                                   if($key == "DATE") {
                                        echo td(date("d M Y", strtotime($value)));
                                   } else if($key == "#" || $key == "LOCATION" || $key == "DESCRIPTION") {
                                        echo td($value);
                                   } else {
                                        echo td(
                                                  number_format(floatval($value), 2),
                                                  array('class'=> array('text-right'))
                                             );
                                   }
                              }
                              echo tr_close();
                         }
                    }
                ?>
          </tbody>
          <tfoot>
               <?php
               if($reports) {
                    echo tr_open();
                    foreach ($reports[0] as $tfoot_key => $tfoot) {
                         if($tfoot_key == "#" || $tfoot_key == "LOCATION" || $tfoot_key == "DATE" || $tfoot_key == "DESCRIPTION") {
                              echo td("");
                         } else {
                              $sum = 0;
                              foreach ($reports as $sum_report) {
                                   foreach ($sum_report as $sum_report_key => $sum_report_value) {
                                        if($tfoot_key == $sum_report_key) {
                                             $sum += floatval($sum_report[$sum_report_key]);
                                             echo "<input type='hidden' name='sum_array[]' value='" . $sum_report_key . "'>";
                                        }

                                   }
                              }
                              echo th("&#8369; " . number_format($sum));
                         }
                    }
                    echo tr_close();
               }
               ?>
          </tfoot>
     </table>
</div>
