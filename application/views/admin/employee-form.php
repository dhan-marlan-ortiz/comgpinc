<style>
	#time-registration-modal form .form-group {
		position: relative;
	}
	#time-registration-modal form label.error {
		bottom: -30px;
		left: 0;	
		color: #dc3545;
	    font-size: 12px;
    	font-family: monospace;
	}
	#time-registration-modal form input.error,
	#time-registration-modal form select.error + label + .select2 .select2-selection {
		border-color: rgba(220, 53, 69, 0.5) !important;
	}

	@media(min-width: 576px) {
		.dataTables_length, .dataTables_filter, #add-time-btn {
	          float: right !important;
	          margin-left: 12px;
	          margin-bottom: 12px;
	          margin-top: 0 !important;
	          display: inline-block;
	    }
    }
    @media(max-width: 575px) {
		.dataTables_length {
			float: right !important;
		}
	}
    #attendance-form-toggle {    	
		right: 15px;
		height: 36px;
		padding: 12px;
		font-size: 12px;
		z-index: 1;
    }

    #attendance-section #admin-table tr td:nth-child(1),
    #attendance-section #admin-table tr td:nth-child(2),
    #attendance-section #admin-table tr td:nth-child(3),
    #attendance-section #admin-table tr td:nth-child(4),
    #attendance-section #admin-table tr th:nth-child(1),
    #attendance-section #admin-table tr th:nth-child(2),
    #attendance-section #admin-table tr th:nth-child(3),
    #attendance-section #admin-table tr th:nth-child(4) {
		display: none;
    }
</style>

<?php 
     if(isset($profile['photo'])) {
          $image_path = $profile['photo'];
     } else {
          $image_path = "placeholder-640x480.png";
     }

	echo admin_settings_tab('Employee'); 
?>

<div class="card border-top-0">
	<div class="card-body">          
		
		<?php 
		if($profile) {
			echo '<form id="employee-registration-form" method="POST" action="' . base_url('Employee/update/') . $profile['id'] . '" class="needs-validation locked pb-3 ' . (validation_errors() ? "was-validated" : "") . '" novalidate autocomplete="off">';
			echo '<h5 class="display-4 pb-3"> <small class="font-weight-light text-twitter">Profile</small> </h5>';
		} else {
			echo '<form id="employee-registration-form" method="POST" action="' . base_url('Employee/form') . '" class="needs-validation pb-3 ' . (validation_errors() ? "was-validated" : "") . '" novalidate autocomplete="off">';
			echo '<h5 class="display-4 pb-3"> <small class="font-weight-light text-twitter">Employee Registration Form</small> </h5>';

		}
		echo validation_errors('<p class="text-danger">', '</p>'); 
		?>           				
		<div class="row">
			<div class="col-md-8">
				<div class="row narrow-gutters">
					<div class="col-sm-4 col-md-6 col-xl-4">
						<div class="form-group">
							<label class="text-muted no-wrap">First Name</label>
							<div class="input-group">
								<input  type="text" class="form-control" name="first_name" placeholder="First name" required maxlength="20"
								value="<?php echo ($profile ? $profile['first_name'] : set_value('first_name')); ?>" 
								>			                    	
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-6 col-xl-4">
						<div class="form-group">
							<label class="text-muted no-wrap">Middle Name</label>
							<div class="input-group">
								<input  type="text" class="form-control" name="middle_name" 
								value="<?php echo ($profile ? $profile['middle_name'] : set_value('middle_name')); ?>" 
								placeholder="Middle Name" required maxlength="20">
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-6 col-xl-4">
						<div class="form-group">
							<label class="text-muted no-wrap">Last Name</label>
							<div class="input-group">
								<input  type="text" class="form-control" name="last_name" 
								value="<?php echo ($profile ? $profile['last_name'] : set_value('last_name')); ?>" 
								placeholder="Last Name" required maxlength="20">	
							</div>
						</div>
					</div>					
					<div class="col-sm-6 col-md-6 col-xl-3">
						<div class="form-group">
							<label class="text-muted no-wrap">Date of Birth</label>
							<input  type="text" class="form-control datepicker" name="date_of_birth" 
							value="<?php echo ($profile ? $profile['date_of_birth'] : set_value('date_of_birth')); ?>" 
							placeholder="Date of Birth" required maxlength="10" required>			                    
						</div>
					</div>
					<div class="col-sm-6 col-md-6 col-xl-3">
						<div class="form-group">
							<label class="text-muted no-wrap">Contact Number</label>
							<input  type="text" class="form-control" name="contact" 
							value="<?php echo ($profile ? $profile['contact'] : set_value('contact')); ?>" 
							placeholder="Contact Number" required maxlength="20">			                    
						</div>
					</div>					
					<div class="col-sm-6 col-md-6 col-xl-3">
						<div class="form-group">
							<label class="text-muted no-wrap">Email Address</label>
							<input  type="email" class="form-control" name="email" 
							value="<?php echo ($profile ? $profile['email'] : set_value('email')); ?>" 
							placeholder="Email Address" maxlength="50">			                    
						</div>
					</div>
					<div class="col-sm-6 col-md-12 col-xl-3">
						<div class="form-group">
							<label class="text-muted no-wrap">Time Tracker Password</label>
							<input  type="text" class="form-control" name="password"
							value="<?php echo ($profile ? $profile['password'] : set_value('password')); ?>" 
							placeholder="Time Tracker Password" required maxlength="20">			                    
						</div>
					</div>
					<div class="col-md-12 col-xl-6">
						<div class="form-group">
							<label class="text-muted no-wrap">Complete Address</label>
							<textarea name="address"  rows="3" class="form-control line-height-1-5em" maxlength="250" placeholder="Complete Address" style="min-height:141px;" required><?php echo ($profile ? $profile['address'] : set_value('address')); ?></textarea>
						</div>	              
					</div>
					<div class="col-md-12 col-xl-6">
						<div class="form-group">
							<label class="text-muted no-wrap">Remarks</label>
							<textarea name="remarks"  rows="3" class="form-control line-height-1-5em" maxlength="250" placeholder="Remarks" style="min-height:141px;"><?php echo ($profile ? $profile['remarks'] : set_value('remarks')); ?></textarea>
						</div>	              
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="row narrow-gutters">
					<div class="col-sm-6 col-md-12 col-xl-6">
						<div class="form-group">
							<label class="text-muted no-wrap">Employee Code</label>
							<input  type="text" class="form-control" name="id" 
							value="<?php echo ($profile ? $profile['id'] : ''); ?>" 
							placeholder="<?php echo (set_value('id') ? set_value('id') : 'Employee Number'); ?>" required maxlength="20">			                    
						</div>
					</div>						
					<div class="col-sm-6 col-md-12 col-xl-6">
						<div class="form-group">
							<label class="text-muted no-wrap">Location</label>
							<select name="location"  class="form-control select2">
								<?php
								foreach ($branches as $branch) {
									if( (null !== set_value('location')) && (set_value('location') == $branch['id']) ) {
												// form validation error repoputate
										$selected = "selected";
									} else if((null == set_value('location')) && $default_location == $branch['id']) {
												// default
										$selected = "selected";
									} else {
										$selected = "";
									}
									echo "<option value='" . $branch['id'] . "' " . $selected . ">" . $branch['name'] . "</option>";
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-sm-6 col-md-12 col-xl-6">
						<div class="form-group">
							<label class="text-muted no-wrap">Salary</label>
							<div class="input-group">
								<input  type="text" class="form-control float-format" name="salary" placeholder="Salary" maxlength="20"
								value="<?php echo ($profile ? $profile['salary'] : set_value('salary')); ?>" 
								>			                    	
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-12 col-xl-6">
						<div class="form-group">
							<label class="text-muted no-wrap">Employment Status</label>
							<div class="input-group">
								<input  type="text" class="form-control" name="employment_status" 
								value="<?php echo ($profile ? $profile['employment_status'] : set_value('employment_status')); ?>" 
								placeholder="Employment Status" maxlength="20">
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label class="text-muted no-wrap">TIN</label>
							<div class="input-group">
								<input  type="text" class="form-control" name="tin" placeholder="TIN" maxlength="20"
								value="<?php echo ($profile ? $profile['tin'] : set_value('tin')); ?>" 
								>			                    	
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label class="text-muted no-wrap">SSS</label>
							<div class="input-group">
								<input  type="text" class="form-control" name="sss" 
								value="<?php echo ($profile ? $profile['sss'] : set_value('sss')); ?>" 
								placeholder="SSS" maxlength="20">
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-12">
						<div class="form-group">
							<label class="text-muted no-wrap">Position</label>
							<input  type="text" class="form-control" name="position" 
							value="<?php echo ($profile ? $profile['position'] : set_value('position')); ?>" 
							placeholder="Position" required maxlength="50">
						</div>
					</div>		
					<div class="col-sm-6 col-md-12">
						<div class="form-group">
							<label class="text-muted no-wrap">Date Hired</label>
							<div class="input-group">
								<input  type="text" class="form-control datepicker border-radius-2" name="date_hired" 
								value="<?php echo ($profile ? $profile['date_hired'] : set_value('date_hired')); ?>" 
								placeholder="Date Hired" maxlength="10" required>
							</div>
						</div>
					</div>									
				</div>											
			</div>
		</div>

		<?php if($profile) { ?>
			<div id="btn-wrapper" class='text-center text-sm-right'>
				<div class="tab-content" id="profile-actions-tab">
					<div class="tab-pane active" id="profile-content-default" role="tabpanel">          									
						<a href="<?php echo base_url('Employee'); ?>" class="btn btn-light border btn-icon-text btn-sm border-radius-2">
							<i class="btn-icon-prepend d-none d-sm-inline-block ti-arrow-left"></i>RETURN
						</a>
						
						<a href="#profile-content-update" id="update-profile-btn" class="btn btn-primary btn-icon-text btn-sm ml-1 border-radius-2" data-toggle="tab" role="tab">
							<i class="btn-icon-prepend d-none d-sm-inline-block ti-pencil-alt"></i>UPDATE
						</a>											
					</div>
					<!-- <a href="#types" class="nav-link " data-toggle="tab" role="tab">Expense Types</a> -->
					<div class="tab-pane" id="profile-content-update" role="tabpanel">          
						<a href="<?php echo base_url('Employee/form?profile=') . $profile['id']; ?>" class="btn btn-light border btn-icon-text btn-sm border-radius-2">
							<i class="btn-icon-prepend d-none d-sm-inline-block ti-close"></i>CANCEL
						</a>
						
						<button type="button" class="btn btn-success btn-icon-text btn-sm border-radius-2 ml-1" data-target="#employee-update-modal" data-toggle="modal">
							<i class="btn-icon-prepend d-none d-sm-inline-block ti-save"></i>SAVE CHANGES
						</button>
					</div>
				</div>
			</div>
		<?php } else { ?>
			<div id="btn-wrapper" class='text-center text-sm-right'>
				<a href="<?php echo base_url("Employee"); ?>" class="border-radius-2 btn btn-secondary btn-icon-text btn-sm text-white"> 
					<i class="btn-icon-prepend d-none d-sm-inline-block ti-arrow-left"></i>RETURN
				</a>
				<button type="button" class="border-radius-2 btn btn-light border btn-icon-text btn-sm ml-1" id="employee-registration-clear">
					<i class="btn-icon-prepend d-none d-sm-inline-block ti-eraser"></i>CLEAR
				</button>
				
				<button type="button" class="border-radius-2 btn btn-primary btn-icon-text btn-sm ml-1" data-target="#employee-registration-modal" data-toggle="modal">
					<i class="btn-icon-prepend d-none d-sm-inline-block ti-check"></i>SUBMIT
				</button>		
			</div>							
		<?php } ?>
		
		<div id="employee-registration-modal" class="modal fade confirm-modal" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Confirmation</h5>
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p class="mb-0">Are you sure want to add new employee?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
						<button type="submit" class="col btn btn-success confirm">CONFIRM</button>
					</div>
				</div>
			</div>
		</div>

		<div id="employee-update-modal" class="modal fade confirm-modal" role="dialog">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Confirmation</h5>
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p class="mb-0">Are you sure want to update profile?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
						<button type="submit" class="col btn btn-success confirm">CONFIRM</button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
	
	</div>
	
	<?php if($profile) { ?>
	<div class="card-body border-top-light">          
		<h5 class="display-4 pb-3"> <small class="font-weight-light text-twitter">Photo</small> </h5>
		<div class="row" id="photo-section">
			<div class="col-md-8 col-lg-6">
				<div class="employee-img-wrapper overflow-hidden mb-3 text-center">
					<img src="<?php echo base_url('assets/images/employees/') . $image_path; ?>" class="img-fluid" alt="Image not found." style="">
				</div>
			</div>
			<div class="col-md-4 col-lg-6">
				<div class="d-block">
					<a href="<?php echo base_url('assets/images/employees/') . $image_path; ?>" class="btn btn-success btn-icon-text btn-sm mb-3 mr-1 text-white" target="_blank">
						<span class="">VIEW </span><i class="ti-new-window btn-icon-append ml-0 ml-sm-2"></i>
					</a>
					<a href="<?php echo base_url('assets/images/employees/') . $image_path; ?>" class="btn btn-warning btn-icon-text btn-sm mb-3 mr-1 text-white" download>
						<span class="">DOWNLOAD </span><i class="ti-import btn-icon-append ml-0 ml-sm-2"></i>
					</a>
					<button type="button" class="btn btn-primary btn-icon-text btn-sm mb-3" data-toggle="modal" data-target="#employee-upload-modal">
						<span class="">UPLOAD </span><i class="ti-pencil-alt btn-icon-append ml-0 ml-sm-2"></i>
					</button>
				</div>

				<?php  
				if(null !== $this->session->flashdata('upload_error')) {
					echo "<div class='text-danger mb-3'>" . $this->session->flashdata('upload_error') . "</div>";
				}

				if(null !== $this->session->flashdata('upload_data')) {
					echo "<div class='text-success mb-3'>File has been successfully uploaded</div>";
				}
				?>

				<p class="card-description d-inline-block mr-4 d-md-block mr-md-0">
					<strong class="d-block">Dimension:</strong>
					<small class="d-block text-monospace">Recommended: 640px X 480px</small>
					<small class="d-block text-monospace">Maximum: 1024px X 768px</small>
				</p>
				<p class="card-description d-inline-block mr-4 d-md-block mr-md-0">
					<strong class="d-block">Size:</strong>
					<small class="d-block text-monospace">Recommended: < 50kb</small>
					<small class="d-block text-monospace">Maximum: 2,000kb or 2mb</small>
				</p>
				<p class="card-description d-inline-block mr-4 d-md-block mr-md-0">
					<strong class="d-block">Format:</strong>
					<small class="d-block text-monospace">Recommended: jpeg</small>
					<small class="d-block text-monospace">Valid: gif, jpeg, png</small>
				</p>   

				<p class="card-description d-inline-block mr-4 d-md-block mr-md-0">
					<strong class="d-block">Optimize image:</strong>
					<small class="d-block text-monospace">Resize Image: <a href="https://resizeimage.net" target="_blank">resizeimage.net</a></small>
					<small class="d-block text-monospace">Compress Image: <a href="https://tinypng.com" target_="blank">tinypng.com</a></small>
				</p>   
			</div>
			<div class="modal fade" id="employee-upload-modal">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<form action="<?php echo base_url('Employee/upload/'); ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8">
							<div class="modal-header">
								<h5 class="modal-title">UPLOAD PHOTO</h5>
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body py-4">
								<input type="hidden" name="id" value="<?php echo $profile['id']; ?>">
								<input type="file" name="photo" size="20" required>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
								<button type="submit" class="btn btn-success">UPLOAD</button>
							</div>
						</form>
					</div>
				</div>
			</div>   
		</div>	
	</div>
	<?php if($attendance) { ?>
	<div class="card-body border-top-light time-tracker-section" id="time-tracker">
		<h5 class="display-4 pb-3 position-sm-absolute"> <small class="font-weight-light text-twitter">Attendance</small> </h5>		
		<div class="row" id="attendance-section">
			<div class="col-12">
				<?php        	
					$options = array('tbody_class' => "no-border");
					admin_table($attendance, $options);						
				?>
			</div>
		</div>
		<!-- 
		<button id="add-time-btn" class="btn btn-primary btn-icon-text btn-sm float-sm-right mb-2 mb-sm-0" data-toggle="modal" data-target="#add-time-modal" style="display: none">
			<i class="btn-icon-prepend ti-timer"></i>ADD TIME
		</button>
		 -->
		<div id="add-time-modal" class="modal fade confirm-modal" role="dialog">
			<form action="<?php echo base_url('Employee/addTime'); ?>" autocomplete="off">
				<div class="modal-dialog modal-sm" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add Time</h5>
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row narrow-gutters">
								<div class="col-12 col-sm-6">
									<div class="form-group">
										<label class="text-muted no-wrap">Date</label>
										<input type="text" class="form-control datepicker text-uppercase border-radius-2" id="date" name="date" placeholder="Date" required>																
									</div>
								</div>
								<div class="col-12 col-sm-6">
									<div class="form-group">
										<label class="text-muted no-wrap d-block">Description</label>
										<select name="" id="description" class="form-control select2" required>
											<option value="" disabled selected>Description</option>
											<option value="time in">TIME IN</option>
											<option value="time out">TIME OUT</option>
										</select>
									</div>
								</div>
							</div>

							
								<!-- <label class="text-muted no-wrap d-block">Time</label> -->
							<div class="row narrow-gutters">
								<!-- <div class="col-12 col-sm-8">
									<div class="input-group">
										<input id="hour" type="number" class="form-control text-center border-right-0" placeholder="Hour" step="1" min="1" max="12" maxlength="2" required>
										<div class="input-group-prepend border-top border-bottom">
											<span class="text-dark font-weight-semibold align-self-center blinking">:</span>
										</div>
										<input id="minute" type="number" class="form-control text-center border-left-0" placeholder="Minute" step="1" min="0" max="59" maxlength="2" required>
									</div>
								</div> -->
								<div class="col-12 col-sm-4">
									<div class="form-group">
										<label class="text-muted no-wrap d-block">Hour</label>
										<select name="" id="hour" class="form-control select2" required>
											<!-- <option value="" disabled selected>Hour</option> -->
											<?php 
												for ($i=1; $i <= 12 ; $i++) { 
													echo "<option value='" . str_pad($i, 2, '0', STR_PAD_LEFT) . "'>" . str_pad($i, 2, '0', STR_PAD_LEFT) . "</option>";
												}


											?>
										</select>
									</div>									
								</div>									
								<div class="col-12 col-sm-4">
									<div class="form-group">
										<label class="text-muted no-wrap d-block">Minute</label>
										<select name="" id="minute" class="form-control select2" required>
											<!-- <option value="" disabled selected>Minute</option> -->
											<?php 
												for ($i=0; $i <= 59 ; $i++) { 
													echo "<option value='" . str_pad($i, 2, '0', STR_PAD_LEFT) . "'>" . str_pad($i, 2, '0', STR_PAD_LEFT) . "</option>";
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-4">
									<div class="form-group">
										<label class="text-muted no-wrap d-block">Period</label>
										<select name="" id="time-period" class="form-control select2" required>
											<!-- <option value="" disabled selected>Period</option> -->
											<option value="AM">AM</option>
											<option value="PM">PM</option>
										</select>
									</div>								
								</div>								
							</div>

							<!-- <div class="form-group">
								<div class="row narrow-gutters">
									<div class="col">
										<label class="text-muted no-wrap d-block">Period</label>
										<select name="" id="time-period" class="form-control select2" required>
											<option value="" disabled selected>Period</option>
											<option value="AM">AM</option>
											<option value="PM">PM</option>
										</select>
									</div>								
									<div class="col">
										<label class="text-muted no-wrap d-block">Description</label>
										<select name="" id="description" class="form-control select2" required>
											<option value="" disabled selected>Description</option>
											<option value="time in">TIME IN</option>
											<option value="time out">TIME OUT</option>
										</select>
									</div>
								</div>
							</div> -->
						</div>
						<div class="modal-footer">
							<button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
							<button type="submit" class="col btn btn-primary confirm">ADD</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php } } ?>

	
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/admin.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready( function() {

		// $("#add-time-modal").modal("show");
		initSelect2();
		formValidation();
		clearFields("#employee-registration-clear", "#employee-registration-form input");			
		numberFormat();	

		$('.datepicker').datepicker({
	        autoclose: true,
	        format: 'dd M yyyy',
	        width: '100%' 
	    });		

		$("#update-profile-btn").on("click", function() {
			$("#employee-registration-form").removeClass('locked');
		});
		
		$('textarea').keypress(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
			}
		});

		$("input[name='password']").on({
			keydown: function(e) {
		    if (e.which === 32)
		      return false;
		    },
		  	change: function() {
		    	this.value = this.value.replace(/\s/g, "");
		  	}
		});

		if($(window).width() > 575) {
			moveToToolbar("#btn-wrapper");	
		}

		var table = $('#admin-table').DataTable({
			"columnDefs": [{
				"searchable": false,
				"orderable": false,
				"targets": 0
			}],
			"order": [[1, 'asc']],
			"columnDefs": [{
				"targets": 'no-sort',
				"searchable": false,
				"orderable": false
			}]
		});	

		// $("#add-time-btn").("#admin-table_filter").show();
		$("#admin-table_wrapper").prepend($("#add-time-btn"));
		$("#add-time-btn").show();

		/*
		$("#hour").on("keyup", function(){
			let h = $(this).val();

			if(h > 12)
				h = 12;
			else if(h < 1)
				h = 1;

			$(this).val(h.padStart(2, '0'));
		});

		$("#minute").on("keyup", function(){
			let m = $(this).val();

			if(m > 59)
				m = 59;
			else if(m < 0)
				m = 0;

			$(this).val(m.padStart(2, '0'));
		});
		*/
		$("#hour").on("focus", function(){
			$(this).val("");
		});
		
		$("#hour").on("keyup", function(){
			let h = $(this).val();
			$(this).val(h.padStart(2, '0'));
		});

		$("#minute").on("keyup", function(){
			let m = $(this).val();
			$(this).val(m.padStart(2, '0'));
		});

		$("#minute").on("focus", function(){
			$(this).val("");
		});

	});

	var resizeTimer;
	$(window).on('resize', function(e) {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {			
			if($(window).width() > 575) {
				moveToToolbar("#btn-wrapper");	
			} else {
				$("#btn-wrapper").appendTo("#employee-registration-form");
			}
		}, 250);
	});

	$(window).on('load', function() {
		
	});
</script>