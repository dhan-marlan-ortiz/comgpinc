<form class="needs-validation d-block" action="<?php echo base_url('Report/shelf'); ?>" method="post" id="report-form" autocomplete="off" novalidate>
     <div class="row narrow-gutters">
          <div class="col-12 col-lg-6">
               <h4 class="font-weight-bold no-wrap mb-3"><?php echo $title; ?></h4>
               <p class="d-inline mr-4 no-wrap">
                    <i class="ti-location-pin font-weight-medium"></i>&ensp;<?php echo $branch_info['name']; ?>
               </p>               
          </div>
          <div class="col-12 col-lg-6 text-lg-right no-wrap align-self-end">
          <?php
               echo form_button(
                              array(
                                   'type' => 'button',
                                   'content' => 'GENERATE REPORT',
                                   'class' => 'btn btn-primary text-white mr-1 mt-2',
                                   'data-toggle' => 'modal',
                                   'data-target' => '#generate-report-modal'
                              ));

               if($reports) {
                    echo form_input(
                                   array(
                                        'type'  => 'submit',
                                        'name'  => 'export',
                                        'value' => 'Export to excel',
                                        'class' => 'btn btn-success mr-1 mt-2',
                                        'download' => 'download'
                              ));
               }

               echo anchor( base_url('Report'), 'Close', array(
                         'class' => 'btn btn-light border mt-2')
                    );
          ?>
               <div class="modal fade" id="generate-report-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                         <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="modal-title text-primary">GENERATE REPORT</h5>
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                   </button>
                              </div>
                              <div class="modal-body">
                                   <div class="form-group mb-2">
                                        <label class='d-block text-left'> Location </label>
                                        <select class="form-control has-loader close-on-click" name="location" required>                              
                                             <?php
                                             foreach ($branches as $fb) {
                                                  echo "<option value='" . $fb['id'] . "' " . (isset($_POST['location']) && $fb['id'] == $_POST['location'] ? "selected" : "" ) . ">" .
                                                            $fb['name'] . " (" . $fb['id'] . ")" .
                                                       "</option>";
                                                  }
                                             ?>
                                        </select>
                                   </div>                    
                              </div>
                              <div class="modal-footer">
                                   <a href="" class="btn btn-light border col" data-dismiss="modal">Cancel</a>
                                   <input type="submit" name="generate" value="generate" class="btn btn-primary col">
                              </div>
                         </div>
                    </div>
               </div>
          </div>              
     </div>
     <?php if($reports) { 
          $sum_exempt = array("#", "transaction_id", "category", "item_type", "product", "description", "entry_date", "serial_number");
          $currency_columns = array("selling_price", "buy_cost");               
          report_table($reports, $sum_exempt, $currency_columns);
          echo reportFooterCTA(true);
     } else {
          result_not_found(base_url('Report'), "mt-5");
     }
     ?>     
</form>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
     $(document).ready( function() {     
          initSelect2();
          numberFormat();
          validateForm();     
          
          if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');          
          }     
          clickedSubmit();
});
$(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>
