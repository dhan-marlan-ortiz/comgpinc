<form class="needs-validation d-block" action="<?php echo base_url('Report/fund'); ?>" method="post" id="report-form" autocomplete="off" novalidate>

     <?php 
          echo report_location_daterange($title, $branch_info, $date_start, $date_end, $branches, ($reports ? true : false), $has_summary = false)
      ?>
     
     <?php if($reports) { ?>
          <div class="table-responsive pt-3 pb-0">
               <table class="table" id="report-table">
                    <thead>
                         <tr>
                              <th>#</th>
                              <th class="text-left">Transaction Id</th>
                              <th class="text-left">Date</th>
                              <!-- <th class="text-left">User</th> -->
                              <!-- <th class="text-left">Origin</th> -->
                              <!-- <th class="text-left">Destination</th> -->
                              <th class="text-left">Description</th>
                              <th>Cash In</th>
                              <th>Cash Out</th>
                              <th>Transfer In</th>
                              <th>Transfer Out</th>
                              <th>Fund Balance</th>

                         </tr>
                    </thead>
                    <tbody class="">
                         <?php
                         $total = 0;
                         $cash_in = 0;
                         $cash_out = 0;
                         $transfer_in = 0;
                         $transfer_out = 0;
                         foreach ($reports as $f => $fund) {
                              $transaction_id = $fund['transaction_id'];
                              $location = trim($this->input->post('location'));
                              $origin = $fund['origin'];
                              $destination = $fund['destination'];
                              $user_fk = $fund['user_fk'];
                              $amount = floatval($fund['amount']);
                              $description = $fund['description'];
                              $tag = $fund['tag'];
                              $tag_fk = $fund['tag_fk'];
                              $date = date("d M Y", strtotime($fund['date']));

                              echo "<tr>";
                              echo      "<td class=''>" . ++$f .  "</td>";
                              echo      "<td>" . $transaction_id . "</td>";
                              echo      "<td>" . $date . "</td>";
                              // echo      "<td>" . $user_fk . "</td>";
                              // echo      "<td>" . $origin . "</td>";
                              // echo      "<td>" . $destination . "</td>";
                              echo      "<td>" . $description . "</td>";

                              if($tag_fk == 3 && $location == $destination && $location != $origin) { // transfer in
                                   echo "<td></td><td></td><td class='text-right'>" . number_format($amount, 2) . "</td><td></td>";
                                   $total += $amount;
                                   $transfer_in += $amount;
                              } else if($tag_fk == 3 && $location == $origin && $location != $destination) { // transfer out
                                   echo "</td><td></td><td></td><td></td><td class='text-right'>" . number_format($amount, 2) . "</td>";
                                   $transfer_out += $amount;
                                   $total -= $amount;
                              } else if($tag_fk == 2) { // cash out
                                   echo "<td></td><td class='text-right'>" . number_format($amount, 2) . "</td><td></td><td></td>";
                                   $cash_out += $amount;
                                   $total -= $amount;
                              } else if($tag_fk == 1) { // cash in
                                   echo "<td class='text-right'>" . number_format($amount, 2) . "</td><td></td><td></td><td></td>";
                                   $cash_in += $amount;
                                   $total += $amount;
                              }
                              echo      "<td class='text-right'>" . number_format($total, 2) . "</td>";
                              echo "</tr>";


                         }
                         ?>
                    </tbody>
                    <tfoot>
                         <tr>
                              <th></th> <th></th> <th></th> <th></th>
                              <th> &#8369; <?php echo number_format($cash_in, 2); ?> </th>
                              <th> &#8369; <?php echo number_format($cash_out, 2); ?> </th>
                              <th> &#8369; <?php echo number_format($transfer_in, 2); ?> </th>
                              <th> &#8369; <?php echo number_format($transfer_out, 2); ?> </th>
                              <th> &#8369; <?php echo number_format($total, 2); ?> </th>
                         </tr>
                    </tfoot>
               </table>
          </div>
     <?php
          echo reportFooterCTA(true);
     } else {
          result_not_found(base_url('Report'), "mt-5");
     }
     ?>
     
</form>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          validateForm();
          clearFields();
          if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');          
          }     
          clickedSubmit();
     });

     $(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>
