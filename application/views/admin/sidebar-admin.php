          <?php
               $controller = $this->router->fetch_class();
               $role = $this->session->userdata('role_fk');
          ?>

          <!-- sidebar start -->
          <nav class="sidebar sidebar-offcanvas" id="sidebar">
               <ul class="nav mb-4">
                    <li class="nav-item d-sm-none d-block">
                         <a class="nav-link py-0" href="#">
                              <i class="ti-user menu-icon"></i>
                              <span class="menu-title">
                                   <small>
                                        <?php echo $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'); ?>
                                   </small>
                              </span>
                         </a>
                    </li>
                    <li class="nav-item d-sm-none d-block">
                         <a class="nav-link border-bottom pt-0" href="#">
                              <i class="ti-location-pin menu-icon"></i>
                              <span class="menu-title">
                                   <small>
                                        <?php echo $this->Generic_model->getBranchInfo($this->session->userdata('branch_fk'))['name']; ?>
                                   </small>
                              </span>
                         </a>
                    </li>

                    <?php if($role == "ADMS") { ?>
                    <li class="nav-item <?php echo ($controller == 'Dashboard' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('Dashboard'); ?>">
                              <i class="ti-dashboard menu-icon"></i>
                              <span class="menu-title">Dashboard</span>
                         </a>
                    </li>
                    <?php } ?>

                    <?php if($role == "ADMS" || $role == "EMP") { ?>
                    <li class="nav-item <?php echo ($controller == 'Transaction' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('Transaction'); ?>">
                              <i class="ti-view-list-alt menu-icon"></i>
                              <span class="menu-title">Transactions</span>
                         </a>
                    </li>
                    <li class="nav-item <?php echo ($controller == 'Purchase' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('Purchase'); ?>">
                              <i class="ti-shopping-cart menu-icon"></i>
                              <span class="menu-title">Purchases</span>
                         </a>
                    </li>
                    <li class="nav-item <?php echo ($controller == 'Retail' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('Retail/inventory'); ?>">
                              <i class="ti-layers menu-icon"></i>
                              <span class="menu-title">Retails</span>
                         </a>
                    </li>
                    <li class="nav-item <?php echo ($controller == 'Fund' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('Fund'); ?>">
                              <i class="ti-wallet menu-icon"></i>
                              <span class="menu-title">Funds</span>
                         </a>
                    </li>
                    <li class="nav-item <?php echo ($controller == 'Expense' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('Expense'); ?>">
                              <i class="ti-receipt menu-icon"></i>
                              <span class="menu-title">Expenses</span>
                         </a>
                    </li>
                    <!-- 
                    style="background-image: url('<?php //echo base_url('assets/svg/icon-new-badge.svg'); ?>'); background-repeat: no-repeat; background-position: right; background-size: 30px; "
                     -->
                    <li class="nav-item <?php echo ($controller == 'Inventory' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('Inventory'); ?>">
                              <i class="ti-archive menu-icon"></i>
                              <span class="menu-title">Inventory</span>
                         </a>
                    </li>
                    <li class="nav-item <?php echo ($controller == 'Customer' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('Customer'); ?>">
                              <i class="ti-agenda menu-icon"></i>
                              <span class="menu-title">Customers</span>
                         </a>
                    </li>
                    <li class="nav-item <?php echo ($controller == 'Transfers' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('transfers'); ?>">
                              <i class="ti-truck menu-icon"></i>
                              <span class="menu-title">Item Transfer</span>
                         </a>
                    </li>
                    <li class="nav-item <?php echo ($controller == 'TimeTracker' ? 'active' : ''); ?> ">
                         <a class="nav-link" href="<?php echo base_url('TimeTracker'); ?>">
                              <i class="ti-timer menu-icon"></i>
                              <span class="menu-title">Time Tracker</span>
                         </a>
                    </li>
                    <?php } ?>

                    <?php if($role == "ADMS") { ?>
                         <li class="nav-item <?php if($controller == 'ItemType' || $controller == 'ItemBrand' || $controller == 'Product' ) { echo 'active'; } ?>">                              
                              <a class="nav-link" href="<?php echo base_url('ItemType'); ?>">
                                   <i class="ti-package menu-icon"></i>
                                   <span class="menu-title">Item Settings</span>                                   
                              </a>                              
                         </li>
                         <li class="nav-item <?php if($controller == 'User' || $controller == 'Employee' || $controller == 'Location' || $controller == 'Appraiser' ) { echo 'active'; } ?>">
                              <a class="nav-link" href="<?php echo base_url('User'); ?>">
                                   <i class="ti-settings menu-icon"></i>
                                   <span class="menu-title">Admin Settings</span>
                              </a>                              
                         </li>
                    <?php } ?>
                    
                    <li class="nav-item <?php if($controller == 'Report') { echo 'active'; } ?>">
                         <a class="nav-link" href="<?php echo base_url('Report'); ?>">
                              <i class="ti-pie-chart menu-icon"></i>
                              <span class="menu-title">Reports</span>
                         </a>
                    </li>

                    <li class="nav-item d-sm-none d-block">
                         <a class="nav-link border-top" href="<?php echo base_url('Logout'); ?>">
                              <i class="ti-power-off menu-icon"></i>
                              <span class="menu-title">Logout</span>
                         </a>
                    </li>

               </ul>
          </nav>
          <!-- sidebar end -->
