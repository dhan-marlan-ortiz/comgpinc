<form class="needs-validation d-block form-inline" action="<?php echo base_url('Report/general'); ?>" method="post" id="report-form" autocomplete="off" novalidate>
     
     <?php
     echo $date_start; 
          // echo report_location_date($title, $branch_info, $branches, $date_end, ($reports ? true : false) ); 
          echo report_location_daterange($title, $branch_info, $date_start, $date_end, $branches, ($reports ? true : false), true );

     ?>

     <ul class="nav nav-tabs d-none">          
          <li class="nav-item">
               <input type="submit" name="summary" class="nav-link active" href="<?php echo base_url('Report/general'); ?>" value="Summary">
          </li>
     </ul>     
     
     <?php 
          if($reports) {
               $sum_exempt = array("#", "date", "branch");
               $currency_columns = array('vault', 'shelf', 'accessory', 'cash_begin', 'cash_in', 'cash_out', 'cash_end', 'actual_cash', 'cash_difference', 'total_item_amount', 'grand_total');                    
               
               if($branch_info['id'] == 'summary') {
                    report_table($reports, $sum_exempt, $currency_columns);     
               } else {
                    $currency_counter = array('vault', 'shelf', 'accessory', 'cash_begin', 'cash_in', 'cash_out', 'cash_end', 'actual_cash', 'cash_difference', 'total_item_amount', 'grand_total');                    
                    $item_counter = array('vault_qty', 'shelf_qty', 'accessory_qty', 'total_qty');
                    report_table($reports, $sum_exempt, $currency_columns, $currency_counter, $item_counter);     
               }
               
               echo reportFooterCTA(true);
          } else {
               result_not_found(base_url('Report'), "bg-white border mt-5 border-radius-2");
          }
     ?>
     
</form>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
     var page_loaded = 0;
     function temp_func() {   
          var trigger = $("select[name='location']");
          
          if(trigger.val() == 'summary') {
               $('.has-datepicker-label').find('label').text('Date');
               $('.datepicker-left').parent('.datepicker-item').hide();
               $('.datepicker-right').parent('.datepicker-item').removeClass('w-50');
               $('.datepicker-right').parent('.datepicker-item').addClass('w-100');
          } else {
               $('.has-datepicker-label').find('label').text('Date Range Filter');
               $('.datepicker-left').parent('.datepicker-item').show();
               $('.datepicker-right').parent('.datepicker-item').removeClass('w-100');
               $('.datepicker-right').parent('.datepicker-item').addClass('w-50');
          }
          
          trigger.on("change", function(){
               if(trigger.val() == 'summary') {
                    $('.has-datepicker-label').find('label').text('Date');
                    $('.datepicker-left').parent('.datepicker-item').hide();
                    $('.datepicker-right').parent('.datepicker-item').removeClass('w-50');
                    $('.datepicker-right').parent('.datepicker-item').addClass('w-100');
               } else {
                    $('.has-datepicker-label').find('label').text('Date Range Filter');
                    $('.datepicker-left').parent('.datepicker-item').show();
                    $('.datepicker-right').parent('.datepicker-item').removeClass('w-100');
                    $('.datepicker-right').parent('.datepicker-item').addClass('w-50');
               }
          });
     }
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          validateForm();
          clearFields();
          if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');          
          }     
          clickedSubmit();
          temp_func();
     });
     $(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>