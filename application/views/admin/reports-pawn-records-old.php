<?php
     $date = date('Y-m-d');
     $role_fk = $this->session->userdata('role_fk');
     $branch_fk = $this->session->userdata['branch_fk'];
?>

     <div class="row narrow-gutters mb-4">
          <div class="col-12 mb-2">
               <h4 class="font-weight-bold"><?php echo $title; ?></h4>
               <p class="text-muted"></p>
          </div>
     </div>
     <div class="card">
          <form class="needs-validation" action="<?php echo base_url('Report/pawnRecord'); ?>" method="post" id="report-form" autocomplete="off" novalidate>
               <div class="card-body pb-0">
                    <div class="row narrow-gutters mb-2">
                         <div class="col-12 col-sm-4 col-md-4 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Location
                                        <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <select class="form-control has-loader close-on-click" name="location" required>
                                        <option value="" disabled selected>SELECT</option>
                                        <?php
                                        foreach ($branches as $fb) {
                                             echo "<option value='" . $fb['id'] . "' " . (isset($_POST['location']) && $fb['id'] == $_POST['location'] ? "selected" : "" ) . ">" .
                                                       $fb['name'] . " (" . $fb['id'] . ")" .
                                                  "</option>";
                                        }
                                        ?>
                                   </select>
                              </div>
                         </div>
                         <div class="col-12 col-sm-8 col-md-8 col-xl-4">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Status
                                        <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                        <span class="float-right mx-2 text-muted">|</span>
                                        <a href="#" class="select-all float-right text-decoration-none" title="Reset Field">
                                             <small>All</small>
                                        </a>
                                   </label>
                                   <select class="form-control has-loader" multiple="multiple" name="status[]" required>
                                        <option value="Due" <?php echo (isset($_POST['status']) && in_array("Due", $_POST['status']) ? "selected" : "" );?> >
                                             DUE
                                        </option>
                                        <option value="Expired" <?php echo (isset($_POST['status']) && in_array("Expired", $_POST['status']) ? "selected" : "" );?> >
                                             EXPIRED
                                        </option>
                                        <option value="Grace Period" <?php echo (isset($_POST['status']) && in_array("Grace Period", $_POST['status']) ? "selected" : "" );?> >
                                             GRACE PERIOD
                                        </option>
                                        <option value="In Contract" <?php echo (isset($_POST['status']) && in_array("In Contract", $_POST['status']) ? "selected" : "" );?> >
                                             IN CONTRACT
                                        </option>
                                        <option value="Freeze" <?php echo (isset($_POST['status']) && in_array("Freeze", $_POST['status']) ? "selected" : "" );?> >
                                             FREEZE
                                        </option>
                                        <option value="For Sale" <?php echo (isset($_POST['status']) && in_array("For Sale", $_POST['status']) ? "selected" : "" );?> >
                                             FOR SALE
                                        </option>
                                        <option value="For Repair" <?php echo (isset($_POST['status']) && in_array("For Repair", $_POST['status']) ? "selected" : "" );?> >
                                             FOR REPAIR
                                        </option>
                                        <option value="For Transfer" <?php echo (isset($_POST['status']) && in_array("For Transfer", $_POST['status']) ? "selected" : "" );?> >
                                             FOR TRANSFER
                                        </option>
                                        <option value="For Disposal" <?php echo (isset($_POST['status']) && in_array("For Disposal", $_POST['status']) ? "selected" : "" );?> >
                                             FOR DISPOSAL
                                        </option>
                                        <option value="Sold" <?php echo (isset($_POST['status']) && in_array("Sold", $_POST['status']) ? "selected" : "" );?> >
                                             SOLD
                                        </option>
                                        <option value="Repurchased" <?php echo (isset($_POST['status']) && in_array("Repurchased", $_POST['status']) ? "selected" : "" );?> >
                                             REPURCHASED
                                        </option>
                                        <option value="Disposed" <?php echo (isset($_POST['status']) && in_array("Disposed", $_POST['status']) ? "selected" : "" );?> >
                                             DISPOSED
                                        </option>
                                   </select>
                              </div>
                         </div>
                         <div class="col-12 col-sm-8 col-md-6 col-xl-4">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Date Range Filter
                                        <a href="#" class="clear-input float-right text-decoration-none " title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <div class="input-group">
                                        <input type="text" placeholder="Start Date" class="form-control has-loader datepicker datepicker-left text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_start" value="<?php echo (isset($_POST['date_start']) ? $_POST['date_start'] : "" );?>" required>
                                        <input type="text" placeholder="End Date" class="form-control has-loader datepicker datepicker-right text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_end" value="<?php echo (isset($_POST['date_end']) ? $_POST['date_end'] : "" );?>" required>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4 col-md-6 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="">&nbsp;</label>
                                   <input type="submit" name="generate" value="generate" class="btn btn-secondary btn-icon-text d-block text-white w-100" >
                              </div>
                         </div>
                    </div>
               </div>
               <?php if(isset($pawnRecord)) { ?>
               <div class="card-body">
                    <div class="table-responsive pb-0">
                         <table class="table table-bordered">
                              <thead>
                                   <tr class="text-center">
                                        <th>#</th>
                                        <th>CODE</th>
                                        <th>LOCATION</th>
                                        <th>USER</th>
                                        <th>CUSTOMER</th>
                                        <th>CONTACT</th>
                                        <th>ITEM TYPE</th>
                                        <th>PRODUCT</th>
                                        <th>SERIAL</th>
                                        <th>APPRAISER</th>
                                        <th>STATUS</th>
                                        <th>ENTRY DATE</th>
                                        <th>DUE DATE</th>
                                        <th>VALUE</th>
                                        <th>AMOUNT DUE</th>
                                        <th>INCOME</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                   $total_appraisal_value = 0;
                                   $total_amount_due = 0;
                                   $total_income = 0;
                                   foreach ($pawnRecord as $pr => $rec):
                                        $transaction_id = $rec['transaction_id'];
                                        $appraisal_value = $rec['value'];
                                        $amount_due = $rec['amount_due'];
                                        $income = $rec['income'];
                                        ?>
                                             <tr>
                                                  <td> <?php echo ++$pr; ?> </td>
                                                  <td> <?php echo "<a href='" . base_url('Transaction?search=') . $transaction_id .  "' class='text-reset'>" . $transaction_id . "</a>"; ?> </td>
                                                  <td> <?php echo $rec['branch_fk']; ?> </td>
                                                  <td> <?php echo $rec['user_fk']; ?> </td>
                                                  <td> <?php echo "<a href='" . base_url('Customer/profile/') . $rec['customer_fk'] .  "' class='text-reset'>" . $rec['first_name'] . " " . $rec['middle_name'] . " " . $rec['last_name'] . "</a>"; ?> </td>
                                                  <td> <?php echo $rec['contact']; ?> </td>
                                                  <td> <?php echo $rec['type']; ?> </td>
                                                  <td> <?php echo $rec['brand'] . " " . $rec['name'] . ($rec['description'] ? " - " . $rec['description'] : ''); ?> </td>
                                                  <td> <?php echo $rec['serial']; ?> </td>
                                                  <td> <?php echo $rec['appraiser']; ?> </td>
                                                  <td> <?php echo $rec['status']; ?> </td>
                                                  <td> <?php echo date("d M Y", strtotime($rec['date']) ); ?> </td>
                                                  <td> <?php echo date("d M Y", strtotime($rec['expiration']) ); ?> </td>
                                                  <td class="text-right"> <?php echo number_format($appraisal_value, 2); ?> </td>
                                                  <td class="text-right"> <?php echo number_format($amount_due, 2); ?> </td>
                                                  <td class="text-right"> <?php echo number_format($income, 2); ?> </td>
                                             </tr>
                                   <?php
                                   $total_appraisal_value += $appraisal_value;
                                   $total_amount_due += $amount_due;
                                   $total_income += $income;
                                   endforeach; ?>
                              </tbody>
                              <tfoot class="border-top-double border-top-dark text-center">
                                   <tr>
                                        <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th>
                                        <th> &#8369; <?php echo number_format($total_appraisal_value, 2); ?> </th>
                                        <th> &#8369; <?php echo number_format($total_amount_due, 2); ?> </th>
                                        <th> &#8369; <?php echo number_format($total_income, 2); ?> </th>
                                   </tr>
                              </tfoot>
                         </table>
                    </div>
               </div>
               <?php
                    echo "<div class='card-body bg-light'>";
                    echo form_input(array(
                              'type'  => 'submit',
                              'name'  => 'export',
                              'value' => 'Export',
                              'class' => 'btn btn-secondary text-white'
                         ));
                    echo "</div>";
               } ?>
          </form>
     </div>
<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          validateForm();
          clearFields();
          selectAll();

          if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');          
          }     
          clickedSubmit();
     });

     $(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>
