<?php
     $date = date('Y-m-d');
     $role_fk = $this->session->userdata('role_fk');
     $branch_fk = $this->session->userdata['branch_fk'];
?>

     <div class="row narrow-gutters mb-4">
          <div class="col-12 mb-2">
               <h4 class="font-weight-bold"><?php echo $title; ?></h4>
               <p class="text-muted"></p>
          </div>
     </div>
     <div class="card">
          <form class="needs-validation" action="<?php echo base_url('Report/retailSalesRecord'); ?>" method="post" id="report-form" autocomplete="off" novalidate>
               <div class="card-body pb-0">
                    <div class="row narrow-gutters mb-2">
                         <div class="col-12 col-sm-4 col-md-4 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Location
                                        <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <select class="form-control has-loader close-on-click" name="location" required>
                                        <option value="" disabled selected>SELECT</option>
                                        <?php
                                        foreach ($branches as $fb) {
                                             echo "<option value='" . $fb['id'] . "' " . (isset($_POST['location']) && $fb['id'] == $_POST['location'] ? "selected" : "" ) . ">" .
                                                       $fb['name'] . " (" . $fb['id'] . ")" .
                                                  "</option>";
                                             }
                                        ?>
                                   </select>
                              </div>
                         </div>
                         <!--
                         <div class="col-12 col-sm-8 col-md-8 col-xl-4">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Tags
                                        <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <select class="form-control has-loader" multiple="multiple" name="tags[]" required>
                                        <option value="1" <?php echo (isset($_POST['tags']) && in_array("1", $_POST['tags']) ? "selected" : "" );?> >
                                             CASH IN
                                        </option>
                                        <option value="2" <?php echo (isset($_POST['tags']) && in_array("2", $_POST['tags']) ? "selected" : "" );?> >
                                             CASH OUT
                                        </option>
                                        <option value="3" <?php echo (isset($_POST['tags']) && in_array("3", $_POST['tags']) ? "selected" : "" );?>>
                                             TRANSFER
                                        </option>
                                   </select>
                              </div>
                         </div>
                         -->
                         <div class="col-12 col-sm-8 col-md-6 col-xl-4">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Date Range Filter
                                        <a href="#" class="clear-input float-right text-decoration-none " title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <div class="input-group">
                                        <input type="text" placeholder="Start Date" class="form-control has-loader datepicker datepicker-left text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_start" value="<?php echo (isset($_POST['date_start']) ? $_POST['date_start'] : "" );?>" required>
                                        <input type="text" placeholder="End Date" class="form-control has-loader datepicker datepicker-right text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_end" value="<?php echo (isset($_POST['date_end']) ? $_POST['date_end'] : "" );?>" required>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4 col-md-2 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="">&nbsp;</label>
                                   <input type="submit" name="generate" value="generate" class="btn btn-secondary btn-icon-text d-block text-white w-100" >
                              </div>
                         </div>
                    </div>
               </div>

               <?php if(isset($retailSalesRecord)) { ?>
               <div class="card-body">
                    <div class="table-responsive  pb-0">
                         <table class="table table-bordered">
                              <thead>
                                   <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Code</th>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">User</th>
                                        <th class="text-center">Location</th>
                                        <th class="text-center">Sales</th>
                                        <th class="text-center">Refunds</th>
                                        <th class="text-center">Revenue</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                        $total_sales = 0;
                                        $total_refund = 0;
                                        $total_revenue = 0;
                                        foreach ($retailSalesRecord as $rsr => $rec):
                                             $sales = $rec['sales'];
                                             $total_sales += $sales;
                                             $refund = $rec['refund'];
                                             $total_refund += $refund;
                                             $revenue = $sales - $refund;
                                             $total_revenue += $revenue;
                                             $transaction_id = $rec['transaction_id'];
                                   ?>
                                        <tr>
                                             <td> <?php echo ++$rsr; ?> </td>
                                             <td>
                                                  <a href="<?php echo base_url('Transaction?search=') . $transaction_id; ?> " class="text-reset">
                                                       <?php echo $transaction_id; ?>
                                                  </a>
                                             </td>
                                             <td> <?php echo date('d M Y', strtotime($rec['date'])); ?> </td>
                                             <td> <?php echo $rec['user_fk']; ?> </td>
                                             <td> <?php echo $rec['branch_fk']; ?> </td>
                                             <td class="text-right"> <?php echo number_format($sales , 2); ?> </td>
                                             <td class="text-right"> <?php echo number_format($refund , 2); ?> </td>
                                             <td class="text-right"> <?php echo number_format($revenue , 2); ?> </td>
                                        </tr>

                                   <?php endforeach; ?>
                              </tbody>
                              <tfoot class="border-top-double border-top-dark text-center">
                                   <tr>
                                        <th></th> <th></th> <th></th> <th></th> <th></th>
                                        <th>
                                             <span class="font-weight-normal">&#8369;</span>
                                             <?php echo number_format($total_sales, 2); ?>
                                        </th>
                                        <th>
                                             <span class="font-weight-normal">&#8369;</span>
                                             <?php echo number_format($total_refund, 2); ?>
                                        </th>
                                        <th>
                                             <span class="font-weight-normal">&#8369;</span>
                                             <?php echo number_format($total_revenue, 2); ?>
                                        </th>
                                   </tr>
                              </tfoot>
                         </table>
                    </div>
               </div>
               <?php
                    echo "<div class='card-body bg-light'>";
                    echo form_input(array(
                              'type'  => 'submit',
                              'name'  => 'export',
                              'value' => 'Export',
                              'class' => 'btn btn-secondary text-white'
                         ));
                    echo "</div>";
               } ?>
          </form>
     </div>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
$(document).ready( function() {
     initDatePicker();
     initSelect2();
     numberFormat();
     validateForm();
     clearFields();
});
$(window).on('load', function() {
     loader();
});
</script>
