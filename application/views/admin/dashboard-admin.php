<?php
if(null !== $this->input->get('branch')) {
	$notifBranch = "&branch=".$this->input->get('branch');
}else {
	$notifBranch = "";
}

?>
<div class="row">
	<div class="col-md-12 mb-3">
		<div class="d-flex justify-content-between align-items-center">
			<div>
				<h4 class="font-weight-bold mb-0"><?php echo $title; ?></h4>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-12 col-md-6 col-lg-3">
		<div class="report-tile__container">
			<a href="#" class="report-tile__inner  ">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-list-check" viewBox="0 0 16 16">
						<path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3.854 2.146a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 3.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 7.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>
					</svg>
				</div>
				<div class="report-tile__text">
						<h5 class=""> Transactions </h5>
						<h3>
							<?php echo number_format($countStatus['transaction']); ?>
						</h3>
						<p class="">
							Overall transactions
						</p>
				</div>
			</a>
		</div>
	</div>
	<div class="col-12 col-md-6 col-lg-3">
		<div class="report-tile__container">
			<a href="#" class="report-tile__inner  ">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
						<path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Purchases </h5>
					<h3>
						<?php echo number_format($countStatus['purchase']); ?>
					</h3>
					<p class="">
						Overall purchases
					</p>
				</div>
			</a>
		</div>
	</div>
	<div class="col-12 col-md-6 col-lg-3">
		<div class="report-tile__container">
			<a href="#" class="report-tile__inner  ">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">
						<path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Customers </h5>
					<h3>
						<?php echo number_format($countStatus['customer']); ?>
					</h3>
					<p class="">
						Valued clients
					</p>
				</div>
			</a>
		</div>
	</div>
	<div class="col-12 col-md-6 col-lg-3">
		<div class="report-tile__container">
			<a href="#" class="report-tile__inner  ">
				<div class="report-tile__icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-shop-window" viewBox="0 0 16 16">
						<path d="M2.97 1.35A1 1 0 0 1 3.73 1h8.54a1 1 0 0 1 .76.35l2.609 3.044A1.5 1.5 0 0 1 16 5.37v.255a2.375 2.375 0 0 1-4.25 1.458A2.371 2.371 0 0 1 9.875 8 2.37 2.37 0 0 1 8 7.083 2.37 2.37 0 0 1 6.125 8a2.37 2.37 0 0 1-1.875-.917A2.375 2.375 0 0 1 0 5.625V5.37a1.5 1.5 0 0 1 .361-.976l2.61-3.045zm1.78 4.275a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 1 0 2.75 0V5.37a.5.5 0 0 0-.12-.325L12.27 2H3.73L1.12 5.045A.5.5 0 0 0 1 5.37v.255a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0zM1.5 8.5A.5.5 0 0 1 2 9v6h12V9a.5.5 0 0 1 1 0v6h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V9a.5.5 0 0 1 .5-.5zm2 .5a.5.5 0 0 1 .5.5V13h8V9.5a.5.5 0 0 1 1 0V13a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V9.5a.5.5 0 0 1 .5-.5z"/>
					</svg>
				</div>
				<div class="report-tile__text">
					<h5 class=""> Locations </h5>
					<h3>
						<?php echo number_format($countStatus['location']); ?>
					</h3>
					<p class="">
						Store branches and offices
					</p>
				</div>
			</a>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div id="notification" class="report-panel__container">
			<div class="report-panel__inner">
				<div class="report-panel__header" style="padding-right: 220px"> 
					<h5> Transaction Notifications </h5>
					<p> List of all pending items that requires action. </p>

					<div class="dropdown dropdown-menu__locations">
						<!-- <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
							<path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
							<path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
						</svg> -->
						<button class="btn btn-lg dropdown-toggle text-uppercase" type="button" id="dropdownBranches" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<?php
							if(null !== $this->input->get('branch')) {
								echo $branchInfo['name'];
							}else {
								echo "ALL";
							}
							?>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<?php
							echo "<a class='dropdown-item text-uppercase' href='".base_url('Dashboard#notification')."'>ALL</a>";
							foreach ($branches as $branch) {
								echo "<a class='dropdown-item text-uppercase' href='".base_url('Dashboard?branch=').$branch['id']."#notification'>".$branch['name']."</a>";
							}
							?>
						</div>
					</div>
				</div>
				<div class="report-panel__content">
					<div class="row text-center">
						<div class="col-6 col-sm-4 mt-3 mb-3">
							<a href="<?php echo base_url('Transaction?status=Due') . $notifBranch; ?> ">								
								<h3> <?php echo $countStatus['due']; ?> </h3>
								<p>Due</p>
							</a>
						</div>
						<div class="col-6 col-sm-4 mt-3 mb-3">
							<a href="<?php echo base_url('Transaction?status=Grace') . $notifBranch; ?>">								
								<h3> <?php echo $countStatus['grace']; ?> </h3>
								<p>Grace Period</p>
							</a>
						</div>
						<div class="col-6 col-sm-4 mt-3 mb-3">
							<a href="<?php echo base_url('Transaction?status=Expired') . $notifBranch; ?>">								
								<h3> <?php echo $countStatus['expired']; ?> </h3>
								<p>Expired</p>
							</a>
						</div>
						<div class="col-6 col-sm-4 mt-3 mb-3">
							<a href="<?php echo base_url('Transaction?status=Transfer') . $notifBranch; ?>">								
								<h3> <?php echo $countStatus['transfer']; ?> </h3>
								<p>For Transfer</p>
							</a>
						</div>
						<div class="col-6 col-sm-4 mt-3 mb-3">
							<a href="<?php echo base_url('Transaction?status=Repair') . $notifBranch; ?>">								
								<h3> <?php echo $countStatus['repair']; ?> </h3>
								<p>For&nbsp;Repair</p>
							</a>
						</div>
						<div class="col-6 col-sm-4 mt-3 mb-3">
							<a href="<?php echo base_url('Transaction?status=Disposal') . $notifBranch; ?>">								
								<h3> <?php echo $countStatus['disposal']; ?> </h3>
								<p>For&nbsp;Disposal</p>
							</a>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
	<div class="col-md-6 grid-margin stretch-card">
		<div id="" class="report-panel__container">
			<div class="report-panel__inner">
				<div class="report-panel__header"> 
					<h5> Monthly Report </h5>
					<p> Total monthly transaction of branches. </p>
					<div class="icon">
						<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-calendar-check" viewBox="0 0 16 16">
							<path d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
							<path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
						</svg>
					</div>
				</div>
				<div class="report-panel__content py-md-4">
					<div class="row narrow-gutters">
						<div class="col-md-12 col-lg-6 d-flex flex-column justify-content-center">
							<h5 class="text-muted mt-3 mt-md-0">Transactions</h5>
							<h2 class="my-2">
								<?php
									$totalMonthTransaction = $this->db->like('date', date("Y-m-"), 'after')->count_all_results('transactions');
									echo number_format($totalMonthTransaction);
								?>
							</h2>							
							<p class="mb-4 mb-xl-0">
								<?php echo date("F") . " 1 - ". cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y")) . ", " . date('Y'); ?>
							</p>
						</div>
						<div class="col-md-12 col-lg-6 d-flex flex-column justify-content-center">
							<div class="table-responsive mb-3 mb-md-0 pb-0">
								<table class="w-100 ">
									<tbody>
										<?php
										foreach ($branches as $branch) {
											$branchMonthlyTransaction = $this->db->where('branch_fk', $branch['id'])->like('date', date("Y-m-"), 'after')->count_all_results('transactions');
											echo "
												<tr>
													<td class='text-muted border-bottom py-2 w-75'>
														<p class='m-0'>" . $branch['name'] . "</p>
													</td>
													<td class='border-bottom py-2 w-25'>
														<p class='m-0 font-weight-bold text-center'>" . number_format($branchMonthlyTransaction) . "</p>
													</td>
												</tr>
											";
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
				</div>
			</div>			
		</div>
	</div>
</div>
<?php
