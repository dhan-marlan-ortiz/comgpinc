<style>
     .dataTables_wrapper .dataTables_length {
          float: right;
          margin-left: 12px;
          margin-bottom: 12px;
     }
</style>

<?php echo admin_settings_tab('User'); ?>

<div class="card border-top-0">
     <div class="card-body">
          <div class="row">     
               <div class="col-md-8 pb-5 pb-md-0">
                    <!-- users table -->
                    <div class="" id="user-list-tab">
                         <!-- <form method='post' action='<?php echo base_url('User/status'); ?>'> -->                              
                              <?php echo tab_header("Accounts", "position-absolute"); ?>
                              <div class="table-responsive pb-0">
                                   <?php
                                        $user = $this->session->userdata('id');
                                        echo "<input type='hidden' class='current-user' value='". $user . "'>";
                                   ?>
                                   <table class="table pt-3" id="user-table">
                                        <thead>
                                             <tr class="shadow-sm">
                                                  <th class="border-0 border-bottom-0 shadow-none text-left">
                                                       <span class="font-12 font-weight-bold text-body py-1 d-block text-uppercase">Full Name&emsp;</span>
                                                  </th>
                                                  <th class="border-0 border-bottom-0 shadow-none text-left">
                                                       <span class="font-12 font-weight-bold text-body py-1 d-block text-uppercase">Username&emsp;</span>
                                                  </th>
                                                  <th class="border-0 border-bottom-0 shadow-none text-left">
                                                       <span class="font-12 font-weight-bold text-body py-1 d-block text-uppercase">Access Type&emsp;</span>
                                                  </th>
                                                  <th class="border-0 border-bottom-0 shadow-none text-left">
                                                       <span class="font-12 font-weight-bold text-body py-1 d-block text-uppercase">Branch&emsp;</span>
                                                  </th>
                                                  <th class="border-0 border-bottom-0 shadow-none no-sort">
                                                       <span class="font-12 font-weight-bold text-body py-1 d-block text-uppercase">Status&emsp;</span>
                                                  </th>
                                                  <th class="border-0 border-bottom-0 shadow-none no-sort">
                                                       <span class="font-12 font-weight-bold text-body py-1 d-block text-uppercase"></span>
                                                  </th>
                                             </tr>
                                        </thead>
                                        <tbody class="no-border narrow-height">
                                        <?php
                                             foreach ($users as $user) {
                                                  echo "<tr class='user-row' data-id='".$user['id']."' data-firstname='".$user['first_name']."' data-lastname='".$user['last_name']."' data-role='".$user['role_fk']."' data-branch='".$user['branch_fk']."' >
                                                            <td>".$user['first_name'] . " " . $user['last_name'] . "</td>
                                                            <td class='user-id'>" . $user['id'] ."</td>
                                                            <td>".$user['role_type'] . "</td>
                                                            <td>".$user['branch'] . "</td>";
                                                       echo "<td class='text-center'>
                                                                 <form method='post' action='" . base_url('User/status/') . $user['id'] . "/" . $user['is_enabled'] . "'>
                                                                      <label class='switch'>
                                                                           <input type='checkbox' " . ($user['is_enabled'] == 1 ? 'checked' : '') . ">
                                                                           <span class='slider round'></span>
                                                                      </label>
                                                                 </form>
                                                            </td>";     
                                                            
                                                            
                                                       // echo "<td class='text-center'>";
                                                       //           if ($user['is_enabled'] == 1) {
                                                       //                echo "
                                                       //                <label class='switch'>
                                                       //                     <input type='checkbox' class='deactivateButton' data-id='" . $user['id'] . "' checked>
                                                       //                     <span class='slider round'></span>
                                                       //                </label>
                                                                      
                                                       //                <div id='deactivateModal' class='modal fade confirm-modal' role='dialog'>
                                                       //                     <div class='modal-dialog modal-sm' role='document'>
                                                       //                          <div class='modal-content'>
                                                       //                               <div class='modal-header'>
                                                       //                                    <h5 class='modal-title'>Confirmation</h5>
                                                       //                                    <button type='button' class='close' data-dismiss='modal'>
                                                       //                                         <span aria-hidden='true'>&times;</span>
                                                       //                                    </button>
                                                       //                               </div>
                                                       //                               <div class='modal-body'>
                                                       //                                    <p class='mb-0'>Are you sure you want to deactivate this user?</p>
                                                       //                               </div>
                                                       //                               <div class='modal-footer'>
                                                       //                                    <input type='hidden' name='deactstatus_id' class='deactstatus'>
                                                       //                                    <button type='button' class='col btn btn-light border cancel' data-dismiss='modal'>CANCEL</button>
                                                       //                                    <button type='submit' class='col btn btn-danger confirm' name='deactivate'>CONFIRM</button>
                                                       //                               </div>
                                                       //                          </div>
                                                       //                     </div>
                                                       //                </div>";
                                                       //           } else {
                                                       //                echo "                                                                      
                                                       //                <label class='switch'>
                                                       //                     <input type='checkbox' class='activateButton' data-toggle='modal' data-target='#activateModal' data-id='" . $user['id'] . "'>
                                                       //                     <span class='slider round'></span>
                                                       //                </label>                                                                           
                                                       //                <div id='activateModal' class='modal fade confirm-modal' role='dialog'>
                                                       //                     <div class='modal-dialog modal-sm' role='document'>
                                                       //                          <div class='modal-content'>
                                                       //                               <div class='modal-header'>
                                                       //                                    <h5 class='modal-title'>Confirmation</h5>
                                                       //                                    <button type='button' class='close' data-dismiss='modal'>
                                                       //                                         <span aria-hidden='true'>&times;</span>
                                                       //                                    </button>
                                                       //                               </div>
                                                       //                               <div class='modal-body'>
                                                       //                                    <p class='mb-0'>Are you sure you want to activate this user?</p>
                                                       //                               </div>
                                                       //                               <div class='modal-footer'>
                                                       //                                    <input type='hidden' name='actstatus_id' class='actstatus'>
                                                       //                                    <button type='button' class='col btn btn-light border cancel' data-dismiss='modal'>CANCEL</button>
                                                       //                                    <button type='submit' class='col btn btn-success confirm' name='activate'>CONFIRM</button>
                                                       //                               </div>
                                                       //                          </div>
                                                       //                     </div>
                                                       //                </div>";
                                                       //           }
                                                       // echo "</td>";

                                                       echo "<td>
                                                                 <button type='button' class='btn btn-twitter btn-sm text-monospace updateButton' data-toggle='tooltip' title='Update User' data-id='" . $user['id'] . "'data-firstname='" . $user['first_name'] . "'data-lastname='" . $user['last_name'] . "'data-role='" . $user['role_fk'] . "'data-branch='" . $user['branch_fk'] . "' >
                                                                      UPDATE
                                                                 </button>
                                                            </td>";
                                                  echo "</tr>";
                                        } ?>
                                        </tbody>
                                        <tfoot class="hidden-footer">
                                             <tr>
                                                  <th colspan="6"></th>
                                             </tr>
                                        </tfoot>
                                   </table>
                              </div>
                         <!-- </form> -->
                    </div>
               </div>
               <div class="col-md-4">
                    <!-- Create user form -->
                    <div class="" id="user-create-tab">
                         <?php echo tab_header("Create User"); ?>
                         <form method="post" action="<?php echo base_url('User'); ?>" id="user-create-form">
                              <div class="row narrow-gutters">
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                             <label>First Name <span class="text-danger">*</span></label>
                                             <input class="form-control" placeholder="First Name" type="text" name="firstname" id="firstname" required>
                                        </div>
                                   </div>
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                             <label>Last Name <span class="text-danger">*</span></label>
                                             <input class="form-control" placeholder="Last Name" type="text" name="lastname" id="lastname" required>
                                        </div>
                                   </div>
                              </div>
                              <div class="row narrow-gutters">
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                             <label>Username <span class="text-danger">*</span></label>
                                             <input class="form-control" placeholder="Username" type="text" name="username" id="username"  autocomplete="false" required>
                                        </div>
                                   </div>
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                             <label>Password <span class="text-danger">*</span></label>
                                             <input class="form-control create-password border" placeholder="Password" type="password" name="password" autocomplete="new-password" required>
                                        </div>
                                   </div>
                              </div>
                              <div class="row narrow-gutters">
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group selectize-group">
                                             <label>Access Type <span class="text-danger">*</span></label>
                                             <select id="user-create-select-role" name="role" placeholder="Type or Select Role" required>
                                                  <option value="" disabled selected>Select</option>
                                                  <?php
                                                  foreach ($roles as $role) {
                                                       echo "<option value='" . $role['id'] . "'>" . $role['access_type'] . "</option>";
                                                  }
                                                  ?>
                                             </select>
                                        </div>
                                   </div>
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group selectize-group">
                                             <label>Location <span class="text-danger">*</span></label>
                                             <select id="user-create-select-branch" name="branch" placeholder="Type or Select Branch" required>
                                                  <option value="" disabled selected>Select</option>
                                                  <?php
                                                  foreach ($branches as $branch) {
                                                       echo "<option value='" . $branch['id'] . "'>" . $branch['name'] . "</option>";
                                                  }
                                                  ?>
                                             </select>
                                        </div>
                                   </div>
                              </div>
                              <div class="form-group row narrow-gutters form-btns">
                                   <div class="col-6">
                                        <button type="button" class="btn btn-light border btn-clear-fields text-uppercase w-100">Clear</button>
                                   </div>
                                   <div class="col-6">
                                        <button type="button" class="btn btn-primary text-uppercase w-100 confirm-button">Save</button>
                                   </div>
                              </div>
                              <div id="createModal" class="modal fade confirm-modal" role="dialog">
                                   <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                             <div class="modal-header">
                                                  <h5 class="modal-title">Confirmation</h5>
                                                  <button type="button" class="close" data-dismiss="modal">
                                                       <span aria-hidden="true">&times;</span>
                                                  </button>
                                             </div>
                                             <div class="modal-body">
                                                  <p clas="mb-0">Are you sure want to add new user?</p>
                                             </div>
                                             <div class="modal-footer">
                                                  <button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
                                                  <button type="submit" class="col btn btn-primary confirm" name="add">CONFIRM</button>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </form>               
                    </div>
                    <!-- Create user form end -->
                    <!-- Update user form -->
                    <div class="hidden" id="user-update-tab">               
                         <?php echo tab_header("Update User"); ?>
                         <form method="post" action="<?php echo base_url('User/update'); ?>" id="update-user-form">
                              <input type="hidden" name="current_id" id="current_id">
                              <div class="row narrow-gutters">
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                             <label>First Name <span class="text-danger">*</span></label>
                                             <input class="form-control" type="text" name="update_firstname" id="update_firstname">
                                             <input type="hidden" class="check-firstname">
                                        </div>
                                   </div>
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                             <label>Last Name <span class="text-danger">*</span></label>
                                             <input class="form-control" type="text" name="update_lastname" id="update_lastname">
                                             <input type="hidden" class="check-lastname">
                                        </div>
                                   </div>
                              </div>
                              <div class="row narrow-gutters">
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                             <label>Username <span class="text-danger">*</span></label>
                                             <input class="form-control" placeholder="Username" type="text" name="update_username" id="update_username">
                                             <input type="hidden" class="check-username">
                                        </div>
                                   </div>
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                             <label style="display: block;">Password</label>
                                             <button type="button" class="btn btn-inverse-success update-password-btn btn-block">Update</button>
                                        </div>
                                   </div>
                              </div>
                              <div class="row narrow-gutters">
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group selectize-group">
                                             <label>Access Type <span class="text-danger">*</span></label>
                                             <select id="update_role" name="update_role" placeholder="Type or Select Role">
                                                  <?php
                                                   foreach ($roles as $role) {
                                                     echo "<option value='" . $role['id'] . "'>" . $role['access_type'] . "</option>";
                                                  }
                                                  ?>
                                             </select>
                                             <input type="hidden" class="check-role">
                                        </div>
                                   </div>
                                   <div class="col-12 col-sm-6">
                                        <div class="form-group selectize-group">
                                             <label>Location <span class="text-danger">*</span></label>
                                             <select id="update_branch" name="update_branch" placeholder="Type or Select Branch" class="check-branch">
                                             <?php
                                             foreach ($branches as $branch) {
                                                  echo "<option value='" . $branch['id'] . "'>" . $branch['name'] . "</option>";
                                             }
                                             ?>
                                             </select>
                                             <input type="hidden" class="check-branch">
                                        </div>
                                   </div>
                              </div>
                              <div class="form-group row narrow-gutters form-btns">
                                   <div class="col-6">
                                        <button type="button" class="btn btn-light text-uppercase w-100  border btn-cancel">Cancel Update</button>
                                   </div>
                                   <div class="col-6">
                                        <button type="button" class="btn btn-primary text-uppercase w-100  confirm-update">Save Changes</button>
                                   </div>
                              </div>
                              <p class="card-description">
                                   <a href="#" class="font-12" id="delete-account-link" data-toggle="modal" data-target="#confirm-delete-modal">
                                        Click here to delete this account permanently
                                   </a>
                              </p>
                              <div id="confirmModal" class="modal fade confirm-modal" role="dialog">
                                   <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                             <div class="modal-header">
                                                  <h5 class="modal-title">Confirmation</h5>
                                                  <button type="button" class="close" data-dismiss="modal">
                                                       <span aria-hidden="true">&times;</span>
                                                  </button>
                                             </div>
                                             <div class="modal-body">
                                                  <p>Are you sure you want to update?</p>
                                             </div>
                                             <div class="modal-footer">
                                                  <button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
                                                  <button type="submit" class="col btn btn-primary confirm" name="update">CONFIRM</button>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <div id="confirm-delete-modal" class="modal fade confirm-modal" role="dialog">
                                   <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                             <div class="modal-header">
                                                  <h5 class="modal-title">Confirmation</h5>
                                                  <button type="button" class="close" data-dismiss="modal">
                                                       <span aria-hidden="true">&times;</span>
                                                  </button>
                                             </div>
                                             <div class="modal-body">
                                                  <p>Are you sure you want to delete user?</p>
                                             </div>
                                             <div class="modal-footer">
                                                  <a href="#" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</a>
                                                  <a href="#" class="col btn btn-danger confirm" id="delete-user-link">CONFIRM</a>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </form>
                         <div id="newpasswordModal" class="modal fade confirm-modal" role="dialog">
                              <form method="post" action="<?php echo base_url('User/newpassword'); ?>" name="update_password_form">
                                   <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                             <div class="modal-header">
                                                  <h5 class="modal-title">Update Password</h5>
                                                  <button type="button" class="close" data-dismiss="modal">
                                                       <span aria-hidden="true">&times;</span>
                                                  </button>
                                             </div>
                                             <div class="modal-body">
                                                  <input type="hidden" name="password_id" class="password-id">
                                                  <div class="row narrow-gutters">
                                                       <div class="col-12 col-sm-6">
                                                            <div class="form-group">
                                                                 <label>New Password <span class="text-danger">*</span></label>
                                                                 <input class="form-control new-password" placeholder="New Password" type="password" name="update_password" required>
                                                            </div>
                                                       </div>
                                                       <div class="col-12 col-sm-6">
                                                            <div class="form-group">
                                                                 <label>Confirm New Password <span class="text-danger">*</span></label>
                                                                 <input class="form-control confirm-password" placeholder="Confirm Password" type="password" name="confirm_password" required>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div class="modal-footer">
                                                  <input type="hidden" name="password_id" class="password-id">
                                                  <button type="button" class="btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
                                                  <button type="submit" class="btn btn-primary confirm" name="confirm_password_btn">SAVE CHANGES</button>
                                             </div>
                                        </div>
                                   </div>
                              </form>
                         </div>               
                    </div>
                    <!-- Update user form end -->
               </div>
          </div>
     </div>
</div>
<script type="text/javascript">
     $(document).ready(function() {
          var base_url = '<?php echo base_url(); ?>';

          var initSelect2 = $("form select").select2({
               placeholder: "SELECT"
          });


          $('#user-table').dataTable( {
               "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false,
               } ]
          } );
          
          $(".updateButton").on('click', function() {
               var current = $(".current-user").val(base_url);
               var id = $(this).attr("data-id");
               var firstname = $(this).attr("data-firstname");
               var lastname = $(this).attr("data-lastname");
               var username = $(this).attr("data-id");
               var role = $(this).attr("data-role");
               var branch = $(this).attr("data-branch");

               $("#user-create-tab").addClass('hidden');
               $("#user-update-tab").removeClass('hidden');

               $(".updateButton, .deactivateButton, .activateButton").css('visibility', 'hidden');

               $(this).closest('tr').addClass('highlighted');
               $("#update_firstname").val(firstname);
               $("#update_lastname").val(lastname);
               $("#update_username").val(username);
               $("#update_role").val(role).trigger('change.select2');
               $("#update_branch").val(branch).trigger('change.select2');
               $(".check-firstname").val(firstname);
               $(".check-lastname").val(lastname);
               $(".check-username").val(username);
               $(".check-role").val(role);
               $(".check-branch").val(branch);
               $("#current_id").val(username);
               $("#delete-user-link").attr('href', base_url + "User/delete/" + id);
          });

          $("#newpasswordModal form").validate({
               rules: {
                    update_password: {
                         minlength: 5
                    },
                    confirm_password: {
                         minlength: 5,
                         equalTo: "input[name='update_password']"
                    }
               }
          });
     });

     $(".activateButton").on('click', function() {
          var id = $(this).attr("data-id");          
          $(".actstatus").val(id);
     });

     $(".deactivateButton").on('click', function() {
          var current = $(".current-user").val();
          var id = $(this).attr("data-id");
          if (current == id) {
               $.notify("Cannot deactivate current logged in user!", "error");
          } else {
               $("#deactivateModal").modal('toggle');
               $(".deactstatus").val(id);
          }
     });

     $(".btn-cancel").on('click', function() {
          $("#user-update-tab").addClass('hidden');
          $("#user-create-tab").removeClass('hidden');
          $(".user-row").removeClass('highlighted');

          $(".updateButton, .deactivateButton, .activateButton").css('visibility', 'visible');

          $("#user-create-form").find("input[type=text]").val("");
          $("#user-create-form").validate().resetForm();

          var fields = $(this).closest('form').find('input');
          fields.each(function() {
               $(this).val('');
          });
     });

     $(".confirm-button").on('click', function() {
          $("#user-create-form").validate({
               rules: {
                    firstname: {
                         required: true,
                         maxlength: 30
                    },
                    lastname: {
                         required: true,
                         maxlength: 30
                    },
                    username: {
                         required: true,
                         maxlength: 20
                    },
                    password: {
                         required: true,
                         maxlength: 255,
                         minlength: 8
                    }
               }
          });
          if ($('#user-create-form').valid()) {
               $("#createModal").modal('toggle');
               var firstname = $("#firstname").val();
               var lastname = $("#lastname").val();
               var username = $("#username").val();
               var role = $("#user-create-select-role option:selected").text();
               var branch = $("#user-create-select-branch option:selected").text();
               $("#name_preview").html(firstname + " " + lastname);
               $("#username_preview").html(username);
               $("#role_preview").html(role);
               $("#branch_preview").html(branch);
          } else {
               console.log("Validate first");
          }
     });

     $(".btn-clear-fields").on('click', function() {
          $("#user-create-form").validate().resetForm();
     });

     $(".confirm-update").on('click', function() {
          $("#update-user-form").validate({
               rules: {
                    update_firstname: {
                         required: true,
                         maxlength: 30
                    },
                    update_lastname: {
                         required: true,
                         maxlength: 30
                    },
                    update_username: {
                         required: true,
                         maxlength: 20
                    },
                    update_password: {
                         required: true,
                         maxlength: 255
                    },
                    confirm_password: {
                         required: true,
                         maxlength: 255,
                         equalTo: '.new-password'
                    }
               },
               messages: {
                    confirm_password: {
                         equalTo: "Password must be same."
                    }
               }
          });
          if ($('#update-user-form').valid()) {
               console.log($('#update-user-form').valid());
               var checkFirstname = $(".check-firstname").val();
               var checkLastname = $(".check-lastname").val();
               var checkUsername = $(".check-username").val();
               var checkRole = $(".check-role").val();
               var checkBranch = $(".check-branch").val();
               $("#confirmModal").modal('toggle');
          } else {}
     });

     $(".update-password-btn").on('click', function() {
          var userID = $("#update_username").val();
          $(".password-id").val(userID);
          $("#newpasswordModal").modal('toggle');
     });


     $(".switch").on('change', function() {
          $(this).closest('form').submit();
     });
</script>
<style>
     #user-create-tab label.error,
     #user-update-tab label.error {
          font-size: 12px;
          line-height: 1em;
          margin-top: 2px;
          font-style: italic;
     }
</style>
