<?php
     $date = date('Y-m-d');
     $role_fk = $this->session->userdata('role_fk');
     $branch_fk = $this->session->userdata['branch_fk'];
?>

     <div class="row narrow-gutters mb-4">
          <div class="col-12 mb-2">
               <h4 class="font-weight-bold"><?php echo $title; ?></h4>
               <p class="text-muted"></p>
          </div>
     </div>
     <div class="card">
          <form class="needs-validation" action="<?php echo base_url('Report/retailSalesTransaction'); ?>" method="post" id="report-form" autocomplete="off" novalidate>
               <div class="card-body pb-0">
                    <div class="row narrow-gutters mb-2">
                         <div class="col-12 col-sm-4 col-md-4 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Location
                                        <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <select class="form-control has-loader close-on-click" name="location" required>
                                        <option value="" disabled selected>SELECT</option>
                                        <?php
                                        foreach ($branches as $fb) {
                                             echo "<option value='" . $fb['id'] . "' " . (isset($_POST['location']) && $fb['id'] == $_POST['location'] ? "selected" : "" ) . ">" .
                                                       $fb['name'] . " (" . $fb['id'] . ")" .
                                                  "</option>";
                                             }
                                        ?>
                                   </select>
                              </div>
                         </div>
                         <!--
                         <div class="col-12 col-sm-8 col-md-8 col-xl-4">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Tags
                                        <a href="#" class="clear-select float-right text-decoration-none" title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <select class="form-control has-loader" multiple="multiple" name="tags[]" required>
                                        <option value="1" <?php echo (isset($_POST['tags']) && in_array("1", $_POST['tags']) ? "selected" : "" );?> >
                                             CASH IN
                                        </option>
                                        <option value="2" <?php echo (isset($_POST['tags']) && in_array("2", $_POST['tags']) ? "selected" : "" );?> >
                                             CASH OUT
                                        </option>
                                        <option value="3" <?php echo (isset($_POST['tags']) && in_array("3", $_POST['tags']) ? "selected" : "" );?>>
                                             TRANSFER
                                        </option>
                                   </select>
                              </div>
                         </div>
                         -->
                         <div class="col-12 col-sm-8 col-md-6 col-xl-4">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="" class="d-block">
                                        Date Range Filter
                                        <a href="#" class="clear-input float-right text-decoration-none " title="Reset Field">
                                             <small class="ti-reload"></small>
                                        </a>
                                   </label>
                                   <div class="input-group">
                                        <input type="text" placeholder="Start Date" class="form-control has-loader datepicker datepicker-left text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_start" value="<?php echo (isset($_POST['date_start']) ? $_POST['date_start'] : "" );?>" required>
                                        <input type="text" placeholder="End Date" class="form-control has-loader datepicker datepicker-right text-uppercase font-12" data-date-format="MM dd, yyyy" name="date_end" value="<?php echo (isset($_POST['date_end']) ? $_POST['date_end'] : "" );?>" required>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4 col-md-2 col-xl-2">
                              <div class="form-group">
                                   <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                                   <label for="">&nbsp;</label>
                                   <input type="submit" name="generate" value="generate" class="btn btn-secondary btn-icon-text d-block text-white w-100" >
                              </div>
                         </div>
                    </div>
               </div>

               <?php if(isset($retailSalesTransaction)) { ?>
               <div class="card-body">
                    <div class="table-responsive  pb-0">
                         <table class="table table-bordered">
                              <thead>
                                   <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Code</th>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">User</th>
                                        <th class="text-center">Location</th>
                                        <th class="text-center">Type</th>
                                        <th class="text-center">Product</th>
                                        <th class="text-center">Unit</th>
                                        <th class="text-center">Qty Sold</th>
                                        <th class="text-center">Unit Price</th>
                                        <th class="text-center">Total Price</th>
                                        <th class="text-center">Qty Returned</th>
                                        <th class="text-center">Refunds</th>
                                        <th class="text-center">Revenue</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php

                                        $sum_selling_price = 0;
                                        $sum_quantity = 0;
                                        $sum_total_price = 0;
                                        $sum_returned_quantity = 0;
                                        $sum_refund_amount = 0;
                                        $sum_revenue = 0;
                                        foreach ($retailSalesTransaction as $rsr => $rec):
                                             $transaction_id = $rec['transaction_id'];

                                             $selling_price = $rec['selling_price'];
                                             $quantity = $rec['quantity'];
                                             $total_price = $selling_price * $quantity;

                                             $returned_quantity = (isset($rec['returned_quantity']) ? $rec['returned_quantity'] : 0);
                                             $refund_amount = (isset($rec['refund_amount']) ? $rec['refund_amount'] : 0);

                                             $revenue = $total_price - $refund_amount;

                                   ?>
                                        <tr>
                                             <td> <?php echo ++$rsr; ?> </td>
                                             <td>
                                                  <a href="<?php echo base_url('Transaction?search=') . $transaction_id; ?> " class="text-reset">
                                                       <?php echo $transaction_id; ?>
                                                  </a>
                                             </td>
                                             <td> <?php echo date('d M Y', strtotime($rec['date'])); ?> </td>
                                             <td> <?php echo $rec['user_fk']; ?> </td>
                                             <td> <?php echo $rec['branch_fk']; ?> </td>
                                             <td> <?php echo $rec['type']; ?> </td>
                                             <td> <?php echo $rec['brand'] . " " . $rec['name']; ?> </td>
                                             <td> <?php echo $rec['unit']; ?> </td>
                                             <td class="text-right"> <?php echo number_format($quantity); ?> </td>
                                             <td class="text-right"> <?php echo number_format($selling_price , 2); ?> </td>
                                             <td class="text-right"> <?php echo number_format($total_price , 2); ?> </td>
                                             <td class="text-right"> <?php echo number_format($returned_quantity); ?> </td>
                                             <td class="text-right"> <?php echo number_format($refund_amount, 2); ?> </td>
                                             <td class="text-right"> <?php echo number_format($revenue, 2); ?> </td>
                                        </tr>

                                   <?php
                                        $sum_selling_price += $selling_price;
                                        $sum_quantity += $quantity;
                                        $sum_total_price += $total_price;
                                        $sum_returned_quantity += $returned_quantity;
                                        $sum_refund_amount += $refund_amount;
                                        $sum_revenue += $revenue;

                                        endforeach;
                                   ?>
                              </tbody>
                              <tfoot class="border-top-double border-top-dark text-center">
                                   <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th> <?php echo number_format($sum_quantity); ?> </th>
                                        <th> &#8369; <?php echo number_format($sum_selling_price, 2); ?> </th>
                                        <th> &#8369; <?php echo number_format($sum_total_price, 2); ?> </th>
                                        <th> <?php echo number_format($sum_returned_quantity); ?> </th>
                                        <th> &#8369; <?php echo number_format($sum_refund_amount, 2); ?> </th>
                                        <th> &#8369; <?php echo number_format($sum_revenue, 2); ?> </th>
                                   </tr>
                              </tfoot>
                         </table>
                    </div>
               </div>
               <?php
                    echo "<div class='card-body bg-light'>";
                    echo form_input(array(
                              'type'  => 'submit',
                              'name'  => 'export',
                              'value' => 'Export',
                              'class' => 'btn btn-secondary text-white'
                         ));
                    echo "</div>";
               } ?>
          </form>
     </div>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">
$(document).ready( function() {
     initDatePicker();
     initSelect2();
     numberFormat();
     validateForm();
     clearFields();
});
$(window).on('load', function() {
     loader();
});
</script>
