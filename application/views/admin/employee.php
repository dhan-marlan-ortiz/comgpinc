<style>
	.dataTables_length, .dataTables_filter {
		float: right !important;
		margin-left: 12px;
		margin-bottom: 12px;
		display: inline-block;
	}
</style>

<?php echo admin_settings_tab('Employee'); ?>

<div class="card border-top-0">
	<div class="card-body">                    
		<div class="" id="type-list-tab">
			<div id="add-employee-toggle-wrapper" class="float-right">
				<div class="input-group">
					<a href="<?php echo base_url('Employee/form'); ?>" class="btn btn-primary btn-icon-text btn-sm text-white border-radius-2" id="add-employee-toggle">
						<i class="btn-icon-prepend ti-plus"></i>ADD EMPLOYEE
					</a>
				</div>
			</div>                         
			<div class="table-responsive pb-0">
				<?php echo tab_header("Employees", "position-absolute"); ?>
				<table class="table pt-3" id="admin-table">
					<thead>
						<tr class="shadow-sm">
							<th class="border-0 border-bottom-0 shadow-none no-sort">
								<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>#</span>
							</th>                                             
							<th class="border-0 border-bottom-0 shadow-none text-left">
								<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Name</span>
							</th>
							<th class="border-0 border-bottom-0 shadow-none text-left">
								<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Contact</span>
							</th>
							<th class="border-0 border-bottom-0 shadow-none text-left">
								<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Code</span>
							</th>
							<th class="border-0 border-bottom-0 shadow-none text-left">
								<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Position</span>
							</th>
							<th class="border-0 border-bottom-0 shadow-none text-left">
								<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Status</span>
							</th>
							<th class="border-0 border-bottom-0 shadow-none text-left">
								<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Location&nbsp;</span>
							</th>
							<th class="border-0 border-bottom-0 shadow-none no-sort">
								<span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>&nbsp;</span>
							</th>
						</tr>
					</thead>
					<tbody class="no-border narrow-height">
						<?php
						foreach ($employees as $employee) {
							echo "
							<tr>
							<td></td>                                                  
							<td class='text-left'>" . $employee['first_name'] . " " . $employee['middle_name'] . " " . $employee['last_name'] . "</td>
							<td class='text-left'>" . $employee['contact'] . "</td>
							<td class='text-left'>" . $employee['id'] . "</td>
							<td class='text-wrap'>" . $employee['position'] . "</td>
							<td class='text-wrap'>" . $employee['employment_status'] . "</td>
							<td>" . $employee['location'] . "</td>
							<td class='text-right'>                                                       
							<a href='" . base_url('Employee/form?profile=') . $employee['id'] . "' class='btn btn-twitter btn-sm text-monospace' >
							<small>VIEW</small>
							</a>                                                            
							</td>
							</tr>";
						}
						?>
					</tbody>
					<tfoot class="hidden-footer">
						<tr>
							<th colspan="6"></th>
						</tr>
					</tfoot>
				</table>
			</div>               
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/admin.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready( function() {
		initDataTable("#admin-table");
		moveToToolbar("#add-employee-toggle-wrapper");
		// moveToToolbar(".dataTables_length");
		// moveToToolbar(".dataTables_filter");
	});
	$(window).on('load', function() {
		
	});
</script>