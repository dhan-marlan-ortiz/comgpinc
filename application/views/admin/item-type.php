<style>
     .dataTables_wrapper .dataTables_length {
          float: right;
          margin-left: 12px;
          margin-bottom: 12px;
     }
</style>

<?php echo item_settings_tab('type'); ?>

<div class="card border-top-0">
     <div class="card-body">
          <div class="row">
               <div class="col-md-8 pb-5">
                    <div id="type-list-tab">                         
                         <?php echo tab_header("Item Types", "position-absolute"); ?>
                         <div class="table-responsive pb-0">
                              <table class="table pt-3" id="type-table">
                                   <thead>
                                        <tr class="shadow-sm">
                                             <th class="border-0 border-bottom-0 bg-light shadow-none no-sort">
                                                     <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>#</span>
                                                </th>                                             
                                                <th class="border-0 border-bottom-0 bg-light shadow-none text-left">
                                                     <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Type</span>
                                                </th>
                                                <th class="border-0 border-bottom-0 bg-light shadow-none text-left">
                                                     <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Code</span>
                                                </th>                                                
                                                <th class="border-0 border-bottom-0 bg-light shadow-none no-sort">
                                                     <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>&nbsp;</span>
                                                </th>
                                           </tr>
                                   </thead>
                                   <tbody class="no-border narrow-height">
                                        <?php
                                        foreach ($types as $type) {
                                             echo "
                                             <tr class='user-row'>
                                                  <td class='text-center'></td>
                                                  <td>
                                                       " . $type['name'] . "
                                                  </td>
                                                  <td>
                                                       " . $type['code'] . "
                                                  </td>
                                                  <td class='text-right'>
                                                       <a href='#' class='btn btn-twitter btn-sm text-monospace updateButton' data-id='" . $type['id'] . "' data-name='" . $type['name'] . "' data-code='" . $type['code'] . "'> <small>UPDATE</small> </a>
                                                       <a class='btn btn-danger btn-sm text-monospace ml-1' title='Remove' href='" . base_url('ItemType/remove/') . $type['id'] . "'> <small>DELETE</small> </a>
                                                  </td>
                                             </tr>";
                                        }
                                        ?>
                                   </tbody>
                                   <tfoot class="hidden-footer">
                                      <tr>
                                           <th colspan="4"></th>
                                      </tr>
                                 </tfoot>
                              </table>
                         </div>                         
                    </div>
               </div>
               <div class="col-md-4">
                    <div id="type-create-tab">
                         <?php echo tab_header("New Type", null); ?>
                         <form method="post" action="<?php echo base_url('ItemType'); ?>" class="type-create-form needs-validation" autocomplete="off" novalidate>
                              <div class="row narrow-gutters">
                                   <div class="col-12">
                                        <div class="form-group">
                                             <label>Type <span class="text-danger">*</span></label>
                                             <input class="form-control text-uppercase" placeholder="Item type name" type="text" name="name" id="name" value="<?php echo set_value('name'); ?>" maxlength="50" required>
                                             <div class="valid-feedback"> Looks good! </div>
                                             <div class="invalid-feedback">Enter a unique item type.</div>
                                        </div>
                                   </div>
                              </div>
                              <div class="row narrow-gutters">
                                   <div class="col-12">
                                        <div class="form-group">
                                             <label>Code <span class="text-danger">*</span></label>
                                             <input class="form-control text-uppercase" placeholder="Item type code. Ex: ABC" type="text" name="code" id="code" value="<?php echo set_value('code'); ?>" maxlength="3" pattern="[A-Za-z]{3}" required>
                                             <div class="valid-feedback"> Looks good! </div>
                                             <div class="invalid-feedback">Enter a unique code. Should be 3 letters from A-Z only.</div>
                                        </div>
                                   </div>
                              </div>
                              <p class="card-description">
                                   <span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
                              </p>
                              <div class="form-group row form-btns narrow-gutters">
                                   <div class="col-6">
                                        <a href="#" class="btn btn-secondary btn-clear-fields w-100 text-uppercase">Clear</a>
                                   </div>
                                   <div class="col-6">
                                        <input type="submit" class="btn btn-primary create-type-btn w-100 text-uppercase" name='add' value="Save">
                                   </div>
                              </div>
                         </form>               
                    </div>
                    <div class="hidden" id="type-update-tab">
                         <?php echo tab_header("Update", null); ?>
                         <form method="post" action="<?php echo base_url('ItemType/update'); ?>" id="type-update-form" class="needs-validation" autocomplete="off" novalidate>
                              <input type="hidden" name="current_id" id="current_id">
                              <div class="row narrow-gutters">
                                   <div class="col-12">
                                        <div class="form-group">
                                             <label>Name <span class="text-danger">*</span></label>
                                             <input class="form-control text-uppercase" placeholder="Item Type Name" type="text" name="update_type_name" value="<?php echo set_value('update_type_name'); ?>" id="update_type_name" maxlength="50" required>
                                             <!-- <input class="form-control" placeholder="" type="hidden" id="check_type_name"> -->
                                             <div class="valid-feedback"> Looks good! </div>
                                             <div class="invalid-feedback">Enter a unique item type.</div>
                                        </div>
                                        <div class="form-group">
                                             <label>Code <span class="text-danger">*</span></label>
                                             <input class="form-control text-uppercase" placeholder="Item type code. Ex: ABC" type="text" name="update_type_code" id="update_type_code" value="<?php echo set_value('update_type_code'); ?>" maxlength="3" pattern="[A-Za-z]{3}" required>
                                             <div class="valid-feedback"> Looks good! </div>
                                             <div class="invalid-feedback">Enter a unique code. Should be 3 letters from A-Z only.</div>
                                        </div>
                                   </div>
                              </div>
                              <p class="card-description">
                                   <span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
                              </p>
                              <div class="form-group row narrow-gutters form-btns">
                                   <div class="col-6">
                                        <a href="#" class="btn btn-secondary btn-cancel w-100 text-uppercase">Cancel Update</a>
                                   </div>
                                   <div class="col-6">
                                        <input type="submit" class="btn btn-primary mr-2 confirm-update w-100 text-uppercase" name="update" value="Save Changes">
                                   </div>
                              </div>
                         </form>               
                    </div>
               </div>
          </div>
     </div>
</div>

<script type="text/javascript">
     $(document).ready(function(){
           var table = $('#type-table').DataTable({
               "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 'no-sort'
               }],
               "order": [
                    [1, 'asc']
               ],
               "autoWidth": false
          });

          table.on('order.dt search.dt', function() {
               table.column(0, {
                    search: 'applied',
                    order: 'applied'
               }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
               });
          }).draw();

          validateForm();
     });

     var validateForm = function() {
          'use strict';
          var forms = document.getElementsByClassName('needs-validation');
          var validation = Array.prototype.filter.call(forms, function(form) {
               form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                         event.preventDefault();
                         event.stopPropagation();
                    } else {
                         $(this).find('button[type="submit"]').text('Processing');
                         $(this).find('button[type="submit"]').attr('disabled', 'disabled');
                    }
                    form.classList.add('was-validated');
               }, false);
          });
     }

     $(".updateButton").on('click', function() {
          var id = $(this).attr("data-id");
          var name = $(this).attr("data-name");
          var code = $(this).attr("data-code");

          $("#type-create-tab").addClass('hidden');
          $("#type-update-tab").removeClass('hidden');
          $(".updateButton").prop("disabled", true);
          $(this).closest('tr').addClass('highlighted');
          $("#update_type_name").val(name);
          $("#update_type_code").val(code);
          $("#current_id").val(id);
     });

     $(".btn-cancel").on('click', function() {
          $("#type-update-tab").addClass('hidden');
          $("#type-create-tab").removeClass('hidden');
          $(".user-row").removeClass('highlighted');
          $(".updateButton").prop("disabled", false);
          var fields = $(this).closest('form').find('input');
          fields.each(function(){
               $(this).val('');
          });
     });

     $(".btn-clear-fields").on('click', function() {
          $(".type-create-form").validate().resetForm();
     });

</script>
