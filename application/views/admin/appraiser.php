<style>
     .dataTables_wrapper .dataTables_length {
          float: right;
          margin-left: 12px;
          margin-bottom: 12px;
     }
</style>

<?php echo admin_settings_tab('Appraiser'); ?>

<div class="card border-top-0">
     <div class="card-body">          
          <div class="row">               
               <div class="col-md-8 pb-5 pb-md-0">
                    
                    <!-- Appraisers table -->
                    <div class="" id="type-list-tab">
                         <?php echo tab_header("Appraisers", "position-absolute"); ?>
                         <div class="table-responsive pb-0">
                              <table class="table pt-3" id="appraiser-table">
                                   <thead>
                                        <tr class="shadow-sm">
                                             <th class="border-0 border-bottom-0 shadow-none no-sort">
                                                  <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>#</span>
                                             </th>
                                             <th class="border-0 border-bottom-0 shadow-none text-left">
                                                  <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>Full Name</span>
                                             </th>
                                             <th class="border-0 border-bottom-0 shadow-none no-sort">
                                                  <span class='font-12 font-weight-bold text-body py-1 d-block text-uppercase'>&nbsp;</span>
                                             </th>
                                        </tr>
                                   </thead>
                                   <tbody class="no-border narrow-height">
                                        <?php
                                        foreach ($appraisers as $appraiser) {
                                             echo "
                                             <tr>
                                                  <td class='text-center'>" . $appraiser['id'] . "</td>
                                                  <td>" . $appraiser['name'] . "</td>
                                                  <td class='text-right'>                                                       
                                                       <a href='#' class='btn btn-twitter btn-sm text-monospace updateButton' data-id='" . $appraiser['id'] . "' data-name='" . $appraiser['name'] . "'>
                                                            UPDATE
                                                       </a>                                                            
                                                  </td>
                                             </tr>";
                                        }
                                        ?>
                                   </tbody>
                                   <tfoot class="hidden-footer">
                                        <tr>
                                             <th colspan="3"></th>
                                        </tr>
                                   </tfoot>

                              </table>
                         </div>               
                    </div>
                    <!-- Appraisers table end -->
               </div>
               <div class="col-md-4">

                    <!-- Create Appraiser form -->
                    <div class="" id="appraiser-create-tab">               
                         <?php echo tab_header("New Appraiser"); ?>
                         <form method="post" action="<?php echo base_url('Appraiser'); ?>" class="appraiser-create-form">
                              <div class="row narrow-gutters">
                                   <div class="col-12">
                                        <div class="form-group">
                                             <label>Appraiser Name <span class="text-danger">*</span></label>
                                             <input class="form-control" placeholder="Appraiser Name" type="text" name="name" id="name" required>
                                        </div>
                                   </div>
                              </div>
                              <p class="card-description">
                                   <span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
                              </p>
                              <div class="form-group row narrow-gutters form-btns">
                                   <div class="col-6">
                                        <button type="button" class="btn btn-light border text-uppercase btn-block btn-clear-fields">Clear</button>
                                   </div>
                                   <div class="col-6">
                                        <button type="button" class="btn btn-primary btn-block text-uppercase create-appraiser-btn">SAVE</button>
                                   </div>                                   
                              </div>
                              <div id="createModal" class="modal fade confirm-modal" role="dialog">
                                   <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                             <div class="modal-header">
                                                  <h5 class="modal-title">Confirmation</h5>
                                                  <button type="button" class="close" data-dismiss="modal">
                                                       <span aria-hidden="true">&times;</span>
                                                  </button>
                                             </div>
                                             <div class="modal-body">
                                                  <p class="mb-0">Are you sure you want to add new appraiser?</p>
                                             </div>
                                             <div class="modal-footer">
                                                  <button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
                                                  <button type="submit" class="col btn btn-primary confirm" name="add">CONFIRM</button>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </form>
                    </div>
                    <!-- Create Apraiser form end -->

                    <!-- Update Appraiser form -->
                    <div class="hidden" id="appraiser-update-tab">
                         <?php echo tab_header("Update Appraiser"); ?>
                         <form method="post" action="<?php echo base_url('Appraiser/update'); ?>" id="appraiser-update-form">
                              <input type="hidden" name="current_id" id="current_id">
                              <div class="row narrow-gutters">
                                   <div class="col-12">
                                        <div class="form-group">
                                             <label>Appraiser Name <span class="text-danger">*</span></label>
                                             <input class="form-control" placeholder="Appraiser Name" type="text" name="update_appraiser_name" id="update_appraiser_name">
                                             <input class="form-control" placeholder="" type="hidden" id="check_appraiser_name">
                                        </div>
                                   </div>
                              </div>
                              <div class="form-group row form-btns narrow-gutters">
                                   <div class="col-6">
                                        <button type="button" class="btn btn-light btn-block border btn-cancel">CANCEL UPDATE</button>
                                   </div>
                                   <div class="col-6">
                                        <button type="button" class="btn btn-primary btn-block confirm-update">SAVE CHANGES</button>
                                   </div>
                              </div>
                              <div id="updateModal" class="modal fade confirm-modal" role="dialog">
                                   <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                             <div class="modal-header">
                                                  <h5 class="modal-title">Confirmation</h5>
                                                  <button type="button" class="close" data-dismiss="modal">
                                                       <span aria-hidden="true">&times;</span>
                                                  </button>
                                             </div>
                                             <div class="modal-body">
                                                  <p class="mb-0">Are you sure you want to update appraiser?</p>
                                             </div>
                                             <div class="modal-footer">
                                                  <button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
                                                  <button type="submit" class="col btn btn-primary confirm" name="update">CONFIRM</button>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </form>               
                    </div>
                    <!-- Update Appraiser form end -->

               </div>
          </div>
     </div>
</div>
<script type="text/javascript">
     $(document).ready(function() {

          var table = $('#appraiser-table').DataTable({
               "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
               }],
               "order": [
                    [1, 'asc']
               ],
               "columnDefs": [{
                    "targets": 'no-sort',
                    "searchable": false,
                    "orderable": false
               }]
          });

          table.on('order.dt search.dt', function() {
               table.column(0, {
                    search: 'applied',
                    order: 'applied'
               }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
               });
          }).draw();

     });

     $(".create-appraiser-btn").on('click', function() {
          $(".appraiser-create-form").validate({
               rules: {
                    name: {
                         required: true,
                         maxlength: 50
                    }
               }
          });
          if ($('.appraiser-create-form').valid()) {
               console.log($('.appraiser-create-form').valid());
               $("#createModal").modal('toggle');
               var name = $("#name").val();

               $("#name_preview").html("NAME: <b>" + name + "</b>");
          } else {
               console.log('not validate');
          }
     });

     $(".updateButton").on('click', function() {
          var id = $(this).attr("data-id");
          var name = $(this).attr("data-name");

          $("#appraiser-create-tab").addClass('hidden');
          $("#appraiser-update-tab").removeClass('hidden');

          $(".updateButton").prop("disabled", true);
          $(this).closest('tr').addClass('highlighted');

          $("#update_appraiser_name").val(name);

          $("#check_appraiser_name").val(name);

          $("#current_id").val(id);
     });

     $(".confirm-update").on('click', function() {
          $("#appraiser-update-form").validate({
               rules: {
                    update_appraiser_name: {
                         required: true,
                         maxlength: 50
                    }
               }
          });
          if ($('#appraiser-update-form').valid()) {
               var checkName = $("#check_appraiser_name").val();

               var inputName = $("#update_appraiser_name").val();

               if ((checkName == inputName)) {
                    $.notify("No changes were made", "warn");
               } else {
                    $("#updateModal").modal('toggle');
               }
          } else {
               console.log('not validate');
          }
     });


     $(".btn-cancel").on('click', function() {
          $("#appraiser-update-tab").addClass('hidden');
          $("#appraiser-create-tab").removeClass('hidden');
          $(".user-row").removeClass('highlighted');
          $(".updateButton").prop("disabled", false);

          var fields = $(this).closest('form').find('input');
          fields.each(function() {
               $(this).val('');
          });
     });

     $(".btn-clear-fields").on('click', function() {
          $(".appraiser-create-form").validate().resetForm();
     });
</script>
