<form class="needs-validation d-block form-inline" action="<?php echo base_url('Report/journal'); ?>" method="post" id="report-form" autocomplete="off" novalidate>

     <?php echo report_location_daterange($title, $branch_info, $date_start, $date_end, $branches, ($reports ? true : false), true ); ?>
     
     <!-- <ul class="nav nav-tabs mt-3">          
          <li class="nav-item">
               <input type="submit" name="summary" class="nav-link active" href="<?php echo base_url('Report/journal'); ?>" value="Summary">
          </li>
     </ul>      -->
     
     <?php 
          if($reports) {
               if($branch_info['id'] == 'summary') {
                    $sum_exempt = array("#", "date_range", "location");
                    $currency_counter = array("#");
               } else {
                    $sum_exempt = array("#", "date", "transaction_id", "type", "particulars", "description", "status");
                    $currency_counter = array("#", "balance");
               }

               $currency_columns = array("cash_in", "cash_out", "difference");
               report_table($reports, $sum_exempt, $currency_columns, $currency_counter);
               echo reportFooterCTA(true);
          } else {
               result_not_found(base_url('Report'), "mt-5");
          }
     ?>
</form>

<div class="text-muted text-left d-inline-block align-top mr-3">
     <p class="text-small mb-0">CASH IN</p>
     <ul class="text-small text-monospace">
          <li>Pawn renew (penalty and charges) </li>
          <li>Pawn repurchase (penalty, charges, and item value)</li>
          <li>Pawn sales</li>
          <li>Fund/cash transfer from other location</li>
          <li>Fund/cash deposit</li>
          <li>Fund/cash deposit</li>
          <li>Retail sales</li>
     </ul>
</div>
<div class="text-muted text-left d-inline-block align-top">
     <p class="text-small mb-0">CASH OUT</p>
     <ul class="text-small text-monospace">
          <li>Pawn item appraised value</li>     
          <li>Pawn item cash refund</li>     
          <li>Purchase item appraised value</li>     
          <li>Purchase item cash refund</li>     
          <li>Fund/cash transfer to other location</li>
          <li>Fund/cash withdrawal</li>
          <li>Expenses</li>
          <li>Retail registration/purchase</li>
          <li>Retail cash refund</li>
     </ul>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">

     
     var page_loaded = 0;
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          validateForm();
          clearFields();
          if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');          
          }     
          clickedSubmit();
     });
     $(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>