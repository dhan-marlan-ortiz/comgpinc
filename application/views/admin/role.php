<div class="row mb-2">
     <div class="col-12 col-sm-8">
          <h4 class="font-weight-bold">Access and Privilage</h4>
          <p class="text-muted">Create account roles/types and set its privillages to system. </p>
     </div>
     <div class="col-12 col-sm-4 text-right-sm">
          <a class="btn btn-danger btn-icon-text btn-rounded" href="<?php echo base_url('User'); ?>" >
               <small class="ti-agenda btn-icon-prepend"></small>User Accounts
          </a>
     </div>
</div>

<div class="row narrow-gutters">
     <div class="col-md-4 grid-margin">

          <!-- Create role form -->
          <div class="card" id="role-create-tab">
               <div class="card-body">
                    <h4 class="card-title text-primary">Add New Role</h4>

                    <form method="post" action="<?php echo base_url('Role'); ?>" class="role-create-form">
                         <div class="row narrow-gutters">
                              <div class="col-12 col-sm-6">
                                   <div class="form-group">
                                        <label>Code <span class="text-danger">*</span></label>
                                        <input class="form-control" placeholder="" type="text" name="id" id="id">
                                   </div>
                              </div>
                              <div class="col-12 col-sm-6">
                                   <div class="form-group">
                                        <label>Access Name <span class="text-danger">*</span></label>
                                        <input class="form-control" placeholder="Role" type="text" name="name" id="name">
                                   </div>
                              </div>
                         </div>
                         <p class="card-description">
                              <span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
                         </p>
                         <div class="form-group row narrow-gutters form-btns">
                              <div class="col-6">
                                   <button type="button" class="btn btn-primary create-role-btn">Save</button>
                              </div>
                              <div class="col-6">
                                   <button type="button" class="btn btn-secondary btn-clear-fields">Clear</button>
                              </div>
                         </div>
                         <div id="createModal" class="modal fade" role="dialog">
                           <div class="modal-dialog modal-sm">
                             <div class="modal-content">
                               <div class="modal-header">
                                   <p class="card-description" style="margin-bottom: 0;">Role Details Preview</p>
                              </div>
                              <div class="modal-body">
                                   <p class="card-description" id="id_preview"></p>
                                   <p class="card-description" id="name_preview"></p>
                                   <button type="submit" class="btn btn-primary mr-2" name="add">Confirm</button>
                                   <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                              </div>
                         </div>
                    </div>
               </div>
          </form>
     </div>
</div>
<!-- Create role form end -->

<!-- Update role form -->
<div class="card hidden" id="role-update-tab">
     <div class="card-body">
          <div class="row">
               <div class="col-6 mb-2">
                    <h4 class="card-title text-primary">Update Role</h4>
               </div>
               <div class="col-6 text-right">
                    <button type="button" class="btn btn-inverse-dark btn-sm btn-icon-text btn-return btn-icon-prepend"><i class="ti-arrow-left"></i> Return</button>
               </div>
          </div>
          <p class="card-description">
               <span class="ti-info-alt text-info">&nbsp;</span>Field marked with <span class="text-danger">*</span> is required.
          </p>
          <form method="post" action="<?php echo base_url('Role/update'); ?>" id="role-update-form">
               <input type="hidden" name="current_id" id="current_id">
               <div class="row narrow-gutters">
                    <div class="col-12 col-sm-3">
                         <div class="form-group">
                              <label>Code <span class="text-danger">*</span></label>
                              <input class="form-control" placeholder="" type="text" name="update_role_id" id="update_role_id">
                              <input class="form-control" placeholder="" type="hidden" id="check_role_id">
                         </div>
                    </div>
                    <div class="col-12 col-sm-9">
                         <div class="form-group">
                              <label>Role <span class="text-danger">*</span></label>
                              <input class="form-control" placeholder="Role" type="text" name="update_role_name" id="update_role_name">
                              <input class="form-control" placeholder="" type="hidden" id="check_role_name">
                         </div>
                    </div>
               </div>
               <div class="form-group row form-btns">
                    <div class="col-6">
                         <button type="button" class="btn btn-primary mr-2 confirm-update">Confirm</button>
                    </div>
                    <div class="col-6">
                         <button type="button" class="btn btn-inverse-danger btn-cancel">Cancel</button>
                    </div>
               </div>
               <div id="updateModal" class="modal fade" role="dialog">
                 <div class="modal-dialog modal-sm">
                   <div class="modal-content">
                    <div class="modal-body">
                         <button type="submit" class="btn btn-primary mr-2" name="update">Confirm</button>
                         <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    </div>
               </div>
          </div>
     </div>
</form>
</div>
</div>
<!-- Update role form end -->
</div>
<div class="col-md-8 grid-margin stretch-card">
     <!-- roles table -->
     <div class="card" id="role-list-tab">
          <div class="card-body">
               <p class="card-title text-primary">role List</p>
               <div class="table-responsive">
                    <table class="table table-hover" id="role-table">
                         <thead>
                              <tr>
                                   <th>#</th>
                                   <th>Code</th>
                                   <th>Access Name</th>
                                   <th>Description</th>
                                   <th>Privilages</th>
                                   <th class="no-sort text-center">Actions</th>
                              </tr>
                         </thead>
                         <tbody>
                              <?php
                              foreach ($roles as $role) {
                                   echo "
                                   <tr class='user-row'>
                                   <td></td>
                                   <td>" . $role['id'] . "</td>
                                   <td>" . $role['access_type'] . "</td>
                                   <td></td>
                                   <td></td>
                                   <td class='text-center'><button type='button' class='btn btn-primary btn-fw btn-sm updateButton' data-id='" . $role['id'] . "' data-name='" . $role['access_type'] . "'>Update</button></td>
                                   </tr>";
                              }
                              ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </div>
</div>
</div>
<script type="text/javascript">
     $(document).ready(function(){
          var table = $('#role-table').DataTable({
               "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 'no-sort',
               }],
               "order": [
                    [1, 'asc']
               ]
          });

          table.on('order.dt search.dt', function() {
               table.column(0, {
                    search: 'applied',
                    order: 'applied'
               }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
               });
          }).draw();
     });
     $(".create-role-btn").on('click', function() {
          $(".role-create-form").validate({
               rules: {
                    id : { required: true, maxlength: 10 },
                    name : { required: true, maxlength: 20 }
               }
          });
          if ($('.role-create-form').valid()) {
               console.log($('.role-create-form').valid());
               $("#createModal").modal('toggle');
               var id = $("#id").val();
               var name = $("#name").val();
               $("#id_preview").html("CODE: <b>" + id + "</b>");
               $("#name_preview").html("NAME: <b>" + name + "</b>");
          }
          else {
               console.log('not validate');
          }
     });
     $(".updateButton").on('click', function() {
          var id = $(this).attr("data-id");
          var name = $(this).attr("data-name");
          $("#role-create-tab").addClass('hidden');
          $("#role-update-tab").removeClass('hidden');
          $(".updateButton").prop("disabled", true);
          $(this).closest('tr').addClass('highlighted');
          $("#update_role_id").val(id);
          $("#update_role_name").val(name);
          $("#check_role_id").val(id);
          $("#check_role_name").val(name);
          $("#current_id").val(id);
     });
     $(".confirm-update").on('click', function() {
          $("#role-update-form").validate({
               rules: {
                    update_role_id : { required: true, maxlength: 10 },
                    update_role_name : { required: true, maxlength: 20 }
               }
          });
          if ($('#role-update-form').valid()) {
               var checkID = $("#check_role_id").val();
               var checkName = $("#check_role_name").val();
               var inputID = $("#update_role_id").val();
               var inputName = $("#update_role_name").val();
               if ((checkID == inputID) && (checkName == inputName)) {
                    $.notify("No changes were made", "warn");
               }
               else {
                    $("#updateModal").modal('toggle');
               }
          }
          else {
               console.log('not validate');
          }
     });
     $(".btn-return").on('click', function() {
          $("#role-update-tab").addClass('hidden');
          $("#role-create-tab").removeClass('hidden');
          $(".user-row").removeClass('highlighted');
          $(".updateButton").prop("disabled", false);
     });
     $(".btn-cancel").on('click', function() {
          $(".user-row").removeClass('highlighted');
          $(".updateButton").prop("disabled", false);
          var fields = $(this).closest('form').find('input');
          fields.each(function(){
               $(this).val('');
          });
     });
     $(".btn-clear-fields").on('click', function() {
          $(".role-create-form").validate().resetForm();
    });
</script>
