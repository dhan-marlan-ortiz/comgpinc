<form class="needs-validation d-block form-inline" action="<?php echo base_url('Report/pawnRecord'); ?>" method="post" id="report-form" autocomplete="off" novalidate>
     
     <?php 
          echo report_location_status_daterange($title, $branch_info, $date_start, $date_end, $status, $branches, ($reports ? true : false) ); ?>
     
     <!-- <ul class="nav nav-tabs mt-3">          
          <li class="nav-item">
               <input type="submit" name="summary" class="nav-link active" href="<?php echo base_url('Report/journal'); ?>" value="Summary">
          </li>
     </ul>      -->
     
     <?php 
          if($reports) {
               // if($branch_info['id'] == 'summary') {
               //      $sum_exempt = array("#", "date_range", "location");
               //      $currency_counter = array("#");
               // } else {
               //      $sum_exempt = array("#", "date", "transaction_id", "type", "particulars", "description", "status");
               //      $currency_counter = array("#", "balance");
               // }

               $sum_exempt = array("#", "transaction_id", "location", "customer", "contact", "item_type", "item_name", "serial", "status", "entry_date", "due_date", "last_update", "last_transaction");
               $currency_columns = array("value", "amount_due", "income");
               // $currency_counter
               report_table($reports, $sum_exempt, $currency_columns);
               echo reportFooterCTA(true);
          } else {
               result_not_found(base_url('Report'), "mt-5");
          }
     ?>
</form>

<script type="text/javascript" src="<?php echo base_url('assets/js/report.js'); ?>"></script>
<script type="text/javascript">

     
     var page_loaded = 0;
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          validateForm();
          clearFields();
          selectAll();
          if(page_loaded == 0) {
               $("#loading-page-modal").modal('show');       
          }     
          clickedSubmit();
          
          var date_rb = '<div class="d-inline-block w-50 text-right-sm">'; 
              date_rb +=      '<input class="" type="radio" name="date-range-option" id="entry-date-rb" value="entry" <?php echo ($this->input->post("date-range-option") == "entry" ? "checked" : ($this->input->post("date-range-option") == "update" ? "" : "checked")); ?> >';
              date_rb +=      '<label class="d-inline-block text-monospace ml-1 mr-3" for="entry-date-rb">ENTRY</label>'; 
              date_rb +=      '<input class="" type="radio" name="date-range-option" id="update-date-rb" value="update" <?php echo ($this->input->post("date-range-option") == "update" ? "checked" : ""); ?> >';
              date_rb +=      '<label class="d-inline-block text-monospace ml-1" for="inlineRadio2">UPDATE</label>';
              date_rb += '</div>';
                  
          $(date_rb).insertAfter("#date-range-filter label");
     });
     $(window).on('load', function() {
          loader();
          $("#loading-page-modal").modal('hide');          
          page_loaded = 1;
     });
</script>