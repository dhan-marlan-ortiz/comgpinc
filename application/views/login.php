<!DOCTYPE html>
<html lang="en">

<head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <title>CGP | Login</title>
     <link rel="shortcut icon" href="<?php echo base_url('assets/') ?>images/favicon.ico" />

     <!-- plugins:css -->
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>vendors/ti-icons/css/themify-icons.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>vendors/base/vendor.bundle.base.css">
     <!-- endinject -->

     <!-- plugin css for this page -->
     <!-- End plugin css for this page -->

     <!-- inject:css -->
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/style.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/main.css">
     <!-- endinject -->

	 <style media="screen">
		.brand-logo img {
			width: 100%;
			max-width: 70px;
		}
		.form-title h1 {			
			font-family: 'Raleway', sans-serif;
			font-size: 20px;
			line-height: 1.3em;
			text-shadow: 0px 0px 2px black;
		}
		h4 {
			font-weight: 400;
    		font-size: 16px;
		}	
		.form-title span {
			font-size: 18px;
		}
		.form-group {
			position: relative;
			margin-bottom: 10px !important;
		}
		input.error {
			margin-bottom: 35px;
			border-color: rgba(204, 0, 0, 0.5);
		}
		label.error {
			position: absolute;
			color: rgba(204, 0, 0, 0.8);
			bottom: -35px;
			font-size: 13px !important;
		}
		.errors-authentication {
			font-size: 13px;
			color: #fff;
			background-color: rgba(255, 0, 0, 0.3);
			padding: 10px;
			margin-top: 15px;
			border: solid 1px red;			
		}
		.content-wrapper {
			/* background-image: linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%); */
			background-image: url('assets/images/auth/lockscreen-bg.jpg');
			/* background-image: url('assets/images/auth/city-view-dawn-ortizph.jpg'); */
			background-position: center;
			background-size: cover;
			background-repeat: no-repeat;
		}
		.auth #user-id,
		.auth #password {
			color: #fff;
			border: none;
			border-bottom: solid 1px #fff;
			padding-left: 50px;
		}
		svg.bi {
			color: #ec1e27;
			width: auto;
			height: 43px;
			padding: 8px;
		}
		input:-webkit-autofill,
		input:-webkit-autofill:hover,
		input:-webkit-autofill:focus,
		input:-webkit-autofill:active {
			transition: background-color 5000s ease-in-out 0s;
		}
		input:-webkit-autofill {
			-webkit-text-fill-color: white !important;
		}
		.input-group-prepend {
			position: absolute;
		}
	</style>
</head>

<body>
<div class="container-scroller">
		<div class="container-fluid page-body-wrapper full-page-wrapper">
			<div class="content-wrapper align-items-center auth px-0">
				<table style="height: 100%; width: 100%;">
					<tr>
						<td>						
							<div class="row w-100 mx-0">
								<div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 mx-auto">						
									<div class="text-left p-4 px-sm-3 mx-auto" style="max-width: 400px;">
										<div class="brand-logo text-center mb-3">
											<img src='<?php echo base_url("assets/images/logo-mini.png"); ?>' alt='logo'>		                        
										</div>
										<div class="form-title text-center">
											<h1 class='text-cgp font-weight-bolder mb-5'>
												COMMUNITY GADGET PHONESHOP
											</h1>
										</div>
										
										<h4 class='text-white' style="text-shadow: 0px 0px 5px black;">Sign in to continue</h4>														
										<form class="pt-1 login-form needs-validation" method="POST" action="<?php echo base_url('Login'); ?>" novalidate>
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-prepend">
														<!-- <span class="input-group-text bg-transparent px-2 py-0" style="font-size: 20px;">
															<i class="ti-user text-cgp"></i>
														</span> -->
														<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
															<path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
														</svg>
													</div>
													<input type="text" class="form-control form-control-lg h-100" name="user_id" id="user-id" placeholder="User ID" value="<?php echo set_value('user-id'); ?>" autofocus required>
												</div>
											</div>
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-prepend">
													<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-key-fill" viewBox="0 0 16 16">
														<path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
													</svg>
														<!-- <span class="input-group-text bg-transparent px-2 py-0" style="font-size: 20px;">
															<i class="ti-lock text-cgp"></i>
														</span> -->
													</div>
													<input type="password" class="form-control form-control-lg h-100" name="password" id="password" placeholder="Password" value="<?php echo set_value('password'); ?>" required>
												</div>
											</div>
											<div class="mt-4">
												<input type="submit" class="btn btn-block btn-danger bg-cgp btn-lg font-weight-medium auth-form-btn" name="login" Value="SIGN IN" id="login">
											</div>								
											<!-- 
											<div class="my-2 d-flex justify-content-between align-items-center">
												<div class="form-check">
													<label class="form-check-label text-muted">
														<input type="checkbox" class="form-check-input"> Keep me signed in
													</label>
												</div>
												<a href="#" class="auth-link text-black">Forgot password?</a>
											</div>
											-->                                                                                                                                                    
										</form>							
										<?php
											if(null !== $this->session->flashdata('error')) {
												echo '<div class="errors-authentication">' . $this->session->flashdata('error') . '</div>';
											}
											if(validation_errors()) {
												echo '<div class="errors-validation text-danger">' . validation_errors('') . '</div>';
											}
										?>
									</div>		
									
									<div class="text-center">
										<a href="<?php echo base_url('Resource'); ?>" class="font-14 d-inline-block mt-4 mb-3 text-white" target="_blank">Click here to open Employee Resource Portal</a>
									</div>
								</div>
							</div>				
						</td>
					</tr>
				</table>
			</div>
			<!-- content-wrapper ends -->
		</div>
	</div>
	

	<!-- container-scroller -->
	<!-- plugins:js -->
	<script src="<?php echo base_url('assets/') ?>vendors/base/vendor.bundle.base.js"></script>
	<!-- endinject -->
	<!-- Plugin js for this page-->
	<script src="<?php echo base_url('assets/') ?>vendors/chart.js/Chart.min.js"></script>
	<!-- End plugin js for this page-->
	<!-- inject:js -->
	<script src="<?php echo base_url('assets/') ?>js/off-canvas.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/hoverable-collapse.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/template.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/todolist.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script src="<?php echo base_url('assets/') ?>js/dashboard.js"></script>

	<!-- End custom js for this page-->

	<script src="<?php echo base_url('assets/') ?>js/jquery-3.4.1.min.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/jquery.validate.min.js"></script>

     <?php
          echo addGoogleTag();
     ?>

     <script type="text/javascript">
   		$(function() {
   			'use strict';
			     var forms = document.getElementsByClassName('needs-validation');
			     var validation = Array.prototype.filter.call(forms, function(form) {
			          form.addEventListener('submit', function(event) {

			               if (form.checkValidity() === false) {
			                    event.preventDefault();
			                    event.stopPropagation();			                    
			               }

			               form.classList.add('was-validated');
			          }, false);
			     });
   		});
	</script>


</body>

</html>
