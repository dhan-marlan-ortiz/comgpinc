<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">
		My Profile
	</h1>
	<div class="btn-toolbar mb-2 mb-md-0">		
		<div id="btn-wrapper" class='text-center text-sm-right'>
			<div class="tab-content" id="profile-actions-tab">
				<div class="tab-pane active" id="profile-content-default" role="tabpanel">          									
					<a href="#profile-content-update" id="update-profile-btn" class="btn btn-sm btn-outline-primary" data-toggle="tab" role="tab">						
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path><polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon></svg>
						UPDATE
					</a>											
				</div>			
				<div class="tab-pane" id="profile-content-update" role="tabpanel">          
					<a href="<?php echo base_url('Resource/myprofile'); ?>" class="btn btn-sm btn-outline-danger">
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
						CANCEL
					</a>
					
					<button type="button" class="btn btn-sm btn-outline-success" data-target="#employee-update-modal" data-toggle="modal">
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="20 6 9 17 4 12"></polyline></svg>
						SAVE CHANGES
					</button>
				</div>
			</div>
		</div>
	</div>
</div>	

<form id="employee-registration-form" method="POST" action="<?php echo base_url('Resource/updateprofile/') . $profile['id']; ?>" class="needs-validation locked pb-3 <?php echo (validation_errors() ? 'was-validated' : '')?> " novalidate autocomplete="off">
	<div class="row">
		<div class="col-md-8">
			<div class="row narrow-gutters">
				<div class="col-sm-4 col-md-6 col-xl-4">
					<div class="form-group">
						<label class="text-muted no-wrap">First Name</label>
						<div class="input-group">
							<input  type="text" class="form-control bg-white" name="first_name" value="<?php echo $profile['first_name']; ?>" readonly>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-6 col-xl-4">
					<div class="form-group">
						<label class="text-muted no-wrap">Middle Name</label>
						<div class="input-group">
							<input  type="text" class="form-control bg-white" name="middle_name" value="<?php echo $profile['middle_name']; ?>" required readonly>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-6 col-xl-4">
					<div class="form-group">
						<label class="text-muted no-wrap">Last Name</label>
						<div class="input-group">
							<input  type="text" class="form-control bg-white" name="last_name"value="<?php echo $profile['last_name']; ?>"required readonly>
						</div>
					</div>
				</div>					
				<div class="col-sm-6 col-md-6 col-xl-3">
					<div class="form-group">
						<label class="text-muted no-wrap">Date of Birth</label>
						<input  type="text" class="form-control datepicker" name="date_of_birth" 
						value="<?php echo ($profile ? $profile['date_of_birth'] : set_value('date_of_birth')); ?>" 
						placeholder="Date of Birth" required maxlength="10" required>			                    
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-xl-3">
					<div class="form-group">
						<label class="text-muted no-wrap">Contact Number</label>
						<input  type="text" class="form-control" name="contact" 
						value="<?php echo ($profile ? $profile['contact'] : set_value('contact')); ?>" 
						placeholder="Contact Number" required maxlength="20">			                    
					</div>
				</div>					
				<div class="col-sm-6 col-md-6 col-xl-3">
					<div class="form-group">
						<label class="text-muted no-wrap">Email Address</label>
						<input  type="email" class="form-control" name="email" 
						value="<?php echo ($profile ? $profile['email'] : set_value('email')); ?>" 
						placeholder="Email Address" maxlength="50">			                    
					</div>
				</div>
				<div class="col-sm-6 col-md-12 col-xl-3">
					<div class="form-group">
						<label class="text-muted no-wrap">Password</label>
						<input  type="text" class="form-control" name="password"
						value="<?php echo ($profile ? $profile['password'] : set_value('password')); ?>" 
						placeholder="Password" required maxlength="20">			                    
					</div>
				</div>
				<div class="col-md-12 col-xl-6">
					<div class="form-group">
						<label class="text-muted no-wrap">Complete Address</label>
						<textarea name="address" id="" rows="3" class="form-control line-height-1-5em" maxlength="250" placeholder="Complete Address" style="min-height:141px;" required><?php echo ($profile ? $profile['address'] : set_value('address')); ?></textarea>
					</div>	              
				</div>
				<div class="col-md-12 col-xl-6">
					<div class="form-group">
						<label class="text-muted no-wrap">Remarks</label>
						<textarea name="remarks" id="" rows="3" class="form-control  bg-white line-height-1-5em" maxlength="250" placeholder="Remarks" style="min-height:141px;" readonly><?php echo ($profile ? $profile['remarks'] : set_value('remarks')); ?></textarea>
					</div>	              
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="row narrow-gutters">
				<div class="col-sm-6 col-md-12 col-xl-6">
					<div class="form-group">
						<label class="text-muted no-wrap">Employee Code</label>
						<input  type="text" class="form-control bg-white" name="id" value="<?php echo $profile['id']; ?>"required readonly>
					</div>
				</div>						
				<div class="col-sm-6 col-md-12 col-xl-6">
					<div class="form-group">
						<label class="text-muted no-wrap">Location</label>
						<input  type="text" class="form-control bg-white" name="location" value="<?php echo $profile['location']; ?>" readonly>
					</div>
				</div>
				<div class="col-sm-6 col-md-12 col-xl-6">
					<div class="form-group">
						<label class="text-muted no-wrap">Salary</label>
						<div class="input-group">
							<input  type="text" class="form-control bg-white float-format" name="salary" placeholder="Salary" value="<?php echo $profile['salary']; ?>" readonly>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-12 col-xl-6">
					<div class="form-group">
						<label class="text-muted no-wrap">Employment Status</label>
						<div class="input-group">
							<input  type="text" class="form-control bg-white" name="employment_status" value="<?php echo $profile['employment_status']; ?>" readonly>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="text-muted no-wrap">TIN</label>
						<div class="input-group">
							<input  type="text" class="form-control bg-white" name="tin" value="<?php echo $profile['tin']; ?>" readonly>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label class="text-muted no-wrap">SSS</label>
						<div class="input-group">
							<input  type="text" class="form-control bg-white" name="sss" value="<?php echo $profile['sss']; ?>"  readonly>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-12">
					<div class="form-group">
						<label class="text-muted no-wrap">Position</label>
						<input type="text" class="form-control bg-white" name="position" value="<?php echo $profile['position']; ?>" required readonly>
					</div>
				</div>		
				<div class="col-sm-6 col-md-12">
					<div class="form-group">
						<label class="text-muted no-wrap">Date Hired</label>
						<div class="input-group">
							<input  type="text" class="form-control datepicker border-radius-2 bg-white" name="date_hired" value="<?php echo $profile['date_hired']; ?>" required readonly>
						</div>
					</div>
				</div>									
			</div>											
		</div>
	</div>	

	<div id="employee-update-modal" class="modal fade confirm-modal" role="dialog">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Confirmation</h5>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="mb-0">Are you sure want to update profile?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</button>
					<button type="submit" class="col btn btn-success confirm">CONFIRM</button>
				</div>
			</div>
		</div>
	</div>	
</form>		

<script type="text/javascript">
	$(document).ready( function() {
		initSelect2();
		initDatePicker();
		formValidation();
		clearFields("#employee-registration-clear", "#employee-registration-form input");			
	
		$("#update-profile-btn").on("click", function() {
			$("#employee-registration-form").removeClass('locked');
			$("#employee-registration-form").addClass('editing');
		});
		
		$('textarea').keypress(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
			}
		});

		$("input[name='password']").on({
			keydown: function(e) {
		    if (e.which === 32)
		      return false;
		    },
		  	change: function() {
		    	this.value = this.value.replace(/\s/g, "");
		  	}
		});
	});	
	
</script>