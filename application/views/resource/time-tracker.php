<style>
	.dataTables_wrapper label {
		/*margin-bottom: 0;*/
		/*margin-left: 20px;*/
		font-weight: 500;
	}
	.dataTables_wrapper label input,
	.dataTables_wrapper label select {
		/*height: 31px;*/
		/*font-size: 14px;*/
		/*padding: 0px 8px;*/
	}
	.dataTables_wrapper .dataTables_length {
		/*float: right*/
	}
	.dataTables_wrapper .dataTables_length select {
		margin: 0 0.25rem;
	}
	.dataTables_wrapper .dataTables_filter {
		
	}
    
	
</style>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">
		Time Tracker
	</h1>
</div>	

<?php        	
	if($attendance) {
		
		$col_count = 0;
		echo
		     "
		          <table class='table table-striped'>
		               <thead>
		               		<tr>
		               			<th class='border-top-0'>Date</th>
		               			<th class='border-top-0'>Time In</th>
		               			<th class='border-top-0'>Time Out</th>
		               		</tr>
		               ";		                         
		echo                    
		               "</thead>
		                <tbody'>";
	                         foreach ($attendance as $r => $a) {
	                              echo tr_open();	         
	                              echo td($a['date']);
	                              echo td($a['time_in']);
	                              echo td($a['time_out']);
	                              echo tr_close();
	                         }
		echo                    
		               "</tbody>		               
		          </table>
		     ";
		}

?>

<script type="text/javascript">
	$(document).ready( function() {

		var table = $('.table').DataTable();
		
	});	
	
</script>