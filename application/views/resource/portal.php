
	
	<form class="form-signin text-center" method="POST" action="<?php echo base_url('Portal'); ?>" >
		<img class="mb-4" src="<?php echo base_url('assets/images/logo-mini.png'); ?>" alt="comgpinc" width="72" height="72">
		
		<h1 class="h4 mb-4 mt-1 font-weight-light text-cgp">Employee Resource Portal</h1>
		
		<label for="inputEmail" class="sr-only">Employee ID</label>
		<input type="text" name="employee_id" class="form-control" placeholder="Employee ID" required autofocus>
		
		<label for="inputPassword" class="sr-only">Password</label>
		<input type="password" name="password" class="form-control" placeholder="Password" required>
		
		<input class="btn btn-lg btn-danger bg-cgp btn-block mt-4" type="submit" name="login" value="Sign in">

		<?php
			if(null !== $this->session->flashdata('error')) {
				echo '<div class="errors-authentication">' . $this->session->flashdata('error') . '</div>';
			}
			if(validation_errors()) {
				echo '<div class="errors-validation text-danger">' . validation_errors('') . '</div>';
			}
		?>

		<p class="mt-4 mb-3 text-muted">
			<a href="<?php echo base_url(); ?>">Return to comgpinc login page</a>
		</p>
	</form>


