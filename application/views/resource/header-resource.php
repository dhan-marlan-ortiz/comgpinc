<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Community Gadget Phoneshop Employee Resource Portal">
	<meta name="author" content="Dhan Marlan Ortiz">
	
	<title>Employee Resource Portal</title>	

	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/select2.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker3.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/resource/jquery.dataTables.min.css'); ?>">
	

	<!-- Favicons -->
	
	<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico'); ?>">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}
	</style>
	<?php 
		if(isset($additional_css)) {
			foreach ($additional_css as $ac_src) {
				echo '<link href="' . base_url($ac_src) .'" rel="stylesheet">';
			}
		}
	?>
	<style type="text/css">/* Chart.js */
    	@-webkit-keyframes chartjs-render-animation{
    		from{
    			opacity:0.99
			}
			to{
				opacity:1
			}
		}
		@keyframes chartjs-render-animation{
			from{
				opacity:0.99
			}
			to{
				opacity:1
			}
		}
		.chartjs-render-monitor{
			-webkit-animation:chartjs-render-animation 0.001s;
			animation:chartjs-render-animation 0.001s;
		}
	</style>

	<link href="<?php echo base_url('assets/css/resource.css'); ?>" rel="stylesheet">

	<script src="<?php echo base_url('assets/') ?>js/jquery-3.4.1.min.js"></script>
</head>
<body>

<?php 

if(null !== $this->session->userdata('employee_id')) { 
	$method = $this->router->fetch_method();
?>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
	<a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="<?php echo base_url('Resource/myprofile'); ?>">Community Gadget Phoneshop</a>
	<button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>	
	<ul class="navbar-nav px-3">
		<li class="nav-item text-nowrap">
			<a class="nav-link" href="<?php echo base_url('Resource/myprofile'); ?>">
				<?php 
					echo $profile['first_name'] . " " . $profile['last_name'];
				?>
			</a>
		</li>
	</ul>
</nav>

<div class="container-fluid">
	<div class="row">
		<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-dark sidebar collapse">
			<div class="sidebar-sticky pt-3">
				<ul class="nav flex-column">
					<li class="nav-item">
						<a class="nav-link text-white-50 <?php echo ($method == 'myprofile' ? 'active' : ''); ?>" href="<?php echo base_url('Resource/myprofile'); ?>" >
							<svg class="feather feather-users" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
								<path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"> </path>
								<circle cx="12" cy="7" r="4"> </circle>
							</svg>
							My profile
						</a>
					</li>				
					<li class="nav-item">
						<a class="nav-link text-white-50 <?php echo ($method == 'timetracker' ? 'active' : ''); ?>" href="<?php echo base_url('Resource/timetracker'); ?>">
							<svg class="feather feather-users" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
								<circle cx="12" cy="12" r="7"></circle>
								<polyline points="12 9 12 12 13.5 13.5"></polyline>
								<path d="M16.51 17.35l-.35 3.83a2 2 0 0 1-2 1.82H9.83a2 2 0 0 1-2-1.82l-.35-3.83m.01-10.7l.35-3.83A2 2 0 0 1 9.83 1h4.35a2 2 0 0 1 2 1.82l.35 3.83"></path>
							</svg>
							Time tracker
						</a>
					</li>				
					<li class="nav-item">
						<hr>
					</li>		
					<li class="nav-item">
						<a class="nav-link text-white-50" href="<?php echo base_url('Logout/resource'); ?>">
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M18.36 6.64a9 9 0 1 1-12.73 0"></path><line x1="12" y1="2" x2="12" y2="12"></line></svg>
							Sign out
						</a>
					</li>		
				</ul>
				
			</div>
		</nav>

		<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">			
			<!-- <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> -->
<?php } ?>