<!DOCTYPE html>
<html lang="en">

<head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <title>CGP | <?php echo $title ?></title>
     <link rel="shortcut icon" href="<?php echo base_url('assets/') ?>images/favicon.ico" />

     <!-- plugins:css -->
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>vendors/ti-icons/css/themify-icons.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>vendors/base/vendor.bundle.base.css">
     <!-- endinject -->

     <!-- plugin css for this page -->
     <!-- End plugin css for this page -->

     <!-- inject:css -->
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/style.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/main.css">
     <!-- endinject -->

</head>

<body>

<?php echo $navbar; ?>

<!-- container scroller -->
<div class="container-scroller">
     <div class="container-fluid page-body-wrapper">

          <?php echo $sidebar; ?>

          <div class="main-panel">
               <div class="content-wrapper">
                    <div class="row">
                         <div class="col-md-12 grid-margin">
                              <div class="d-flex justify-content-between align-items-center">
                                   <div>
                                        <h4 class="font-weight-bold mb-0">
                                             <?php echo $title ?>
                                        </h4>
                                   </div>
                              </div>
                         </div>
                    </div>
