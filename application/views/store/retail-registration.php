<?php
     $date = date('Y-m-d');
     $role_fk = $this->session->userdata('role_fk');
     $branch_fk = $this->session->userdata['branch_fk'];
?>

<div class="row no-gutters">
     <div class="col-lg-1">
          <h4 class='font-weight-bold mb-3'>Retails</h4>
     </div>     
     <div class="col-lg-11 text-right animated fadeIn delay-1s">
          <div class="d-inline-block mb-2">
               <?php if($role_fk == 'ADMS') { ?>
               <div id="filter-location-wrapper" class="dataTables_filter">
                    <div class="input-group">
                         <div class="input-group-prepend">
                              <span class="input-group-text py-0 border-radius-left-2 ti-location-pin"></span>
                         </div>
                         <div class="input-group-append">
                              <select id="filter-location" class="select2 select2-sm" name="branch">
                                   <option value=""> ALL </option>
                                   <?php
                                   foreach ($branches as $fb) {
                                        echo "<option value='" . $fb['name'] . "'>" . $fb['name'] . " (" . $fb['id'] . ")</option>";
                                   }
                                    ?>
                              </select>
                         </div>
                    </div>
               </div>
               <?php } ?>
          </div>
          <div class="d-inline-block mb-2 ml-2" id="table-length"></div>
          <div class="d-inline-block mb-2 ml-2 text-uppercase" id="table-filter"></div>
          <div class="d-inline-block mb-2 ml-2">
               <button type="button" class="btn btn-secondary btn-icon-text btn-sm text-white border-radius-2" data-toggle="modal" data-target="#unit-modal">
                    UNITS<i class="btn-icon-append ti-ruler-pencil"></i>
               </button>
          </div>
          <div class="d-inline-block mb-2 ml-2">
               <button type="button" class="btn btn-secondary btn-icon-text btn-sm text-white border-radius-2" data-toggle="modal" data-target="#registration-modal">
                    ADD PRODUCT<i class="btn-icon-append ti-plus"></i>
               </button>
          </div>
     </div>
</div>

<div class="row no-gutters">
     <div class="col-12">
          <?php retail_tab('registration') ?>
     </div>     
</div>

<div class="row">
     <div class="col-12 col-sm-12">
          <div class="card border-top-0">
               <div class="card-body pt-4">
                    <div class="table-responsive pb-0">
                         <table class="table animated fadeIn" id="product-table">
                              <thead>
                                   <tr class="text-uppercase">
                                        <th class="no-sort bg-img-none">#</th>
                                        <th class="text-left">Registration ID</th>
                                        <th class="text-left">Date</th>
                                        <th class="text-left">Type</th>
                                        <th class="text-left">Product</th>
                                        <?php if($role_fk == 'ADMS') { ?>
                                             <th class="text-left">Location</th>
                                        <?php } ?>
                                        <th class="text-left">Status</th>
                                        <th class="no-sort">Qty</th>
                                        <th class="text-left no-sort">Unit</th>
                                        <th class="no-sort">Buy Price</th>
                                        <th class="no-sort">Total Price</th>
                                        <?php if($role_fk == 'ADMS') { ?>
                                             <th class='no-sort'> </th>
                                        <?php } ?>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                        foreach ($retails as $r => $retail) {
                                             $quantity = $retail['quantity'];
                                             $buy_price = $retail['buy'];
                                             $total_buy_price = $buy_price * $quantity;
                                             $product = $retail['brand'] . " " . $retail['name'];
                                             $type = $retail['type'];
                                             $registration_id = $retail['registration_id'];
                                             $branch = $retail['branch'];
                                             $fullname = $retail['first_name'] . " " . $retail['last_name'];
                                             $unit = $retail['unit'];
                                             $pk = $retail['pk'];
                                             $status = $retail['status'];

                                             $qty_by_status = array(
                                                  array('status_name' => 'for sale', 'status_count' => $retail['available']),
                                                  array('status_name' => 'sold', 'status_count' => $retail['sold']),
                                                  array('status_name' => 'returned', 'status_count' => $retail['returned']),
                                                  array('status_name' => 'for transfer', 'status_count' => $retail['shipments']),
                                                  array('status_name' => 'for repair', 'status_count' => $retail['repair']),
                                                  array('status_name' => 'transferred', 'status_count' => $retail['transferred']),
                                                  array('status_name' => 'for disposal', 'status_count' => $retail['disposal']),
                                                  array('status_name' => 'disposed', 'status_count' => $retail['disposed'])
                                             );
                                   ?>
                                             <tr>
                                                  <td class=""> <?php echo ++$r; ?> </td>
                                                  <td class=" text-uppercase"> <?php echo $registration_id; ?> </td>
                                                  <td class=" text-uppercase"> <?php echo date('d M Y', strtotime($retail['date']));  ?> </td>
                                                  <td class=" text-wrap"> <?php echo $type;  ?> </td>
                                                  <td class=""> <?php echo $product; ?> </td>
                                                  <?php if($role_fk == 'ADMS') { ?>
                                                       <td class=""> <?php echo $branch; ?> </td>
                                                  <?php } ?>
                                                  <td class="text-uppercase text-wrap">
                                                       <ul class="list-inline list-inline-barred m-0 d-inline pl-2 text-small">
                                                            <?php 
                                                                 foreach ($qty_by_status as $q => $qbs) {
                                                                      if($qbs['status_count'] > 0) {
                                                                           echo '<li class="list-inline-item text-monospace">';
                                                                           echo $qbs['status_name'] .': ' . $qbs['status_count'];
                                                                           echo '</li>';
                                                                      }
                                                                 }
                                                            ?>
                                                            </li>
                                                       </ul>
                                                  </td>
                                                  <td class=" text-right"> <?php echo number_format($quantity); ?> </td>
                                                  <td class=""> <?php echo $unit; ?> </td>
                                                  <td class=" text-right"> <?php echo number_format($buy_price, 2); ?> </td>
                                                  <td class=" text-right weight-medium"> <?php echo number_format($total_buy_price, 2); ?> </td>

                                                  <?php if($role_fk == 'ADMS') { ?>
                                                       <td class=" text-center">
                                                            <a href="#" class="text-decoration-none text-danger font-11" data-toggle='modal' data-target='#remove-modal' title="Remove" data-id='<?php echo $registration_id; ?>'>
                                                                 VOID
                                                            </a>                                                            
                                                       </td>
                                                  <?php } ?>
                                             </tr>
                                   <?php
                                        }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>

<form class="" action="<?php echo base_url('Retail/register'); ?>" method="post" id="registration-form" autocomplete="off">
     <div class="modal fade form-modal" id="registration-modal">
          <div class="modal-dialog modal-lg" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">RETAIL ITEM REGISTRATION</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="row">
                              <div class="col-sm-12 col-lg-4 border-right-sm">
                                   <div class="row pb-lg-0 pb-3">
                                        <div class="col-sm-6 col-lg-12">
                                             <div class="form-group">
                                                  <label>Location <span class="text-danger">*</span></label>
                                                  <?php
                                                  if($role_fk == 'ADMS') {
                                                       echo '<select id="select-branch" class="form-control text-uppercase" name="branch" required></select>';
                                                  } else {
                                                       echo '<input type="text" class="form-control" name="branch" value="' . $branch_fk . '" readonly required>';
                                                  }
                                                  ?>
                                             </div>
                                        </div>
                                        <div class="col-sm-6 col-lg-12">
                                             <div class="form-group">
                                                  <label>Date <span class="text-danger">*</span></label>
                                                  <input type="text" name="date" id="date" placeholder="MM dd, yyyy" class="form-control border-radius-2 datepicker text-uppercase" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y", strtotime($date)); ?>"  required>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <div class="col-sm-6 col-lg-4 border-right-sm">
                                   <div class="form-group">
                                        <label>Type <span class="text-danger">*</span></label>
                                        <label class="float-right">
                                             <small>
                                                  <a href="#" id="new-type-toggle" class="active">Add</a>
                                             </small>
                                        </label>
                                        <select id="select-type" class="form-control text-uppercase" name="type" required></select>
                                        <input id="input-type" type="text" class="form-control d-none" name="type-new" value="" placeholder="Enter new type" disabled required>
                                   </div>
                                   <div class="form-group">
                                        <label>
                                             Brand <span class="text-danger">*</span>
                                             <em class="text-muted"><small>(Type is a prerequisite)</small> </em>
                                        </label>
                                        <label class="float-right">
                                             <small>
                                                  <a href="#" id="new-brand-toggle" class="active">Add</a>
                                             </small>
                                        </label>
                                        <select id="select-brand" class="form-control text-uppercase" name="brand" required></select>
                                        <input id="input-brand" type="text" class="form-control d-none" name="brand-new" value="" placeholder="Enter new brand name" disabled required>

                                   </div>
                                   <div class="form-group">
                                        <label>
                                             Name <span class="text-danger">*</span>
                                             <em class="text-muted"> <small>(Brand is a prerequisite)</small> </em>
                                        </label>
                                        <label class="float-right">
                                             <small>
                                                  <a href="#" id="new-name-toggle" class="active">Add</a>
                                             </small>
                                        </label>
                                        <select id="select-name" class="form-control text-uppercase" name="product" required></select>
                                        <input id="input-name" type="text" class="form-control d-none" name="product-new" value="" placeholder="Enter new name" disabled>
                                   </div>
                              </div>
                              <div class="col-sm-6 col-lg-4">
                                   <div class="form-group">
                                        <label class="d-block">Quantity <span class="text-danger">*</span> </label>
                                        <input id="input-quantity" type="text" class="form-control d-inline-block w-50" name="quantity" value="" placeholder="Quantity" required maxlength="255">
                                        <div class="pl-2 w-50 float-right">
                                             <select class="form-control" name="unit" required>
                                                  <?php
                                                  foreach ($units as $u => $unit) {
                                                       echo "<option value='" . $unit['id'] . "'>" . $unit['unit'] . "</option>";
                                                  }
                                                  ?>
                                             </select>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label>Buy Price <span class="text-danger">*</span> </label>
                                        <input id="input-buy" type="text" class="form-control" name="buy" placeholder="Buy Price" required maxlength="11">
                                   </div>
                                   <div class="form-group">
                                        <label>Total Price</label>
                                        <input id="total-buy" type="text" class="form-control" value="&#8369; 0.00" tabindex="-1" readonly>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                              CLOSE <i class="ti-close btn-icon-append"></i>
                         </button>
                         <button type="submit" class="btn btn-primary btn-sm">
                              SUBMIT <i class="ti-check btn-icon-append"></i>
                         </button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to register item?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#registration-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm" name="create" value="create">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>

<?php if($role_fk == "ADMS"){ ?>
<div class="modal fade" id="unit-modal" tabindex="-1">
     <form action="<?php echo base_url('Retail/addUnit'); ?>" method="POST" class="needs-validation mb-1 mt-4" novalidate autocomplete="off">
          <div class="modal-dialog modal-sm">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">ITEM UNITS</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span>&times;</span>
                         </button>
                    </div>
                    <div class="modal-body border-bottom-nav">
                         <div class="form-group mb-0">
                              <label>Add Unit</label>
                              <div class="input-group">
                                   <input type="text" name="unit_name" class="form-control border-radius-2 text-uppercase" placeholder="Ex: BOX, PIECE, UNIT" maxlength="20" required>
                                   <div class="input-group-prepend">
                                        <button class="btn btn-sm btn-primary border-radius-2 ml-2" type="submit"> ADD </button>
                                   </div>
                                   <div class="valid-feedback"> Looks good! </div>
                                   <div class="invalid-feedback"> Enter a unique unit name </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-body">
                         <div class="form-group mb-0">
                              <label>Unit List</label>
                              <ul class="list-group border-radius-2">
                                   <?php
                                        foreach ($units as $u => $unit) {
                                             echo "<li class='list-group-item p-2'>" .
                                                       $unit['unit'] .
                                                       "<a href='" . base_url('Retail/removeUnit/') . $unit['id']. "' class='float-right text-danger text-decoration-none'>
                                                            <span class='ti-close'></i>
                                                       </a>" .
                                                  "</li>";
                                        }
                                   ?>
                              </ul>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
               </div>
          </div>
     </form>
</div>
<?php } ?>

<div class="modal fade confirm-modal" id="remove-modal">
     <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
               <div class="modal-header">
                    <h5 class="modal-title">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal">
                         <span aria-hidden="true">&times;</span>
                    </button>
               </div>
               <div class="modal-body">
                    <p class="mb-0">Are you sure want to delete product? Action can't be undone. </p>
               </div>
               <div class="modal-footer">
                    <a href="#" class="col btn btn-secondary" data-dismiss="modal">CANCEL</a>
                    <a id="confirm-remove-btn" href="" data-href="<?php echo base_url('Retail/removeRegistration/'); ?>" class="col btn btn-danger" name="create" value="create">CONFIRM</a>
               </div>
          </div>
     </div>
</div>

<!-- UDPATE STATUS MODAL -->
<form class="d-inline needs-validation" action="<?php echo base_url('Retail/updateStatus'); ?>" method="post" novalidate>
     <div class="modal fade form-modal" id="status-modal" tabindex="-1">
          <input type="hidden" name="id" value="">
          <div class="modal-dialog modal-sm">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-uppercase text-primary">Update Status</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="form-group mb-0">
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Sale" required>
                                        For Sale
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Repair" required>
                                        For Repair
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Disposal" required>
                                        For Disposal
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="Disposed" required>
                                        Disposed
                                   </label>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary" data-dismiss="modal">CANCEL</button>
                         <button type="button" class="col btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#confirm-status">UPDATE</button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="confirm-status">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to update status?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#status-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm" name="update_status" value="update">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>

<style>
     #product-table_filter input,
     #filter-location {
          margin-right: 0;
     }

     #product-table_info,
     #product-table_paginate {
          margin-top: 13px;     
     }
     
</style>

<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/retail.js'); ?>"></script>

<script type="text/javascript">
     var branchesJSON = <?php echo json_encode($branches); ?>;
     var currentBranchJSON = "<?php echo $branch_fk; ?>";
     var typesJSON = <?php echo json_encode($types); ?>;
     var brandsJSON = <?php echo json_encode($brands); ?>;
     var productsJSON = <?php echo json_encode($products); ?>;
</script>
<script type="text/javascript">
     $(document).ready( function() {
          // $("#registration-modal").modal('show');
          initSelect2();
          initDataTable();
          initDatePicker();
          initModal();
          getBranches(branchesJSON, currentBranchJSON);
          getTypes(typesJSON, brandsJSON, productsJSON);
          getNewType();
          getNewBrand();
          getNewName();
          checkItemDetails();
          getTotalPrices();
          validateForm2();
          validateFormBS();
          validateForm("#registration-form");
          $('#input-quantity').number(true);
          $('#input-buy').number(true, 2);
          $('#input-retail').number(true, 2);

          $('a[data-toggle="popover"], button[data-toggle="popover"], label[data-toggle="popover"]').popover({
               trigger: 'hover'
          });
          $("#product-table_length").appendTo("#table-length");
          $("#product-table_filter").appendTo("#table-filter");
     });
</script>
