<div class="row">
     <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
               <div class="card-body">
                    <p class="card-title mb-0">Top Products</p>
                    <div class="table-responsive">
                         <table class="table table-hover">
                              <thead>
                                   <tr>
                                        <th>User</th>
                                        <th>Product</th>
                                        <th>Sale</th>
                                        <th>Status</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <tr>
                                        <td>Jacob</td>
                                        <td>Photoshop</td>
                                        <td class="text-danger"> 28.76% <i class="ti-arrow-down"></i></td>
                                        <td>
                                             <label class="badge badge-danger">Pending</label>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>Messsy</td>
                                        <td>Flash</td>
                                        <td class="text-danger"> 21.06% <i class="ti-arrow-down"></i></td>
                                        <td>
                                             <label class="badge badge-warning">In progress</label>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>John</td>
                                        <td>Premier</td>
                                        <td class="text-danger"> 35.00% <i class="ti-arrow-down"></i></td>
                                        <td>
                                             <label class="badge badge-info">Fixed</label>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>Peter</td>
                                        <td>After effects</td>
                                        <td class="text-success"> 82.00% <i class="ti-arrow-up"></i></td>
                                        <td>
                                             <label class="badge badge-success">Completed</label>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>Dave</td>
                                        <td>53275535</td>
                                        <td class="text-success"> 98.05% <i class="ti-arrow-up"></i></td>
                                        <td>
                                             <label class="badge badge-warning">In progress</label>
                                        </td>
                                   </tr>
                                   <tr>
                                        <td>Messsy</td>
                                        <td>Flash</td>
                                        <td class="text-danger"> 21.06% <i class="ti-arrow-down"></i></td>
                                        <td>
                                             <label class="badge badge-info">Fixed</label>
                                        </td>
                                   </tr>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
     <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
               <div class="card-body">
                    <h4 class="card-title">To Do Lists</h4>
                    <div class="list-wrapper pt-2">
                         <ul class="d-flex flex-column-reverse todo-list todo-list-custom">
                              <li>
                                   <div class="form-check form-check-flat">
                                        <label class="form-check-label">
                                             <input class="checkbox" type="checkbox"> Become A Travel Pro In One Easy Lesson
                                        </label>
                                   </div>
                                   <i class="remove ti-trash"></i>
                              </li>
                              <li class="completed">
                                   <div class="form-check form-check-flat">
                                        <label class="form-check-label">
                                             <input class="checkbox" type="checkbox" checked> See The Unmatched Beauty Of The Great Lakes
                                        </label>
                                   </div>
                                   <i class="remove ti-trash"></i>
                              </li>
                              <li>
                                   <div class="form-check form-check-flat">
                                        <label class="form-check-label">
                                             <input class="checkbox" type="checkbox"> Copper Canyon
                                        </label>
                                   </div>
                                   <i class="remove ti-trash"></i>
                              </li>
                              <li class="completed">
                                   <div class="form-check form-check-flat">
                                        <label class="form-check-label">
                                             <input class="checkbox" type="checkbox" checked> Top Things To See During A Holiday In Hong Kong
                                        </label>
                                   </div>
                                   <i class="remove ti-trash"></i>
                              </li>
                              <li>
                                   <div class="form-check form-check-flat">
                                        <label class="form-check-label">
                                             <input class="checkbox" type="checkbox"> Travelagent India
                                        </label>
                                   </div>
                                   <i class="remove ti-trash"></i>
                              </li>
                         </ul>
                    </div>
                    <div class="add-items d-flex mb-0 mt-4">
                         <input type="text" class="form-control todo-list-input mr-2" placeholder="Add new task">
                         <button class="add btn btn-icon text-primary todo-list-add-btn bg-transparent"><i class="ti-location-arrow"></i></button>
                    </div>
               </div>
          </div>
     </div>
</div>
<div class="row">
     <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
               <div class="card-body">
                    <table border='1' id="product-table" class="table-responsive">
                         <thead>
                              <tr>
                                   <td>sku</td>
                                   <td>name</td>
                                   <td>description</td>
                                   <td>purchase_date</td>
                                   <td>quantity</td>
                                   <td>amount</td>
                                   <td>registration_date</td>
                                   <td>brand_fk</td>
                                   <td>brand</td>
                                   <td>type_fk</td>
                                   <td>type</td>
                              </tr>
                         </thead>
                         <tbody>
                              <?php
                                   if(isset($products)) {
                                        foreach ($products as $p => $product):
                                             echo "
                                             <tr>
                                             <td>".$product['sku']."</td>
                                             <td class='product-name'>".$product['name']."</td>
                                             <td>".$product['description']."</td>
                                             <td>".$product['purchase_date']."</td>
                                             <td>".$product['quantity']."</td>
                                             <td>".$product['amount']."</td>
                                             <td>".$product['registration_date']."</td>
                                             <td>".$product['brand_fk']."</td>
                                             <td class='product-brand' data-id='".$product['brand']."'>".$product['brand']."</td>
                                             <td>".$product['type_fk']."</td>
                                             <td class='product-type' data-id='".$product['type_fk']."'>".$product['type']."</td>
                                             </tr>";
                                        endforeach;
                                   } else {
                                        echo "<tr><td colspan='11'>Result not found.</tr>";
                                   }
                                   ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </div>
     <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
               <div class="card-body">
                    <select class="" name="" id="product-type-select" required>
                         <option value="" selected disabled>Select Type</option>
                    </select>

                    <select class="" name="" id="product-brand-select" disabled>
                         <option value="" selected disabled>Select Brand</option>
                    </select>

                    <select class="" name="" id="product-name-select" disabled>
                         <option value="" selected disabled>Select Name</option>
                    </select>
               </div>
          </div>
     </div>
</div>
<div class="row">
</div>
<script type="text/javascript">
     $(document).ready(function() {
          var names = <?php echo json_encode($products); ?>;
          var brands = <?php echo json_encode($brands); ?>;
          var types = <?php echo json_encode($types); ?>;

          var select_type = "#product-type-select";
          var select_brand = "#product-brand-select";
          var select_name = "#product-name-select";

          /* populate type drowpdown */
          $(types).each(function(key, value) {
               $(select_type).append('<option value=' + value['id'] + '>' + value['name'] + '</option>');
          });

          /* populate brands drowpdown */
          $(select_type).on("change", function() {
               $(select_brand).empty();
               $(select_brand).append('<option value="" selected disabled>Select Brand</option>');
               var id = $(this).val();
               $(brands).each(function() {
                    var brand = $(this)[0];
                    if (brand['type_fk'] == id) {
                         $(select_brand).append('<option value=' + brand['id'] + '>' + brand['name'] + '</option>');
                    }

               });
               $(select_brand).removeAttr('disabled');
          })

          /* populate names drowpdown */
          $(select_brand).on("change", function() {
               $(select_name).empty();
               $(select_name).append('<option value="" selected disabled>Select Name</option>');
               var id = $(this).val();
               $(names).each(function() {
                    var name = $(this)[0];
                    if (name['brand_fk'] == id) {
                         $(select_name).append('<option value=' + name['id'] + '>' + name['name'] + '</option>');
                    }
               });
               $(select_name).removeAttr('disabled');
          })

     });
</script>
