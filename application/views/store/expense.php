<style>
     #expenses-table_length,
     #expenses-table_filter {
          float: right;
          margin-left: 8px;
          margin-bottom: 0.5rem;
          margin-top: 0;
     }
</style>

<div class="row no-gutters">
     <div class="col-sm-1">
          <h4 class='font-weight-bold mb-3'>
               <?php echo $title;?>
          </h4>
     </div>     
     <div class="col-sm-11">
          <div id="filter-wrapper" style="display: none;">
               <div class="d-inline-block float-right ml-2 mb-2" id="filter-location-wrapper">
                    <?php if($this->session->userdata('role_fk') == "ADMS") { ?>
                    <div class="input-group">
                         <div class="input-group-prepend">
                              <span class="input-group-text py-0 border-radius-left-2 ti-location-pin"></span>
                         </div>      
                         <div class="input-group-append">                                       
                              <select class="form-control select2 select2-sm" id="filter-location">
                                   <option value="ALL">ALL LOCATIONS</option>
                                   <?php foreach ($branches as $b => $branch) {
                                        echo "<option value='" . trim(strtoupper($branch['name'])) . "'>" . trim(strtoupper($branch['name'])) . "</option>";
                                   } ?>
                              </select>
                         </div>
                    </div>
                    <?php } ?>
               </div>                   
               <div class="d-inline-block float-right ml-2 mb-2" id="filter-type-wrapper">
                    <div class="input-group">
                         <div class="input-group-prepend">
                              <span class="input-group-text py-0 border-radius-left-2 ti-ruler-pencil"></span>
                         </div>      
                         <div class="input-group-append">                                       
                              <select class="form-control select2 select2-sm" id="filter-type">
                                   <option value="ALL">ALL TYPES</option>
                                   <?php foreach ($types as $t => $type) {
                                        echo "<option value='" . $type['type'] . "'>" . $type['type'] . "</option>";
                                   } ?>
                              </select>
                         </div>
                    </div>
               </div>  
               <div class="d-inline-block float-right ml-2 mb-2" id="filter-date-wrapper">
                    <div class="input-group date-range">
                         <div class="input-group-prepend">
                              <span class="input-group-text py-0 border-radius-left-2 ti-calendar" style="height: 36px;"></span>
                         </div>
                         <input type="text" id="trans-min-date" value="<?php echo date("01 M Y"); ?>" placeholder="From" data-date-format="dd M yyyy" class="date-rage-filter datepicker form-control text-uppercase font-12 py-0 text-center" style="border-radius:0;z-index:1!important;height:36px;max-width:100px">                                        
                         <span class="input-group-text date-range-separator">&ndash;</span>
                         <input type="text" id="trans-max-date" value="<?php echo date("d M Y"); ?>" placeholder="To" data-date-format="dd M yyyy" class="date-rage-filter datepicker form-control text-uppercase border-radius-right-2 font-12 py-0 text-center" style=";z-index:1!important;height:36px;max-width:100px">                                        
                    </div> 
               </div>
          </div>
     </div>
</div>

<ul class="nav nav-tabs">
     <li class="nav-item">
          <!-- <a href="#" class="nav-link active">Records</a> -->
          <a href="#records" class="nav-link <?php echo ($tab == 'records' ? 'active': '') ?>" data-toggle="tab" role="tab">Records</a>
     </li>
     <li class="nav-item">
          <!-- <a href="#" class="nav-link" data-toggle="modal" data-target="#addTransactionModal">New Expense</a> -->
          <a href="#new" class="nav-link <?php echo ($tab == 'new' ? 'active': '') ?>" data-toggle="tab" role="tab">New Expense</a>
     </li>
     <?php if($this->session->userdata('role_fk') == 'ADMS') {  ?>
     <li class="nav-item">
          <!-- <a href="#" class="nav-link" data-toggle="modal" data-target="#expenseTypeModal">Expense Types</a> -->
          <a href="#types" class="nav-link <?php echo ($tab == 'types' ? 'active': '') ?>" data-toggle="tab" role="tab">Expense Types</a>
     </li>
     <?php } ?>
</ul>

<div class="card border-top-0">
     <div class="card-body py-4">
          <div class="tab-content" id="tab-content">
               <div class="tab-pane fade show <?php echo ($tab == 'records' ? 'active': '') ?>" id="records" role="tabpanel">          
                    <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                    <div class="table-responsive pb-0 fade">                         
                         <table class="table pb-3" id="expenses-table">
                              <thead class="text-uppercase">
                                   <tr>
                                        <th class="no-sort bg-img-none">#</th>
                                        <th class="text-left">Date</th>
                                        <th class="text-left">Type</th>
                                        <th class="text-left">Description</th>
                                        <th class='text-center'>Amount</th>
                                        <th class="text-left">Location</th>
                                        <th class="no-sort text-center <?php echo ($role_fk != 'ADMS' ? ' d-none': ''); ?>"></th>
                                   </tr>
                              </thead>
                              <tbody class="text-uppercase"> </tbody>
                              <tfoot>
                                   <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th class="text-left">
                                             <?php if($expenses) { echo "TOTAL AMOUNT:"; } ?>
                                        </th>
                                        <th class="text-center">
                                             <span class="font-14 text-monospace font-weight-bold" id="total-expenses"></span>
                                        </th>
                                        <th></th>
                                        <th class="<?php echo ($role_fk != 'ADMS' ? ' d-none': ''); ?>"></th>
                                   </tr>
                              </tfoot>
                         </table>
                    </div>
               </div>
               <div class="tab-pane fade show <?php echo ($tab == 'new' ? 'active': '') ?>" id="new" role="tabpanel">          
                    <form action="<?php echo base_url('Expense/addTransaction'); ?>" method="POST" class="needs-validation from-inline" autocomplete="off" novalidate>                         
                         <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                         <table class="mx-auto fade">
                              <tr>
                                   <td colspan="2">
                                        <h5 class="border-bottom display-4 pb-3">
                                             <small>Expense Form</small>
                                        </h5>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium">Expense Type</td>
                                   <td class="py-2 px-3">
                                        <select class="form-control select2" name="type" required>
                                             <option value="" selected disabled>SELECT TYPE</option>
                                             <?php foreach ($types as $t => $type) {
                                                  echo "<option value='" . $type['id'] . "'>" . $type['type'] . "</option>";
                                             } ?>
                                        </select>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium">Date</td>
                                   <td class="py-2 px-3">
                                        <input type="text" name="date" class="form-control datepicker bg-white border-radius-2 text-uppercase" placeholder="DATE" data-date-format="dd M yyyy" value="" required autocomplete="off">
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium">Amount</td>
                                   <td class="py-2 px-3">
                                        <input type="text" name="amount" class="form-control border-radius-2 number-format" placeholder="AMOUNT" required>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium">Description</td>
                                   <td class="py-2 px-3">
                                        <textarea name="description" class="form-control" rows="4" spellcheck="false" placeholder="DESCRIPTION"></textarea>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium"></td>
                                   <td class="py-2 px-3">
                                        <button type="button" class="btn btn-primary border-radius-2" data-toggle="modal" data-target="#expense-confirm"> SUBMIT </button>
                                   </td>
                              </tr>
                         </table>
                         <div class="modal fade confirm-modal" id="expense-confirm">
                              <div class="modal-dialog modal-sm" role="document">
                                   <div class="modal-content">
                                        <div class="modal-header">
                                             <h5 class="modal-title">Confirmation</h5>
                                             <button type="button" class="close" data-dismiss="modal">
                                                  <span aria-hidden="true">&times;</span>
                                             </button>
                                        </div>
                                        <div class="modal-body">
                                             <p class="mb-0">Are you sure want to add new expense?</p>
                                        </div>
                                        <div class="modal-footer">
                                             <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#cash-modal">CANCEL</button>
                                             <button type="submit" class="col btn btn-success confirm" name="create" value="create">CONFIRM</button>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </form>
               </div>
               <div class="tab-pane fade show <?php echo ($tab == 'types' ? 'active': '') ?>" id="types" role="tabpanel">         
                    <form action="<?php echo base_url('Expense/addType'); ?>" method="POST" class="needs-validation from-inline" novalidate>
                         <div class="row narrow-gutters">
                              <div class="col-md-6">
                                   <table class="mx-auto">
                                        <tr>
                                             <td colspan="2">
                                                  <h5 class="border-bottom display-4 pb-3">
                                                       <small>New Type Form</small>
                                                  </h5>
                                             </td>
                                        </tr>
                                        <tr>
                                             <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell">Type Name</td>
                                             <td class="py-2 px-3">
                                                  <label class="d-block d-sm-none">Type Name</label>
                                                  <input type="text" name="type_name" class="form-control border-radius-2" placeholder="Type Name" maxlength="50" required>
                                             </td>
                                        </tr>
                                        <tr>
                                             <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-block"></td>
                                             <td class="py-2 px-3">
                                                  <button type="button" class="btn btn-primary border-radius-2" data-toggle="modal" data-target="#new-type-confirm"> SUBMIT </button>
                                             </td>
                                        </tr>
                                   </table>
                                   <div class="modal fade confirm-modal" id="new-type-confirm">
                                        <div class="modal-dialog modal-sm" role="document">
                                             <div class="modal-content">
                                                  <div class="modal-header">
                                                       <h5 class="modal-title">Confirmation</h5>
                                                       <button type="button" class="close" data-dismiss="modal">
                                                            <span aria-hidden="true">&times;</span>
                                                       </button>
                                                  </div>
                                                  <div class="modal-body">
                                                       <p class="mb-0">Are you sure want to add new type?</p>
                                                  </div>
                                                  <div class="modal-footer">
                                                       <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#cash-modal">CANCEL</button>
                                                       <button type="submit" class="col btn btn-success confirm" name="create" value="create">CONFIRM</button>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="d-block d-sm-none pt-2 py-3">
                                        <hr>
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="table-responsive pb-0">                         
                                        <table class="table pt-3" id="product-table">
                                             <thead>
                                                  <tr>
                                                       <th class="text-left text-uppercase">Expense Types</th>
                                                       <th class='no-sort'></th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  <?php
                                                  foreach ($types as $t => $type) {
                                                       echo "<tr>" .
                                                                 "<td>" . $type['type'] . "</td>" .
                                                                 "<td class='text-center'>" . 
                                                                      "<a href='" . base_url('Expense/removeType/') . $type['id']. "' class='text-danger text-decoration-none'>
                                                                           <span class='ti-close'></i>
                                                                      </a>"  . 
                                                                 "</td>" .
                                                            "</tr>";
                                                  }
                                                  ?>
                                             </tbody>
                                             <tfoot>
                                                  <tr>
                                                       <td colspan="2"></td>
                                                  </tr>
                                             </tfoot>
                                        </table>
                                   </div>
                              </div>
                         </div>
                    </form>
              </div>
          </div>
     </div>
</div>

<div class="modal fade" id="expenseTypeModal" tabindex="-1">
     
</div>
     
<div class="modal fade" id="addTransactionModal" tabindex="-1">
     
</div>
<div class="modal fade confirm-modal" id="removeExpenseModal">
     <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
               <div class="modal-header">
                    <h5 class="modal-title">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal">
                         <span aria-hidden="true">&times;</span>
                    </button>
               </div>
               <div class="modal-body">
                    <p class="mb-0">Are you sure want to delete transaction? Action can't be undone. </p>
               </div>
               <div class="modal-footer">
                    <a href="#" class="col btn btn-secondary" data-dismiss="modal">CANCEL</a>
                    <a id="confirm-remove-btn" href="" data-href="<?php echo base_url('Expense/removeExpense/'); ?>" class="col btn btn-danger" name="create" value="create">CONFIRM</a>
               </div>
          </div>
     </div>
</div>

<?php
     if(null !== $this->session->flashdata('modal')) {
          echo $modal = $this->session->flashdata('modal');
          echo "<script>$(function() { $('#".$modal."').modal('show'); })</script>";
     }
?>

<script type="text/javascript" src="<?php echo base_url('assets/js/expense.js'); ?>"></script>
<script> var expenses = <?php echo json_encode($expenses); ?>; </script>
<script> var role_fk = <?php echo "'" . $role_fk . "'"; ?>; </script>
<script type="text/javascript"> var base_url = '<?php echo base_url(); ?>'; </script>
<script type="text/javascript"> var tab = '<?php echo $tab; ?>'; </script>
<script>
     $(function() {
          updateUrl(base_url)
          getExpenses(expenses, role_fk);
          numberFormat();
          initDataTable(tab);
          initDatePicker();
          validateForm();
          initSelect2();
          getSum();
     })
</script>

<script type="text/javascript">
     $(window).on("load", function() {
          loader();
     });
</script>
