<?php 
     if(isset($customers['photo'])) {
          $image_path = $customers['photo'];
     } else {
          $image_path = "placeholder-640x480.png";
     }
?>

<style type="text/css">
     #customer-update-tab {
          display: none;
     }
     .form-control[readonly] {
          background-color: #fafafa !important;
     }
</style>


<div class="row no-gutters">
     <div class="col-lg-1">
          <h4 class='font-weight-bold mb-3'>
               <?php echo $title;?>
          </h4>
     </div>       
</div>

<ul class="nav nav-tabs">
     <li class="nav-item">
          <a href="<?php echo base_url("Customer?tab=records"); ?>" class="nav-link">
               Records
          </a>
     </li>
     <li class="nav-item">
          <a href="<?php echo base_url("Customer?tab=new"); ?>" class="nav-link">
               New Customer
          </a>
     </li>
     <li class="nav-item">
          <a href="#" class="nav-link active">
               Customer Profile
          </a>
     </li>
</ul>

<!-- Customer profile -->
<div class="card border-top-0" id="customer-profile-tab">
     <div class="card-body">
          <form class="needs-validation" method="post" action="<?php echo base_url('Customer/update'); ?>" id="#customer-update-form" novalidate autocomplete="close">
               <input type="hidden" name="id" value="<?php echo $customers['id']; ?>">
               <div class="row narrow-gutters">
                    <div class="col">
                         <p class="card-title text-primary">Customer Information</p>                         
                    </div>
                    <div class="col">
                         <div id="profile-form-buttons" class="text-right">
                              <button type="button" class="btn btn-primary btn-icon-text btn-sm" id="customer-update-btn">
                                   <span class="d-none d-sm-inline-block">UPDATE</span><i class="ti-pencil-alt btn-icon-append ml-0 ml-sm-2"></i>
                              </button>
                              <button type="button" class="btn btn-light btn-icon-text btn-sm border mr-1" id="customer-cancel-update-btn" style="display: none;">
                                   <span class="d-none d-sm-inline-block">CANCEL</span><i class="ti-close btn-icon-append ml-0 ml-sm-2"></i>
                              </button>
                              <button type="button" class="btn btn-primary btn-icon-text btn-sm" id="customer-save-update-btn"data-toggle="modal" data-target="#customer-update-modal"style="display: none;">
                                   <span class="d-none d-sm-inline-block">SAVE CHANGES</span><i class="ti-save btn-icon-append ml-0 ml-sm-2"></i>
                              </button>
                         </div>
                    </div>
               </div>
               <div class="row narrow-gutters">
                    <div class="col-sm-3">
                         <div class="form-group">
                              <label>First Name <span class="text-danger">*</span></label>
                              <input class="form-control" placeholder="First Name" type="text" name="firstname" value="<?php echo $customers['first_name']; ?>" required readonly>
                         </div>
                    </div>
                    <div class="col-6 col-sm-3">
                         <div class="form-group">
                              <label>Middle Name</label>
                              <input class="form-control" placeholder="Middle Name" type="text" name="middlename" value="<?php echo $customers['middle_name']; ?>" readonly>
                         </div> 
                    </div>
                    <div class="col-6 col-sm-3">
                         <div class="form-group">
                              <label>Last Name <span class="text-danger">*</span></label>
                              <input class="form-control" placeholder="Last Name" type="text" name="lastname" value="<?php echo $customers['last_name']; ?>" required readonly>
                         </div>
                    </div>
                    <div class="col-6 col-sm-3">
                         <div class="form-group selectize-group">
                              <label>Gender <span class="text-danger">*</span></label>
                              <select class="form-control select2" name="gender" placeholder="Select Gender" required>
                                   <option value="male" <?php echo ($customers['gender'] == 'male' ? 'selected' : ''); ?> >MALE</option>
                                   <option value="female" <?php echo ($customers['gender'] == 'female' ? 'selected' : ''); ?> >FEMALE</option>
                              </select>
                         </div>
                    </div>
               
                    <div class="col-6 col-sm-3">
                         <div class="form-group">
                              <label>Contact Number <span class="text-danger">*</span></label>
                              <input class="form-control" type='number' placeholder="Contact Number" maxlength="11" step="1" name='contact' value="<?php echo $customers['contact']; ?>" required readonly>
                         </div>
                    </div>
                    <div class="col-sm-6">
                         <div class="form-group">
                              <label>Address <span class="text-danger">*</span></label>
                              <input class="form-control" type='text' placeholder="Address" name='address' value="<?php echo $customers['address']; ?>" required readonly>
                         </div>
                    </div>
                    <div class="col-sm-3">
                         <div class="form-group selectize-group">
                              <label>City & Province <span class="text-danger">*</span></label>
                              <select class="form-control text-uppercase select2" name="city" placeholder="Type or Select City" required>
                                   <?php
                                   if(null !== $cities) {
                                        foreach ($cities as $city):
                                             echo "<option value='". $city['id'] . "' " . ($city['id'] == $customers['city_fk'] ? 'selected' : '' ) . ">" . 
                                                       strtoupper($city['name']) ." - ". strtoupper($city['province']) . 
                                                  "</option>";
                                        endforeach;
                                   }else {
                                        echo "<option value='' disabled>Result not found</option>";
                                   }
                                   ?>
                              </select>
                         </div>
                    </div>
               
                    <div class="col-6 col-sm-3">
                         <div class="form-group">
                              <label>ID Presented <span class="text-danger">*</span></label>
                              <input class="form-control" type='text' name='id_type' value="<?php echo $customers['identification_type']; ?>" placeholder="ID Presented" required readonly>
                         </div>
                    </div>
                    <div class="col-6 col-sm-3">
                         <div class="form-group">
                              <label>ID Number <span class="text-danger">*</span></label>
                              <input class="form-control" type='text' name='id_number' value="<?php echo $customers['identification_number']; ?>" placeholder="ID Number" required readonly>
                         </div>
                    </div>
                    <div class="col-sm-6">
                         <div class="form-group">
                              <label>Remarks</label>
                              <input class="form-control" type="text" name="remarks" value="<?php echo $customers['remarks']; ?>" placeholder="Remarks" readonly>
                         </div>
                    </div>
               </div>

               <div class="modal fade confirm-modal" id="customer-update-modal">
                    <div class="modal-dialog modal-sm" role="document">
                         <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="modal-title">Confirmation</h5>
                                   <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">&times;</span>
                                   </button>
                              </div>
                              <div class="modal-body">
                                   <p class="mb-0">Are you sure want to update customer information?</p>
                              </div>
                              <div class="modal-footer">
                                   <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
                                   <button type="submit" class="col btn btn-success confirm" name="create" value="create">CONFIRM</button>
                              </div>
                         </div>
                    </div>
               </div>               
          </form>
     </div>
     <div class="card-body" id="photo-section">
          <div class="row">
               <div class="col-12">
                    <p class="card-title text-primary">Photo</p>
               </div>
          </div>
          
          <div class="row">
               <div class="col-md-8 col-lg-6">
                    <div class="customer-img-wrapper overflow-hidden mb-3 text-center">
                         <img src="<?php echo base_url('assets/images/customers/') . $image_path; ?>" class="img-fluid" alt="Image not found.">
                    </div>
               </div>
               <div class="col-md-4 col-lg-6">
                    <div class="d-block">
                         <a href="<?php echo base_url('assets/images/customers/') . $image_path; ?>" class="btn btn-success btn-icon-text btn-sm mb-3 mr-1 text-white" target="_blank">
                              <span class="">VIEW </span><i class="ti-new-window btn-icon-append ml-0 ml-sm-2"></i>
                         </a>
                         <a href="<?php echo base_url('assets/images/customers/') . $image_path; ?>" class="btn btn-warning btn-icon-text btn-sm mb-3 mr-1 text-white" download>
                              <span class="">DOWNLOAD </span><i class="ti-import btn-icon-append ml-0 ml-sm-2"></i>
                         </a>
                         <button type="button" class="btn btn-primary btn-icon-text btn-sm mb-3" data-toggle="modal" data-target="#customer-upload-modal">
                              <span class="">UPLOAD </span><i class="ti-pencil-alt btn-icon-append ml-0 ml-sm-2"></i>
                         </button>
                    </div>

                    <?php  
                         if(null !== $this->session->flashdata('upload_error')) {
                              echo "<div class='text-danger mb-3'>" . $this->session->flashdata('upload_error') . "</div>";
                         }

                         if(null !== $this->session->flashdata('upload_data')) {
                              echo "<div class='text-success mb-3'>File has been successfully uploaded</div>";
                         }
                    ?>
                    
                    <div class="modal fade" id="customer-upload-modal">
                         <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                   <form action="<?php echo base_url('Customer/upload/'); ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                        <div class="modal-header">
                                             <h5 class="modal-title">UPLOAD PHOTO</h5>
                                             <button type="button" class="close" data-dismiss="modal">
                                                  <span aria-hidden="true">&times;</span>
                                             </button>
                                        </div>
                                        <div class="modal-body py-4">
                                             <input type="hidden" name="customer-id" value="<?php echo $customers['id']; ?>">
                                             <input type="file" name="photo" size="20" required>
                                        </div>
                                        <div class="modal-footer">
                                             <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
                                             <button type="submit" class="btn btn-success">UPLOAD</button>
                                        </div>
                                   </form>
                              </div>
                         </div>
                    </div>   

                    <p class="card-description d-inline-block mr-4 d-md-block mr-md-0">
                         <strong class="d-block">Dimension:</strong>
                         <small class="d-block text-monospace">Recommended: 640px X 480px</small>
                         <small class="d-block text-monospace">Maximum: 1024px X 768px</small>
                    </p>
                    <p class="card-description d-inline-block mr-4 d-md-block mr-md-0">
                         <strong class="d-block">Size:</strong>
                         <small class="d-block text-monospace">Recommended: < 50kb</small>
                         <small class="d-block text-monospace">Maximum: 2,000kb or 2mb</small>
                    </p>
                    <p class="card-description d-inline-block mr-4 d-md-block mr-md-0">
                         <strong class="d-block">Format:</strong>
                         <small class="d-block text-monospace">Recommended: jpeg</small>
                         <small class="d-block text-monospace">Valid: gif, jpeg, png</small>
                    </p>   

                    <p class="card-description d-inline-block mr-4 d-md-block mr-md-0">
                         <strong class="d-block">Optimize image:</strong>
                         <small class="d-block text-monospace">Resize Image: <a href="https://resizeimage.net" target="_blank">resizeimage.net</a></small>
                         <small class="d-block text-monospace">Compress Image: <a href="https://tinypng.com" target_="blank">tinypng.com</a></small>
                    </p>   
               </div>
          </div>
     </div>

     <div class="card-body">
          <div class="row">
               <div class="col-12">
                    <p class="card-title text-primary">Transactions</p>
               </div>
               <div class="col-12">
                    <div class="table-responsive">                         
                         <table class="table text-uppercase">
                              <thead>
                                   <th>#</th>
                                   <th class="text-left">TRANSACTION DATE</th>
                                   <th class="text-left">TRANSACTION ID</th>
                                   <th class="text-left">ITEM TYPE</th>
                                   <th class="text-left">ITEM NAME</th>
                                   <th class="text-left">STATUS</th>
                                   <th></th>
                              </thead>
                              <tbody>
                                   <?php
                                        if($transactions) {
                                             foreach ($transactions as $t => $transaction) {
                                                  $date = new DateTime($transaction['date']);
                                                  echo "<tr>";
                                                  echo "<td>" . ++$t . "</td>";
                                                  echo "<td>" . $date->format('F d, Y') . "</td>";
                                                  echo "<td>" . $transaction['transaction_id'] . "</td>";
                                                  echo "<td>" . $transaction['type'] . "</td>";
                                                  echo "<td>" . $transaction['brand'] . " " . $transaction['product'] . "</td>";
                                                  echo "<td>" . $transaction['status'] . "</td>";
                                                  echo "<td class='text-center' style='width:85px;'>
                                                            <a href='" . base_url("Transaction?search=") . $transaction['transaction_id'] . "' class='btn btn-primary btn-sm'>
                                                                 VIEW
                                                            </a>
                                                       </td>";
                                                  echo "</tr>";
                                             }
                                        }
                                   ?>
                              </tbody>
                         </table>                         
                    </div>
               </div>
          </div>
     </div>
</div>
<!-- Customer profile end -->




<script type="text/javascript">
     /*
     var validateForm = function(target) {
          $(target).validate({
               submitHandler: function(form) {
                    $(target + " .confirm-modal").modal("show");
                    $(target + " .confirm-modal .confirm").on("click", function() {
                         form.submit();
                    });
               }
          });
     }
     $(document).ready(function() {
          var table = $('#customer-transactions-table').DataTable();

          $('#customer-update-select-gender').selectize();

          $('#customer-update-select-city').selectize();

          $(".btn-update").on("click", function() {
               $("#customer-profile-tab").hide();
               $("#customer-update-tab").show();
          });

          $(".btn-show-profile").on("click", function() {
               $("#customer-update-tab").hide();
               $("#customer-profile-tab").show();
          });

          $("#customer-transactions-tab table tbody tr").on("click", function() {
               window.location.href = $(this).attr("data-href");
          });

          validateForm("#customer-update-form");
     })
     */
</script>

<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/customer.js'); ?>"></script>
<script type="text/javascript"> var base_url = '<?php echo base_url(); ?>'; </script>

<script type="text/javascript">
     $(document).ready(function() {
          initSelect2("select.select2");
          // initDataTable(tab);
          // updateUrl(base_url);
          formValidation();
          $('#customer-profile-tab select').select2("enable", false)

          $("#customer-update-btn").on("click", function() {
               $('#customer-profile-tab select').select2("enable");
               $('#customer-profile-tab input').attr("readonly", false);
               $("#customer-update-btn").hide();
               $("#customer-cancel-update-btn").show();
               $("#customer-save-update-btn").show();
          });

          $("#customer-cancel-update-btn").on("click", function() {
               $('#customer-profile-tab select').select2("enable", false);
               $('#customer-profile-tab input').attr("readonly", true);
               $("#customer-update-btn").show();
               $("#customer-cancel-update-btn").hide();
               $("#customer-save-update-btn").hide();
          });
     });
</script>

<script type="text/javascript">
     $(window).on("load", function() {
          // loader();
     });
</script>

