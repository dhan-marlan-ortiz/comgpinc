<div class="row no-gutters">
     <div class="col-lg-1">
          <h4 class='font-weight-bold mb-3'>Retails</h4>
     </div>     
     <div class="col-lg-11 text-right animated fadeIn delay-1s">
          <div class="d-inline-block mb-2">
               <div class="input-group">
                    <div class="input-group-prepend">
                         <span class="input-group-text py-0 border-radius-left-2 ti-location-pin"></span>
                    </div>
                    <?php if($role_fk == 'ADMS') { ?>
                    <div class="input-group-append">
                         <select id="filter-location" name="location" class="select2 select2-sm border-radius-right-2" style="height:36px;z-index:1!important;font-size:12px;padding:10px 7px;" required>
                              <option value=""> ALL </option>
                              <?php 
                              foreach ($branches as $branch_option) {
                                   echo "<option value='" . $branch_option['name'] . "'>" . $branch_option['name'] . " (" . $branch_option['id'] . ")" . "</option>";
                                   // echo "<option value='" . $branch_option['name'] . "' " . ($branch_option['id'] == $branch_fk ? "selected" : "" ) . ">" . $branch_option['name'] . " (" . $branch_option['id'] . ")" . "</option>";
                              }
                              ?>                                        
                         </select>
                    </div>
                    <?php 
                    } else {
                         echo '<input type="text" name="location" id="location" placeholder="DATE" class="form-control" value="'.$branch_fk.'" readonly required style="height:36px;font-size:12px;">';
                         }
                    ?>
               </div>           
          </div>
          <div class="d-inline-block mb-2 ml-2" id="table-length"></div>
          <div class="d-inline-block mb-2 ml-2 text-uppercase" id="table-filter"></div>          
     </div>
</div>

<div class="row no-gutters">
     <div class="col-12">
          <?php retail_tab('disposedAndTransferred') ?>
     </div>     
</div>

<div class="row">
     <div class="col-12 col-sm-12">
          <div class="card border-top-0">
               <div class="card-body pt-4">
                    <div class="table-responsive pb-0">
                         <table class="table animated fadeIn text-uppercase" id="product-table">
                              <thead>
                                   <tr class="text-uppercase">
                                        <th class="no-sort bg-img-none">#</th>
                                        <th class="text-left">Registration ID</th>
                                        <th class="text-left">Date</th>
                                        <th class="text-left">Type</th>
                                        <th class="text-left">Product</th>
                                        <?php if($role_fk == 'ADMS') { ?>
                                             <th class="text-left">Location</th>
                                        <?php } ?>
                                        <th class="text-left">Status</th>
                                        <th class="text-center no-sort">Qty</th>
                                        <th class="text-left no-sort">Unit</th>
                                        <th class="text-center no-sort">Buy Price</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php 
                                   foreach ($records as $r => $rec) {
                                        echo tr_open();
                                        echo td($rec['registration_id']);
                                        echo td($rec['date']);
                                        echo td($rec['type']);
                                        echo td($rec['brand']);
                                        echo td($rec['name']);
                                        if($role_fk == 'ADMS') {
                                             echo td($rec['branch']);
                                        }
                                        echo td($rec['status']);
                                        echo td(number_format($rec['quantity']), array('class' => array('text-center')) );
                                        echo td($rec['unit']);
                                        echo td(number_format($rec['buy'], 2), array('class' => array('text-right')) );

                                        echo tr_close();
                                   }
                                   ?>
                                   
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>

<br>
<br>
<div class="text-muted text-left d-inline-block align-top mr-3">
     <p class="text-small mb-0">Table above displays list of items with the following status:</p>
     <ul class="text-small text-monospace">
          <li>Disposed</li>
          <li>Transferred</li>
     </ul>
</div>

<style>
     #product-table_filter input,
     #filter-location {
          margin-right: 0;
     }
     #product-table_info,
     #product-table_paginate {
          margin-top: 13px;     
     }    
</style>

<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/retail.js'); ?>"></script>

<script type="text/javascript">
     var branchesJSON = '<?php echo json_encode($branches); ?>';
</script>
<script type="text/javascript">
     $(document).ready( function() {
          initSelect2();
          initDataTable();
          initDatePicker();
          initModal();          
          validateForm2();
          $("#product-table_length").appendTo("#table-length");
          $("#product-table_filter").appendTo("#table-filter");
     });
</script>
