<?php
     $date = date('Y-m-d');
     $role_fk = $this->session->userdata('role_fk');
     $branch_fk = $this->session->userdata['branch_fk'];
?>

<form class="needs-validation" action="<?php echo base_url('Retail/cartCheckout'); ?>" method="post" autocomplete="off" novalidate>
     <div class="modal fade confirm-modal">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure you want to sell item(s)?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>

     <div class="row">
          <div class="col-lg-1">
               <h4 class='font-weight-bold mb-3'>Retails</h4>
          </div>
          <div class="col-lg-11 text-right animated fadeIn delay-1s">
               <div class="d-inline-block mb-2 mr-2">
                    <div class="input-group">
                         <div class="input-group-prepend">
                              <span class="input-group-text py-0 border-radius-left-2 ti-location-pin"></span>
                         </div>
                         <?php if($role_fk == 'ADMS') { ?>
                              <div class="input-group-append">
                                   <select id="location" class="select2 select2-sm border-radius-right-2" name="location" style="height:36px;z-index:1!important;font-size:12px;padding:10px 7px;" required>
                                        <?php 
                                        foreach ($branches as $branch_option) {
                                             echo "<option value='" . $branch_option['id'] . "' " . ($branch_option['id'] == $branch_fk ? "selected" : "" ) . ">" . $branch_option['name'] . " (" . $branch_option['id'] . ")" . "</option>";
                                        }
                                        ?>                                        
                                   </select>
                              </div>
                         <?php 
                              } else {
                                   echo '<input type="text" name="location" id="location" placeholder="DATE" class="form-control" value="'.$branch_fk.'" readonly required style="height:36px;font-size:12px;">';
                              }
                         ?>
                    </div>           
               </div>
               <div class="d-inline-block mb-2 mr-2">
                    <div class="input-group">
                         <div class="input-group-prepend">
                              <label for="date" class="input-group-text py-0 border-radius-left-2 ti-calendar"></label>
                         </div>
                         <input type="text" name="date" id="date" placeholder="DATE" class="form-control border-radius-2 datepicker text-uppercase" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y", strtotime($date)); ?>"  style="height:36px;z-index:1!important;font-size:12px" required>
                    </div>
               </div>
               <div class="d-inline-block mb-2 mr-2">
                    <div class="input-group">
                         <button type="button" class="btn btn-secondary btn-icon-text btn-sm text-white border-radius-2" data-toggle="modal" data-target="#inventory-modal">
                              ADD PRODUCT<i class="btn-icon-append ti-plus"></i>
                         </button>
                    </div>
               </div>
               <div class="d-inline-block mb-2">
                    <div class="input-group">
                         <button type="button" class="btn btn-secondary btn-icon-text btn-sm text-white border-radius-2" id="submit-cart-btn" data-toggle="modal" data-target=".confirm-modal" disabled>
                              SUBMIT<i class="btn-icon-append ti-check"></i>
                         </button>
                    </div>
               </div>
          </div>
     </div>

     <div class="row no-gutters">
          <div class="col-12">
               <?php retail_tab('sell') ?>
          </div>
     </div>

     <div class="row">
          <div class="col-12 col-sm-12">
               <div class="card border-top-0">
                    <div class="card-body pt-4">
                         <div class="table-responsive pb-0">
                              <table class="table animated fadeIn" id="cart">
                                   <thead class="text-uppercase">
                                        <tr>
                                             <th class="bg-img-none">#</th>
                                             <th>Product</th>
                                             <th>Quantity</th>
                                             <th>Unit</th>
                                             <th>Amount</th>
                                             <th>Sub Total</th>
                                             <th></th>
                                        </tr>
                                   </thead>
                                   <tbody></tbody>
                                   <tfoot style="display: none;">
                                        <tr>
                                             <th></th>
                                             <th></th>
                                             <th>0</th>
                                             <th></th>
                                             <th>&#8369; 0.00</th>
                                             <th>&#8369; 0.00</th>
                                             <th></th>
                                        </tr>
                                   </tfoot>
                              </table>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</form>

<div class="modal fade" id="addtocart-modal">
     <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
               <form class="addtocart-validation" autocomplete="off" novalidate>
               <!-- <form class="addtocart-validation" autocomplete="off"> -->
                    <input type="hidden" id="addtocart-pk">
                    <input type="hidden" id="addtocart-id">
                    <div class="modal-header">
                         <h5 class="modal-title"></h5>
                         <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#inventory-modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="form-group">
                              <label>Quantity</label>
                              <input type="number" class="form-control" id="addtocart-quantity" min="1" max="" step="1" placeholder="Quantity" max-length="11" required>
                              <div class="valid-feedback">Looks good! </div>
                              <div class="invalid-feedback">
                                   Enter a valid quantity. Available stocks: <strong id="addtocart-stock"></strong> <strong class="text-lowercase" id="addtocart-unit"></strong>
                              </div>
                         </div>
                         <div class="form-group">
                              <label>Amount</label>
                              <input type="text" class="form-control number-format" id="addtocart-amount" placeholder="Amount" max-length="11" required><div class="valid-feedback"> Looks good! </div>
                              <div class="invalid-feedback"> Enter a valid amount. </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-secondary col" id="addtocart-cancel" data-dismiss="modal" data-toggle="modal" data-target="#inventory-modal">CANCEL</button>
                         <button type="button" class="btn btn-success col" id="addtocart-continue">CONTINUE</button>
                         <!-- <button type="submit" class="btn btn-success col" id="addtocart-continue">CONTINUE</button> -->
                    </div>
               </form>
          </div>
     </div>
</div>

<div class="modal fade" id="inventory-modal">
     <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
               <div class="modal-header">
                    <h5 class="modal-title">ADD PRODUCT</h5>
                    <button type="button" class="close" data-dismiss="modal">
                         <span aria-hidden="true">&times;</span>
                    </button>
               </div>
               <div class="modal-body">
                    <div class="table-responsive pt-1 pb-0">
                         <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                         <table class="table table-sm d-none pt-2" id="product-table">
                              <thead>
                                   <tr class="text-uppercase">
                                        <th class="text-center no-sort">#</th>
                                        <th class="text-left">Type</th>
                                        <th class="text-left">Product</th>
                                        <th class="text-center no-sort">Stocks</th>
                                        <th class="text-left no-sort">Unit</th>
                                        <th class='no-sort text-center'></th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                   foreach ($retails as $r => $retail) {
                                        $quantity = $retail['quantity'];
                                        $buy_price = $retail['buy'];
                                        $total_buy_price = $buy_price * $quantity;
                                        $product = $retail['brand'] . " " . $retail['name'];
                                        $type = $retail['type'];                                        
                                        $registration_id = $retail['registration_id'];
                                        $branch = $retail['branch'];
                                        $fullname = $retail['first_name'] . " " . $retail['last_name'];
                                        $unit = $retail['unit'];
                                        $pk = $retail['pk'];
                                        $stock = $retail['stock'];
                                        ?>
                                        <tr>
                                             <td class="cell-key text-center">
                                                  <?php echo ++$r; ?>
                                             </td>
                                             <td class='cell-type'> <?php echo $type; ?> </td>
                                             <td class='cell-product'>
                                                  <span class='font-weight-medium'>
                                                       <?php echo $product; ?>
                                                  </span>                                                  
                                             </td>
                                             <td class="cell-stock text-center"> <?php echo number_format($stock); ?> </td>
                                             <td class='cell-unit'> <?php echo $unit; ?> </td>
                                             <td class="cell-action text-center">
                                                  <?php
                                                  if($stock > 0 ) {
                                                       echo '<button type="button" class="btn btn-primary btn-sm addtocart-btn" data-id="' . $registration_id . '" data-pk="' . $pk . '" data-product="' . $product . '" data-stock="' . $stock . '" data-unit="' . $unit . '" data-toggle="modal" data-target="#addtocart-modal" data-dismiss="modal">ADD TO CART</button>';
                                                  } else {
                                                       echo '<button class="btn btn-link btn-sm addtocart-btn text-danger" data-id="' . $registration_id . '" data-pk="' . $pk . '" disabled>NOT AVAILABLE</button>';
                                                  }
                                                  ?>
                                             </td>
                                        </tr>
                                        <?php
                                   }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>

<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/retail.js'); ?>"></script>

<script type="text/javascript">
     var branchesJSON = <?php echo json_encode($branches); ?>;
     var currentBranchJSON = "<?php echo $branch_fk; ?>";
</script>

<script type="text/javascript">
     $(document).ready( function() {
          getBranches(branchesJSON, currentBranchJSON);
          initDatePicker();
          numberFormat();
          addToCart();
          initSelect2();
          initDataTable();
          validateForm2();
     });
</script>
