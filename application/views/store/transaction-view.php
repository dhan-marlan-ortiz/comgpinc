<?php
     $category = $transaction['category'];
     $status = $transaction['status'];
     $appraisal_value = $transaction['value'];
     $amount_due = $transaction['amount'];
     $sales_id = 0;
     $selling_price = 0;
     $return_id = 0;
     $transaction_id = $transaction['transaction_id'];
     $role_fk = $this->session->userdata('role_fk');

     
?>

<div class="row no-gutters">
     <div class="col-12 col-md-6 col-xl-4 align-self-end">
          <?php transaction_tab("view"); ?>
     </div>
     <div class="col-12 col-md-6 col-xl-8 text-right border-bottom-nav d-none d-md-block align-self-end">
          <div id='table-toolbar'></div>
     </div>
</div>

<div class="row">
     <div class="col-12 col-sm-12 grid-margin stretch-card">
          <div class="card border-top-0" id="transaction-details">
               <?php
               if($category == 1) {
                    echo $category1_view;
               } else if($category == 2) {
                    echo $category2_view;
               } else if($category == 3) {
                    echo $category3_view;
               } else {
                    echo $category0_view;
               } ?>
          </div>
     </div>
</div>

<!-- SALES INFORMATION  -->
     <!-- SALES DETAILS -->
     <?php if($sales  && $category != 3) { ?>
     <div class="row">
          <div class="col-12 col-sm-12 grid-margin stretch-card">
               <div class="card" id="sales-details">
                    <div class="card-heading bg-light-2 p-3">
                         <p class="card-title font-weight-semibold text-body mb-0">
                              <i class="ti-shopping-cart"></i>&emsp;SALES DETAILS 
                         </p>
                    </div>
                    <div class="card-body py-0">                         
                         <div class="table-responsive pb-0 my-2">
                              <table class="table">
                                   <thead >
                                        <th>DATE</th> <th>SELLING PRICE</th> <th>REMARKS</th> <th>LOCATION</th> <th> USER</th>
                                   </thead>
                                   <tbody>
                                        <?php
                                        foreach ($sales as $s => $sale) {
                                             $is_void = $sale['is_void'];
                                             $sales_id = $sale['sales_id'];
                                             $selling_price = $sale['selling_price'];
                                             if($is_void == 0) {
                                                  $sales_status = "SOLD";
                                             } else if($is_void == 1) {
                                                  $sales_status = "VOID";
                                             } else if($is_void == 2) {
                                                  $sales_status = "RETURNED - " . $sale['remarks'];
                                             }
                                             echo "<tr>" .
                                                       "<td>" . strtoupper(date("d M Y", strtotime($sale['date_sold']))) . "</td>" .
                                                       "<td>" . number_format($sale['selling_price'], 2) . "</td>" .
                                                       "<td>" . strtoupper($sales_status) .  "</td>" .
                                                       "<td>" . $sale['branch_name'] . " (" . $sale['branch_fk'] . ")</td>" .
                                                       "<td>" . $sale['first_name'] . " " . $sale['last_name'] . "</td>" .
                                                       "<td class='text-right'>";
                                                            if($is_void == 0) {
                                                                 echo "<button data-sales-id='" . $sale['sales_id'] . "' data-reference-id='" . $sale['reference_id'] . "' class='btn btn-danger btn-sm mr-1' data-toggle='modal' data-target='#void-sales-transaction-modal'> VOID </button>";
                                                                 echo "<button class='btn btn-warning btn-sm text-white' data-toggle='modal' data-target='#return-sales-transaction-modal'> RETURN </button>";
                                                            }
                                             echo      "</td>" .
                                                  "</tr>";
                                        }
                                        ?>
                                   </tbody>
                              </table>
                         </div>
                    </div>
               </div>
          </div>
     </div>

     <!-- VOID SALES MODAL -->
     <div class="modal fade confirm-modal" id="void-sales-transaction-modal">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to void?</p>
                    </div>
                    <div class="modal-footer">
                         <a href="#" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
                         <a href="#" class="col btn btn-danger confirm">CONFIRM</a>
                    </div>
               </div>
          </div>
     </div>
     <!-- VOID SALES MODAL END -->

     <!-- RETURN SALES MODAL -->
     <form class="d-inline needs-validation" action="<?php echo base_url("Transaction/returnSalesTransaction/") . $transaction_id . "/" . $sales_id; ?> " method="post" autocomplete="off" novalidate>
          <div class="modal fade form-modal" id="return-sales-transaction-modal" tabindex="-1">
               <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                         <div class="modal-header">
                              <h5 class="modal-title text-uppercase text-warning">
                                   Return Item
                              </h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                         </div>
                         <div class="modal-body">
                              <div class="form-group">
                                   <label>Date <span class="text-danger">*</span></label>
                                   <input type="text" name="return_date" class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y"); ?>"  required>
                              </div>
                              <div class="form-group">
                                   <label>Cash Refund</label>
                                   <div class="input-group">
                                        <div class="input-group-prepend">
                                             <span class="input-group-text">&#8369;</span>
                                        </div>
                                        <input type="text" name="return_amount" class="form-control h-auto" value="<?php echo number_format($selling_price, 2) ?>"  required readonly>
                                   </div>
                              </div>
                              <div class="form-group mb-3">
                                   <label>Remarks <span class="text-danger">*</span></label>
                                   <textarea name="return_remarks" class="form-control" rows="4" spellcheck="false" placeholder="Remarks" required></textarea>
                              </div>
                              <div class="form-group mb-0">
                                   <label>Status <span class="text-danger">*</span></label>
                                   <select class="form-control" name="return_status" required>
                                        <option value="For Sale">For Sale</option>
                                        <option value="For Repair">For Repair</option>
                                        <option value="For Disposal">For Disposal</option>
                                   </select>
                                   <p class="text-muted mt-1 mb-0 font-italic text-small">
                                        Item status after return
                                   </p>
                              </div>
                         </div>
                         <div class="modal-footer">
                              <button type="button" class="col btn btn-secondary" data-dismiss="modal"> CANCEL </button>
                              <button type="button" class="col btn btn-warning" data-dismiss="modal" data-toggle="modal" data-target="#return-modal-confirm" > RETURN </button>
                         </div>
                    </div>
               </div>
          </div>
          <div class="modal fade confirm-modal" id="return-modal-confirm">
               <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                              <h5 class="modal-title">Confirmation</h5>
                              <button type="button" class="close" data-dismiss="modal">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                         </div>
                         <div class="modal-body">
                              <p class="mb-0">Are you sure want to return the item?</p>
                         </div>
                         <div class="modal-footer">
                              <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#return-sales-transaction-modal">CANCEL</button>
                              <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                         </div>
                    </div>
               </div>
          </div>
     </form>
     <!-- RETURN SALES MODAL END-->

     <?php } ?>
     <!-- SALES DETAILS end -->

     <!-- RETURN DETAILS -->
     <?php
     if($transaction_returns && $category == 1) { ?>
          <div class="row">
               <div class="col-12 col-sm-12 grid-margin stretch-card">
                    <div class="card" id="return-details">
                         <div class="card-heading bg-light-2 p-3">
                              <p class="card-title font-weight-semibold text-body mb-0">
                                   <i class="ti-shopping-cart"></i>&emsp;RETURN DETAILS
                              </p>
                         </div>
                         <div class="card-body py-0">                              
                              <div class="table-responsive pb-0 my-2">
                                   <table class="table table-light-striped">
                                        <thead>
                                             <th>DATE</th> <th>CASH REFUND</th> <th>LOCATION</th> <th></th> <th>USER </th>
                                        </thead>
                                        <tbody>
                                             <?php
                                                  foreach ($transaction_returns as $r => $return) {
                                                       $return_id = $return['id'];
                                                       echo "<tr>" .
                                                                 "<td>" . strtoupper(date("d M Y", strtotime($return['date']))) . "</td>" .
                                                                 "<td>" . number_format($return['amount'], 2) . "</td>" .
                                                                 "<td>" . strtoupper($return['remarks']) .  "</td>" .
                                                                 "<td>" . $return['branch_name'] . " (" . $return['branch_fk'] . ")</td>" .
                                                                 "<td>" . $return['first_name'] . " " . $return['last_name'] . "</td>" .
                                                                 "<td class='text-right'>";
                                                                      if($return['sales_id'] == $sales_id) {
                                                                           echo "<button class='btn btn-danger btn-sm mr-1' data-toggle='modal' data-target='#void-return-transaction-modal'> VOID </button>";
                                                                      }
                                                       echo      "</td>" .
                                                            "</tr>";
                                                  }
                                             ?>
                                        </tbody>
                                   </table>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
          <div class="modal fade confirm-modal" id="void-return-transaction-modal">
               <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                              <h5 class="modal-title">Confirmation</h5>
                              <button type="button" class="close" data-dismiss="modal">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                         </div>
                         <div class="modal-body">
                              <p class="mb-0">Are you sure want to void return?</p>
                         </div>
                         <div class="modal-footer">
                              <a href="#" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
                              <a href="<?php echo base_url('Transaction/voidTransactionSalesReturn/') . $transaction_id . "/" . $return_id . "/" . $sales_id; ?>" class="col btn btn-danger confirm">CONFIRM</a>
                         </div>
                    </div>
               </div>
          </div>
     <?php } ?>
     <!-- RETURNN DETAILS end -->
<!-- SALES INFORMATION  END -->

<!-- PAYMENTS INFORMATION -->
     <!-- PAYMENT HISTORY -->
     <?php if($category == 1 && $payments) { ?>
     <div class="row">
          <div class="col-12 col-sm-12 grid-margin stretch-card">
               <div class="card" id="payment-history">
                    <div class="card-heading bg-light-2 p-3">
                         <p class="card-title font-weight-semibold text-body mb-0">
                              <i class="ti-money"></i>&emsp;CUSTOMER'S PAYMENTS
                              <a class="text-decoration-none float-right" data-toggle="collapse" href="#payment-history-wrapper" role="button" aria-expanded="true" aria-controls="payment-history-wrapper">
                                   <i class="ti-more icon-sm"></i>
                              </a>
                         </p>
                    </div>
                    <div class="card-body py-0">                         
                         <div class="row narrow-gutters collapse show" id="payment-history-wrapper">
                              <div class="col-12">
                                   <div class="table-responsive pb-0 my-2">
                                        <table class="table table-sm text-uppercase table-light-striped">
                                             <thead>
                                                  <tr>
                                                       <th class="align-middle">#</th>
                                                       <th class="align-middle">Payment Date</th>
                                                       <th class="align-middle">Description</th>
                                                       <th class="">
                                                            <table class='table table-sm table-borderless text-right'>
                                                                 <tr>
                                                                      <th class='w-50'>PARTICULARS</th>
                                                                      <td class='w-25'></td>
                                                                      <td class='w-25'></td>
                                                                 </tr>
                                                            </table>
                                                       </th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  <?php
                                                       $pay_key = 1;
                                                       $grand_total = 0;
                                                       foreach ($payments as $pay) {
                                                            $payment_id = $pay['id'];
                                                            $status = $pay['status'];
                                                            $transaction_fee = $pay['transaction_fee'];
                                                            $penalty_charge = $pay['penalty_charge'];
                                                            $total_payment = 0;

                                                            echo "<tr>";
                                                            echo      "<td class='font-weight-bold align-text-top'>" . $pay_key . "</td>";
                                                            echo      "<td class='align-text-top'>" . date("d M Y", strtotime($pay['date'])) . "</td>";
                                                            echo      "<td class='align-text-top'>" . $status . "</td>";
                                                            echo      "<td class='align-text-top'>";
                                                            echo           "<table class='table table-sm table-borderless text-right'>";

                                                                           if($status == "Repurchased") {
                                                                                $total_payment = $transaction_fee + $penalty_charge + $appraisal_value;
                                                            echo                "<tr>
                                                                                     <td class='w-50 border-0'>Item Value</td>
                                                                                     <td class='w-25 border-bottom border-bottom-style-dotted font-weight-medium'>" . number_format($appraisal_value, 2) . "</td>
                                                                                     <td class='w-25 border-bottom border-bottom-style-dotted'></td>
                                                                                </tr>";
                                                                           } else {
                                                                                $total_payment = $transaction_fee + $penalty_charge;
                                                                           }
                                                            echo                "<tr>
                                                                                     <td class='w-50 border-0'>Transaction Fee</td>
                                                                                     <td class='w-25 border-bottom border-bottom-style-dotted font-weight-medium'>" . number_format($transaction_fee, 2) . "</td>
                                                                                     <td class='w-25 border-bottom border-bottom-style-dotted'></td>
                                                                                </tr>
                                                                                <tr>
                                                                                     <td class='border-0'>Penalty Charge:</td>
                                                                                     <td class='border-bottom border-bottom-style-dotted font-weight-medium'>" . number_format($penalty_charge, 2) . "</td>
                                                                                     <td class='border-bottom border-bottom-style-dotted'></td>
                                                                                </tr>
                                                                                <tr>
                                                                                     <td><b>Sub Total</b></td>
                                                                                     <td class='border-top-double'></td>
                                                                                     <td class='border-top-double px-0 font-weight-bold'>" . number_format($total_payment, 2) . "</td>
                                                                                </tr>
                                                                           </table>";
                                                            echo      "</td>";
                                                            echo "</tr>";

                                                            $pay_key++;
                                                            $grand_total += $total_payment;
                                                       }
                                                  ?>
                                             </tbody>
                                             <tfoot class="">
                                                  <tr>
                                                       <th colspan="3">
                                                            <?php if($role_fk == "ADMS" ) { ?>
                                                            <button type="button" class="btn btn-dark btn-sm text-uppercase btn-icon-text mt-2 mr-1" data-toggle="modal" data-target="#void-last-payment-modal">
                                                                 Void Last Payment<i class="ti-trash btn-icon-append"></i>
                                                            </button>
                                                            <div class="modal fade confirm-modal" id="void-last-payment-modal">
                                                                 <div class="modal-dialog modal-sm" role="document">
                                                                      <div class="modal-content">
                                                                           <div class="modal-header">
                                                                                <h5 class="modal-title">Confirmation</h5>
                                                                                <button type="button" class="close" data-dismiss="modal">
                                                                                     <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                           </div>
                                                                           <div class="modal-body">
                                                                                <p class="mb-0">Are you sure want to void payment?</p>
                                                                           </div>
                                                                           <div class="modal-footer">
                                                                                <a href="#" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
                                                                                <a href="<?php echo base_url('Transaction/voidLastPayment/') . $transaction_id; ?>" class="col btn btn-danger confirm">CONFIRM</a>
                                                                           </div>
                                                                      </div>
                                                                 </div>
                                                            </div>
                                                            <?php } ?>
                                                       </th>
                                                       <th>
                                                            <table class='table table-sm table-borderless text-right'>
                                                                 <tr>
                                                                      <th class='w-50'>TOTAL</th>
                                                                      <td class='w-25'></td>
                                                                      <th class='w-25'>&#8369; <?php echo number_format($grand_total, 2); ?></th>
                                                                 </tr>
                                                            </table>
                                                       </th>
                                                  </tr>
                                             </tfoot>
                                        </table>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <?php } ?>
     <!-- PAYMENT HISTORY -->
<!-- PAYMENTS INFORMATION END -->

<!-- TRANSACTION HISTORY -->
     <?php if($transaction && $category != 3 ) { ?>
     <div class="row">
          <div class="col-12 col-sm-12 grid-margin stretch-card">
               <div class="card" id="transaction-history">
                    <div class="card-heading bg-light-2 p-3">
                         <p class="card-title font-weight-semibold text-body mb-0">
                              <i class="ti-clipboard"></i>&emsp;TRANSACTION HISTORY
                              <a class="text-decoration-none float-right" data-toggle="collapse" href="#transaction-history-wrapper" role="button" aria-expanded="true" aria-controls="transaction-history-wrapper">
                                   <i class="ti-more icon-sm"></i>
                              </a>
                         </p>
                    </div>
                    <div class="card-body py-0">
                         <div class="row narrow-gutters collapse show" id="transaction-history-wrapper">
                              <div class="col-12">
                                   <div class="table-responsive pb-0 my-2">
                                        <table class="table text-uppercase table-light-striped">
                                             <thead>
                                                  <tr>
                                                       <th>#</th>
                                                       <th>Actual Date</th>
                                                       <th>Description</th>                                                       
                                                       <th>User</th>
                                                       <th>Location</th>
                                                       <th>Remarks</th>
                                                       <th>&nbsp;</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  <?php
                                                       $log_key = 1;
                                                       foreach ($logs as $log) {
                                                            echo "
                                                                 <tr>
                                                                      <td class='font-weight-bold align-text-top'>" . $log_key . "</td>
                                                                      <td class='align-text-top'>" . date("d M Y", strtotime($log['date'])) . "</td>
                                                                      <td class='align-text-top'>" . $log['description'] . "</td>                                                                      
                                                                      <td class='align-text-top'>" . $log['first_name'] . " " . $log['last_name'] . "</td>
                                                                      <td class='align-text-top'>" . $log['branch_name'] . "</td>
                                                                      <td class='align-text-top'>" . $log['remarks'] . "</td>                                                                      
                                                                      <td class='align-text-top'>
                                                                           <a class='update-history-remarks-btn' href='#' data-toggle='modal' data-target='#update-history-remarks-modal' data-id='" . $log['id'] . "'>
                                                                                <i class='ti-pencil-alt'></i>
                                                                           </a>
                                                                      </td>
                                                                 </tr>
                                                            ";
                                                            $log_key++;
                                                       }
                                                  ?>
                                             </tbody>
                                        </table>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <form class="d-inline needs-validation" action="<?php echo base_url("Transaction/updateHistoryRemarks/") . $transaction_id; ?>" method="post" autocomplete="off" novalidate>
          <input type="hidden" name="history_id" value="">
          <div class="modal fade form-modal" id="update-history-remarks-modal" tabindex="-1">
               <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                         <div class="modal-header">
                              <h5 class="modal-title text-uppercase text-primary">
                                   UPDATE REMARKS
                              </h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                         </div>
                         <div class="modal-body">                                                            
                              <div class="form-group mb-0">
                                   <label>Remarks</label>
                                   <textarea name="history_remarks" class="form-control" rows="4" spellcheck="false" maxlength="255" placeholder="Remarks" required></textarea>
                              </div>
                         </div>
                         <div class="modal-footer">
                              <button type="button" class="col btn btn-secondary" data-dismiss="modal"> CANCEL </button>
                              <button type="button" class="col btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#update-history-remarks-confirm" > SAVE </button>
                         </div>
                    </div>
               </div>
          </div>
          <div class="modal fade confirm-modal" id="update-history-remarks-confirm">
               <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                              <h5 class="modal-title">Confirmation</h5>
                              <button type="button" class="close" data-dismiss="modal">
                                   <span aria-hidden="true">&times;</span>
                              </button>
                         </div>
                         <div class="modal-body">
                              <p class="mb-0">Are you sure want to save changes?</p>
                         </div>
                         <div class="modal-footer">
                              <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#update-history-remarks-modal">CANCEL</button>
                              <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                         </div>
                    </div>
               </div>
          </div>
     </form>
     <?php } ?>
<!-- TRANSACTION HISTORY END -->

<script type="text/javascript" src="<?php echo base_url('assets/js/transaction-view.js'); ?>"></script>
<script type="text/javascript">
     var base_url = <?php echo "'" . base_url() . "'"; ?>
</script>
<script>
     $(function() {
          initSelect2();
          initDatePicker();
          updateStatus();
          updateHistoryRemarks();
          validateForm2();
          validateForm("#sell-item-modal form");
          validateForm("#update-sales-modal form");
          validateForm("#repurchase-modal form");
          getRenewalPayment({
               fee: "input[name='renTransFee']",
               charge: "input[name='renPenCharge']",
               due: "input[name='renAmountDue']"
          });
          getRepurchasePayment({
               value: "input[name='repAppVal']",
               fee: "input[name='repTransFee']",
               charge: "input[name='repPenCharge']",
               due: "input[name='repAmountDue']"
          });
          voidSalesTransaction(base_url);
          $('.number-format').number(true, 2);


          $("select[name='return_action']").on("change", function() {
               var val = $(this).val();
               var target = $("#return-quantity");

               if(val == "Cash Refund") {
                    target.slideDown('fast');
                    target.find('input').attr('disabled', false);
               } else {
                    target.slideUp('fast');
                    target.find('input').attr('disabled', 'disabled');
               }
          });

          $("select[name='return_sales_id']").on("change", function() {
               var val = $(this).val();
               var qty = $(this).find("option[value='" + val + "']").attr('data-quantity');

               $("input[name='return_quantity']").attr('max', qty);
          });

     });
</script>
