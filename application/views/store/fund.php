<style>
     .dataTables_wrapper .dataTables_length,
     .dataTables_filter {
          float: none;
          margin: 0;
     }
</style>

<?php
     $date = date('Y-m-d');
     $role_fk = $this->session->userdata('role_fk');
     $branch_name = $mybranch['name'] . " (" . $mybranch['id'] . ")";
     $total_deposits = 0;
     $total_withdrawals = 0;
     $total_transferin = 0;
     $total_transferout = 0;
     $total_result = 0;
 ?>
<div class="row no-gutters">
     <div class="col-lg-1">
          <h4 class='font-weight-bold mb-3'> <?php echo $title;?> </h4>
     </div>     
     <div class="col-lg-11">
          <form action="<?php echo base_url('Fund'); ?>" method="post" class="m-0">
               <div id="filter-wrapper" style="display: none;">
                    <?php if($role_fk == "ADMS") { ?>
                    <div class="d-inline-block float-right ml-2 mb-2" id="filter-location-wrapper" >
                         <div class="input-group">
                              <div class="input-group-prepend">
                                   <span class="input-group-text py-0 border-radius-left-2 ti-location-pin"></span>
                              </div>
                              <div class="input-group-append">
                                   <select id="filter-location" class="select2 select2-sm" name="branch">
                                   <?php foreach ($branches as $fb) {
                                        echo "<option value='" . $fb['id'] . "' " . ($filterLocation == $fb['id'] ? 'selected' : '') . ">" .
                                                       $fb['name'] . " (" . $fb['id'] . ")" .
                                             "</option>";
                                   } ?>
                                   </select>
                              </div>
                         </div>
                    </div>
                    <?php } ?>

                    <div class="d-inline-block float-right ml-2 mb-2" id="filter-date-wrapper">
                         <div class="input-group">
                              <div class="input-group-prepend">
                                   <span class="input-group-text py-0 border-radius-left-2 ti-calendar" style="height: 36px;"></span>
                              </div>          
                              <input type="text" value="<?php echo (isset($date_begin) ? $date_begin : ''); ?>" name="date_begin" placeholder="FROM" class="form-control datepicker date-range-filter text-uppercase min border-right-0" id="trans-min-date" data-date-format="dd M yyyy" autocomplete="off" style="border-radius:0;z-index:1!important;">
                              <input type="text" value="<?php echo (isset($date_end) ? $date_end : ''); ?>" name="date_end" placeholder="TO" class="form-control datepicker date-range-filter text-uppercase max border-right-0" id="trans-max-date" data-date-format="dd M yyyy" autocomplete="off" style="border-radius:0;z-index:1!important;">
                              <input type="hidden" name="filter-location" value="<?php echo $filterLocation; ?>" required>
                              <?php if(isset($date_begin) || isset($date_end)) {
                                   echo '<input type="submit" class="btn btn-light border-light-2 text-primary btn-sm border-right-0 font-12" name="date-filter-clear" value="CLEAR" style="border-radius:0;">';
                              } ?>
                              <input type="submit" name="date-filter" value="FILTER" class="btn btn-light border-light-2 text-primary btn-sm border-radius-right-2 font-12">
                         </div>
                    </div>               
                    <div class="d-inline-block float-right ml-2 mb-2" id="filter-search-wrapper">
                    </div>
                    <div class="d-inline-block float-right ml-2 mb-2" id="filter-length-wrapper">
                    </div>
               </div>
          </form>
     </div>
</div>

<ul class="nav nav-tabs">
     <li class="nav-item">
          <a href="#record" class="nav-link <?php echo ($tab == 'record' ? 'active': '') ?>" data-toggle="tab" role="tab">Records</a>
     </li>
     <li class="nav-item">
          <a href="#cashin" class="nav-link <?php echo ($tab == 'cashin' ? 'active': '') ?>" data-toggle="tab" role="tab">Cash Deposit</a>
     </li>
     <li class="nav-item">
          <a href="#cashout" class="nav-link <?php echo ($tab == 'cashout' ? 'active': '') ?>" data-toggle="tab" role="tab">Cash Withdrawal</a>
     </li>
     <li class="nav-item">
          <a href="#transfer" class="nav-link <?php echo ($tab == 'transfer' ? 'active': '') ?>" data-toggle="tab" role="tab">Cash Transfer</a>
     </li>
</ul>

<div class="tab-content" id="myTabContent">
     <div class="tab-pane <?php echo ($tab == 'record' ? 'active': '') ?>" id="record" role="tabpanel">
          <div class="card border-top-0">
               <div class="card-body py-4">
                    <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                    <div class="table-responsive pb-0 fade">
                         <table class="table d-none table-hover pb-3" id="fund-table">
                              <thead class="border-bottom-dark-2">
                                   <tr class="text-uppercase">
                                        <th class="no-sort bg-img-none">#</th>
                                        <th class="text-left">Date</th>
                                        <th class="text-left">Transaction ID</th>
                                        <th class="">Deposits</th>
                                        <th class="">Withdrawals</th>
                                        <th class="">Transafer In</th>
                                        <th class="">Transafer Out</th>
                                        <th class="no-sort">BALANCE</th>
                                        <th class="no-sort"></th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                        foreach ($funds as $f => $fund) {
                                             $amount = $fund['amount'];
                                             $tag = $fund['tag'];
                                             $tag_fk = $fund['tag_fk'];
                                             $origin = $fund['origin'];
                                             $destination = $fund['destination'];
                                             $is_acknowledged = $fund['is_acknowledged'];
                                             $class = "";

                                             if($origin == $filterLocation && $destination !== $filterLocation) {
                                                  $tag .= " OUT";

                                             } else if($destination == $filterLocation && $origin !== $filterLocation) {
                                                  $tag .= " IN";
                                             }

                                             if($tag_fk == 1 || $tag_fk == 2) {
                                                  $class = "fund-cash";
                                             } else if($tag_fk == 3) {
                                                  $class = "fund-transfer";
                                             }

                                             echo "<tr
                                                       data-id='" . $fund['id'] . "'
                                                       data-transaction='" . $fund['transaction_id'] . "'
                                                       data-date='" . date("F d, Y", strtotime($fund['date'])) . "'
                                                       data-origin='" . $fund['origin_name'] . "'
                                                       data-destination='" . $fund['destination_name'] . "'
                                                       data-user='" . $fund['first_name'] . " " . $fund['last_name'] . "'
                                                       data-tag='" . $tag . "'
                                                       data-tag-fk='" . $tag_fk . "'
                                                       data-amount='" . number_format($amount, 2) . "'
                                                       data-description='" . $fund['description']  . "'
                                                       data-acknowledged='" . $fund['is_acknowledged']  . "'

                                                       class='" . $class . "'
                                                  >";
                                             echo      "<td>" . ++$f . "</td>";
                                             echo      "<td class='text-uppercase' data-sort='".$fund['date']."'>" . date("d M Y", strtotime($fund['date'])) . "</td>";
                                             echo      "<td>" . $fund['transaction_id'] . "</td>";

                                             if($tag_fk == 1) {
                                                  $total_deposits += $amount;
                                                  $total_result += $amount;
                                                  echo      "<td class='text-right'>" . number_format($amount, 2) . "</td>";
                                                  echo      "<td></td>";
                                                  echo      "<td></td>";
                                                  echo      "<td></td>";
                                             } if($tag_fk == 2) {
                                                  $total_withdrawals += $amount;
                                                  $total_result -= $amount;
                                                  echo      "<td></td>";
                                                  echo      "<td class='text-right'>(" . number_format($amount, 2) . ")</td>";
                                                  echo      "<td></td>";
                                                  echo      "<td></td>";
                                             } if($tag_fk == 3 && $tag == "TRANSFER IN") {
                                                  $total_transferin += $amount;
                                                  $total_result += $amount;
                                                  echo      "<td></td>";
                                                  echo      "<td></td>";
                                                  echo      "<td class='text-right'>" . number_format($amount, 2) . "</td>";
                                                  echo      "<td></td>";
                                             } if($tag_fk == 3 && $tag == "TRANSFER OUT") {
                                                  $total_transferout += $amount;
                                                  $total_result -= $amount;
                                                  echo      "<td></td>";
                                                  echo      "<td></td>";
                                                  echo      "<td></td>";
                                                  echo      "<td class='text-right'>(" . number_format($amount, 2) . ")</td>";
                                             }

                                             echo      "<td class='text-right'>" . number_format($total_result, 2) . "</td>";
                                             echo      "<td>";
                                                            if($is_acknowledged == 0 && $tag_fk == 3) {
                                                                 echo "<a href='#' class='text-decoration-none text-reset d-block' data-toggle='popover' data-placement='top' title='Alert' data-content='This item requires acknowledgement' style='position: relative'>
                                                                           <h4 class='ti-alert text-danger' style='position: absolute; right: 0; top: 50%; transform: translateY(-60%);'></h4
                                                                      </a>";
                                                            }
                                             echo "</tr>";
                                        }
                                   ?>
                              </tbody>
                              <tfoot>
                                   <tr class="text-uppercase">
                                        <th class=""></th>
                                        <th class=""></th>
                                        <th class=""></th>
                                        <th class="">
                                             <?php echo number_format($total_deposits, 2); ?>
                                        </th>
                                        <th class="">
                                             <?php
                                                  if($total_withdrawals == 0) {
                                                       echo number_format($total_withdrawals, 2);
                                                  } else {
                                                       echo "(" . number_format($total_withdrawals, 2) . ")";
                                                  }
                                             ?>
                                        </th>
                                        <th class="">
                                             <?php echo number_format($total_transferin, 2); ?>
                                        </th>
                                        <th class="">
                                             <?php
                                             if($total_transferout == 0) {
                                                  echo number_format($total_transferout, 2);
                                             } else {
                                                  echo "(" . number_format($total_transferout, 2) . ")";
                                             }
                                             ?>
                                        </th>
                                        <th class="">
                                             <?php echo number_format($total_result, 2); ?>
                                        </th>
                                        <th class=""></th>
                                   </tr>
                              </tfoot>
                         </table>
                    </div>
               </div>
          </div>
          <div class="row">
               <div class="col-lg-4 grid-margin stretch-card d-none">
                    <div class="card">
                         <div class="card-body">
                              <p class="card-title text-primary">GENERAL SUMMARY</p>
                              <div class="table-responsive">
                                   <!-- <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader"> -->
                                   <table class="table">
                                        <tbody>
                                             <?php
                                                  $fundTotal = $sumCashIn + $sumTransferIn - $sumCashOut - $sumTransferOut;
                                             ?>
                                             <tr>
                                                  <td>DEPOSITS</td>
                                                  <td class="text-right">
                                                       <?php echo number_format($sumCashIn, 2); ?>
                                                  </td>
                                             </tr>
                                             <tr>
                                                  <td>WITHDRAWALS</td>
                                                  <td class="text-right">
                                                       <?php
                                                            if($sumCashOut == 0) {
                                                                 echo number_format($sumCashOut, 2);
                                                            } else {
                                                                 echo "(" . number_format($sumCashOut, 2) . ")";
                                                            }
                                                       ?>
                                                  </td>
                                             </tr>
                                             <tr>
                                                  <td>TRANSFER IN</td>
                                                  <td class="text-right">
                                                       <?php echo number_format($sumTransferIn, 2); ?>
                                                  </td>
                                             </tr>
                                             <tr>
                                                  <td>TRANSFER OUT</td>
                                                  <td class="text-right">
                                                       <?php
                                                       if($sumTransferOut == 0) {
                                                            echo number_format($sumTransferOut, 2);
                                                       } else {
                                                            echo "(" . number_format($sumTransferOut, 2) . ")";
                                                       }
                                                       ?>
                                                  </td>
                                             </tr>
                                        </tbody>
                                        <tfoot>
                                             <tr>
                                                  <th>TOTAL FUNDS</th>
                                                  <th class="text-right">
                                                       &#8369;
                                                       <?php echo number_format($fundTotal, 2) ?>
                                                  </th>
                                             </tr>
                                        </tfoot>
                                   </table>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="tab-pane fade show <?php echo ($tab == 'cashin' ? 'active': '') ?>" id="cashin" role="tabpanel">
          <div class="card border-top-0">
               <div class="card-body">
                    <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                    <form class="needs-validation form-inline fade" action="<?php echo base_url('Fund/insertFund/1'); ?>" method="post" autocomplete="off" novalidate>
                         <table class="mx-auto">
                              <tr>
                                   <td colspan="2">
                                        <h5 class="border-bottom display-4 pb-3">
                                             <small>Cash Deposit Form</small>
                                        </h5>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"> Location </td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Location </label>
                                        <select class="form-control select2" name="location" required >
                                             <?php
                                                  if($role_fk == 'ADMS') {
                                                       echo '<option value="" selected disabled> SELECT </option>';
                                                       foreach ($branches as $fb) {
                                                            echo "<option value='" . $fb['id'] . "' " . ($filterLocation == $fb['id'] ? 'selected' : '') . ">" .
                                                                      $fb['name'] . " (" . $fb['id'] . ")" .
                                                                 "</option>";
                                                       }
                                                  } else {
                                                       echo "<option value='" . $mybranch['id'] . "' >" .
                                                                 $branch_name  .
                                                            "</option>";
                                                  }
                                             ?>
                                        </select>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"> Date </td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Date </label>
                                        <input type="text" name="date" placeholder="MM dd, yyyy" class="form-control border-radius-2 datepicker text-uppercase" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y", strtotime($date)); ?>"  required>
                                   </td>
                              </tr>
                              <tr> 
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"> Amount </td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Amount </label>
                                        <input type="text" class="form-control float-format" name="amount" placeholder="AMOUNT" required maxlength="11" required>                                        
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"> Description</td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Description</label>
                                        <textarea class="form-control text-uppercase w-100" name="description" rows="4" spellcheck="false" maxlength="255" placeholder="Description"></textarea>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"></td>
                                   <td class="py-2 px-3">
                                        <button type="button" class="btn btn-primary border-radius-2" data-toggle="modal" data-target="#cashin-confirm"> SUBMIT </button>
                                   </td>
                              </tr>
                         </table>
                         <div class="modal fade confirm-modal" id="cashin-confirm">
                              <div class="modal-dialog modal-sm" role="document">
                                   <div class="modal-content">
                                        <div class="modal-header">
                                             <h5 class="modal-title">Confirmation</h5>
                                             <button type="button" class="close" data-dismiss="modal">
                                                  <span aria-hidden="true">&times;</span>
                                             </button>
                                        </div>
                                        <div class="modal-body">
                                             <p class="mb-0">Are you sure want to process?</p>
                                        </div>
                                        <div class="modal-footer">
                                             <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#cash-modal">CANCEL</button>
                                             <button type="submit" class="col btn btn-success confirm" name="create" value="create">CONFIRM</button>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </form>               
               </div>
          </div>          
     </div>   
     <div class="tab-pane fade show <?php echo ($tab == 'cashout' ? 'active': '') ?>" id="cashout" role="tabpanel">
          <div class="card border-top-0">
               <div class="card-body">
                    <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                    <form class="needs-validation form-inline fade" action="<?php echo base_url('Fund/insertFund/2'); ?>" method="post" autocomplete="off" novalidate>
                         <table class="mx-auto">
                              <tr>
                                   <td colspan="2">
                                        <h5 class="border-bottom display-4 pb-3">
                                             <small>Cash Withdrawal Form</small>
                                        </h5>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"> Location </td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Location</label>
                                        <select class="form-control select2" name="location" required >
                                             <?php
                                                  if($role_fk == 'ADMS') {
                                                       echo '<option value="" selected disabled> SELECT </option>';
                                                       foreach ($branches as $fb) {
                                                            echo "<option value='" . $fb['id'] . "' " . ($filterLocation == $fb['id'] ? 'selected' : '') . ">" .
                                                                      $fb['name'] . " (" . $fb['id'] . ")" .
                                                                 "</option>";
                                                       }
                                                  } else {
                                                       echo "<option value='" . $mybranch['id'] . "' >" .
                                                                 $branch_name  .
                                                            "</option>";
                                                  }
                                             ?>
                                        </select>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"> Date </td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Date</label>
                                        <input type="text" name="date" placeholder="MM dd, yyyy" class="form-control border-radius-2 datepicker text-uppercase" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y", strtotime($date)); ?>"  required>
                                   </td>
                              </tr>
                              <tr> 
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"> Amount </td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Amount</label>
                                        <input type="text" class="form-control float-format" name="amount" placeholder="AMOUNT" required maxlength="11" required>                                        
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"> Description</td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Description</label>
                                        <textarea class="form-control text-uppercase w-100" name="description" rows="4" spellcheck="false" maxlength="255" placeholder="Description"></textarea>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"></td>
                                   <td class="py-2 px-3">
                                        <button type="button" class="btn btn-primary border-radius-2" data-toggle="modal" data-target="#cashout-confirm"> SUBMIT </button>
                                   </td>
                              </tr>
                         </table>
                         <div class="modal fade confirm-modal" id="cashout-confirm">
                              <div class="modal-dialog modal-sm" role="document">
                                   <div class="modal-content">
                                        <div class="modal-header">
                                             <h5 class="modal-title">Confirmation</h5>
                                             <button type="button" class="close" data-dismiss="modal">
                                                  <span aria-hidden="true">&times;</span>
                                             </button>
                                        </div>
                                        <div class="modal-body">
                                             <p class="mb-0">Are you sure want to process?</p>
                                        </div>
                                        <div class="modal-footer">
                                             <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#cash-modal">CANCEL</button>
                                             <button type="submit" class="col btn btn-success confirm" name="create" value="create">CONFIRM</button>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </form>               
               </div>
          </div>          
     </div>      
     <div class="tab-pane fade show <?php echo ($tab == 'transfer' ? 'active': '') ?>" id="transfer" role="tabpanel">
          <div class="card border-top-0">
               <div class="card-body">
                    <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                    <form class="needs-validation fade" action="<?php echo base_url('Fund/transferFund'); ?>" method="post" id="transfer-form" autocomplete="off" novalidate>
                         <table class="mx-auto">
                              <tr>
                                   <td colspan="2">
                                        <h5 class="border-bottom display-4 pb-3">
                                             <small>Cash Transfer Form</small>
                                        </h5>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell">Origin</td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Origin</label>
                                        <select class="form-control select2" name="origin" required >
                                             <?php
                                                  if($role_fk == 'ADMS') {
                                                       echo '<option value="" selected disabled> SELECT </option>';
                                                       foreach ($branches as $fb) {
                                                            echo "<option value='" . $fb['id'] . "' " . ($filterLocation == $fb['id'] ? 'selected' : '') . ">" .
                                                                      $fb['name'] . " (" . $fb['id'] . ")" .
                                                                 "</option>";
                                                       }
                                                  } else {
                                                       echo "<option value='" . $mybranch['id'] . "' >" .
                                                                 $branch_name  .
                                                            "</option>";
                                                  }
                                             ?>
                                        </select>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell">Destination</td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Destination</label>
                                        <select class="form-control select2" name="destination" required >
                                             <option value="" selected disabled> SELECT </option>
                                             <?php
                                             foreach ($branches as $fb) {
                                                  if($fb['id'] != $filterLocation) {
                                                       echo "<option value='" . $fb['id'] . "' >" .
                                                                 $fb['name'] . " (" . $fb['id'] . ")" .
                                                            "</option>";
                                                  }
                                             }
                                         ?>
                                        </select>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell">Date</td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Date</label>
                                        <input type="text" name="date" placeholder="MM dd, yyyy" class="form-control border-radius-2 datepicker text-uppercase" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y", strtotime($date)); ?>"  required>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell">Amount</td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Amount</label>
                                        <input type="text" class="form-control float-format text-uppercase h-auto" name="amount" placeholder="AMOUNT" required maxlength="11" required>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell">Description</td>
                                   <td class="py-2 px-3">
                                        <label for="" class="d-block d-sm-none">Description</label>
                                        <textarea class="form-control text-uppercase" name="description" rows="4" spellcheck="false" maxlength="255" placeholder="Description"></textarea>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="py-2 px-3 text-right font-14 font-weight-medium d-none d-sm-table-cell"></td>
                                   <td class="py-2 px-3">
                                        <button type="button" class="btn btn-primary mt-1" data-toggle="modal" data-target="#transfer-confirm"> TRANSFER </button>
                                   </td>
                              </tr>
                         </table>
                         <div class="modal fade confirm-modal" id="transfer-confirm">
                              <div class="modal-dialog modal-sm" role="document">
                                   <div class="modal-content">
                                        <div class="modal-header">
                                             <h5 class="modal-title">Confirmation</h5>
                                             <button type="button" class="close" data-dismiss="modal">
                                                  <span aria-hidden="true">&times;</span>
                                             </button>
                                        </div>
                                        <div class="modal-body">
                                             <p class="mb-0">Are you sure want to process?</p>
                                        </div>
                                        <div class="modal-footer">
                                             <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#transfer-modal">CANCEL</button>
                                             <button type="submit" class="col btn btn-success confirm" name="create" value="create">CONFIRM</button>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </form>
               </div>                    
          </div>
     </div>
</div>

<form class="needs-validation" action="<?php echo base_url('Fund/void'); ?>" method="post" id="details-form" autocomplete="off" novalidate>
     <div class="modal fade form-modal" id="details-modal">
          <div class="modal-dialog" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">TRANSACTION DETAILS</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="row narrow-gutters">
                              <div class="col-6">
                                   <div class="form-group">
                                        <label>Transaction ID</label>
                                        <input id="details-transaction" name="details-transaction" type="text" class="form-control text-dark" tabindex="-1" readonly>
                                   </div>
                              </div>
                              <div class="col-6">
                                   <div class="form-group">
                                        <label>Date</label>
                                        <input id="details-date" name="details-date" type="text" class="form-control text-dark text-uppercase" tabindex="-1" readonly>
                                   </div>
                              </div>

                              <div class="col-6" style="display: none;">
                                   <div class="form-group">
                                        <label>Location</label>
                                        <input id="details-location" name="details-location" type="text" class="form-control text-dark" tabindex="-1" readonly>
                                   </div>
                              </div>
                              <div class="col-6" style="display: none;">
                                   <div class="form-group">
                                        <label>Origin</label>
                                        <input id="details-origin" name="details-origin" type="text" class="form-control text-dark" tabindex="-1" readonly>
                                   </div>
                              </div>
                              <div class="col-6" style="display: none;">
                                   <div class="form-group">
                                        <label>Destination</label>
                                        <input id="details-destination" name="details-destination" type="text" class="form-control text-dark" tabindex="-1" readonly>
                                   </div>
                              </div>
                              <div class="col-6">
                                   <div class="form-group">
                                        <label>User</label>
                                        <input id="details-user" name="details-user" type="text" class="form-control text-dark" tabindex="-1" readonly>
                                   </div>
                              </div>

                              <div class="col-6">
                                   <div class="form-group">
                                        <label>Tag</label>
                                        <input id="details-tag" name="details-tag" type="text" class="form-control text-dark" tabindex="-1" readonly>
                                   </div>
                              </div>
                              <div class="col">
                                   <div class="form-group">
                                        <label>Amount</label>
                                        <div class="input-group">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text">&#8369;</span>
                                             </div>
                                             <input id="details-amount" name="details-amount" type="text" class="form-control float-format text-uppercase h-auto" tabindex="-1" readonly>
                                        </div>
                                   </div>
                              </div>
                              <div class="col-6">
                                   <div class="form-group">
                                        <label>Acknowledged</label>
                                        <input id="details-acknowledged" name="details-acknowledged" type="text" class="form-control text-dark" tabindex="-1" readonly>
                                   </div>
                              </div>
                              <div class="col-12">
                                   <div class="form-group mb-0">
                                        <label>Description / Remarks</label>
                                        <textarea id="details-description" name="details-description" class="form-control text-uppercase" rows="4" spellcheck="false" tabindex="-1" readonly></textarea>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <div class="w-100">
                              <?php if($role_fk == "ADMS") { ?>
                                   <button type="button" class="btn btn-danger border-radius-2" data-dismiss="modal" data-toggle="modal" data-target="#cash-void-modal"> VOID </button>
                              <?php } ?>
                              <a href="#" class="btn btn-primary border-radius-2" id="details-acknowledgedment-btn"> ACKNOWLEDGE </a>
                              <button type="button" class="btn btn-secondary border-radius-2 float-right" data-dismiss="modal"> CLOSE </button>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="cash-void-modal">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to void?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#details-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-danger confirm" name="create" value="create">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>

<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/fund.js'); ?>"></script>
<script type="text/javascript"> var base_url = '<?php echo base_url(); ?>'; </script>
<script type="text/javascript"> var tab = '<?php echo $tab; ?>'; </script>
<script type="text/javascript">
     $(document).ready( function() {
          initDatePicker();
          initSelect2();
          numberFormat();
          initDataTable(base_url, tab);
          filterLocation();
          cashDetailsModal(base_url);          
          transferModal(base_url);
          formValidation();
          updateUrl(base_url)
     });
</script>

<script type="text/javascript">
     $(window).on("load", function() {
          loader();
     });
</script>
