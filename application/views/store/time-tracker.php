<style>
	#time-registration-modal form .form-group {
		position: relative;
	}
	#time-registration-modal form label.error {
		bottom: -30px;
		left: 0;	
		color: #dc3545;
	    font-size: 12px;
    	font-family: monospace;
	}
	#time-registration-modal form input.error,
	#time-registration-modal form select.error + label + .select2 .select2-selection {
		border-color: rgba(220, 53, 69, 0.5) !important;
	}

	@media(min-width: 576px) {
		.dataTables_length, .dataTables_filter {
	          float: right !important;
	          margin-left: 12px;
	          margin-bottom: 12px;
	          display: inline-block;
	    }
	    .dataTables_length {
	        margin-right: 114px;
	    }
    }
    #attendance-form-toggle {    	
		right: 15px;
		height: 36px;
		padding: 12px;
		font-size: 12px;
		z-index: 1;
    }
</style>

<div class="row">
	<div class="col-12">
		<h4 class='font-weight-bold mb-sm-3 d-sm-inline-block'> <?php echo $title;?> </h4>
		<!-- <input type="text" id="time" class="d-sm-inline-block float-sm-right border-0 bg-transparent text-monospace text-uppercase font-14 w-100" maxlength="23"> -->
		<p id="server-time" class="text-nowrap d-sm-inline-block float-sm-right text-monospace text-uppercase "></p>		
	</div>
</div>


<div class="card" style="min-height: calc(100vh - 125px);">
	<div class="card-body">
		<?php echo tab_header("", "position-sm-absolute"); ?>
		
		<button id="attendance-form-toggle" type="button" data-toggle="modal" data-target="#time-registration-modal" class="btn btn-twitter position-absolute">TIME IN / OUT</button>		

        <?php        	
			if($attendance) {
				$options = array('tbody_class' => "no-border");
				admin_table($attendance, $options);	
			}
		?>

	</div>
</div>

<div id="time-registration-modal" class="modal fade confirm-modal" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form action="<?php echo base_url('TimeTracker/register'); ?>" method="POST" autocomplete="off">
				<div class="modal-header">
					<h5 class="modal-title">Attendance Form</h5>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="text-muted no-wrap">Server Time</label>
						<input type="text" id="time" class="form-control text-center bg-white font-weight-semibold user-select-none pointer-events-none transition-2" readonly required>
					</div>
					<div class="form-group">
						<label class="text-muted no-wrap">Employee</label>
						<select name="employee"  class="form-control select2" required style="width: calc(100% - 2rem) !important; height:43.19px !important"> 
							<option value="" selected disabled>SELECT</option>
							<?php 					
								foreach ($employees as $emp) {
									$emp_id = $emp['id'];
									$password = $emp['password'];
									$emp_fullname = $emp['first_name'] . "&ensp;" . $emp['middle_name'][0] . ".&ensp;" . $emp['last_name'] . "&ensp;-&ensp;".  $emp_id;
									echo "<option value='$emp_id $password'>$emp_fullname</option>";
								}
							?>
						</select>
					</div>
					<div class="form-group mb-2">
						<label class="text-muted no-wrap">Password</label>
						<input type="text" name="password_confirm" class="form-control" required>
						<input type="hidden" name="password" id="password" required readonly>
						<input type="hidden" name="employee_id" required readonly>
						<!-- <input type="hidden" name="description" required readonly> -->
					</div>
					
				</div>
				<div class="modal-footer">
					<a href="#" class="col btn btn-light border cancel" data-dismiss="modal">CANCEL</a>
					<input type="submit" name="description" value="time in" class="col btn btn-success confirm text-uppercase">					
				</div>
			</form>
		</div>
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/js/admin.js'); ?>"></script>
<script type="text/javascript">
	var time_record = <?php echo json_encode($time_record); ?>;
</script>


<script type="text/javascript">
	var timestamp = '<?php echo time(); ?>';	
	
	function updateTime(){	
		var	stamp = Date(timestamp);
		var explode = new Array();
		explode = stamp.split(" ");		
		$('#server-time').html( "<span class='text-muted'>Server Time: </span>" + explode[4]);

		$('#time').val(explode[4]);
		$('h5 small').text(explode[0] + ", " + explode[1] + " " + explode[2] + ", " + explode[3]);
		timestamp++;
	}

	function checkRecord(emp_id) {
		var result = "time in";
		$.map(time_record, function(elem, index) {
			if (elem.employee_id == emp_id && elem.description == 'time in') {
				result = "time out";
			} else if (elem.employee_id == emp_id && elem.description == 'time out') {
				result = "away";				
			}
		});		
		return result;
	}

	$(function(){
		setInterval(updateTime, 1000);
		initSelect2();

		var attendance_modal = "#time-registration-modal";
		var attendance_form = "#time-registration-modal form";

		var form_validation = $(attendance_form).validate({
			rules: {
				password: "required",
				employee_id: "required",
				password_confirm: {
					equalTo: "#password",
					required: true
				}
			},
			messages: {				
				password_confirm: {
					equalTo: "Invalid password"				
				}
			}			
		});

		$("select[name='employee']").on('change', function(){
			if($(attendance_modal).is(':visible')) {
				var employee_id = $(this).val().split(" ")[0];
				var password = $(this).val().split(" ")[1];
				var description = checkRecord(employee_id);
				var submit_btn = attendance_modal + " input[name='description']";
				$(attendance_modal + " input[name='employee_id']").val(employee_id);
				$(attendance_modal + " input[name='password']").val(password);

				if(description == 'away') {
					$(submit_btn).hide();
				} else {
					$(submit_btn).val(description);
					$(submit_btn).show();
				}
			}
		});
		
		$(attendance_modal).on('hidden.bs.modal', function (e) {
			$("select[name='employee']").val('').trigger('change'); // reset select			
			$(attendance_form).trigger('reset'); // clear input fields
			form_validation.resetForm(); // remove validation errors
			$(attendance_form).find(".error").removeClass("error"); // remove error style
			$(attendance_modal + " input[name='description']").show(); // show submit btn
		});	

		var table = $('#admin-table').DataTable({
			"columnDefs": [{
				"searchable": false,
				"orderable": false,
				"targets": 0
			}],
			"order": [[1, 'asc']],
			"columnDefs": [{
				"targets": 'no-sort',
				"searchable": false,
				"orderable": false
			}]
		});

		table.on('order.dt search.dt', function() {
			table.column(0, {
				search: 'applied',
				order: 'applied'
			}).nodes().each(function(cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();

	});
</script>


