<div class="row">
     <div class="col-12 col-sm-6 col-md-4 grid-margin stretch-card">
          <div class="card">
               <div class="card-body">
                    <h4 class="card-title text-primary">New Transaction</h4>

                    <div class="form-group mb-3">
                         <label>Branch Name <span class="text-danger">*</span></label>
                         <select id="select-branch" class="form-control" name="branch" required></select>
                    </div>

                    <div class="form-group mb-3">
                         <label>Customer Name <span class="text-danger">*</span></label>
                         <select id="select-customer" class="form-control" name="customer" required></select>
                         <label class="mt-1">
                              <small>
                                   <a href="#">Add new customer</a>
                              </small>
                         </label>
                    </div>

                    <div class="form-group mb-3">
                         <label>Item Type <span class="text-danger">*</span></label>
                         <select id="select-type" class="form-control" name="type" required></select>
                         <label class="mt-1">
                              <small>
                                   <a href="#">Add new type</a>
                              </small>
                         </label>
                    </div>

                    <div class="form-group mb-3">
                         <label>Brand Name <span class="text-danger">*</span></label>
                         <select id="select-brand" class="form-control" name="brand" required></select>
                         <label class="mt-1">
                              <small>
                                   <a href="#">Add new brand</a>
                              </small>
                         </label>
                    </div>

                    <div class="form-group mb-3">
                         <label>Serial Number</label>
                         <input type="text" class="form-control" name="serial" value="" placeholder="Serial Number" required>
                    </div>
                    <div class="row narrow-gutters">
                         <div class="col-6">
                              <div class="form-group mb-3">
                                   <label>Duration <span class="text-danger">*</span></label>
                                   <select id="select-duration" class="form-control" name="duration" required>
                                        <option value="15">15</option>
                                        <option value="30">30</option>
                                   </select>
                              </div>
                         </div>
                         <div class="col-6">
                              <div class="form-group mb-3">
                                   <label>Unit Value <span class="text-danger">*</span></label>
                                   <input type="text" class="form-control" name="value" value="" placeholder="Unit Value" required>
                              </div>
                         </div>
                    </div>
                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                              <div class="form-group mb-3">
                                   <label>Appraiser <span class="text-danger">*</span></label>
                                   <select id="select-appraiser" class="form-control" name="appraiser" required></select>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                              <div class="form-group mb-3">
                                   <label>Secondary&nbsp;Appraiser</label>
                                   <select id="select-appraiser2" class="form-control" name="appraiser2"></select>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="col-12 col-sm-6 col-md-8 grid-margin stretch-card">
          <div class="card">
               <div class="card-body">
                    <h4 class="card-title text-primary">Transaction Summary</h4>

                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Branch</label>
                                        <p> PCM Manila </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Transaction Date</label>
                                        <p><?php echo date("F d, Y"); ?></p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Status
                                             <!-- <strong class="text-success ">&#9864;</strong> -->
                                        </label>
                                        <p>
                                             In Contract</mark>
                                        </p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <hr>

                    <div class="row">
                         <div class="col-12 col-sm-6">
                              <p class="card-description">
                                   CUSTOMER INFORMATION
                              </p>
                         </div>
                    </div>

                    <div class="row">
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7">
                              <div class="form-group row narrow-gutters mb-0">
                                   <div class="col-12 col-sm-12 col-md-4 text-field d-block">
                                        <label>First name</label>
                                        <p> John Kenneth </p>
                                   </div>
                                   <div class="col-12 col-sm-12 col-md-4 text-field d-block">
                                        <label>Middle name</label>
                                        <p> Perez </p>
                                   </div>
                                   <div class="col-12 col-sm-12 col-md-4 text-field d-block">
                                        <label>Last name</label>
                                        <p> Frey </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5">
                              <div class="form-group row narrow-gutters mb-0">
                                   <div class="col-6">
                                        <div class="text-field d-block">
                                             <label>Contact number</label>
                                             <p> 0919 223 2933 </p>
                                        </div>
                                   </div>
                                   <div class="col-6">
                                        <div class="text-field d-block">
                                             <label>Gender</label>
                                             <p>Male</p>
                                        </div>

                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row">
                         <div class="col-12">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Complete Address</label>
                                        <p>2nd Floor Green Field District, Mandaluyong City, Metro Manila</p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row">
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7">
                              <div class="form-group row narrow-gutters mb-0">
                                   <div class="col-6 text-field d-block">
                                        <label>ID Presented</label>
                                        <p> Passport </p>
                                   </div>
                                   <div class="col-6 text-field d-block">
                                        <label>ID Number</label>
                                        <p> P0000000A  </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Remarks</label>
                                        <p>None</p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <hr>

                    <div class="row">
                         <div class="col-12 col-sm-6">
                              <p class="card-description">
                                   ITEM INFORMATION
                              </p>
                         </div>
                    </div>

                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Item Type</label>
                                        <p> Laptop computer </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Brand Name</label>
                                        <p> Acer </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Serial Number</label>
                                        <p> XX-1234ABC</p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row narrow-gutters">
                         <div class="col-4 col-sm-6">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Duration</label>
                                        <p> 30 Days </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-8 col-sm-6">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Expiry Date</label>
                                        <p> November 24, 2019 </p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Unit Value</label>
                                        <p> 34,990 </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Transaction Fee</label>
                                        <p> 349 </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Amount Due</label>
                                        <p> 35,339</p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Description</label>
                                        <p> Good condition </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Appraiser</label>
                                        <p> Jane Doe </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Secondary Appraiser</label>
                                        <p> None</p>
                                   </div>
                              </div>
                         </div>
                    </div>

               </div>
          </div>
     </div>
</div>


<script type="text/javascript">

     var initSelect2 = function() {
          $("select").select2({
               allowClear: true,
               placeholder: "Select"
          });
     }

     var getCustomers = function() {
          var customers = <?php echo json_encode($customers); ?>;
          var customer_option = "<option value='null' selected disabled>Select</option>";

          $(customers).each(function(c, customer) {
               var fullname = customer['first_name'] + " " + customer['middle_name'] + " " + customer['last_name'];
               var complete_address = customer['address'] + ", " + customer['city_name'] + ", " + customer['province'];
               var city_address = customer['city_name'];
               var id = customer['id'];

               customer_option += "<option value='" + id + "'>" + fullname + " - " + city_address + "</option>";
          });

          $(customer_option).appendTo("#select-customer");
     }

     var getBranches = function() {
          var branches = <?php echo json_encode($branches); ?>;
          var branch_option = "<option value='null' selected disabled>Select</option>";

          $(branches).each(function(b, branch) {
               var name = branch['name'];
               var address = branch['address'];
               var id = branch['id'];

               branch_option += "<option value='" + id + "'>" + id + " - " + name + "</option>";
          });

          $(branch_option).appendTo("#select-branch");
     }

     var getProducts = function() {
          var products = <?php echo json_encode($products); ?>;
          var product_option = "<option value='null' selected disabled>Select</option>";
     }

     var getBrands = function() {
          var brands = <?php echo json_encode($brands); ?>;
          var brand_option = "<option value='null' selected disabled>Select</option>";

          $(brands).each(function(b, brand) {
               var name = brand['name'];
               var id = brand['id'];

               brand_option += "<option value='" + id + "'>" + name + "</option>";
          });

          $(brand_option).appendTo("#select-brand");
     }

     var getTypes = function() {
          var types = <?php echo json_encode($types); ?>;
          var type_option = "<option value='null' selected disabled>Select</option>";

          $(types).each(function(t, type) {
               var name = type['name'];
               var id = type['id'];

               type_option += "<option value='" + id + "'>" + name + "</option>";
          });

          $(type_option).appendTo("#select-type");
     }

     $(function() {
          initSelect2();
          getCustomers();
          getBranches();
          getBrands();
          getTypes();
     });

</script>
<!--
<pre>
     <?php
          print_r($brands)
      ?>
</pre> -->
