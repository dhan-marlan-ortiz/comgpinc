<?php 
     $date = date('Y-m-d');
     $date_due = date('Y-m-d', strtotime($date. ' + 30 days'));
     $branch_fk = $branchInfo['id'];
     $role_fk = $this->session->userdata('role_fk');
?>
<form action="<?php echo base_url('Transaction/create'); ?>" method="post" id="transaction-create-form" autocomplete="off">

     <div id="form-input">
          <div class="row no-gutters">
               <div class="col-12 col-sm-6 align-self-end">
                    <?php
                         transaction_tab("create");
                    ?>
               </div>
               <div class="col-12 col-sm-6 text-right border-bottom-nav align-self-end d-none d-sm-block">
                    <a href="<?php echo base_url('Transaction/create'); ?>" class="btn btn-secondary btn-icon-text btn-sm mb-2 mr-1">
                         RESET<i class="ti-reload btn-icon-append"></i>
                    </a>
                    <button type="button" class="btn btn-primary btn-icon-text text-white btn-sm mb-2 next-btn">
                         NEXT<i class="ti-arrow-right btn-icon-append"></i>
                    </button>
               </div>
          </div>
          <div class="card border-top-0">
               <div class="card-body">
                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-4">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Branch ID <span class="text-danger">*</span></label>
                                   <?php if($role_fk == 'ADMS') { ?>
                                        <select id="select-branch" class="form-control" name="branch" required></select> 
                                   <?php } else { ?>
                                        <input type="text" value="<?php echo $branch_fk; ?>" class="form-control" name="branch" readonly> 
                                   <?php  } ?>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Transaction Date <span class="text-danger">*</span></label>
                                   <input type="text" name="date" id="date" placeholder="MM dd, yyyy" class="form-control text-uppercase" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y", strtotime($date)); ?>"  required>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Customer Name <span class="text-danger">*</span></label>
                                   <label class="float-right d-none">
                                        <small>
                                             <a href="#">New customer</a>
                                        </small>
                                   </label>
                                   <select id="select-customer" class="form-control" name="customer" required></select>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="card-body bg-light">
                    <div class="row narrow-gutters">
                         <div class="col-12">
                              <p class="card-description">
                                   ITEM DETAILS
                              </p>
                         </div>
                         <div class="col-12 col-sm-6 col-md-3">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Item Type <span class="text-danger">*</span></label>
                                   <label class="float-right">
                                        <small>
                                             <a href="#" id="new-type-toggle" class="active">Add new type</a>
                                        </small>
                                   </label>
                                   <select id="select-type" class="form-control" name="type" required></select>
                                   <input id="input-type" type="text" class="form-control d-none" name="type-new" value="" placeholder="Enter new type" disabled required>
                              </div>
                         </div>
                         <div class="col-12 col-sm-6 col-md-3">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Brand Name <span class="text-danger">*</span></label>
                                   <label class="float-right">
                                        <small>
                                             <a href="#" id="new-brand-toggle" class="active">Add new brand</a>
                                        </small>
                                   </label>
                                   <select id="select-brand" class="form-control" name="brand" required></select>
                                   <input id="input-brand" type="text" class="form-control d-none" name="brand-new" value="" placeholder="Enter new brand name" disabled required>
                                   <em class="text-muted">
                                        <sub>Item type is a prerequisite</sub>
                                   </em>
                              </div>
                         </div>
                         <div class="col-12 col-sm-6 col-md-3">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Item Name <span class="text-danger">*</span></label>
                                   <label class="float-right">
                                        <small>
                                             <a href="#" id="new-name-toggle" class="active">Add new name</a>
                                        </small>
                                   </label>
                                   <select id="select-name" class="form-control" name="product" required></select>
                                   <input id="input-name" type="text" class="form-control d-none" name="product-new" value="" placeholder="Enter new name" disabled required>
                                   <em class="text-muted">
                                        <sub>Brand name is a prerequisite</sub>
                                   </em>
                              </div>
                         </div>
                         <div class="col-12 col-sm-6 col-md-3">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Serial Number</label>
                                   <input id="input-serial" type="text" class="form-control" name="serial" value="" placeholder="Serial Number">
                              </div>
                         </div>
                    </div>
               </div>
               <!--
               <div class="card-body">
                    <div class="row narrow-gutters">
                         <div class="col-6">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Time Period <span class="text-danger">*</span></label>
                                   <select id="select-duration" class="form-control" name="duration" required>
                                        <option value="null" disabled selected>Select</option>
                                        <option value="15">15</option>
                                        <option value="30">30</option>
                                   </select>
                              </div>
                         </div>
                    </div>
               </div>
               -->
               <div class="card-body">
                    <div class="row narrow-gutters">
                         <div class="col-12">
                              <p class="card-description">
                                   APPRAISAL
                              </p>
                         </div>
                         <div class="col-12 col-sm-6 col-md-3">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Appraised Value <span class="text-danger">*</span></label>
                                   <input id="input-value" type="text" class="form-control" placeholder="Unit Value" required>
                              </div>
                         </div>
                         <div class="col-12 col-sm-6 col-md-3">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Description</label>
                                   <input id="input-description" type="text" class="form-control" name="description" value="" placeholder="Description">
                              </div>
                         </div>
                         <div class="col-12 col-sm-6 col-md-3">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Appraiser <span class="text-danger">*</span></label>
                                   <select id="select-appraiser" class="form-control" name="appraiser" required></select>
                              </div>
                         </div>
                         <div class="col-12 col-sm-6 col-md-3">
                              <div class="form-group mb-3">
                                   <label class="font-weight-medium">Secondary&nbsp;Appraiser</label>
                                   <select id="select-appraiser2" class="form-control" value="0" name="appraiser2"></select>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
          <div class="row text-right">
               <div class="col">
                    <a href="<?php echo base_url('Transaction/create'); ?>" class="btn btn-secondary btn-icon-text btn-sm mt-2 mr-1">
                         RESET<i class="ti-reload btn-icon-append"></i>
                    </a>
                    <button type="button" class="btn btn-primary btn-icon-text text-white btn-sm mt-2 next-btn">
                         NEXT<i class="ti-arrow-right btn-icon-append"></i>
                    </button>          
               </div>
          </div>
     </div>
     <div id="form-summary" style="display: none;">
          <div class="row no-gutters">
               <div class="col-12 col-sm-6 align-self-end">
                    <?php
                         transaction_tab("create");
                    ?>
               </div>
               <div class="col-12 col-sm-6 text-right border-bottom-nav align-self-end d-none d-sm-block">
                    <button type="button" class="btn btn-secondary btn-sm btn-icon-text mb-2 mr-1 back-btn">
                         BACK<i class="ti-arrow-left btn-icon-append"></i>
                    </button>
                    <button type="button" class="btn btn-primary btn-sm btn-icon-text mb-2 finish-btn">
                         FINISH<i class="ti-check btn-icon-append"></i>
                    </button>
               </div>
          </div>
          <div class="card border-top-0">
               <div class="card-body">
                    <h4 class="card-title text-primary">Transaction Summary</h4>

                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-6">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Branch</label>
                                        <p id="field-branch">
                                             <?php
                                             echo $branchInfo['name'] . " (" . $branchInfo['id'] . ") ";
                                             ?>
                                        </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-6">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Transaction Date</label>
                                        <p id="field-date"><?php echo date("F d, Y", strtotime($date)); ?></p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4 d-none">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Status </label>
                                        <p>
                                             In Contract</mark>
                                        </p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <hr>

                    <!-- CUSTOMER INFORMATION -->
                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-6">
                              <p class="card-description">
                                   CUSTOMER INFORMATION
                              </p>
                         </div>
                         <div class="col-12 col-sm-6 text-right">
                              <small>
                                   <a href="#" data-toggle="collapse" data-target=".more-customer-info">Show more</a>
                              </small>
                         </div>
                    </div>
                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-4 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>First name</label>
                                        <p id="field-first-name"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Middle name</label>
                                        <p id="field-middle-name"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Last name</label>
                                        <p id="field-last-name"> </p>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="row narrow-gutters more-customer-info collapse">
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Complete Address</label>
                                        <p id="field-address"></p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Contact number</label>
                                        <p id="field-contact"></p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Gender</label>
                                        <p id="field-gender"></p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Remarks</label>
                                        <p id="field-remarks"></p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>ID Presented</label>
                                        <p id="field-id-presented"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>ID Number</label>
                                        <p id="field-id-number"> </p>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <!-- CUSTOMER INFORMATION end-->

                    <hr>

                    <!-- ITEM INFORMATION -->
                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-6">
                              <p class="card-description">
                                   ITEM INFORMATION
                              </p>
                         </div>
                    </div>
                    <div class="row narrow-gutters">
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Item Type</label>
                                        <p id="field-type"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Brand Name</label>
                                        <p id="field-brand"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Item Name</label>
                                        <p id="field-item-name"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Serial Number</label>
                                        <p id="field-serial"> </p>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <hr>
                    <div class="row narrow-gutters">
                         <div class="col-12">
                              <p class="card-description">
                                   CONTRACT
                              </p>
                         </div>
                         <div class="col-4 col-sm-6">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Time Period</label>
                                        <p id="field-duration">30 Days</p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-8 col-sm-6">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Expiry Date</label>
                                        <p id="field-expiry"><?php echo date("F d, Y", strtotime($date_due));?></p>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <hr>
                    <div class="row narrow-gutters">
                         <div class="col-12">
                              <p class="card-description">
                                   APPRAISAL
                              </p>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Appraised Value</label>
                                        <p id="field-value"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Transaction Fee</label>
                                        <p id="field-fee"> </p>
                                        <!-- <em class="text-muted">
                                             <sub>10 percent of the appraised value</sub>
                                        </em> -->
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Amount Due</label>
                                        <p id="field-amount"> </p>
                                        <!-- <em class="text-muted">
                                             <sub>Sum of the appraised value and transaction fee</sub>
                                        </em> -->
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Description</label>
                                        <p id="field-description"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Appraiser</label>
                                        <p id="field-appraiser"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block no-wrap">
                                        <label>Secondary Appraiser</label>
                                        <p id="field-appraiser2"> </p>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
          <div class="row text-right">
               <div class="col">
                    <button type="button" class="btn btn-secondary btn-sm btn-icon-text mt-2 mr-1 back-btn">
                         BACK<i class="ti-arrow-left btn-icon-append"></i>
                    </button>
                    <button type="button" class="btn btn-primary btn-sm btn-icon-text mt-2 finish-btn">
                         FINISH<i class="ti-check btn-icon-append"></i>
                    </button>
               </div>
          </div>
     </div>

     <div class="modal fade confirm-modal">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to add new transaction?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
                         <button type="button" class="col btn btn-success confirm" id="confirm-btn">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>    

     <input type="hidden" name="status" value="In Contract">
     <input type="hidden" name="fee" value="">
     <input type="hidden" name="amount" value="">
     <input type="hidden" name="value" value="">
     <input type="hidden" name="duration" value="30">
     <input type="hidden" name="expiration" value="<?php echo $date_due; ?>">
</form>
<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/transaction-create.js'); ?>"></script>

<script type="text/javascript">
     var customersJSON = <?php echo json_encode($customers); ?>;
     var branchesJSON = <?php echo json_encode($branches); ?>;
     var currentBranchJSON = "<?php echo $this->session->userdata['branch_fk']; ?>";
     var typesJSON = <?php echo json_encode($types); ?>;
     var appraisersJSON = <?php echo json_encode($appraiser); ?>;
     var brandsJSON = <?php echo json_encode($brands); ?>;
     var productsJSON = <?php echo json_encode($products); ?>;
</script>

<script type="text/javascript">
     $(function() {
          initSelect2();
          getCustomers(customersJSON);
          getBranches(branchesJSON, currentBranchJSON);
          getTypes(typesJSON, brandsJSON, productsJSON);
          getSerial();
          getDates();
          getUnitValue();
          getAppraiser();
          getAppraiser2();
          getDescription();
          validateForm("#transaction-create-form");
          getCharges();
          getNewType();
          getNewBrand();
          getNewName();
          checkItemDetails();
     });
</script>