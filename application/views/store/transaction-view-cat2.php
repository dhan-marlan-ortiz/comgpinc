<?php
     $status = $transaction['status'];
     $transactionId = $transaction['transaction_id'];
     $purchaseId = $this->Purchase_model->getPurchaseIdFromSales($transactionId);
     if($transaction['category'] == 1) {
          $category = 'Pawn';
     } else if($transaction['category'] == 2) {
          $category = 'Purchase';
     } else if($transaction['category'] == 3) {
          $category = 'Retail';
     } else {
          $category = "";
     }

     // print_r($transaction);
?>

<!-- SOLD PURCHASED ITEM - TRANSACTION -->
<div class="card-body <?php echo ($status == 1 ? 'void' : ($status == 2 ? 'returned' : '') ); ?>">
     <div class="row">
          <div class="col-12 col-sm-6">
               <h4 class="card-title text-primary">INFORMATION</h4>
          </div>
     </div>
     <div class="row narrow-gutters text-nowrap">
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>Location</label>
                         <p> <?php echo $transaction['branch_name'] . " (" . $transaction['branch_fk'] . ")"; ?> </p>
                    </div>
               </div>
          </div>
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>Transaction ID</label>
                         <p><?php echo $transactionId; ?></p>
                    </div>
               </div>
          </div>
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>Status</label>
                         <p> <?php echo $transaction['status']; ?> </p>
                    </div>
               </div>
          </div>
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>Category</label>
                         <p> <?php echo $category; ?> </p>
                    </div>
               </div>
          </div>
     </div>
     <hr>
     <!--
     <div class="row">
          <div class="col-12">
               <div class="table-responsive pt-3">
                    <table class="table text-uppercase">
                         <thead class="border-bottom-dark-2">
                              <tr>
                                   <th>Type</th>
                                   <th>Product</th>
                                   <th>Purchase Reference</th>
                              </tr>
                         </thead>
                         <tbody>
                              <tr>
                                   <td class="lead align-top">
                                        <p class="mb-0 lead">
                                             <?php echo $transaction['type_name']; ?>
                                        </p>
                                   </td>
                                   <td class="text-wrap">
                                        <p class="mb-1 lead">
                                             <?php echo $transaction['brand_name'] . " " . $transaction['product_name']; ?>
                                        </p>
                                        <p class="mb-0 text-muted">
                                             <?php echo $transaction['description']; ?>
                                        </p>
                                   </td>
                                   <td class="lead align-top">
                                        <p class="mb-0 lead">
                                             <?php
                                                  echo "<a href='" . base_url('Purchase?search=') . $purchaseId . "'>" . $purchaseId . "</a>";
                                             ?>
                                        </p>
                                   </td>
                              </tr>
                         </tbody>
                    </table>
               </div>
          </div>
     </div>
      -->

	<div class="row">
		<div class="col-12">
			<p class="font-weight-semibold mb-3 mt-2">
				<i class="ti-package"></i>&emsp;ITEM DETAILS
			</p>
		</div>
	</div>
	<div class="row narrow-gutters">
		<div class="col">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Item Type</label>
					<p>
                              <?php echo $transaction['type_name']; ?>
                         </p>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Brand Name</label>
                         <p>
                              <?php echo $transaction['brand_name']; ?>
                         </p>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Item Name</label>
					<p>
                              <?php echo $transaction['product_name']; ?>
                         </p>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Description</label>
                         <p>
                              <?php echo $transaction['description']; ?>
                         </p>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Purchase Reference</label>
					<p>
                              <?php echo "<a href='" . base_url('Purchase?search=') . $purchaseId . "'>" . $purchaseId . "</a>"; ?>
                         </p>
				</div>
			</div>
		</div>
	</div>
     
</div>
