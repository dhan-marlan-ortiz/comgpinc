<div class="row no-gutters">
     <div class="col-lg-1">
          <h4 class='font-weight-bold mb-3'>Retails</h4>
     </div>     
     <div class="col-lg-11 text-right animated fadeIn delay-1s">
          <div class="d-inline-block mb-2">
               <?php if($role_fk == 'ADMS') { ?>
               <div id="filter-location-wrapper" class="dataTables_filter">
                    <div class="input-group">
                         <div class="input-group-prepend">
                              <span class="input-group-text py-0 border-radius-left-2 ti-location-pin"></span>
                         </div>
                         <div class="input-group-append">
                              <select id="filter-location" class="select2 select2-sm" name="branch" style="height:36px;z-index:1!important;font-size:12px;margin:0;">
                                   <option value=""> ALL </option>
                                   <?php
                                   foreach ($branches as $fb) {
                                        echo "<option value='" . $fb['name'] . "'>" . $fb['name'] . " (" . $fb['id'] . ")</option>";
                                   } ?>
                              </select>
                         </div>
                    </div>                    
               </div>
               <?php }   ?>
          </div>
          <div class="d-inline-block mb-2 ml-2" id="table-length"></div>
          <div class="d-inline-block mb-2 ml-2 text-uppercase" id="table-filter"></div>
     </div>
</div>

<div class="row no-gutters">
     <div class="col-12">
          <?php retail_tab('inventory') ?>
     </div>     
</div>

<div class="row">
     <div class="col-12 col-sm-12">
          <div class="card border-top-0">
               <div class="card-body pt-4">
                    <div class="table-responsive pb-0">
                         <table class="table animated fadeIn" id="product-table">
                              <thead>
                                   <tr class="text-uppercase">
                                        <th class="text-center no-sort bg-img-none">#</th>
                                        <th class="text-left">Type</th>
                                        <th class="text-left">Product</th>
                                        <th class="text-center">Stocks</th>
                                        <th class="text-left no-sort">Unit</th>
                                        <?php if($role_fk == "ADMS") {
                                             echo "<th class='text-left'>Location</th>";
                                        } ?>
                                        <th class="no-sort"></th>
                                   </tr>
                              </thead>
                                   <?php
                                        foreach ($retails as $r => $retail) {
                                             $quantity = $retail['quantity'];
                                             $buy_price = $retail['buy'];
                                             $total_buy_price = $buy_price * $quantity;
                                             $product = $retail['brand'] . " " . $retail['name'];
                                             $type = $retail['type'];
                                             $registration_id = $retail['registration_id'];
                                             $branch = $retail['branch'];
                                             $fullname = $retail['first_name'] . " " . $retail['last_name'];
                                             $unit = $retail['unit'];
                                             $pk = $retail['pk'];
                                             $stock = $retail['stock']; ?>
                                             <tr>
                                                  <td class="text-center"> <?php echo ++$r; ?> </td>
                                                  <td> <?php echo $type; ?> </td>
                                                  <td>
                                                       <?php
                                                            echo "<span class='font-weight-medium'>" . $product . "</span>";
                                                       ?>
                                                  </td>
                                                  <td class="text-center"> <?php echo number_format($stock); ?> </td>
                                                  <td> <?php echo $unit; ?> </td>

                                                  <?php if($role_fk == "ADMS"){
                                                       echo "<td>" . $branch . "</td>";
                                                  } ?>

                                                  <td class="text-center" style='width:110px;'>
                                                            <?php if($stock > 0 ) { ?>
                                                                 <a href="#"class="transfer-btn text-decoration-none d-block font-11"alt="Transfer"tabindex="0"data-toggle="popover"data-placement="top"data-content="Transfer"data-type="<?php echo $retail['type']; ?>"data-type-fk="<?php echo $retail['type_fk']; ?>"data-brand="<?php echo $retail['brand']; ?>"data-brand-fk="<?php echo $retail['brand_fk']; ?>"data-name="<?php echo $retail['name']; ?>"data-name-fk="<?php echo $retail['name_fk']; ?>"data-unit="<?php echo $retail['unit']; ?>"data-unit-fk="<?php echo $retail['unit_fk']; ?>"data-buy-price="<?php echo $retail['buy']; ?>"data-stock="<?php echo $stock; ?>"> 
                                                                      TRANSFER 
                                                                 </a>
                                                            <?php } else { ?>
                                                                 <div class="text-muted d-block font-11" href="#" disabled>
                                                                      <i>NOT AVAILABLE</i>
                                                                 </div>
                                                            <?php } ?>
                                                  </td>
                                             </tr>
                                        <?php
                                        }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>

<form class="d-inline needs-validation" action="<?php echo base_url("Retail/createShipping"); ?>" method="post" novalidate autocomplete="off">
     <input id="transfer-type-fk" name="transfer_type_fk" type="hidden">
     <input id="transfer-brand-fk" name="transfer_brand_fk" type="hidden">
     <input id="transfer-name-fk" name="transfer_name_fk" type="hidden">
     <div class="modal fade form-modal" id="transfer-modal" tabindex="-1">
          <div class="modal-dialog modal-lg">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-uppercase text-info">
                              Transfer Item
                         </h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="row">
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label>Date</label>
                                        <input type="text" name="transfer_date" class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date('F d, Y'); ?>" required>
                                   </div>
                                   <div class="form-group">
                                        <label>Origin</label>
                                        <?php if($this->session->role_fk == "ADMS"){ ?>
                                             <select class="form-control" name="transfer_origin" required>
                                                  <?php
                                                       foreach ($branches as $branch) {
                                                            echo "<option value='" . $branch['id'] . "'  " . ($branch['id'] == $this->session->userdata('branch_fk') ? 'selected' : '') . "> " .
                                                                      $branch['name'] . " (" . $branch['id'] . ")
                                                                 </option>";
                                                       }
                                                  ?>
                                             </select>
                                        <?php } else { ?>
                                             <select class="form-control" name="transfer_origin" required>
                                                  <?php
                                                       foreach ($branches as $branch) {
                                                            if($branch['id'] == $this->session->userdata('branch_fk'))
                                                            echo "<option value='" . $branch['id'] . "'> " . $branch['name'] . " (" . $branch['id'] . ") </option>";
                                                       }
                                                  ?>
                                             </select>

                                        <?php } ?>
                                   </div>
                                   <div class="form-group">
                                        <label>Destination</label>
                                        <?php if($this->session->role_fk == "ADMS"){ ?>
                                             <select class="form-control" name="transfer_destination" required>
                                                  <option value="" selected disabled>SELECT</option>
                                                  <?php
                                                       foreach ($branches as $branch) {
                                                            echo "<option value='" . $branch['id'] . "' > " .
                                                                      $branch['name'] . " (" . $branch['id'] . ")
                                                                 </option>";
                                                       }
                                                  ?>
                                             </select>
                                        <?php } else { ?>
                                             <select class="form-control" name="transfer_destination" required>
                                                  <option value="" selected disabled>SELECT</option>
                                                  <?php
                                                       foreach ($branches as $branch) {
                                                            if($branch['id'] !== $this->session->userdata('branch_fk'))
                                                            echo "<option value='" . $branch['id'] . "'> " . $branch['name'] . " (" . $branch['id'] . ") </option>";
                                                       }
                                                  ?>
                                             </select>
                                        <?php } ?>
                                   </div>
                              </div>
                              <div class="col-md-4 border-left-md border-right-md">
                                   <div class="form-group">
                                        <label>Type</label>
                                        <input id="tansfer-type" type="text" class="form-control text-uppercase" disabled>
                                   </div>
                                   <div class="form-group">
                                        <label>Brand</label>
                                        <input id="transfer-brand" type="text" class="form-control text-uppercase" disabled>
                                   </div>
                                   <div class="form-group">
                                        <label>Name</label>
                                        <input id="transfer-name" type="text" class="form-control text-uppercase" disabled>
                                   </div>
                              </div>
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label>Quantity</label>
                                        <div class="row narrow-gutters">
                                             <div class="col">
                                                  <input id="input-quantity" name="transfer_quantity" type="number" min="1" step="1" class="form-control h-auto" placeholder="Quantity" required>
                                             </div>
                                             <div class="col">
                                                  <span id="transfer-unit" class="input-group-text" style="padding-left:12px;">UNIT</span>
                                                  <input id="transfer-unit-fk" name="transfer_unit_fk" type="hidden">
                                             </div>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <div class="row narrow-gutters">
                                             <div class="col">
                                                  <label>Buy Price</label>
                                                  <input id="input-buy" type="text" class="form-control" name="transfer_buy_price" placeholder="Buy Price" required maxlength="11" >
                                             </div>
                                             <div class="col">
                                                  <label>Total Price</label>
                                                  <input id="total-buy" type="text" class="form-control h-auto" value="&#8369; 0.00" tabindex="-1" readonly>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label>Remarks / Reason of transfer</label>
                                        <input type="text" class="form-control" name="transfer_remarks" placeholder="Remarks / Reason of transfer" maxlength="255">
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal"> CANCEL </button>
                         <button type="button" class="btn btn-info" data-dismiss="modal" data-toggle="modal" data-target="#transfer-confirm" > TRANSFER </button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="transfer-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to transfer the item?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#transfer-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>

<style>
     #product-table_filter input,
     #filter-location {
          margin-right: 0;
     }
     
     #product-table_info,
     #product-table_paginate {
          margin-top: 13px;     
     }
     
</style>

<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/retail.js'); ?>"></script>

<script type="text/javascript">

</script>
<script type="text/javascript">
     $(document).ready( function() {
          initDataTable();
          initSelect2();
          initDatePicker();
          validateForm2();
          getTotalPrices();
          $('#input-buy').number(true, 2);
          $(".transfer-btn").on("click", function() {
               var type = $(this).attr("data-type");
               var brand = $(this).attr("data-brand");
               var name = $(this).attr("data-name");

               var type_fk = $(this).attr("data-type-fk");
               var brand_fk = $(this).attr("data-brand-fk");
               var name_fk = $(this).attr("data-name-fk");

               var unit = $(this).attr("data-unit");
               var unit_fk = $(this).attr("data-unit-fk");
               var stock = $(this).attr("data-stock");
               var buy_price = $.number($(this).attr("data-buy-price"), 2);

               $("#tansfer-type").val(type);
               $("#transfer-brand").val(brand);
               $("#transfer-name").val(name);
               $("#transfer-unit").text(unit);
               $("#transfer-type-fk").val(type_fk);
               $("#transfer-brand-fk").val(brand_fk);
               $("#transfer-name-fk").val(name_fk);
               $("#transfer-unit-fk").val(unit_fk);
               $("#input-quantity").attr("max", stock);
               $("#input-buy").attr("value", buy_price);

               $("#transfer-modal").modal("show");
          });
          
          $("#product-table_length").appendTo("#table-length");
          $("#product-table_filter").appendTo("#table-filter");
     });
</script>
