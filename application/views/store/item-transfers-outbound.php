<?php
     $role_fk = $this->session->userdata('role_fk');
     transfer_tab('outbound');
?>
<input type="hidden" name="" value="<?php echo base_url(); ?>" id="base-url">
<div class="row">
     <div class="col-12 col-sm-12 grid-margin stretch-card">
          <div class="card border-top-0" id="transaction-records">
               <div class="card-body">
                    <div class="table-responsive pb-0">
                         <table id="transaction-table" class="table">
                              <thead class="text-uppercase">
                                   <tr>
                                        <th class="no-sort">#</th>
                                        <th>Date</th>
                                        <th>Reference   ID</th>
                                        <th>Category</th>
                                        <th>Item Type</th>
                                        <th>Item Name</th>
                                        <th class="no-sort text-center">Qty</th>
                                        <th class="no-sort">Remarks</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th class="no-sort"></th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                        if($transfers) {
                                             foreach ($transfers as $t => $transfer) {
                                                  $category_name = "";
                                                  $quantity = 1;
                                                  if($transfer['transactions_category']) {
                                                       $category_name = "Pawned Item";
                                                  } else if($transfer['purchases_category']) {
                                                       $category_name = "Purchased Item";
                                                  }

                                                  echo "<tr>";
                                                  echo      "<td class='text-uppercase'> </td>";
                                                  echo      "<td class='text-uppercase'>" . date("d M Y", strtotime($transfer['date_created'])) . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $transfer['transaction_id'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $category_name . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $transfer['transactions_type_name'] . $transfer['purchases_type_name'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" .
                                                                 $transfer['transactions_brand_name'] . " " . $transfer['transactions_item_name'] .
                                                                 $transfer['purchases_brand_name'] . " " . $transfer['purchases_item_name'] .
                                                            "</td>";

                                                  echo      "<td class='text-uppercase text-center'>" . $quantity. "</td>";
                                                  echo      "<td class='text-uppercase'>" . $transfer['remarks'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $transfer['origin_name'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $transfer['destination_name'] . "</td>";

                                                  if($role_fk == 'ADMS') {
                                                  echo      "<td class='text-center'>
                                                                 <a href='" . base_url('Transaction?search=') . $transfer['transaction_id'] . "' class='btn btn-danger btn-sm text-decoration-none'>
                                                                      <small>VIEW TRANSACTION</small>
                                                                 </a>
                                                            </td>";
                                                  }

                                                  echo "</tr>";
                                             }
                                        }

                                        if($shipments) {
                                             foreach ($shipments as $s => $shipment) {
                                                  echo "<tr>";
                                                  echo      "<td class='text-uppercase'></td>";
                                                  echo      "<td class='text-uppercase'>" . date("d M Y", strtotime($shipment['date'])) . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['transaction_id'] . "</td>";
                                                  echo      "<td class='text-uppercase'>Retail Item</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['type'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['brand'] . " " . $shipment['name'] . " (" . $shipment['unit'] . ")</td>";
                                                  echo      "<td class='text-uppercase text-center'>" . $shipment['quantity'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['remarks'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['origin_name'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['destination_name'] . $shipment['retail_id'] . "</td>";
                                                  
                                                  if($role_fk == 'ADMS') {
                                                       if(isset($shipment['retail_id'])) {
                                                            echo '<td class="text-center">
                                                                      <a href="' . base_url('Retail/voidReturnTransfer/') . $shipment['id'] . '/' . $shipment['retail_id'] . '" class="btn btn-danger btn-sm text-decoration-none text-small">
                                                                           <small>VOID TRANSFER</small>
                                                                      </a>
                                                                 </td>';
                                                       } else {
                                                            echo "<td class='text-center'>
                                                                      <a href='" . base_url('Retail/removeOutbound/') . $shipment['transaction_id'] . "' class='btn btn-danger btn-sm text-decoration-none text-small'>
                                                                           <small>VOID TRANSFER</small>
                                                                      </a>
                                                                 </td>";
                                                       }
                                                  }
                                                  echo "</tr>";
                                             }
                                        }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
<div class="modal fade confirm-modal" id="remove-shipment-modal">
     <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
               <div class="modal-header">
                    <h5 class="modal-title">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal">
                         <span aria-hidden="true">&times;</span>
                    </button>
               </div>
               <div class="modal-body">
                    <p class="mb-0">Are you sure want to remove? Action can't be undone. </p>
               </div>
               <div class="modal-footer">
                    <a href="#" class="col btn btn-secondary" data-dismiss="modal">CANCEL</a>
                    <a id="confirm-remove-shipment" href="" data-href="<?php echo base_url('Retail/removeOutbound/'); ?>" class="col btn btn-danger">CONFIRM</a>
               </div>
          </div>
     </div>
</div>
<script type="text/javascript">
     $(function() {
          var table = $('.table').DataTable({
               "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 'no-sort'
               }],
               "order": [
                    [1, 'asc']
               ]
          });

          table.on('order.dt search.dt', function() {
               table.column(0, {
                    search: 'applied',
                    order: 'applied'
               }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
               });
          }).draw();

          /*
          $('#remove-shipment-modal').on('show.bs.modal', function (event) {
               var button = $(event.relatedTarget);
               var dataid = button.data('id');
               var modal = $(this);
               var href = $("#confirm-remove-shipment").attr('data-href');
               modal.find('#confirm-remove-shipment').attr('href', href + dataid);
          })
          */

          $("#transaction-table_filter").appendTo("#table-toolbar");
          $("#transaction-table_length").appendTo("#table-toolbar");
     });
</script>
