<div class="row">
     <div class="col-md-12 mb-3">
          <div class="d-flex justify-content-between align-items-center">
               <div>
                    <h4 class="font-weight-bold mb-0"><?php echo $title; ?></h4>
               </div>
               <div>
                    <a class="btn btn-primary btn-icon-text btn-rounded" href="<?php echo base_url('Transaction/create'); ?>">
                         <small class="ti-plus btn-icon-prepend"></small>Create
                    </a>
               </div>
          </div>
     </div>
</div>
<div class="row">
     <div class="col-12 col-sm-12 grid-margin stretch-card">
          <div class="card" id="transaction-records">
               <div class="card-body">
                    <table id="my-table" class="table table-hover table-striped">
                         <thead >
                              <tr>
                                   <th>
                                        <input type="search" placeholder="Search" class="form-control column-search" />
                                   </th>
                                   <th>
                                        <input type="search" placeholder="Search" class="form-control column-search" />
                                   </th>
                                   <th>
                                        <div class="input-group input-daterange">
                                             <input type="text" id="min-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="From:">
                                             <input type="text" id="max-date" class="form-control date-range-filter" data-date-format="yyyy-mm-dd" placeholder="To:">
                                        </div>
                                   </th>
                              </tr>
                              <tr>
                                   <th>ID</th>
                                   <th>Name</th>
                                   <th>Created At</th>
                              </tr>
                         </thead>
                         <tbody>
                              <tr>
                                   <th>1</th>
                                   <th>Bob</th>
                                   <th>2019-04-20</th>
                              </tr>
                              <tr>
                                   <th>2</th>
                                   <th>Jane</th>
                                   <th>2019-02-15</th>
                              </tr>
                              <tr>
                                   <th>3</th>
                                   <th>Jill</th>
                                   <th>2019-03-15</th>
                              </tr>
                              <tr>
                                   <th>4</th>
                                   <th>Joe</th>
                                   <th>2019-01-06</th>
                              </tr>
                         </tbody>
                    </table>
               </div>

               <div class="card-body">
                    <h4 class="card-title text-primary">Transaction Records</h4>
               </div>
               <div class="card-body bg-light py-0">
                    <div class="row mt-4">
                         <div class="col-6 col-sm-6">
                              <h4 class="card-title text-dark"><i class='ti-filter'></i> Filters</h4>
                         </div>
                         <div class="col-6 col-sm-6 text-right">
                              <small> <a href="#" data-toggle="collapse" data-target="#filters">Hide filter options</a> </small>
                         </div>
                    </div>

                    <div class="row narrow-gutters collapse show" id="filters">
                         <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-3">
                              <div class="form-group mb-4">
                                   <label>Branch</label>
                                   <select id="select-branch" class="form-control" name="branch"></select>
                              </div>
                         </div>
                         <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-3">
                              <div class="form-group mb-4">
                                   <label>Customer Name/Number</label>
                                   <select id="select-customer" class="form-control" name="customer"></select>
                              </div>
                         </div>
                         <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-2">
                              <div class="form-group mb-4">
                                   <label>Transaction Number</label>
                                   <input id="search-transaction" type="search" class="form-control" placeholder="Search">
                              </div>
                         </div>
                         <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                              <div class="form-group mb-4 date-range">
                                   <label>Date Range</label>
                                   <input type="date" name="" placeholder="From" class="date-range-from form-control" id="min">
                                   <input type="date" name="" placeholder="To" class="date-range-to form-control" id="max">
                              </div>
                         </div>
                    </div>
               </div>
               <div class="card-body">
                    <div class="col-12 col-sm-4">
                         <div class="form-group">
                              <div class="text-field d-block">
                                   <label>Branch</label>
                                   <p id="selected-branch"> </p>
                              </div>
                         </div>
                    </div>
                    <div class="col-12">
                         <div class="form-group">
                              <div class="text-field d-block">
                                   <label></label>
                                   <ul id="field-transactions"> </ul>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
          <div class="card d-none" id="transaction-details">
               <div class="card-body">
                    <h4 class="card-title text-primary">Transaction Details</h4>

                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-none">
                                        <label>Branch</label>
                                        <p id="field-branch"> </p>
                                   </div>
                                   <div class="text-field d-block">
                                        <label>Transaction ID</label>
                                        <p id="field-transactionID"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Transaction Date</label>
                                        <p id="field-date"></p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Status
                                        </label>
                                        <p id="field-status">
                                             In Contract
                                        </p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <hr>

                    <div class="row">
                         <div class="col-12 col-sm-6">
                              <p class="card-description">
                                   CUSTOMER INFORMATION
                              </p>
                         </div>
                         <div class="col-12 col-sm-6 text-right">
                              <small>
                                   <a href="#" data-toggle="collapse" data-target=".more-customer-info">Show more</a>
                              </small>
                         </div>
                    </div>

                    <div class="row">
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7">
                              <div class="form-group row narrow-gutters mb-0">
                                   <div class="col-12 col-sm-12 col-md-4 text-field d-block">
                                        <label>First name</label>
                                        <p id="field-first-name"> </p>
                                   </div>
                                   <div class="col-12 col-sm-12 col-md-4 text-field d-block">
                                        <label>Middle name</label>
                                        <p id="field-middle-name"> </p>
                                   </div>
                                   <div class="col-12 col-sm-12 col-md-4 text-field d-block">
                                        <label>Last name</label>
                                        <p id="field-last-name"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5">
                              <div class="form-group row narrow-gutters mb-0">
                                   <div class="col-6">
                                        <div class="text-field d-block">
                                             <label>Contact number</label>
                                             <p id="field-contact"></p>
                                        </div>
                                   </div>
                                   <div class="col-6">
                                        <div class="text-field d-block">
                                             <label>Gender</label>
                                             <p id="field-gender"></p>
                                        </div>

                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row more-customer-info collapse">
                         <div class="col-12">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Complete Address</label>
                                        <p id="field-address"></p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row more-customer-info collapse">
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7">
                              <div class="form-group row narrow-gutters mb-0">
                                   <div class="col-6 text-field d-block">
                                        <label>ID Presented</label>
                                        <p id="field-id-presented"> </p>
                                   </div>
                                   <div class="col-6 text-field d-block">
                                        <label>ID Number</label>
                                        <p id="field-id-number"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Remarks</label>
                                        <p id="field-remarks"></p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <hr>

                    <div class="row">
                         <div class="col-12 col-sm-6">
                              <p class="card-description">
                                   ITEM INFORMATION
                              </p>
                         </div>
                    </div>

                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Item Type</label>
                                        <p id="field-type"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Brand Name</label>
                                        <p id="field-brand"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Serial Number</label>
                                        <p id="field-serial"> </p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row narrow-gutters">
                         <div class="col-4 col-sm-6">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Duration</label>
                                        <p id="field-duration"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-8 col-sm-6">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Expiry Date</label>
                                        <p id="field-expiry"> </p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Unit Value</label>
                                        <p id="field-value"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Transaction Fee</label>
                                        <p id="field-fee"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Amount Due</label>
                                        <p id="field-amount"> </p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Description</label>
                                        <p id="field-description"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Appraiser</label>
                                        <p id="field-appraiser"> </p>
                                   </div>
                              </div>
                         </div>
                         <div class="col-12 col-sm-12 col-md-4">
                              <div class="form-group mb-0">
                                   <div class="text-field d-block">
                                        <label>Secondary Appraiser</label>
                                        <p id="field-appraiser2"> </p>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div class="row narrow-gutters mt-3">
                         <div class="col-12">
                              <button type="button" class="btn btn-primary close-btn mr-3">Update</button>
                              <button type="button" class="btn btn-light close-btn">Close</button>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<script type="text/javascript">
     var transactions = <?php echo json_encode($transactions); ?>;
     var branchCode = "<?php echo $this->session->userdata('branch_fk'); ?>";
     var branchTransactions = "<?php echo $branchTransactions ?>";
</script>
<script type="text/javascript">

     var initDataTable = function() {
          // Bootstrap datepicker
          $('.input-daterange input').each(function() {
               $(this).datepicker('clearDates');
          });

          // Set up your table
          var table = $('#my-table').DataTable();

          $('#my-table .column-search').on( 'keyup',function (e) {
              table.column( $(this).parent().index() ).search( this.value ).draw();
          });


          // Extend dataTables search
          $.fn.dataTable.ext.search.push(
               function(settings, data, dataIndex) {
                    var min = $('#min-date').val();
                    var max = $('#max-date').val();
                    var createdAt = data[2] || 0; // Our date column in the table

                    if (
                         (min == "" || max == "") ||
                         (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max))
                    ) {
                         return true;
                    }
                    return false;
               }
          );

          // Re-draw the table when the a date range filter changes
          $('.date-range-filter').change(function() {
               table.draw();
          });
     }


     var initSelect2 = function() {
          $("select").select2({
               allowClear: true,
               placeholder: "Select"
          });
     }

     var getCustomers = function() {
          var customers = <?php echo json_encode($customers); ?>;
          var customer_option = "<option value='null' selected disabled>Select</option>";

          $(customers).each(function(c, customer) {
               var fullname = customer['first_name'] + " " + customer['middle_name'] + " " + customer['last_name'];
               var complete_address = customer['address'] + ", " + customer['city_name'] + ", " + customer['province'];
               var city_address = customer['city_name'];
               var id = customer['id'];
               var contact = customer['contact'];

               customer_option += "<option value='" + id + "'>" + fullname + " - " + contact + "</option>";
          });

          $(customer_option).appendTo("#select-customer");

          $("#select-customer").on("change", function() {
               var value = $(this).val();
               $.map(customers, function(cust) {
                    if (cust.id === value) {
                         $("#field-first-name").text(cust.first_name);
                         $("#field-middle-name").text(cust.middle_name);
                         $("#field-last-name").text(cust.last_name);
                         $("#field-contact").text(cust.contact);
                         $("#field-gender").text(cust.gender);
                         $("#field-id-presented").text(cust.identification_type);
                         $("#field-id-number").text(cust.identification_number);
                         $("#field-remarks").text(cust.remarks);
                         $("#field-address").text(cust.address + ", " + cust.city_name + ", " + cust.province);
                    }
               });
          });

          $('.more-customer-info').on('shown.bs.collapse', function() {
               $("a[data-target='.more-customer-info']").text("Show less");
          })

          $('.more-customer-info').on('hidden.bs.collapse', function() {
               $("a[data-target='.more-customer-info']").text("Show more");
          })
     }

     var getBranches = function() {
          var branches = <?php echo json_encode($branches); ?>;
          var branch_option = "<option value='ALL' selected>ALL BRANCH</option>";

          $(branches).each(function(b, branch) {
               var name = branch['name'];
               var address = branch['address'];
               var id = branch['id'];

               branch_option += "<option value='" + id + "' data-value='" + name + "'>" + id + " - " + name + "</option>";
          });

          $(branch_option).appendTo("#select-branch");
     }

     var formatDate = function(dateToFormat) {
          const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
          date = new Date(dateToFormat);

          return months[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear();
     }

     var getDailyTransactions = function(branch_fk) {
          var dates_html = "";
          var target = $("#field-transactions");

          $.map(transactions, function(transaction, branch_id) {
               if (branch_id == branch_fk) {
                    target.empty();
                    $("#selected-branch").text(branch_id);
                    $(transaction).each(function(ed, eachDate) {
                         var details = eachDate.details;
                         var dates = eachDate.dates;
                         var details_html = "<ul>";
                         $(details).each(function(d, details) {
                              var transaction_id = details['transaction_id'];
                              var customer = details['first_name'] + " " + details['middle_name'] + " " + details['last_name'];
                              details_html += "<li>" +
                                   "<a href='#' " +
                                   "data-id='" + transaction_id +
                                   "'data-branch='" + details['branch_fk'] +
                                   "'data-date='" + details['date'] +
                                   "'data-status='" + details['status'] +
                                   "'data-firstname='" + details['first_name'] +
                                   "'data-middlename='" + details['middle_name'] +
                                   "'data-lastname='" + details['last_name'] +
                                   "'data-contact='" + details['contact'] +
                                   "'data-gender='" + details['gender'] +
                                   "'data-type='" + details['type_name'] +
                                   "'data-brand='" + details['brand_name'] +
                                   "'data-serial='" + details['serial'] +
                                   "'data-duration='" + details['duration'] +
                                   "'data-expiration='" + details['expiration'] +
                                   "'data-value='" + details['value'] +
                                   "'data-fee='" + details['fee'] +
                                   "'data-amount='" + details['amount'] +
                                   "'data-description='" + details['description'] +
                                   "'data-appraiser='" + details['appraiser'] +
                                   "'data-appraiser2='" + details['appraiser2'] +
                                   "'class='transaction-id'>" + transaction_id +
                                   "</a> - " + customer +
                                   "</li>";
                         });
                         details_html += "</ul>"
                         dates_html += "<li>" + formatDate(dates) + details_html + "</li>";
                    });
               }
          });
          $(dates_html).appendTo(target);

          viewTransaction();
          closeTransaction();
     }

     var getBranchTransactions = function() {
          $("#select-branch").on("change", function() {
               var value = $(this).val();
          });
          getDailyTransactions(branchCode);
     }

     var getAllTransactions = function() {

     }

     var generateDataTable = function() {
          var table = $('#daily-transactions').DataTable();
     }

     var viewTransaction = function() {
          var trigger = ".transaction-id";
          var recordsContainer = $("#transaction-records");
          var detailsContainer = $("#transaction-details");
          $(trigger).on("click", function(e) {
               e.preventDefault();
               recordsContainer.addClass('d-none');
               detailsContainer.removeClass('d-none');

               var d = $(this);

               $("#field-branch").text(d.attr('data-branch'));
               $("#field-transactionID").text(d.attr('data-id'));
               $("#field-date").text(d.attr('data-date'));
               $("#field-status").text(d.attr('data-status'));
               $("#field-first-name").text(d.attr('data-firstname'));
               $("#field-middle-name").text(d.attr('data-middlename'));
               $("#field-last-name").text(d.attr('data-lastname'));
               $("#field-contact").text(d.attr('data-contact'));
               $("#field-gender").text(d.attr('data-gender'));
               $("#field-type").text(d.attr('data-type'));
               $("#field-brand").text(d.attr('data-brand'));
               $("#field-serial").text(d.attr('data-serial'));
               $("#field-duration").text(d.attr('data-duration'));
               $("#field-expiry").text(d.attr('data-expiration'));
               $("#field-value").text(d.attr('data-value'));
               $("#field-fee").text(d.attr('data-fee'));
               $("#field-amount").text(d.attr('data-amount'));
               $("#field-description").text(d.attr('data-description'));
               $("#field-appraiser").text(d.attr('data-appraiser'));
               $("#field-appraiser2").text(d.attr('data-appraiser2'));
          });
     }

     var closeTransaction = function() {
          var trigger = "#transaction-details .close-btn";
          var recordsContainer = $("#transaction-records");
          var detailsContainer = $("#transaction-details");
          $(trigger).on("click", function(e) {
               e.preventDefault();
               recordsContainer.removeClass('d-none');
               detailsContainer.addClass('d-none');
          });
     }

     var toggleFilters = function() {
          $('#filters').on('shown.bs.collapse', function() {
               $("a[data-target='#filters']").text("Hide filter options");
          })

          $('#filters').on('hidden.bs.collapse', function() {
               $("a[data-target='#filters']").text("Show filter options");
          })
     }

     $(function() {
          initSelect2();
          initDataTable();
          getCustomers();
          getBranches();
          getBranchTransactions();
          // generateDataTable();
          toggleFilters();
          getAllTransactions();
     });
</script>
<pre>
<?php
     print_r($branchTransactions);
?>
</pre>
