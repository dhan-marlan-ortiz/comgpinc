<div class="row">
     <div class="col-md-12 mb-3">
          <div class="d-flex justify-content-between align-items-center">
               <div>
                    <h4 class="font-weight-bold mb-0"><?php echo $title; ?></h4>
               </div>
               <div>
                    <a class="btn btn-primary btn-icon-text btn-rounded" href="<?php echo base_url('Purchase'); ?>">
                         <small class="ti-agenda btn-icon-prepend"></small>Records
                    </a>
               </div>
          </div>
     </div>
</div>
<div class="row">
     <div class="col-12 col-sm-12 col-md-4 col-lg-5 col-xl-4 grid-margin stretch-card">
          <div class="card">
               <div class="card-body">
                    <h4 class="card-title text-primary">Add Product</h4>
                    <div class="form-group d-none">
                         <label>Select Branch <span class="text-danger">*</span></label>
                         <select class="form-control" name="branch" id="branch-select" required>
                              <option value="" selected disabled>Select Branch</option>
                         </select>
                    </div>
                    <form method="post" action="<?php echo base_url('Purchase'); ?>" id="purchase-create-form">
                         <div class="row narrow-gutters">
                             <!--  <div class="col-12 col-sm-12">
                                   <div class="form-group mb-0 font-weight-semibold">
                                        <label>Add Product</label>
                                   </div>
                              </div> -->
                              <div class="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-6">
                                   <div class="form-group">
                                        <label>Item Type <span class="text-danger">*</span></label>
                                        <select class="form-control" name="type" id="product-type-select" required>
                                             <option value="" selected disabled>Item type</option>
                                        </select>
                                   </div>
                              </div>
                              <div class="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-6">
                                   <div class="form-group">
                                        <label>Brand Name <span class="text-danger">*</span></label>
                                        <select class="form-control" name="brand" id="product-brand-select" disabled required>
                                             <option value="" selected disabled>Brand Name</option>
                                        </select>
                                   </div>
                              </div>
                         </div>
                         <div class="form-group mb-3">
                              <label>Product Name <span class="text-danger">*</span></label>
                              <select class="form-control" name="productName" id="product-name-select" disabled required>
                                   <option value="" selected disabled>Product Name</option>
                              </select>
                         </div>
                         <div class="row narrow-gutters mb-3">
                              <div class="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-6">
                                   <script type="text/javascript">
                                        $(function() {
                                             $('input[name="quantity"]').number(true);
                                        });
                                   </script>
                                   <div class="form-group">
                                        <label>Quantity <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="quantity" value="" placeholder="Quantity" required>
                                   </div>
                              </div>
                              <div class="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-6">
                                   <script type="text/javascript">
                                        $(function() {
                                             $('input[name="amount"]').number(true, 2);
                                        });
                                   </script>
                                   <div class="form-group">
                                        <label>Amount <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="amount" value="" placeholder="Amount" required>
                                   </div>
                              </div>
                         </div>
                         <div class="row">
                              <div class="row-12 col-sm-6">
                                   <button type="submit" class="btn btn-primary btn-block mb-3" id="purchase-add-item">Add to List</button>
                              </div>
                              <div class="row-12 col-sm-6">
                                   <button type="button" class="btn btn-light btn-block mb-3" id="purchase-clear-form">Clear Fields</button>
                              </div>
                         </div>
                    </form>
               </div>
          </div>
     </div>
     <div class="col-12 col-sm-12 col-md-8 col-lg-7 col-xl-8 grid-margin stretch-card">
          <div class="card">
               <form class="" action="<?php echo base_url('purchase/create'); ?>" method="post">
                    <div class="card-body">
                         <h4 class="card-title text-primary">Purchase Summary</h4>
                         <div class="row narrow-gutters">
                              <div class="col-12 col-sm-6">
                                   <div class="form-group mb-0">
                                        <label>Date:&emsp;
                                             <span class="text-muted font-weight-semibold"><?php echo date("F d, Y"); ?></span>
                                        </label>
                                   </div>
                                   <div class="form-group mb-0 summary-branch d-none">
                                        <label>Branch:&emsp;<span class="text-muted font-weight-semibold"></span>
                                        </label>
                                        <input type="hidden" name="branch_fk" value="URD">
                                   </div>
                              </div>
                              <div class="col-12 col-sm-6 d-none">
                                   <div class="form-group mb-0">
                                        <label>Transaction No:&emsp;
                                             <em class="text-muted">System&nbsp;Generated</em>
                                        </label>
                                   </div>
                                   <div class="form-group mb-0">
                                        <label>User ID:&emsp;
                                             <span class="text-muted font-weight-semibold"><?php echo $this->session->userdata('id'); ?></span>
                                        </label>
                                   </div>
                              </div>
                         </div>
                         <table class="table" id="purchase-summary-table">
                              <thead>
                                   <tr>
                                        <th>Item No.</th>
                                        <th>Particulars</th>
                                        <th>Quantity</th>
                                        <th>Amount</th>
                                        <th>Total</th>
                                   </tr>
                              </thead>
                              <tbody>
                              </tbody>
                              <tfoot>
                                   <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                   </tr>
                              </tfoot>
                         </table>
                         <button type="submit" class="btn btn-primary mb-3 mr-2 d-none" id="purchase-submit-form" name="submit">Submit</button>
                    </div>
               </form>
          </div>
     </div>
     <div class="col-12 grid-margin stretch-card d-none">
          <div class="card">
               <div class="card-body">
                    <a href="<?php echo base_url('purchase'); ?>" class="btn btn-light mb-3" id="purchase-cancel">Cancel</a>
               </div>
          </div>
     </div>
</div>

<script type="text/javascript">
     $(document).ready(function() {
          $("select").select2({
               allowClear: true
          });
          var names = <?php echo json_encode($products); ?>;
          var brands = <?php echo json_encode($brands); ?>;
          var types = <?php echo json_encode($types); ?>;

          var select_type = "#product-type-select";
          var select_brand = "#product-brand-select";
          var select_name = "#product-name-select";

          var branches = <?php echo json_encode($branches); ?>;
          var select_branch = "#branch-select";

          $(branches).each(function(key, value) {
               $(select_branch).append('<option value="' + value['id'] + "%" + value['name'] + '">' + value['id'] + ' - ' + value['name'] + ' - ' + value['address'] + '</option>');
          });
          $(types).each(function(key, value) {
               $(select_type).append('<option value=' + value['id'] + '>' + value['name'] + '</option>');
          });
          $(select_type).on("change", function() {
               $(select_brand).empty();
               var id = $(this).val();
               $(brands).each(function() {
                    var brand = $(this)[0]
                    if (brand['type_fk'] == id) {
                         $(select_brand).append('<option value=' + brand['id'] + '>' + brand['name'] + '</option>');
                    }
               });
               $(select_brand).prepend('<option value="" disabled selected>Brand Name</option>');
               $(select_brand).removeAttr('disabled');
          });
          $(select_brand).on("change", function() {
               $(select_name).empty();
               var id = $(this).val();
               $(names).each(function() {
                    var name = $(this)[0];
                    if (name['brand_fk'] == id) {
                         $(select_name).append('<option value=' + name['id'] + '>' + name['name'] + '</option>');
                    }
               });
               $(select_name).prepend('<option value="" disabled selected>Product Name</option>');
               $(select_name).removeAttr('disabled');
          });
          $(select_branch).on("change", function() {
               var value = '';
               try{
                    value = $(this).val().split("%");
               }catch(e) {}
               var branch_id = value[0];
               var branch_name = value[1];
               $(".summary-branch label > span").text(branch_name);
               $("input[name='branch_fk']").attr('value', branch_id);
          });
          $("#purchase-clear-form").on("click", function() {
               clearPurchaseForm();
          });
          function getTotalAmount() {
               var table = "#purchase-summary-table";
               var body = table + " tbody tr td:nth-child(7)";
               var foot = table + " tfoot tr td:nth-child(7)";
               var total_amount = 0;
               $(body).each(function() {
                    total_amount += parseFloat($(this).text());
               });
               return total_amount;
          }
          function getTotalQuantity() {
               var table = "#purchase-summary-table";
               var body = table + " tbody tr td:nth-child(6)";
               var foot = table + " tfoot tr td:nth-child(6)";
               var total_quantity = 0;
               $(body).each(function() {
                    total_quantity += parseFloat($(this).text());
               });
               return total_quantity;
          }
          function removeRow() {
               $(".remove-row").on("click", function(event) {
                    event.preventDefault();
                    $(this).closest('tr').remove();
                    var totalQuantity = $("#purchase-summary-table tfoot tr th:nth-child(3)").text($.number(getTotalQuantity()));
                    var totalAmount = $("#purchase-summary-table tfoot tr th:nth-child(5)").text($.number(getTotalAmount(), 2));
               });
          }
          function clearPurchaseForm() {
               // $("#branch-select").val('').trigger('change');
               $("#product-type-select").val('').trigger('change');
               $("#product-brand-select").val('').trigger('change');
               $("#product-name-select").val('').trigger('change');
               $("#product-brand-select").attr('disabled', 'disabled');
               $("#product-name-select").attr('disabled', 'disabled');
               $("#purchase-create-form")[0].reset();
          }

          $("#purchase-create-form").validate({
               rules: {
                    productName: {
                         required: true
                    }
               },
               errorPlacement: function(error, element) {
                    parent_container = element.parent("div");
                    error_wrapper = "<div class='error-wrapper'></div>";
                    $(error_wrapper).appendTo(element.parent("div"));
                    error.appendTo(element.siblings(".error-wrapper"));
               },
               submitHandler: function(form, event) {
                    event.preventDefault();
                    var summary_table = "#purchase-summary-table";
                    var summary_body = "#purchase-summary-table tbody";
                    var id = $(form).find('select[name="productName"]').val();
                    var quantity = $(form).find('input[name="quantity"]').val();
                    var amount = $(form).find('input[name="amount"]').val();
                    var type_id = $(form).find('select[name="type"]').val();
                    var type = "";
                    var brand_id = $(form).find('select[name="brand"]').val();
                    var brand = "";
                    var row = "<tr>";
                    $(brands).each(function() {
                         var b = $(this)[0]
                         if (b['id'] == brand_id) {
                              brand = b['name'];
                         }
                    });
                    $(types).each(function() {
                         var t = $(this)[0]
                         if (t['id'] == type_id) {
                              type = t['name'];
                         }
                    });
                    $(names).each(function() {
                         var name = $(this)[0];
                         if (name['id'] == id) {
                              row += "<td>" + name['sku'] + "<input type='hidden' name='product_fk[]' value='" + name['id'] + "'/></td>";
                              row += "<td>" +
                                   "<p>" + name['name'] + "</p>" +
                                   "<p><small>" + name['description'] + "</small></p>" +
                                   "<p><small>" + "<span>Item Type: </span>" + type + "<span> &emsp; | &emsp; Brand Name: </span>" + brand + "</small></p>" +
                                   "</td>";
                              row += "<td>" + $.number(quantity) + "</td>";
                              row += "<td>" + $.number(amount, 2) + "</td>";
                              row += "<td>" + $.number(quantity * amount, 2) + "<button type='button' class='btn btn-danger btn-icon btn-rounded btn-sm remove-row'><i class='ti-close'></i></button></td>";
                              row += "<td>" + quantity + "<input type='hidden' name='quantity[]' value='" + name['quantity'] + "'/></td>";
                              row += "<td>" + quantity * amount + "<input type='hidden' name='amount[]' value='" + name['amount'] + "'/></td>";
                         }
                    });
                    row += "</tr>";
                    $(row).appendTo(summary_body);
                    var totalQuantity = $("#purchase-summary-table tfoot tr th:nth-child(3)").text($.number(getTotalQuantity()));
                    var totalAmount = $("#purchase-summary-table tfoot tr th:nth-child(5)").text($.number(getTotalAmount(), 2));
                    clearPurchaseForm();
                    removeRow();

                    $("#purchase-submit-form").removeClass('d-none');
               }
          });
     });
</script>
