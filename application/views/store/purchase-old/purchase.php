<div class="row">
     <div class="col-md-12 mb-3">
          <div class="d-flex justify-content-between align-items-center">
               <div>
                    <h4 class="font-weight-bold mb-0"><?php echo $title; ?></h4>
               </div>
               <div>
                    <a class="btn btn-primary btn-icon-text btn-rounded" href="<?php echo base_url('Purchase/create'); ?>">
                         <small class="ti-plus btn-icon-prepend"></small>Create
                    </a>
               </div>
          </div>
     </div>
</div>
<div class="row">
     <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
               <div class="card-body">
                    <div class="row">
                         <div class="col-12 col-sm-6">
                              <h4 class="card-title text-primary">Purchase Records</h4>
                         </div>
                         <div class="col-12 col-sm-6 text-right">
                              <p class="card-description">
                                   <i class="ti-location-pin"></i>
                                   <?php
                                        echo $branchInfo['name'] . " (" . $branchInfo['id'] . ")";
                                   ?>
                              </p>
                         </div>
                    </div>                    
                    <div class="table-responsive">
                         <table class="table" id="purchase-table">
                              <thead>
                                   <tr>
                                        <th>Purchase ID</th>
                                        <th>Date</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                   if(null !== $purchases){
                                        foreach ($purchases as $p => $purchase) {
                                             $date = new DateTime($purchase['date']);
                                             echo "
                                             <tr>
                                             <td><a href='".base_url('Purchase?search=').$purchase['transaction_id']."'>".$purchase['transaction_id']."</a></td>
                                             <td>".$date->format("F d, Y")."</td>
                                             </tr>
                                             ";
                                        }
                                   }
                                   ?>
                              </tbody>
                              <tfoot>
                                   <tr>
                                        <th>Transaction ID</th>
                                        <th>Date</th>
                                   </tr>
                              </tfoot>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>

<script type="text/javascript">
     $(document).ready(function () {
     // Setup - add a text input to each footer cell
     $('#purchase-table tfoot th').each(function () {
          var title = $(this).text();
          $(this).html('<input type="text" placeholder="Search ' + title + '" />');
     });

     // DataTable
     var table = $('#purchase-table').DataTable();

     // Apply the search
     table.columns().every(function () {
          var that = this;

          $('input', this.footer()).on('keyup change clear', function () {
               if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
               }
          });
     });
});
</script>
