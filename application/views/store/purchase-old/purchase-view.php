<div class="row">
     <div class="col-12 col-sm-12 grid-margin stretch-card">
          <div class="card">
               <div class="card-body">
                    <h4 class="card-title text-primary">Purchase Summary</h4>
                    <div class="row narrow-gutters">
                         <div class="col-12 col-sm-6">
                              <div class="form-group mb-0">
                                   <label>Date:&emsp;
                                        <span class="text-muted font-weight-semibold">
                                             <?php 
                                                  $date = new DateTime($transaction[0]['date']);
                                                  echo $date->format("F d, Y")
                                             ?>
                                        </span>
                                   </label>
                              </div>
                         </div>
                         <div class="col-12 col-sm-6">
                              <div class="form-group mb-0">
                                   <label>ID:&emsp;
                                        <span class="text-muted font-weight-semibold">
                                             <?php echo $transaction[0]['transaction_id']; ?>
                                        </span>
                                   </label>                                        
                              </div>
                         </div>
                    </div>
                    <table class="table" id="purchase-summary-table">
                         <thead>
                              <tr>
                                   <th>Item No.</th>
                                   <th>Particulars</th>
                                   <th>Quantity</th>
                                   <th>Amount</th>
                                   <th>Total</th>
                              </tr>
                         </thead>
                         <tbody>
                              <?php 
                                   $grand_total = 0;
                                   $total_qty = 0;
                                   foreach ($transaction as $i => $item) {
                                        $quantity = $item['quantity'];
                                        $amount = $item['amount'];
                                        $total = $quantity * $amount;
                                        $grand_total += $total;
                                        $total_qty += $quantity;
                                        echo "<tr>";
                                        echo "    <td>".$item['sku']."</td>";                                             
                                        echo "    <td>";
                                        echo "         <p>".$item['name']."</p>";
                                        echo "         <p><small>".$item['description']."</small></p>";
                                        echo "    <td>".number_format($quantity, 2)."</td>";
                                        echo "    <td>".number_format($amount, 2)."</td>";
                                        echo "    <td>".number_format($total, 2)."</td>";
                                        echo "</tr>";
                                   }
                              ?>
                         </tbody>
                         <tfoot>
                              <tr>
                                   <th></th>
                                   <th></th>
                                   <th><?php echo number_format($total_qty, 2); ?></th>
                                   <th></th>
                                   <th><?php echo number_format($grand_total, 2); ?></th>                                                                               
                              </tr>
                         </tfoot>
                    </table>
               </div>
          </div>
     </div>
     <div class="col-12 grid-margin stretch-card d-none">
          <div class="card">
               <div class="card-body">
                    <a href="<?php echo base_url('purchase'); ?>" class="btn btn-light mb-3" id="purchase-cancel">Cancel</a>
               </div>
          </div>
     </div>
</div>

<div class="row">
     <div class="col-12">
          <a href="<?php echo base_url('Purchase'); ?>" class="btn btn-primary mr-3">
               Back to Purchase Records
          </a>
          <a href="<?php echo base_url('Purchase/create'); ?>" class="btn btn-primary">
               Add New Purchase
          </a>
     </div>     
</div>
