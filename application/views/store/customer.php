<style>
     .paging_simple_numbers {
          padding-top: 20px !important;
     }
     .dataTables_info {
          padding-top: 30px !important;
     }

     @media (min-width: 576px) {
          #new-form-buttons {
               position: absolute;
               z-index: 1;
               right: 0;
               top: -45px;     
          }     
     }
     @media (max-width: 575px) {
          #new-form-buttons {
               padding: 0 16px;
               margin-top: -15px;
               margin-bottom: 20px;
          }
          #new-form-buttons a,
          #new-form-buttons button {
               width: 48%;
          }
     }
</style>

<div class="row no-gutters">
     <div class="col-lg-1">
          <h4 class='font-weight-bold mb-3'>
               <?php echo $title;?>
          </h4>
     </div>  
     <div class="col-lg-11">
          <div id="filter-wrapper" style="display: none;">
               <div class="d-inline-block float-right ml-2 mb-2" id="filter-length-wrapper">
               </div>  
               <div class="d-inline-block float-right ml-2 mb-2" id="filter-search-wrapper">
               </div>  
          </div>  
     </div>  
</div>

<ul class="nav nav-tabs">
     <li class="nav-item">
          <a href="#records" class="nav-link <?php echo ($tab == 'records' ? 'active': '') ?>" data-toggle="tab" role="tab">
               Records
          </a>
     </li>
     <li class="nav-item">
          <a href="#new" class="nav-link <?php echo ($tab == 'new' ? 'active': '') ?>" data-toggle="tab" role="tab">
               New Customer
          </a>
     </li>
</ul>

<div class="tab-content" id="myTabContent">
     <div class="tab-pane <?php echo ($tab == 'records' ? 'active': '') ?>" id="records" role="tabpanel">
          <!-- Customers table -->
          <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
          <div class="card border-top-0" id="customer-list-tab" style="display: none">
               <div class="card-body pt-4">          
                    <div class="table-responsive pb-1">
                         <table class="table text-uppercase" id="customer-table">
                              <thead>
                                   <tr> 
                                        <th class="no-sort bg-img-none">#</th>
                                        <th class="text-left">First Name</th>
                                        <th class="text-left">Middle Name</th>
                                        <th class="text-left">Last Name</th>
                                        <th class="text-left">Contact</th>
                                        <th class="text-left">Address</th>
                                        <th class="text-left">City</th>
                                        <th class="text-left">Province</th>
                                        <th class='no-sort'></th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                   foreach ($customers as $customer) {
                                        echo "<tr>
                                                  <td></td>
                                                  <td>" . $customer['first_name'] . "</td>
                                                  <td>" . $customer['middle_name'] . "</td>
                                                  <td>" . $customer['last_name'] . "</td>
                                                  <td>" . $customer['contact'] . "</td>
                                                  <td class='text-wrap'>" . $customer['address'] . "</td>
                                                  <td>" . $customer['city_name'] . "</td>
                                                  <td>" . $customer['province'] . "</td>
                                                  <td class='text-center'>
                                                       <a class='btn btn-primary btn-sm font-11' href='" . base_url('Customer/profile/') . $customer['id'] . "'>
                                                            VIEW
                                                       </a>
                                                  </td>
                                             </tr>";
                                   }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
          <!-- Customers table end -->
     </div>
     
     <div class="tab-pane <?php echo ($tab == 'new' ? 'active': '') ?>" id="new" role="tabpanel">
          <!-- Create customer form -->
          <form class="needs-validation" method="post" action="<?php echo base_url('Customer/create'); ?>" id="customer-create-form" novalidate autocomple="off">
               <div class="card border-top-0" id="customer-create-tab">
                    <div class="card-body">
                         <div class="row narrow-gutters">
                              <div class="col-sm-3">
                                   <div class="form-group">
                                        <label>First Name <span class="text-danger">*</span></label>
                                        <input class="form-control" placeholder="First Name" type="text" name="firstname" required>
                                   </div>
                              </div>
                              <div class="col-6 col-sm-3">
                                   <div class="form-group">
                                        <label>Middle Name</label>
                                        <input class="form-control" placeholder="Middle Name" type="text" name="middlename">
                                   </div>
                              </div>
                              <div class="col-6 col-sm-3">
                                   <div class="form-group">
                                        <label>Last Name <span class="text-danger">*</span></label>
                                        <input class="form-control" placeholder="Last Name" type="text" name="lastname" required>
                                   </div>
                              </div>
                              <div class="col-6 col-sm-3">
                                   <div class="form-group selectize-group">
                                        <label>Gender <span class="text-danger">*</span></label>
                                        <select class="form-control" id="customer-create-select-gender" name="gender" placeholder="Select Gender" required>
                                             <option value="" selected="selected">SELECT GENDER</option>
                                             <option value="male">MALE</option>
                                             <option value="female">FEMALE</option>
                                        </select>
                                   </div>
                              </div>
                         
                              <div class="col-6 col-sm-3">
                                   <div class="form-group">
                                        <label>Contact Number <span class="text-danger">*</span></label>
                                        <input class="form-control" type='number' placeholder="Contact Number" maxlength="11" step="1" name='contact' required>
                                   </div>
                              </div>
                              <div class="col-sm-6">
                                   <div class="form-group">
                                        <label>Address <span class="text-danger">*</span></label>
                                        <input class="form-control" type='text' placeholder="Address" name='address' required>
                                   </div>
                              </div>
                              <div class="col-sm-3">
                                   <div class="form-group selectize-group">
                                        <label>City & Province <span class="text-danger">*</span></label>
                                        <select class="form-control text-uppercase" id="customer-create-select-city" name="city" placeholder="Type or Select City" required>
                                             <option value="" selected="selected">SELECT CITY & PROVINCE</option>
                                             <?php
                                             if(null !== $cities) {
                                                  foreach ($cities as $city):
                                                       echo "<option value='".$city['id']."'>". strtoupper($city['name']) ." - ". strtoupper($city['province']) . "</option>";
                                                  endforeach;
                                             }else {
                                                  echo "<option value='' disabled>Result not found</option>";
                                             }
                                             ?>
                                        </select>
                                   </div>
                              </div>
                         
                              <div class="col-6 col-sm-3">
                                   <div class="form-group">
                                        <label>ID Presented <span class="text-danger">*</span></label>
                                        <input class="form-control" type='text' name='id_type' placeholder="ID Presented" required>
                                   </div>
                              </div>
                              <div class="col-6 col-sm-3">
                                   <div class="form-group">
                                        <label>ID Number <span class="text-danger">*</span></label>
                                        <input class="form-control" type='text' name='id_number' placeholder="ID Number" required>
                                   </div>
                              </div>
                              <div class="col-sm-6">
                                   <div class="form-group">
                                        <label>Remarks</label>
                                        <input class="form-control" type='text' name='remarks' placeholder="Remarks">
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div id="new-form-buttons">
                         <a href="<?php echo base_url('Customer?tab=new'); ?>" class="btn btn-light btn-icon-text btn-sm mr-1 border">
                              CLEAR <i class="ti-eraser btn-icon-append"></i>
                         </a>
                         <button type="button" data-toggle="modal" data-target="#new-customer-modal" class="btn btn-primary btn-icon-text btn-sm" name="add">
                              SAVE <i class="ti-save btn-icon-append"></i>
                         </button>
                    </div>
               </div>
               <div class="modal fade confirm-modal" id="new-customer-modal">
                    <div class="modal-dialog modal-sm" role="document">
                         <div class="modal-content">
                              <div class="modal-header">
                                   <h5 class="modal-title">Confirmation</h5>
                                   <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">&times;</span>
                                   </button>
                              </div>
                              <div class="modal-body">
                                   <p class="mb-0">Are you sure want to add new customer?</p>
                              </div>
                              <div class="modal-footer">
                                   <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
                                   <button type="submit" class="col btn btn-success confirm" name="create" value="create">CONFIRM</button>
                              </div>
                         </div>
                    </div>
               </div>
          </form>
          <!-- Create customer form end -->
     </div>
</div>

<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/customer.js'); ?>"></script>
<script type="text/javascript"> var tab = '<?php echo $tab; ?>'; </script>
<script type="text/javascript"> var base_url = '<?php echo base_url(); ?>'; </script>

<script type="text/javascript">
     $(document).ready(function() {
          initSelect2("form select");
          initDataTable(tab);
          updateUrl(base_url);
          formValidation();
          // initDataTable("#customer-table");
          // clickRow("#customer-table");
          // validateForm("#customer-create-form");
          
     });
</script>

<script type="text/javascript">
     $(window).on("load", function() {
          loader();
     });
</script>
