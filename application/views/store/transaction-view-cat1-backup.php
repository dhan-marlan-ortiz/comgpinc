<!-- PAWN TRANSACTIONS -->
<?php
$status = $transaction['status'];
$today = date_create(date("Y-m-d"));
$expiration = date_create($transaction['expiration']);
$overdue = intval(date_diff($expiration, $today)->format("%R%a"));
$days_left = intval(date_diff($expiration, $today)->format("%a"));
$role_fk = $this->session->userdata('role_fk');
$penalty = 0;

$appraisal_value = $transaction['value'];
$amount_due = $transaction['amount'];

if($overdue > 0) {
	$penalty = ($appraisal_value * 0.01) * $overdue;

	if($days_left == 1) {
		$days_left = "<em class='text-danger'>-&nbsp;" . $days_left . "&nbsp;Day</em>";
	} else {
		$days_left = "<em class='text-danger'>-&nbsp;" . $days_left . "&nbsp;Days</em>";
	}

}else {
	if($days_left == 1) {
		$days_left = "<em class='text-success'>+&nbsp;" . $days_left . "&nbsp;Day</em>";
	} else {
		$days_left = "<em class='text-success'>+&nbsp;" . $days_left . "&nbsp;Days</em>";
	}
}
?>
<!-- BASIC INFORMATION -->
<div class="card-body <?php echo ($status == 'Void' ? 'void' : ''); ?>">
	<div class="row">
		<div class="col-12 col-sm-6">
			<h4 class="card-title text-primary">INFORMATION</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7">
			<div class="form-group row narrow-gutters mb-0">
				<div class="col-12 col-sm-12 col-md-4 text-field d-block">
					<label>Location</label>
					<p id="field-branch">
						<?php echo $transaction['branch_name'] . " (" . $transaction['branch_fk'] . ")"; ?>
					</p>
				</div>
				<div class="col-12 col-sm-12 col-md-4 text-field d-block">
					<label>Transaction ID</label>
					<p id="field-transactionID">
						<?php echo $transaction['transaction_id']; ?>
					</p>
				</div>
				<div class="col-12 col-sm-12 col-md-4 text-field d-block">
					<label>Transaction Date</label>
					<p id="field-date">
						<?php
							$date = date_create($transaction['date']);
							echo date_format($date,"F d, Y");
						?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5">
			<div class="form-group row narrow-gutters mb-0">
				<div class="col-6">
					<div class="text-field d-block">
						<label>Status</label>
						<p id="field-status">
							<?php echo $status; ?>
						</p>
					</div>
				</div>
				<div class="col-6">
					<div class="text-field d-block">
						<label>Last Update</label>
						<p id="field-updated">
							<?php
								$updated_at = date_create($transaction['updated_at']);
								echo date_format($updated_at,"F d, Y");
							?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- BASIC INFORMATION end -->

<!-- CUSTOMER PROFILE -->
<div class="card-body bg-light">
	<div class="row">
		<div class="col-12 col-sm-6">
			<p class="font-weight-semibold mb-3 mt-2">
				<i class="ti-user"></i>&emsp;CUSTOMER PROFILE
			</p>
		</div>
		<div class="col-12 col-sm-6 text-right">
			<a class="text-decoration-none float-right" data-toggle="collapse" href=".more-customer-info" role="button" aria-expanded="true" aria-controls="more-customer-info">
				<i class="ti-more icon-sm"></i>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7">
			<div class="form-group row narrow-gutters mb-0">
				<div class="col-12 col-sm-12 col-md-4 text-field d-block">
					<label>First name</label>
					<p id="field-first-name">
						<?php echo $transaction['fname']; ?>
					</p>
				</div>
				<div class="col-12 col-sm-12 col-md-4 text-field d-block">
					<label>Middle name</label>
					<p id="field-middle-name">
						<?php echo $transaction['mname']; ?>
					</p>
				</div>
				<div class="col-12 col-sm-12 col-md-4 text-field d-block">
					<label>Last name</label>
					<p id="field-last-name">
						<?php echo $transaction['lname']; ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5">
			<div class="form-group row narrow-gutters mb-0">
				<div class="col-6">
					<div class="text-field d-block">
						<label>Contact number</label>
						<p id="field-contact">
							<?php echo $transaction['contact']; ?>
						</p>
					</div>
				</div>
				<div class="col-6">
					<div class="text-field d-block">
						<label>Gender</label>
						<p id="field-gender">
							<?php echo $transaction['gender']; ?>
						</p>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="collapse more-customer-info">
		<div class="row">
			<div class="col-12">
				<div class="form-group mb-0">
					<div class="text-field d-block">
						<label>Complete Address</label>
						<p id="field-address">
							<?php echo $transaction['address']; ?>,
							<?php echo $transaction['city']; ?>,
							<?php echo $transaction['province']; ?>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7">
				<div class="form-group row narrow-gutters mb-0">
					<div class="col-6 text-field d-block">
						<label>ID Presented</label>
						<p id="field-id-presented">
							<?php echo $transaction['idtype']; ?>
						</p>
					</div>
					<div class="col-6 text-field d-block">
						<label>ID Number</label>
						<p id="field-id-number">
							<?php echo $transaction['idnum']; ?>
						</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5">
				<div class="form-group mb-0">
					<div class="text-field d-block">
						<label>Remarks</label>
						<p id="field-remarks">
							<?php echo $transaction['remarks']; ?>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<a href="<?php echo base_url("Customer/profile/").$transaction['customer_fk']; ?>" class="btn btn-secondary btn-icon-text mt-4 mb-2 text-white">
					Profile Page<i class="ti-user btn-icon-append"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<!-- CUSTOMER PROFILE end-->

<!-- ITEM INFORMATION -->
<div class="card-body">
	<div class="row">
		<div class="col-12">
			<p class="font-weight-semibold mb-3 mt-2">
				<i class="ti-package"></i>&emsp;ITEM DETAILS
				<a class="text-decoration-none float-right" data-toggle="collapse" href="#item-info-wrapper" role="button" aria-expanded="true" aria-controls="item-info-wrapper">
					<i class="ti-more icon-sm"></i>
				</a>
			</p>
		</div>
	</div>
	<div class="row narrow-gutters collapse show" id="item-info-wrapper">
		<div class="col-12 col-sm-12 col-md-4 col-xl-3">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Item Type</label>
					<p id="field-type">
						<?php echo $transaction['type_name']; ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-4 col-xl-3">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Brand Name</label>
					<p id="field-brand">
						<?php echo $transaction['brand_name']; ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-4 col-xl-3">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Item Name</label>
					<p id="field-product">
						<?php echo $transaction['product_name']; ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-4 col-xl-3">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Description</label>
					<p id="field-description">
						<?php echo $transaction['description']; ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-6 col-sm-4 col-xl-3">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Serial Number</label>
					<p id="field-serial">
						<?php echo $transaction['serial']; ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-6 col-sm-4 col-xl-3 <?php echo ($status != "Sold" && $status != "Repurchased" & $status != "Disposed" ? "" : "d-none") ?>">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label class="w-100">Due Date
						<span class='text-uppercase font-weight-normal float-right'>
							<?php echo $days_left; ?>
						</span>
					</label>
					<p id="field-expiry">
						<?php echo date("F d, Y", strtotime($transaction['expiration'])); ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-4 col-sm-12 col-md-4 col-xl-2">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label class="d-block"> Appraisal Value </label>
					<?php if($role_fk == "ADMS" ) { ?>
						<a href="#" class="float-right mt-2 mr-1" title="Update appraisal value" data-toggle="modal" data-target="#update-appraisal-value">
							<i class="ti-pencil-alt"></i>
						</a>
					<?php } ?>
					
						<p id="field-value" class="pr-4">
							&#8369; <?php echo number_format($appraisal_value, 2); ?>
						</p>					
					
				</div>
			</div>
		</div>
		<div class="col-4 col-sm-12 col-md-4 col-xl-2 <?php echo ($status != "Sold" && $status != "Repurchased" & $status != "Disposed" ? "" : "d-none") ?>">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Transaction Fee</label>
					<p id="field-fee">
						&#8369; <?php echo number_format($transaction['fee'], 2); ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-4 col-sm-12 col-md-4 col-xl-2 <?php echo ($status != "Sold" && $status != "Repurchased" & $status != "Disposed" ? "" : "d-none") ?>   ">
			<div class="form-group mb-0">
				<div class="text-field d-block">
					<label>Amount Due</label>
					<p id="field-amount">
						&#8369; <?php echo number_format($amount_due, 2); ?>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ITEM INFORMATION end -->

<!-- APPRAISERS -->
<div class="card-body bg-light">
	<div class="row">
		<div class="col-12">
			<p class="font-weight-semibold mb-3 mt-2">
				<i class="ti-stamp"></i>&emsp;APPRAISERS
				<a class="text-decoration-none float-right" data-toggle="collapse" href="#appraiser-wrapper" role="button" aria-expanded="true" aria-controls="appraiser-wrapper">
					<i class="ti-more icon-sm"></i>
				</a>
			</p>
		</div>
	</div>
	<div class="row narrow-gutters collapse show" id="appraiser-wrapper">
	<div class="col-12 col-sm-12 col-md-4">
		<div class="form-group mb-0">
			<div class="text-field d-block">
				<label>Primary Appraiser</label>
				<p id="field-appraiser">
					<?php echo $transaction['appraiser']; ?>
				</p>
			</div>
		</div>
	</div>
	<div class="col-12 col-sm-12 col-md-4">
		<div class="form-group mb-0">
			<div class="text-field d-block">
				<label>Secondary Appraiser</label>
				<p id="field-appraiser2">
					<?php echo $transaction['appraiser2']; ?>
				</p>
			</div>
		</div>
	</div>
</div>
</div>
<!-- APPRAISERS end -->

<div class="card-body d-block d-md-none">
	<div class="row">
		<div class="col-12">
			<div id="form-buttons">
			<!-- BUTTONS -->
			<?php if($status != "Void") { ?>
				<!-- VOID -->
				<?php if(!isset($payments[0]) && !$sales && $this->session->userdata('role_fk') == 'ADMS') { ?>
					<button type="button" class="btn btn-dark btn-sm text-uppercase btn-icon-text mb-2 ml-1" data-toggle="modal" data-target="#void-transaction">
						Void<i class="ti-trash btn-icon-append"></i>
					</button>
				<?php } ?>
				<!-- VOID end -->

				<!-- UDPATE STATUS -->
				<?php if($status != "Sold" && $status != "Repurchased") { ?>
					<button type="button" class="btn btn-primary btn-sm text-uppercase btn-icon-text mb-2 ml-1" id="update-status-btn">
						Status<i class="ti-bookmark btn-icon-append"></i>
					</button>
				<?php } ?>
				<!-- UDPATE STATUS END -->

				<!-- SELL -->
				<?php if($status == "For Sale") { ?>
					<button type="button" class="btn btn-danger btn-sm text-uppercase btn-icon-text mb-2 ml-1" id="sell-item-btn" data-toggle="modal" data-target="#sell-item-modal">
						Sell Item<i class="ti-shopping-cart btn-icon-append"></i>
					</button>
				<?php } ?>
				<!-- SELL END -->

				<?php if($status == "In Contract" || $status == "Grace Period" || $status == "Due" || $status == "For Sale" || $status == "Expired" || $status == "Freeze") { ?>
					<!-- RENEW -->
					<button type="button" class="btn btn-success btn-sm text-uppercase btn-icon-text mb-2 ml-1" id="" data-toggle="modal" data-target="#renew-modal">
						Renew<i class="ti-layers btn-icon-append"></i>
					</button>
					<!-- RENEW END -->

					<!-- REPURCHASE -->
					<button type="button" class="btn btn-warning btn-sm text-uppercase btn-icon-text mb-2 ml-1 text-white" id="" data-toggle="modal" data-target="#repurchase-modal">
						Repurchase<i class="ti-check-box btn-icon-append"></i>
					</button>
					<!-- REPURCHASE END -->
				<?php } ?>				

				<!-- TRANSFER -->
				<?php if($transaction['status'] == "Expired" || $transaction['status'] == "For Sale" || $transaction['status'] == "For Repair" || $transaction['status'] == "For Disposal" || $transaction['status'] == "Disposed") { ?>
					<button type="button" class="btn btn-dribbble btn-sm text-uppercase btn-icon-text mb-2 ml-1 text-white" id="" data-toggle="modal" data-target="#transfer-modal">
						Transfer<i class="ti-truck btn-icon-append"></i>
					</button>
				<?php } ?>
				<!-- TRANSFER END -->
				
				<!-- VOID TRANSFER -->
				<?php if($transaction['status'] == "For Transfer" && ($transaction['branch_fk'] == $this->session->userdata('branch_fk') || $this->session->userdata('role_fk') == "ADMS") ) { ?>
					<button type="button" class="btn btn-dribbble btn-sm text-uppercase btn-icon-text mb-2 ml-1" id="" data-toggle="modal" data-target="#void-transfer-modal">
						Void Transfer<i class="ti-truck btn-icon-append"></i>
					</button>
				<?php } ?>
				<!-- VOID TRANSFER END -->
			<?php } ?>
			<!-- BUTTONS end -->
			</div>
		</div>
	</div>
</div>

<!-- VOID -->
<form class="d-inline needs-validation" action="<?php echo base_url('Transaction/voidTransaction');?>" method="post" novalidate>
	<div class="modal fade confirm-modal" id="void-transaction">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Confirmation</h5>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="mb-0">Are you sure want to void transaction?</p>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
					<button type="button" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
					<button type="submit" class="col btn btn-danger confirm">CONFIRM</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- VOID END -->

<!-- UPDATE -->
<form class="d-inline needs-validation" action="<?php echo base_url('Transaction/update'); ?>" method="post" autocomplete="off" novalidate>
	<div class="modal fade form-modal" id="status-modal" tabindex="-1">
		<div class="modal-dialog modal-sm">
			<input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-uppercase text-primary">Update Status</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-12">
							<div class="form-group mb-0">
								<div class="form-check form-check-primary">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="transaction_status" value="Freeze" <?php if($transaction['status'] == "Freeze") echo "checked"; ?> required>
										Freeze
										<i class="input-helper"></i>
										<small class="text-muted mt-1 d-none">
											These are items that are on hold with the seller's confirmation of repurchasing or renewing contract
										</small>
									</label>
								</div>
								<div class="form-check form-check-primary">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="transaction_status" value="For Sale" <?php if($transaction['status'] == "For Sale") echo "checked"; ?> required>
										For Sale
										<i class="input-helper"></i>
										<small class="text-muted mt-1 d-none">
											These are items that has expired contract and purchased items that are on sale
										</small>
									</label>
								</div>
							</div>
							<?php if($transaction['status'] == "Expired" || $transaction['status'] == "For Transfer" || $transaction['status'] == "For Sale" || $transaction['status'] == "For Repair" || $transaction['status'] == "For Disposal" || $transaction['status'] == "Disposed") { ?>
								<div class="form-group mb-0">
									<div class="form-check form-check-primary">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="transaction_status" value="For Repair" <?php if($transaction['status'] == "For Repair") echo "checked"; ?> required>
											For Repair
											<i class="input-helper"></i>
											<small class="text-muted mt-1 d-none">
												These are items that are for repair
											</small>
										</label>
									</div>
									<div class="form-check form-check-primary">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="transaction_status" value="For Disposal" <?php if($transaction['status'] == "For Disposal") echo "checked"; ?> required>
											For Disposal
											<i class="input-helper"></i>
											<small class="text-muted mt-1 d-none">
												These are items that cannon be sold due to item's condition
											</small>
										</label>
									</div>
									<div class="form-check form-check-primary">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="transaction_status" value="Disposed" <?php if($transaction['status'] == "Disposed") echo "checked"; ?> required>
											Disposed
											<i class="input-helper"></i>
											<small class="text-muted mt-1 d-none">
												These are items that are disposed
											</small>
										</label>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="col btn btn-secondary" data-dismiss="modal">CANCEL</button>
					<button type="button" class="col btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#confirm-status">UPDATE</button>
				</div>
			</div>

		</div>
	</div>
	<div class="modal fade confirm-modal" id="confirm-status">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Confirmation</h5>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="mb-0">Are you sure want to update transaction?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#status-modal">CANCEL</button>
					<input type="submit" class="col btn btn-success confirm" name="update_status" value="CONFIRM" />
				</div>
			</div>
		</div>
	</div>
</form>
<!-- UPDATE END -->

<!-- SELL -->
<form class="d-inline needs-validation" action="<?php echo base_url('Transaction/createSales'); ?>" method="post" autocomplete="off" novalidate>
	<div class="modal fade form-modal" id="sell-item-modal" tabindex="-1">
		<div class="modal-dialog modal-sm">
			<input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-uppercase">
						<?php echo $transaction['brand_name'] . " " . $transaction['product_name'] . " " . $transaction['description']; ?>
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label> Date </label>
						<input type="text" name="date_sold" id="date_sold"  class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y"); ?>"  required>
					</div>
					<div class="form-group  mb-0">
						<label>
							Selling Price
						</label>
                      	<input type="text" id="input-sell" placeholder="SELLING PRICE" name="selling_price" class="number-format form-control h-auto" required>
					</div>
				</div>
				<div class="modal-footer mt-2">
					<button type="button" class="col btn btn-secondary" data-dismiss="modal">
						CANCEL
					</button>
					<button type="button" class="col btn btn-danger" data-dismiss="modal" data-toggle="modal" data-target="#sell-item-confirm">
						SELL ITEM
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade confirm-modal" id="sell-item-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to sell?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#sell-item-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- SELL END -->

<!-- UPDATE APPRAISAL VALUE -->
<form class="d-inline needs-validation" action="<?php echo base_url('Transaction/updateAppraisalValue'); ?>" method="post" autocomplete="off" novalidate>
	<div class="modal fade form-modal" id="update-appraisal-value" tabindex="-1">
		<div class="modal-dialog modal-sm">
			<input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-uppercase text-primary">							
						UPDATE APPRAISAL VALUE
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group mb-3">
						<label> Current Value </label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">&#8369;</span>
							</div>
							<input type="text" value="<?php echo number_format($appraisal_value, 2); ?>" placeholder="NEW VALUE" name="old_value" class="number-format form-control h-auto" required readonly>
						</div>
					</div>
					<div class="form-group mb-0">
						<label> New Value </label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">&#8369;</span>
							</div>
							<input type="text" placeholder="NEW VALUE" name="new_value" class="number-format form-control h-auto" required>
						</div>
					</div>
				</div>
				<div class="modal-footer mt-2">
					<button type="button" class="col btn btn-secondary" data-dismiss="modal">
						CANCEL
					</button>
					<button type="button" class="col btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#update-appraisal-value-confirm">
						UPDATE
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade confirm-modal" id="update-appraisal-value-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to update appraisal value?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#update-appraisal-value">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- UPDATE APPRAISAL VALUE END -->

<!-- RENEW -->
<form class="d-inline needs-validation" action="<?php echo base_url("Transaction/Renew"); ?>" method="post" autocomplete="off" novalidate>
	<div class="modal fade form-modal" id="renew-modal" tabindex="-1">
		<div class="modal-dialog">
			<input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
			<input type="hidden" name="expiration" value="<?php echo $transaction['expiration']; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-uppercase text-success">
						Renew Item
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row narrow-gutters">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="" for="renDate"> Payment Date </label>
								<input type="text" name="renDate" id="renDate"  class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date('F d, Y'); ?>"  required>
							</div>
							<div class="form-group">
								<label class="" for="renDue"> Next Due </label>
								<input type="text" name="renDue" id="renDue"  class="datepicker text-uppercase form-control" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date('F d, Y', strtotime('+30 days')); ?>"  required>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="" for="renTransFee"> Transaction Fee </label>
								<div class="input-group">
									<div class="input-group-prepend">
                            			<span class="input-group-text">&#8369;</span>
                   					</div>
									<input id="renTransFee" name="renTransFee" type="text" class="number-format form-control h-auto" placeholder="0.00" required value="<?php echo $transaction['fee']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="" for="renPenCharge"> Penalty Charge </label>
								<div class="input-group">
									<div class="input-group-prepend">
                            			<span class="input-group-text">&#8369;</span>
                   					</div>
									<input id="renPenCharge" name="renPenCharge" type="text" class="number-format form-control h-auto" placeholder="0.00" required value="<?php echo $penalty; ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-body bg-success shadow">
					<div class="row">
						<div class="col-6 text-right">
							<label class="py-2 my-1 font-weight-bold text-uppercase">
								Amount&nbsp;Due&nbsp;:
							</label>
						</div>
						<div class="col-6 font-weight-bold">
							<span class="w-25">&#8369;</span>
							<input id="" name="renAmountDue" type="text" class="number-format font-weight-bold border-top-0 border-0 py-2 my-1 w-75 bg-success" placeholder="" required readonly="readonly" value="<?php echo $amount_due - $appraisal_value; ?>">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"> CANCEL </button>
					<button type="button" class="btn btn-success" data-dismiss="modal" data-toggle="modal" data-target="#renew-item-confirm"> RENEW </button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade confirm-modal" id="renew-item-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to renew?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#renew-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
 </form>
<!-- RENEW end -->

<!-- REPURCHASE -->
<form class="d-inline needs-validation" action="<?php echo base_url("Transaction/repurchase"); ?>" method="post" autocomplete="off" novalidate>
	<input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
	<input type="hidden" name="expiration" value="<?php echo $transaction['expiration']; ?>">
	<div class="modal fade form-modal" id="repurchase-modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-uppercase text-warning">
						Repurchase Item
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row narrow-gutters">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Repurchase Date </label>
								<input type="text" name="repDate" class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y"); ?>"  required>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label> Appraisal&nbsp;Value&nbsp; </label>
								<div class="input-group">
									<div class="input-group-prepend">
                            			<span class="input-group-text">&#8369;</span>
                   					</div>
									<input id="" name="repAppVal" type="text" class="number-format form-control h-auto" placeholder=""  readonly="readonly" required value="<?php echo $appraisal_value; ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="row narrow-gutters">
						<div class="col-sm-6">
							<div class="form-group">
								<label> Transaction&nbsp;Fee&nbsp; </label>
								<div class="input-group">
									<div class="input-group-prepend">
                            			<span class="input-group-text">&#8369;</span>
                   					</div>
									<input id="" name="repTransFee" type="text" class="number-format form-control h-auto" placeholder="0.00" required value="<?php echo $transaction['fee']; ?>">
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label> Penalty&nbsp;Charge&nbsp; </label>
								<div class="input-group">
									<div class="input-group-prepend">
                            			<span class="input-group-text">&#8369;</span>
                   					</div>
									<input id="" name="repPenCharge" type="text" class="number-format form-control h-auto" placeholder="0.00" required value="<?php echo $penalty; ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-body bg-warning shadow">
					<div class="row">
						<div class="col-6 text-right text-uppercase">
							<label class="py-2 my-1 font-weight-bold text-dark">
								Amount&nbsp;Due&nbsp;
							</label>
						</div>
						<div class="col-6">
							<span class="w-25 text-dark font-weight-bold">&#8369;</span>
							<input id="" name="repAmountDue" type="text" class="number-format font-weight-bold border-top-0 border-0 py-2 my-1 w-75 text-dark bg-warning" placeholder="" required readonly="readonly" value="<?php echo $amount_due; ?>">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"> CANCEL </button>
					<button type="button" class="btn btn-warning" data-dismiss="modal" data-toggle="modal" data-target="#repurchase-confirm"> REPURCHASE </button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade confirm-modal" id="repurchase-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to repurchase?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#repurchase-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- REPURCHASE end -->

<!-- TRANSFER -->
<form class="d-inline needs-validation" action="<?php echo base_url("Transaction/createTransfer"); ?>" method="post" autocomplete="off" novalidate>
	<input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
	<div class="modal fade form-modal" id="transfer-modal" tabindex="-1">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-uppercase text-dribbble">
						Transfer Item
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Date</label>
						<input type="text" name="date" class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date('F d, Y'); ?>" required>
					</div>
					<div class="form-group">
						<label>Origin</label>
						<?php if($this->session->role_fk == "ADMS"){ ?>
							<select class="form-control" name="transfer_origin" required>
								<?php
									foreach ($branches as $branch) {
										echo "<option value='" . $branch['id'] . "'  " . ($branch['id'] == $this->session->userdata('branch_fk') ? 'selected' : '') . "> " .
												$branch['name'] . " (" . $branch['id'] . ")
											</option>";
									}
								?>
							</select>
						<?php } else { ?>
							<select class="form-control" name="transfer_origin" required>
								<?php
									foreach ($branches as $branch) {
										if($branch['id'] == $this->session->userdata('branch_fk'))
										echo "<option value='" . $branch['id'] . "'> " . $branch['name'] . " (" . $branch['id'] . ") </option>";
									}
								?>
							</select>

						<?php } ?>
					</div>
					<div class="form-group">
						<label>Destination</label>
						<?php if($this->session->role_fk == "ADMS"){ ?>
							<select class="form-control" name="transfer_destination" required>
								<option value="" selected disabled>SELECT</option>
								<?php
									foreach ($branches as $branch) {
										echo "<option value='" . $branch['id'] . "' > " .
												$branch['name'] . " (" . $branch['id'] . ")
											</option>";
									}
								?>
							</select>
						<?php } else { ?>
							<select class="form-control" name="transfer_destination" required>
								<option value="" selected disabled>SELECT</option>
								<?php
									foreach ($branches as $branch) {
										if($branch['id'] !== $this->session->userdata('branch_fk'))
										echo "<option value='" . $branch['id'] . "'> " . $branch['name'] . " (" . $branch['id'] . ") </option>";
									}
								?>
							</select>
						<?php } ?>
					</div>
					<div class="form-group mb-0">
						<label>Remarks / Reason of transfer</label>
						<textarea name="transfer_remarks" class="form-control" rows="4" spellcheck="false" maxlength="255" placeholder="Remarks"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="col btn btn-secondary" data-dismiss="modal"> CANCEL </button>
					<button type="button" class="col btn btn-dribbble" data-dismiss="modal" data-toggle="modal" data-target="#transfer-confirm" > TRANSFER </button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade confirm-modal" id="transfer-confirm">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Confirmation</h5>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="mb-0">Are you sure want to transfer the item?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#transfer-modal">CANCEL</button>
					<button type="submit" class="col btn btn-success confirm">CONFIRM</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- TRANSFER END -->

<!-- VOID TRANSFER -->
<form class="d-inline needs-validation" action="<?php echo base_url("Transaction/voidTransfer"); ?>" method="post" novalidate>
	<input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
	<div class="modal fade form-modal" id="void-transfer-modal" tabindex="-1">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-uppercase text-dribbble">
						Void Transfer
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Set Status</label>
						<select class="form-control" name="void_transfer_status" required>
							<option value="For Sale">For Sale</option>
							<option value="For Repair">For Repair</option>
							<option value="For Disposal">For Disposal</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="col btn btn-secondary" data-dismiss="modal"> CANCEL </button>
					<button type="button" class="col btn btn-dribbble" data-dismiss="modal" data-toggle="modal" data-target="#void-transfer-confirm" > VOID </button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade confirm-modal" id="void-transfer-confirm">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Confirmation</h5>
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="mb-0">Are you sure want to void item transfer?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#void-transfer-modal">CANCEL</button>
					<button type="submit" class="col btn btn-danger confirm">CONFIRM</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- VOID TRANSFER END -->

<script>
  $(function() {
    $("#form-buttons").clone().appendTo("#table-toolbar");
  });
</script>