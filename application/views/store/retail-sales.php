<?php
     $date = date('Y-m-d');
     $role_fk = $this->session->userdata('role_fk');
?>

<div class="row mb-2">
     <div class="col-12 col-sm-6 mb-4">
          <h4 class="font-weight-bold"><?php echo $title; ?> / Sales Record</h4>
          <p class="text-muted"></p>
     </div>
</div>

<div class="row">
     <div class="col-12 col-sm-12 grid-margin">
          <div class="card">
               <div class="card-body">
                    <div class="table-responsive">
                         <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                         <table class="table d-none pt-4 mb-4 border-bottom" id="product-table">
                              <thead>
                                   <tr class="text-uppercase">
                                        <th class="text-center font-weight-bold">#</th>
                                        <th class="font-weight-bold">Date</th>
                                        <th class="font-weight-bold">TRANSACTION ID</th>
                                        <th class="font-weight-bold">Type</th>
                                        <th class="font-weight-bold">Brand</th>
                                        <th class="font-weight-bold">Product</th>                                        
                                        <th class="text-center font-weight-bold">Quantity</th>
                                        <th class="text-center font-weight-bold">Unit</th>
                                        <th class="text-right font-weight-bold">Price</th>
                                        <th class="text-right font-weight-bold">Sub Total</th>
                                        <?php if($role_fk == "ADMS") {
                                             echo "<th class='font-weight-bold'>Location</th>";
                                        } ?>
                                   </tr>
                              </thead>
                              <tbody>

                                   <?php foreach ($sales as $s => $sale) {
                                        $transactionId = $sale['transaction_id'];
                                        $type = $sale['type'];
                                        $brand = $sale['brand'];
                                        $name = $sale['name'];
                                        $unit = $sale['unit'];
                                        $quantity = $sale['quantity'];
                                        $price = $sale['selling_price'];
                                        $sub_total = $quantity * $price;                                        
                                        $branch = $sale['branch_fk'];
                                        $date = date("d M Y", strtotime($sale['date_sold']));

                                   ?>
                                             <tr>
                                                  <td class="text-center"></td>
                                                  <td> <?php echo $date; ?> </td>
                                                  <td> 
                                                       <a href="<?php echo base_url('Transaction?search=') . $transactionId; ?>">
                                                            <?php echo $transactionId; ?> 
                                                       </a>
                                                  </td>
                                                  <td> <?php echo $type; ?> </td>
                                                  <td> <?php echo $brand; ?> </td>
                                                  <td> <?php echo $name; ?> </td>                                                  
                                                  <td class="text-center"> <?php echo $quantity; ?> </td>
                                                  <td> <?php echo $unit; ?> </td>
                                                  <td class="text-right"> <?php echo number_format($price, 2); ?> </td>
                                                  <td class="text-right"> <?php echo number_format($sub_total, 2); ?> </td>

                                                  <?php if($role_fk == "ADMS"){
                                                       echo "<td>" . $branch . "</td>";
                                                  } ?>
                                             </tr>

                                   <?php } ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/retail.js'); ?>"></script>

<script type="text/javascript">

</script>
<script type="text/javascript">
     $(document).ready( function() {
          initDataTable();
     });
</script>
