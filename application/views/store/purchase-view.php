<?php
     $transactionId = $transaction['transaction_id'];
     $brand = $transaction['brand_name'];
     $brand_fk = $transaction['brand_fk'];
     $item = $transaction['product_name'];
     $type_fk = $transaction['type_fk'];
     $product = $transaction['brand_name'] . " " . $transaction['product_name'];
     $name_fk = $transaction['name_fk'];
     $description = $transaction['description'];
     $type = $transaction['type_name'];
     $quantity = $transaction['quantity'];
     $price = $transaction['selling_price'];
     $revenue = $quantity * $price;
     $cost = $transaction['buy_cost'];
     $date_c = new DateTime($transaction['date_created']);
     $date_created = $date_c->format('F d, Y');
     $date_u = new DateTime($transaction['date_updated']);
     $date_updated = $date_u->format('F d, Y');
     $location = $transaction['branch_name'] . "&nbsp;(" . $transaction['branch_id'] . ")";
     $user = $transaction['first_name'] . "&nbsp;" . $transaction['last_name'] . "&nbsp;(" . $transaction['user_id'] . ")";
     $is_void = $transaction['is_void'];
     $status = $transaction['status'];

     if($sales) {
          $status = "Sold";
          foreach ($sales as $s => $sale) {
               $referenceId = $sale['reference_id'];
          }
     }

?>

<div id="purchase-page" class="<?php echo ($is_void == 1 ? 'void' : '' ); ?>">
     <div class="row no-gutters">
          <div class="col-12 col-md-6 align-self-end">
               <?php 
                    $date = date('Y-m-d');
                    purchase_tab('view'); 
               ?>
          </div>
          <div class="col-12 col-md-6 text-right border-bottom-nav d-none d-md-block align-self-end">
               
               <a href="<?php echo base_url("Purchase"); ?>" class="btn btn-light btn-sm btn-icon-text border mb-2 mr-1">
                    CLOSE<i class="ti-close btn-icon-append"></i>
               </a>

               <?php if($this->session->userdata('role_fk') == 'ADMS' && !isset($sales[0]) && $is_void == 0 && $status != 'For Transfer') { ?>
                    
                    <button type="button" class="btn btn-dark btn-sm text-uppercase btn-icon-text mb-2 mr-1 text-white" data-toggle="modal" data-target="#void-purchase">
                         Void<i class="ti-trash btn-icon-append"></i>
                    </button>

                    <?php if($status != "For Transfer" && $is_void == 0 && $status != "Sold") { ?>
                         <button class="btn btn-primary btn-sm text-uppercase btn-icon-text mb-2 mr-1 text-white" data-toggle='modal' data-target='#status-modal' title="Update Status">
                              STATUS<i class="ti-bookmark btn-icon-append"></i>
                         </button>
                    
                    <?php } ?>

               <?php } ?>

               <?php if($status == "For Sale" && $is_void == 0) { ?>
                    <button type="button" class="btn btn-danger btn-sm btn-icon-text mb-2 mr-1" id="sell-item-btn" data-toggle="modal" data-target="#sell-item-modal">
                         SELL ITEM<i class="ti-shopping-cart btn-icon-append"></i>
                    </button>
               <?php } ?>

               <?php if($status != "Sold" && $status != "Void" && $status != "Disposed" && $status != "For Transfer") { ?>
                    <button type="button" class="btn btn-dribbble btn-sm text-uppercase btn-icon-text mb-2 mr-1 text-white" id="" data-toggle="modal" data-target="#transfer-modal">
                         Transfer<i class="ti-truck btn-icon-append"></i>
                    </button>
               <?php } ?>

               <?php if($status == "For Transfer" && $this->session->userdata('role_fk') == 'ADMS') { ?>
                    <button type="button" class="btn btn-dribbble btn-sm text-uppercase btn-icon-text mb-2 text-white" id="" data-toggle="modal" data-target="#void-transfer-modal">
                         Void Transfer<i class="ti-truck btn-icon-append"></i>
                    </button>
               <?php } ?>

               <?php if($sales) {
                    echo '<a class="btn btn-danger btn-sm btn-icon-text mb-2" href="' . base_url('Transaction?search=') . $referenceId . '">
                         SALES TRANSACTION<i class="ti-view-list-alt btn-icon-append"></i>
                         </a>';
               } ?>
          </div>
     </div>
     <div class="row narrow-gutters">
          <div class="col-12">
               <div class="card border-top-0">
                    <div class="card-body">
                         <div class="row">
                              <div class="col-12">
                                   <h4 class="card-title text-primary">INFORMATION</h4>
                              </div>
                         </div>
                         <div class="row text-nowrap narrow-gutters">
                              <div class="col">
                                   <div class="form-group">
                                        <div class="text-field d-block">
                                             <label>LOCATION</label>
                                             <p> <?php echo $location; ?> </p>
                                        </div>
                                   </div>
                              </div>
                              <div class="col">
                                   <div class="form-group">
                                        <div class="text-field d-block">
                                             <label>PURCHASE ID</label>
                                             <p><?php echo $transactionId; ?></p>
                                        </div>
                                   </div>
                              </div>
                              <div class="col">
                                   <div class="form-group">
                                        <div class="text-field d-block">
                                             <label>PURCHASE DATE</label>
                                             <p><?php echo $date_created; ?></p>
                                        </div>
                                   </div>
                              </div>
                              <div class="col">
                                   <div class="form-group">
                                        <div class="text-field d-block">
                                             <label>STATUS</label>
                                             <p><?php echo $status; ?></p>
                                        </div>
                                   </div>
                              </div>
                              <div class="col">
                                   <div class="form-group">
                                        <div class="text-field d-block">
                                             <label>CATEGORY</label>
                                             <p>PURCHASE</p>
                                        </div>
                                   </div>
                              </div>
                              <div class="col">
                                   <div class="form-group">
                                        <div class="text-field d-block">
                                             <label>SALES REFERENCE</label>
                                             <p>
                                                  <?php
                                                  if($sales) {
                                                       echo '<a href="' . base_url('Transaction?search=') . $referenceId . '">' .
                                                                 $referenceId .
                                                            '</a>';
                                                  } else {
                                                       echo "Not yet available";
                                                  }
                                                  ?>
                                             </p>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-12">
                                   <div class="table-responsive pt-3">
                                        <table class="table text-uppercase">
                                             <thead>
                                                  <tr>
                                                       <th>Item Type</th>
                                                       <th>Item Name</th>
                                                       <th>Item Description</th>
                                                       <th>Buy Price</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  <tr>
                                                       <td class="align-top">
                                                            <p class="mb-0 lead">
                                                                 <?php echo $type; ?>
                                                            </p>
                                                       </td>
                                                       <td class="align-top">
                                                            <p class="mb-0 lead">
                                                                 <?php echo $brand . " " . $item; ?>
                                                            </p>
                                                       </td>
                                                       <td class="align-top text-wrap">
                                                            <p class="mb-0 lead">
                                                                 <?php echo $description; ?>
                                                            </p>
                                                       </td>
                                                       <td class=" align-top">
                                                            <p class="mb-0 lead">
                                                                 &#8369; <?php echo number_format($cost, 2); ?>
                                                            </p>
                                                       </td>
                                                  </tr>
                                             </tbody>
                                        </table>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="row d-block d-md-none">
          <div class="col-12 text-right">
               <a href="<?php echo base_url("Purchase"); ?>" class="btn btn-light btn-sm btn-icon-text border mt-2 mr-1">
                    CLOSE<i class="ti-close btn-icon-append"></i>
               </a>

               <?php if($this->session->userdata('role_fk') == 'ADMS' && !isset($sales[0]) && $is_void == 0 && $status != 'For Transfer') { ?>
                    
                    <button type="button" class="btn btn-dark btn-sm text-uppercase btn-icon-text mt-2 mr-1 text-white" data-toggle="modal" data-target="#void-purchase">
                         Void<i class="ti-trash btn-icon-append"></i>
                    </button>

                    <?php if($status != "For Transfer" && $is_void == 0 && $status != "Sold") { ?>
                         <button class="btn btn-primary btn-sm text-uppercase btn-icon-text mt-2 mr-1 text-white" data-toggle='modal' data-target='#status-modal' title="Update Status">
                              STATUS<i class="ti-bookmark btn-icon-append"></i>
                         </button>
                    
                    <?php } ?>

               <?php } ?>

               <?php if($status == "For Sale" && $is_void == 0) { ?>
                    <button type="button" class="btn btn-danger btn-sm btn-icon-text mt-2 mr-1" id="sell-item-btn" data-toggle="modal" data-target="#sell-item-modal">
                         SELL ITEM<i class="ti-shopping-cart btn-icon-append"></i>
                    </button>
               <?php } ?>

               <?php if($status != "Sold" && $status != "Void" && $status != "Disposed" && $status != "For Transfer") { ?>
                    <button type="button" class="btn btn-dribbble btn-sm text-uppercase btn-icon-text mt-2 mr-1 text-white" id="" data-toggle="modal" data-target="#transfer-modal">
                         Transfer<i class="ti-truck btn-icon-append"></i>
                    </button>
               <?php } ?>

               <?php if($status == "For Transfer" && $this->session->userdata('role_fk') == 'ADMS') { ?>
                    <button type="button" class="btn btn-dribbble btn-sm text-uppercase btn-icon-text mt-2 text-white" id="" data-toggle="modal" data-target="#void-transfer-modal">
                         Void Transfer<i class="ti-truck btn-icon-append"></i>
                    </button>
               <?php } ?>

               <?php if($sales) {
                    echo '<a class="btn btn-danger btn-sm btn-icon-text mt-2" href="' . base_url('Transaction?search=') . $referenceId . '">
                         SALES TRANSACTION<i class="ti-view-list-alt btn-icon-append"></i>
                         </a>';
               } ?>
          </div>
     </div>
</div>

<!-- UPDATE STATUS -->
<form class="d-inline needs-validation" action="<?php echo base_url('Purchase/updateStatus/') . $transactionId; ?>" method="post" novalidate autocomplete="off">
     <div class="modal fade form-modal" id="status-modal" tabindex="-1">
          <div class="modal-dialog modal-sm">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-uppercase text-primary">Update Status</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="form-group mb-0">
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Sale" <?php echo ($status == 'For Sale' ? 'checked' : ''); ?> required>
                                        For Sale
                                   </label>
                              </div>
                              <div class="form-check form-check-primary d-none">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Transfer" <?php echo ($status == 'For Transfer' ? 'checked' : ''); ?> required>
                                        For Transfer
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Repair" <?php echo ($status == 'For Repair' ? 'checked' : ''); ?> required>
                                        For Repair
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Disposal" <?php echo ($status == 'For Disposal' ? 'checked' : ''); ?> required>
                                        For Disposal
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="Disposed" <?php echo ($status == 'Disposed' ? 'checked' : ''); ?> required>
                                        Disposed
                                   </label>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary" data-dismiss="modal">CANCEL</button>
                         <button type="button" class="col btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#confirm-status">UPDATE</button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="confirm-status">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to update status?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#status-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm" name="update_status" value="update">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- UPDATE STATUS END -->

<!-- SELL ITEM -->
<form class="d-inline needs-validation" action="<?php echo base_url('Purchase/createSales'); ?>" method="post" autocomplete="off" novalidate>
     <input type="hidden" name="transaction_id" value="<?php echo $transactionId?>">
     <input type="hidden" name="brand_fk" value="<?php echo $brand_fk?>">
     <input type="hidden" name="type_fk" value="<?php echo $type_fk?>">
     <input type="hidden" name="name_fk" value="<?php echo $name_fk?>">
     <input type="hidden" name="description" value="<?php echo $description?>">
     <div class="modal fade form-modal" id="sell-item-modal" tabindex="-1">
          <div class="modal-dialog modal-sm">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-uppercase">
                              <?php echo $product; ?>
                         </h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="form-group">
                              <label>Date Sold</label>
                              <input type="text" name="selling_date" class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date('F d, Y'); ?>" required>
                         </div>
                         <div class="form-group mb-0">
                              <label>
                                   Selling Price <span class="text-danger">*</span>
                              </label>
                              <input type="text" id="input-sell" placeholder="SELLING PRICE" name="selling_price" class="float-format form-control h-auto" required>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary" data-dismiss="modal"> CANCEL </button>
                         <button type="submit" class="col btn btn-danger" data-dismiss="modal" data-target="#sell-confirm" data-toggle="modal"> SELL ITEM </button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="sell-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to sell?</p>
                    </div>
                    <div class="modal-footer">
                         <input type="hidden" name="transaction_id" value="<?php echo $transactionId; ?>">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-target="#sell-item-modal" data-toggle="modal">CANCEL</button>
                         <button type="submit" class="col btn btn-danger confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- SELL ITEM END -->

<!-- VOID TRANSFER -->
<form class="d-inline needs-validation" action="<?php echo base_url("Purchase/voidTransfer"); ?>" method="post" autocomplete="off" novalidate>
     <input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
     <div class="modal fade" id="void-transfer-modal" tabindex="-1">
          <div class="modal-dialog modal-sm">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-uppercase text-dribbble"> Void Transfer </h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="form-group">
                              <label>Set Status</label>
                              <select class="form-control" name="void_transfer_status" required>
                                   <option value="For Sale">For Sale</option>
                                   <option value="For Repair">For Repair</option>
                                   <option value="For Disposal">For Disposal</option>
                              </select>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary" data-dismiss="modal"> CANCEL </button>
                         <button type="button" class="col btn btn-dribbble" data-dismiss="modal" data-toggle="modal" data-target="#void-transfer-confirm" > VOID </button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="void-transfer-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to void item transfer?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#void-transfer-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-danger confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- VOID TRANSFER END -->

<!-- ITEM TRANSFER -->
<form class="d-inline needs-validation" action="<?php echo base_url("Purchase/createTransfer"); ?>" method="post" autocomplate="off" novalidate>
     <div class="modal fade form-modal" id="transfer-modal" tabindex="-1">
          <div class="modal-dialog modal-sm">
               <input type="hidden" name="transaction_id" value="<?php echo $transaction['transaction_id']; ?>">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-uppercase text-dribbble">
                              Transfer Item
                         </h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="form-group">
                              <label>Date</label>
                              <input type="text" name="date" class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date('F d, Y'); ?>" required>
                         </div>
                         <div class="form-group">
                              <label>Origin</label>
                              <?php if($this->session->role_fk == "ADMS"){ ?>
                                   <select class="form-control" name="transfer_origin" required>
                                        <?php
                                             foreach ($branches as $branch) {
                                                  echo "<option value='" . $branch['id'] . "'  " . ($branch[id] == $this->session->userdata('branch_fk') ? 'selected' : '') . "> " .
                                                            $branch['name'] . " (" . $branch['id'] . ")
                                                       </option>";
                                             }
                                        ?>
                                   </select>
                              <?php } else { ?>
                                   <select class="form-control" name="transfer_origin" required>
                                        <?php
                                             foreach ($branches as $branch) {
                                                  if($branch[id] == $this->session->userdata('branch_fk'))
                                                  echo "<option value='" . $branch['id'] . "'> " . $branch['name'] . " (" . $branch['id'] . ") </option>";
                                             }
                                        ?>
                                   </select>

                              <?php } ?>
                         </div>
                         <div class="form-group">
                              <label>Destination</label>
                              <?php if($this->session->role_fk == "ADMS"){ ?>
                                   <select class="form-control" name="transfer_destination" required>
                                        <option value="" selected disabled>SELECT</option>
                                        <?php
                                             foreach ($branches as $branch) {
                                                  echo "<option value='" . $branch['id'] . "' > " .
                                                            $branch['name'] . " (" . $branch['id'] . ")
                                                       </option>";
                                             }
                                        ?>
                                   </select>
                              <?php } else { ?>
                                   <select class="form-control" name="transfer_destination" required>
                                        <option value="" selected disabled>SELECT</option>
                                        <?php
                                             foreach ($branches as $branch) {
                                                  if($branch[id] !== $this->session->userdata('branch_fk'))
                                                  echo "<option value='" . $branch['id'] . "'> " . $branch['name'] . " (" . $branch['id'] . ") </option>";
                                             }
                                        ?>
                                   </select>
                              <?php } ?>
                         </div>
                         <div class="form-group mb-0">
                              <label>Remarks / Reason of transfer</label>
                              <textarea name="transfer_remarks" class="form-control" rows="4" spellcheck="false" maxlength="255" placeholder="Remarks"></textarea>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary" data-dismiss="modal"> CANCEL </button>
                         <button type="button" class="col btn btn-dribbble" data-dismiss="modal" data-toggle="modal" data-target="#transfer-confirm" > TRANSFER </button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="transfer-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to transfer the item?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#transfer-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- ITEM TRANSFER END -->

<!-- VOID  -->
<form class="d-inline needs-validation" action="<?php echo base_url('Purchase/voidPurchase');?>" method="post" novalidate>
     <div class="modal fade confirm-modal" id="void-purchase">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to void purchase?</p>
                    </div>
                    <div class="modal-footer">
                         <input type="hidden" name="transaction_id" value="<?php echo $transactionId; ?>">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
                         <button type="submit" class="col btn btn-danger confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- VOID END -->

<script type="text/javascript" src="<?php echo base_url('assets/js/purchase.js'); ?>"></script>
<script>
     $(function() {
          initDatePicker();
          // sellItemAmount();
          validateForm("#sell-item-modal form");
          $('.float-format').number(true, 2);
          $('.int-format').number(true);
          validateForm2();
     });
</script>
