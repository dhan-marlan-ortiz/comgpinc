<div class="phoneshop">
	<div class="container-scroller">
		<?php $this->view('parts/header.php'); ?>
		<div class="container-fluid page-body-wrapper">
			<?php $this->view('parts/sidebar.php'); ?>
			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-lg-3 grid-margin stretch-card">
							<div class="card">
								<div class="card-body">
									<h2>New Customer</h2>
									<form method="post" action="<?php echo base_url('Customer'); ?>" id="customerForm">
										<div class="form-group">
											<label for="firstname">First Name <span class="text-danger">*</span></label>
											<input type="text" class="form-control" name="firstname" id="firstname" placeholder="" >
										</div>
										<div class="form-group">
											<label for="middlename">Middle Name</label>
											<input type="text" class="form-control" name="middlename" id="middlename" placeholder="">
										</div>
										<div class="form-group">
											<label for="lastname">Last Name <span class="text-danger">*</span></label>
											<input type="text" class="form-control" name="lastname" id="lastname" placeholder="" >
										</div>
										<div class="form-group">
											<label for="gender">Gender <span class="text-danger">*</span></label>
											<select class="" name='gender' id="gender" placeholder='Select gender' required>
												<option value="" selected="selected"></option>
												<option value='male'>Male</option>
												<option value='female'>Female</option>
											</select>
										</div>
										<div class="form-group">
											<label for="address">Address <span class="text-danger">*</span></label>
											<input type='text' class="form-control" name='address' id="address" placeholder='' >
										</div>
										<div class="form-group">
											<label for="city">City <span class="text-danger">*</span></label>
											<select class="select-city" name="city" id="city" placeholder="Type or select city" required>
									               <option value="" selected="selected"></option>
									               <?php
									               if(null !== $cities) {
									                    foreach ($cities as $city):
									                         echo "<option value='".$city['id']."'>".$city['name'] ." - ". $city['province'] . "</option>";
									                    endforeach;
									               }else {
									                    echo "<option value='' disabled>Result not found</option>";
									               }
									               ?>
									          </select>
										</div>
										<div class="form-group">
											<label for="contact">Contact Number <span class="text-danger">*</span></label>
											<input type='text' class="form-control" name='contact' id="contact" placeholder='' >
										</div>
										<div class="form-group">
											<label for="id-type">ID Presented <span class="text-danger">*</span></label>
											<input type='text' class="form-control" name='id_type' id="id-type" placeholder='' >
										</div>
										<div class="form-group">
											<label for="id-number">ID Number <span class="text-danger">*</span></label>
											<input type='text' class="form-control" name='id_number' id="id-number" placeholder='' >
										</div>
										<div class="form-group">
											<label for="Remarks">Remarks</label>
											<textarea class="form-control" name='remarks' rows="5" spellcheck="false" id="Remarks"></textarea>
										</div>
										<div class="form-group">
											<label class="text-danger">
												<em>Fields with (*) are required.</em>
											</label>
										</div>
										<div class="form-group">
											<input type="submit" class="btn btn-primary btn-block mr-2 text-uppercase" name="add">
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-lg-9 grid-margin stretch-card">
							<div class="card">
								<div class="card-body">
									<h2>Customer List</h2>
									<div class="row">
										<div class="col-lg-12 grid-margin mb-0">
											<form method="get" action="<?php echo base_url('Customer'); ?>" id="searchForm" class="mb-0">
												<div class="form-group">
													<label>SEARCH BY</label>
													<div class="input-group advanced-search">
														<div class="input-group-prepend">
															<select class="btn btn-primary" name="filter">
																<option value="name">NAME</option>
																<option value="contact">CONTACT #</option>
															</select>
														</div>
														<input type="search" class="form-control search-bar" name="search" aria-label="Text input with dropdown button">
														<div class="input-group-append">
															<input type="submit" class="btn btn-primary mr-2 ti-search menu-icon icon-btns" value="&#xe610;">
															<a class="btn btn-inverse-info btn-fw icon-btns" href="Customer" data-toggle="tooltip" title="Clear Search"><i class="ti-reload menu-icon"></i></a>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
									<hr>
									<div class="table-responsive">
										<table class="table" id="customer_table">
											<thead>
												<tr>
													<th class="hidden">ID</th>
													<th width="20%">NAME</th>
													<th width="20%">CONTACT</th>
													<th width="30%">ADDRESS</th>
													<th width="10%">ACTION</th>
												</tr>
											</thead>
											<tbody>
												<?php
												foreach ($customers as $customer) {
													echo "<tr>
													<td class='hidden'>".$customer['id']."</td>
													<td>".$customer['first_name'] . " " . $customer['last_name'] . "</td>
													<td>" . $customer['contact'] ."</td>
													<td>".$customer['address'] . ", " . $customer['city_name'] . ", " . $customer['province'] . "</td>
													<td>
													<a href='".base_url()."Customer/profile/".$customer['id']."' class='btn btn-primary btn-rounded btn-icon'><i class='ti-eye menu-icon'></i></a>
													</td>
													</tr>
													";
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
