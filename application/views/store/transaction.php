<input type="hidden" name="" value="<?php echo base_url(); ?>" id="base-url">
<div class="row no-gutters">
   <div class="col-12 col-lg-4 align-self-end">
      <?php
         transaction_tab("records", $item_notif);
         ?>
   </div>
   <div class="col-12 col-lg-8 text-right border-bottom-nav align-self-end">
      <?php
         if($item_notif != null) {
             $item_notif_due = $item_notif['due'];
             $item_notif_expired = $item_notif['expired'];
             $item_notif_grace = $item_notif['grace'];
         
             echo "
             <div class='item-notification my-2 my-lg-2'>
         	    <ul class='list-inline text-uppercase d-inline border bg-white'>
         		    <li class='list-inline-item mr-3'>
         			    <a class='text-reset text-decoration-none' href='" . base_url('Transaction?status=Due') . "'>
         				    <span class='text-small'>Due:</span><span class='font-weight-bold'>&ensp;" . $item_notif_due . "</span>
         			    </a>
         		    </li>
         		    <li class='list-inline-item mr-3'>
         			    <a class='text-reset text-decoration-none' href='" . base_url('Transaction?status=Expired') . "'>
         				    <span class='text-small'>Expired:</span><span class='font-weight-bold'>&ensp;" . $item_notif_expired . "</span>
         			    </a>
         		    </li>
         		    <li class='list-inline-item'>
         			    <a class='text-reset text-decoration-none' href='" . base_url('Transaction?status=Grace') . "'>
         				    <span class='text-small'>Grace Period:</span><span class='font-weight-bold'>&ensp;" . $item_notif_grace . "</span>
         			    </a>
         		    </li>
         	    </ul>
         	    <button type='button' class='btn " . ($item_notif_due || $item_notif_expired || $item_notif_grace > 0 ? 'btn-warning' : 'btn-secondary') . " btn-rounded btn-icon'>
         		    <i class='ti-bell text-white'></i>
         	    </button>
             </div>
             ";
         }
         ?>
      <div id='table-toolbar'>
          <div id="table-options" class="table-options d-none mb-2" style="margin-left:7px">
            <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text py-0 border-radius-left-2 ti-filter">
                  </span>
               </div>
               <a href="#" id="filter-toggle" class="btn btn-light btn-sm border border-radius-right-2 font-11" data-toggle="collapse" data-target="#filters"> SHOW FILTERS </a>
               <!-- <a href="#" class="btn btn-secondary text-white btn-sm shrinked border border-radius-right-2 d-none font-11" id="expand-table-btn"> <span>EXPAND TABLE</span> </a> -->
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-12 col-sm-12 grid-margin stretch-card">
      <div class="card border-top-0" id="transaction-records">
         <div class="card-body pt-4">
            <div class="text-uppercase mb-2 collapse" id="filters">
               <div class="row narrow-gutters">
                  <div class="col-6 col-sm-4 col-md-2">
                     <div class="form-group mb-3">
                        <label class="mb-0 text-muted"><small class='ti-filter'></small>&ensp;Location</label>
                        <select id="select-branch" class="form-control" name="branch"></select>
                     </div>
                  </div>
                  <div class="col-6 col-sm-4 col-md-2">
                     <div class="form-group mb-3">
                        <label class="mb-0 text-muted"><small class='ti-filter'></small>&ensp;Customer</label>
                        <select id="select-customer" class="form-control" name="customer"></select>
                     </div>
                  </div>
                  <div class="col-6 col-sm-4 col-md-2">
                     <div class="form-group mb-3">
                        <label class="mb-0 text-muted"><small class='ti-filter'></small>&ensp;Transaction</label>
                        <input id="search-transaction" type="text" class="form-control" placeholder="Search">
                     </div>
                  </div>
                  <div class="col-6 col-sm-4 col-md-2">
                     <div class="form-group mb-3">
                        <label class="mb-0 text-muted"><small class='ti-filter'></small>&ensp;Status</label>
                        <select id="select-status" class="form-control" name="status">
                           <option value="ALL">ALL</option>
                           <option value="DISPOSED">DISPOSED</option>
                           <option value="DUE">DUE</option>
                           <option value="EXPIRED">EXPIRED</option>
                           <option value="FOR DISPOSAL">FOR DISPOSAL</option>
                           <option value="FOR REPAIR">FOR REPAIR</option>
                           <option value="FOR SALE">FOR SALE</option>
                           <option value="FOR TRANSFER">FOR TRANSFER</option>
                           <option value="FREEZE">FREEZE</option>
                           <option value="GRACE PERIOD">GRACE PERIOD</option>
                           <option value="IN CONTRACT">IN CONTRACT</option>
                           <option value="REPURCHASED">REPURCHASED</option>
                           <option value="SOLD">SOLD</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-12 col-sm-8 col-md-4">
                    <div class="position-absolute w-100 text-right">
                         <div class="form-check form-check-inline d-inline-block my-0">
                              <input class="form-check-input d-inline-block h-auto" type="radio" name="date-range-option" id="entry-date-rb" value="entry" checked>
                              <label class="form-check-label d-inline-block ml-0" for="inlineRadio1">Entry</label>
                         </div>
                         <div class="form-check form-check-inline d-inline-block my-0">
                              <input class="form-check-input d-inline-block h-auto" type="radio" name="date-range-option" id="update-date-rb" value="update">
                              <label class="form-check-label d-inline-block ml-0" for="inlineRadio2">Update</label>
                         </div>
                    </div>

                    <div class="form-group mb-0 date-range" id="entry-date">
                        <label class="mb-0 text-muted"><small class='ti-filter'></small>&ensp;Date Range</label>
                        <input type="text" placeholder="FROM" class="date-range-from form-control mb-2 from" id="trans-min-date" data-date-format="dd M yyyy">
                        <input type="text" placeholder="TO" class="date-range-to form-control mb-0 to" id="trans-max-date" data-date-format="dd M yyyy">
                    </div>

                    <div class="form-group mb-0 date-range" id="update-date" style="display:none">
                        <label class="mb-0 text-muted"><small class='ti-filter'></small>&ensp;Date Range</label>
                        <input type="text" placeholder="FROM" class="date-range-from form-control mb-2 from" id="update-min-date" data-date-format="dd M yyyy">
                        <input type="text" placeholder="TO" class="date-range-to form-control mb-0 to" id="update-max-date" data-date-format="dd M yyyy">
                    </div>
                  </div>
               </div>
            </div>
            <div class="table-responsive pb-3">
               <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
               <table id="transaction-table" style="display: none">
                  <thead>
                     <tr class="table-header">
                        <th>Status</th>
                        <th>Entry Date</th>
                        <th>Last Update</th>
                        <th>Customer Name</th>
                        <th>Item Type</th>
                        <th>Item Name</th>
                        <th>Serial</th>
                        <th>Value</th>
                        <th>Trans ID</th>
                        <th>Location</th>
                        <th>Category</th>
                        <th class='no-sort bg-img-none'></th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        if($transactions) {
                        	foreach ($transactions as $t => $tran) {
                              $category = $tran['category'];
                              if($category  == 3) {
                                 $product = "<div style='line-height:1.5em'>";
                                 $item_type = "<div style='line-height:1.5em'>";
                                 $category_name =  'RETAIL';
                                 $customer = "NOT AVAILABLE";
                                 $value = $this->Transaction_model->getRetailSalesAmount($tran['transaction_id']);
                                 
                                 $retail_items = $this->Transaction_model->getRetailSalesItem($tran['transaction_id']);                                 
                                 foreach ($retail_items as $r => $retail_item) {
                                    $product .= "<span class='d-block no-wrap'>" . $retail_item['product'] . "</span>";
                                    $item_type .= "<span class='d-block no-wrap'>" . $retail_item['type'] . "</span>";
                                 }
                                 
                                 $product .= "</div>";
                                 $item_type .= "</div>";
                                            
                              }else if($category == 2) {
                                 $item_type = $tran['type_name'];
                                 $product =  $tran['brand_name'] . " " . $tran['product_name'];
                                 $category_name =  'PAWN';
                                 $customer = "NOT AVAILABLE";
                                 $value = $tran['amount'];
                              }else if($category == 1) {
                                 $item_type = $tran['type_name'];
                                 $product =  $tran['brand_name'] . " " . $tran['product_name'];
                                 $category_name =  'PURCHASE';
                                 $customer = $tran['fname'] . " " . $tran['lname'] . " " . $tran['mname'];
                                 $value = $tran['value'];
                              }

                        		echo "<tr data-status='" . $tran['status'] . "'>";
                        		echo      "<td class='no-wrap'>" . 
                        					$tran['status'] . " <span>" . $tran['last_transaction'] .
                        				"</span></td>";
                        		echo      "<td class='no-wrap' data-sort='" . $tran['date'] . "'>" . 
                        					date("d M Y", strtotime($tran['date'])) .                         					
                        				"</td>";
                              echo      "<td class='no-wrap' data-sort='" . $tran['updated_at'] . "'>" . 
                                             date("d M Y", strtotime($tran['updated_at'])) .                                        
                                        "</td>";
                        		echo      "<td>" . $customer . "</td>";
                        		echo      "<td>" . $item_type . "</td>";
                        		echo      "<td>" . $product . "</td>";
                        		echo      "<td>" . 
                        					    //$tran['description'] . 
                        				        $tran['serial'] .
                        				     "</td>";
                        		echo      "<td class='text-right'>" . number_format($value, 2) . "</td>";
                        		echo      "<td>" . $tran['transaction_id'] . "</td>";
                        		echo      "<td class='no-wrap'>" . $tran['branch_name'] . "</td>";
                        		echo      "<td>" . $category_name . "</td>";
                        		echo      "<td data-sort='" . $tran['updated_at'] . "'>
                        					<a class='btn btn-primary btn-sm font-11 p-2 w-100'  href='" . base_url('Transaction?search=') . $tran['transaction_id'] . "'>
                        						VIEW
                        					</a>
                        				</td>";
                        		echo "</tr>";
                        	}
                        }
                        ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row d-block d-md-none">
   <div class="col-12 text-right">
   </div>
</div>
<div class="row">
   <div class="col-12">
      <p class="text-small font-weight-medium">
         STATUS LEGEND:
      </p>
      <ul class="transaction-status list-inline mb-0">
         <li class="list-inline-item status-due"> Due </li>
         <li class="list-inline-item status-expired"> Expired </li>
         <li class="list-inline-item status-grace"> Grace Period </li>
         <li class="list-inline-item status-contract"> In Contract </li>
         <li class="list-inline-item status-freeze"> Freeze </li>
         <li class="list-inline-item status-sale"> For Sale </li>
         <li class="list-inline-item status-repair"> For Repair / Transfer </li>
         <li class="list-inline-item status-disposal"> For Disposal </li>
         <li class="list-inline-item status-sold"> Sold / Repurchased </li>
         <li class="list-inline-item status-disposed"> Disposed </li>
      </ul>
   </div>
</div>
<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/transaction.js'); ?>"></script>
<script type="text/javascript">
   var transactions = <?php echo json_encode($transactions); ?>;
   var clients = <?php echo json_encode($clients); ?>;
   var branches = <?php echo json_encode($branches); ?>;
</script>
<script type="text/javascript">
   $(function() {
   	initSelect2();
   	getCustomers(clients);
   	getBranches(branches);
   	updateStatus();
   	toggleFilters();
   	initDataTable();
   	initSlimScroll();
   	showTable();
   	// expandTable();
   	
   	$("#transaction-table_length").prependTo("#table-toolbar");
     $("#transaction-table_filter").prependTo("#table-toolbar");
   
   	$(".item-notification button").on("click", function() {
   		var target = $(this).siblings("ul");
   		if(target.hasClass('slide')) {
   			target.removeClass('slide');
   		} else {
   			target.addClass('slide');
   		}
   	});
   });
</script>