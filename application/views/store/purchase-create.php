<form action="<?php echo base_url('Purchase/create'); ?>" method="post" id="transaction-create-form" autocomplete="off">
     <div class="row no-gutters">
          <div class="col-12 col-md-6 align-self-end">
               <?php 
                    $date = date('Y-m-d');
                    purchase_tab('create'); 
               ?>
          </div>
          <div class="col-12 col-md-6 text-right border-bottom-nav d-none d-md-block align-self-end">
               <a href="<?php echo base_url('Purchase'); ?>" class="btn btn-sm btn-light btn-icon-text border mb-2 mr-1">
                    CLOSE<i class="ti-close btn-icon-append"></i>
               </a>
               <a href="<?php echo base_url('Purchase/create'); ?>" class="btn btn-sm btn-secondary btn-icon-text border mb-2 mr-1">
                    RESET<i class="ti-reload btn-icon-append"></i>
               </a>
               <button type="submit" class="btn btn-sm btn-primary btn-icon-text mb-2 mr-1">
                    <span>SUBMIT</span><i class="ti-check btn-icon-append"></i>
               </button>
          </div>
     </div>
     <div class="row narrow-gutters">
          <div class="col">
               <div class="card border-top-0">
                    <div class="card-body">
                         <div class="row">
                              <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                   <div class="form-group">
                                        <label>Location <span class="text-danger">*</span></label>
                                        <div class="input-group">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text py-0 border-radius-left-2 ti-location-pin"></span>
                                             </div>
                                             <div class="input-group-append">
                                                  <?php if($this->session->userdata('role_fk') == 'ADMS'){ ?>
                                                       <select id="select-branch" class="form-control text-uppercase" name="branch" required></select>
                                                  <?php } else { ?>
                                                       <input type="text" class="form-control text-uppercase" name="branch" value='<?php echo $this->session->userdata('branch_fk'); ?>' readonly="readonly" required="required">
                                                  <?php } ?>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                        <label>Purchase Date <span class="text-danger">*</span></label>
                                        <div class="input-group">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text py-0 border-radius-left-2 ti-calendar"></span>
                                             </div>
                                             <input type="text" name="date" id="date" placeholder="MM dd, yyyy" class="form-control border-radius-2 datepicker text-uppercase" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y", strtotime($date)); ?>"  required>
                                        </div>
                                        
                                   </div>
                              </div>
                              <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 border-left-md border-right-md">
                                   <div class="form-group">
                                        <label>Item Type <span class="text-danger">*</span></label>
                                        <label class="float-right">
                                             <small>
                                                  <a href="#" id="new-type-toggle" class="active">Add</a>
                                             </small>
                                        </label>
                                        <div class="input-group">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text py-0 border-radius-left-2 ti-package"></span>
                                             </div>
                                             <div class="input-group-append">
                                                  <select id="select-type" class="form-control text-uppercase" name="type" required></select>
                                             </div>
                                        </div>
                                        <input id="input-type" type="text" class="form-control text-uppercase d-none" name="type-new" value="" placeholder="Enter new type" disabled required>
                                   </div>
                                   <div class="form-group">
                                        <label>Brand <span class="text-danger">*</span></label>
                                        <label class="float-right">
                                             <small>
                                                  <a href="#" id="new-brand-toggle" class="active">Add</a>
                                             </small>
                                        </label>
                                        <div class="input-group">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text py-0 border-radius-left-2 ti-package"></span>
                                             </div>
                                             <div class="input-group-append">
                                                  <select id="select-brand" class="form-control text-uppercase" name="brand" required></select>
                                             </div>
                                        </div>
                                        <input id="input-brand" type="text" class="form-control d-none text-uppercase" name="brand-new" value="" placeholder="Enter new brand name" disabled required>
                                        <em class="text-muted">
                                             <sub>Item type is a prerequisite</sub>
                                        </em>
                                   </div>
                                   <div class="form-group">
                                        <label>Item Name <span class="text-danger">*</span></label>
                                        <label class="float-right">
                                             <small>
                                                  <a href="#" id="new-name-toggle" class="active">Add</a>
                                             </small>
                                        </label>
                                        <div class="input-group">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text py-0 border-radius-left-2 ti-package"></span>
                                             </div>
                                             <div class="input-group-append">
                                                  <select id="select-name" class="form-control text-uppercase" name="product" required></select>
                                             </div>
                                        </div>
                                        <input id="input-name" type="text" class="form-control text-uppercase d-none" name="product-new" value="" placeholder="Enter new name" disabled>
                                        <em class="text-muted">
                                             <sub>Brand name is a prerequisite</sub>
                                        </em>
                                   </div>
                              </div>
                              <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                   <div class="form-group">
                                        <label>Item Description</label>
                                        <div class="input-group">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text py-0 border-radius-left-2 ti-align-left"></span>
                                             </div>
                                             <div class="input-group-append">
                                                  <textarea id="input-description" type="text text-uppercase" class="form-control text-uppercase" name="description" value="" rows="4" spellcheck="false" placeholder="Item Description"></textarea>
                                             </div>
                                        </div>
                                        
                                   </div>
                                   <div class="form-group">
                                        <label>Buy Price <span class="text-danger">*</span></label>
                                        <div class="input-group">
                                             <div class="input-group-prepend">
                                                  <span class="input-group-text py-0 border-radius-2">&#8369;</span>
                                             </div>
                                             <input id="input-cost" type="text" class="form-control text-uppercase" name="cost" value="" placeholder="Buy Price" required>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="row d-block d-md-none">
          <div class="col-12 text-right">
               <a href="<?php echo base_url('Purchase'); ?>" class="btn btn-light border mt-3 mr-1">
                    CLOSE
               </a>
               <a href="<?php echo base_url('Purchase/create'); ?>" class="btn btn-secondary border mt-3 mr-1">
                    RESET
               </a>
               <button type="submit" class="btn btn-primary mt-3">
                    <span>SUBMIT</span>
               </button>
          </div>
     </div>
     <div class="modal fade confirm-modal">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure you want to add new transaction?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm" name="create" value="create"><span>CONFIRM</span></button>
                    </div>
               </div>
          </div>
     </div>
</form>

<style>
     .error-wrapper {
          display: none;      
     }
     .datepicker {
          z-index: 1 !important;
     }
</style>
<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/purchase-create.js'); ?>"></script>

<script type="text/javascript">
     var branchesJSON = <?php echo json_encode($branches); ?>;
     var currentBranchJSON = "<?php echo $this->session->userdata['branch_fk']; ?>";
     var typesJSON = <?php echo json_encode($types); ?>;
     var brandsJSON = <?php echo json_encode($brands); ?>;
     var productsJSON = <?php echo json_encode($products); ?>;
</script>

<script type="text/javascript">
     $(function() {
          initSelect2();
          initDatePicker();
          getDates();
          getBranches(branchesJSON, currentBranchJSON);
          getTypes(typesJSON, brandsJSON, productsJSON);
          validateForm("#transaction-create-form");
          // validateForm();
          getNewType();
          getNewBrand();
          getNewName();
          getDescription();
          getCost();
          getQuantity();
          getPrice();
          getTotal();
          checkItemDetails();
     });
</script>
