<div class="row no-gutters">
     <div class="col-12 col-md-6 align-self-end">
          <?php 
               $date = date('Y-m-d');
               purchase_tab('records'); 
          ?>
     </div>
     <div class="col-12 col-md-6 text-right border-bottom-nav align-self-end mt-md-0 mt-3">
          <div id='table-toolbar'>

               <div id="table-options" class="table-options d-inline-block">
                    <div class="input-group">
                         <div class="input-group-prepend">
                              <span class="input-group-text py-0 border-radius-left-2 ti-filter">
                              </span>
                         </div>
                         <a href="#" id="filter-toggle" class="btn btn-light border-radius-right-2 btn-sm border font-11" data-toggle="collapse" data-target="#filters">
                              SHOW FILTERS
                         </a>
                    </div>
               </div>
          </div>
     </div>
</div>


<div class="row">
     <div class="col-12 col-sm-12 grid-margin stretch-card">
          <div class="card border-top-0" id="transaction-records">
               <div class="card-body pt-4">
                    <!-- FILTERS  -->
                    <div class="text-uppercase mb-2 collapse" id="filters">
                         <div class="row narrow-gutters">
                              <div class="col-12 col-sm-4 col-lg-2 offset-lg-5">
                                   <div class="form-group mb-3">
                                        <label class="mb-0 text-muted"><small class='ti-filter'></small>&ensp;Location</label>
                                        <select id="select-branch" class="form-control select2" name="branch"></select>
                                   </div>
                              </div>
                              <div class="col-12 col-sm-3 col-lg-2">
                                   <div class="form-group mb-3">
                                        <label class="mb-0 text-muted"><small class='ti-filter'></small>&ensp;Status</label>
                                        <select id="select-status" class="form-control select2" name="status">
                                             <option value="ALL">ALL</option>
                                             <option value="FOR SALE">FOR SALE</option>
                                             <option value="SOLD">SOLD</option>
                                        </select>
                                   </div>
                              </div>
                              <div class="col-12 col-sm-5 col-lg-3">
                                   <div class="form-group mb-0 date-range">
                                        <label class="mb-0 text-muted"><small class='ti-filter'></small>&ensp;Date Range</label>
                                        <input type="text" placeholder="FROM" class="date-range-from form-control mb-2 from" id="trans-min-date" data-date-format="yyyy-mm-dd">
                                        <input type="text" placeholder="TO" class="date-range-to form-control mb-0 to" id="trans-max-date" data-date-format="yyyy-mm-dd">
                                   </div>
                              </div>
                         </div>
                    </div>
                    <!-- FILTERS  -->

                    <div class="table-responsive">
                         <img src="<?php echo base_url('assets/images/loader.gif'); ?>" alt="Loading" class="loader">
                         <table id="transaction-table" class="fade">
                              <thead>
                                   <tr class="table-header">
                                        <th class='cell-status'>Status</th>
                                        <th class='cell-transaction-date'>Date</th>
                                        <th class='cell-item-type'>Item Type</th>
                                        <th class='cell-product'>Item Name</th>
                                        <th class='cell-product'>Item Description</th>
                                        <th class='cell-transaction-id'>Transaction ID</th>
                                        <th class='cell-branch'>Location</th>
                                        <th class='no-sort'></th>
                                   </tr>
                              </thead>
                              <tbody>
                                   
                                   <?php
                                        if($transactions) {
                                             foreach ($transactions as $t => $tran) {
                                                  echo "<tr>";
                                                  echo      "<td class='text-nowrap'>" .
                                                                 $tran['status'] . 
                                                            "</td>";
                                                  echo      "<td class='text-nowrap'>" . 
                                                                 date('d M Y', strtotime($tran['date'])) .
                                                            "</td>";
                                                  echo      "<td>" . 
                                                                 $tran['item_type'] . 
                                                            "</td>";
                                                  echo      "<td>" . 
                                                                 $tran['item_name'] .
                                                            "</td>";
                                                  echo      "<td>" . 
                                                                 $tran['item_description'] . 
                                                            "</td>";
                                                  echo      "<td>" . 
                                                                 $tran['transaction_id'] . 
                                                            "</td>";
                                                  echo      "<td class='text-nowrap'>" . 
                                                                 $tran['location'] .
                                                            "</td>";
                                                  echo      "<td>" . 
                                                                 "<a class='btn btn-primary btn-sm font-11 p-2 w-100'  href='" . base_url('Purchase?search=') . $tran['transaction_id'] . "'>
                                                                      VIEW
                                                                 </a>" .
                                                            "</td>";
                                                  echo "</tr>";
                                             }

                                        }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>

               <div class="card-body d-none">
                    <div class="col-12 col-sm-4">
                         <div class="form-group">
                              <div class="text-field d-block">
                                   <label>Branch</label>
                                   <p id="selected-branch"> </p>
                              </div>
                         </div>
                    </div>
                    <div class="col-12">
                         <div class="form-group">
                              <div class="text-field d-block">
                                   <label></label>
                                   <ul id="field-transactions"> </ul>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>

<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/purchase.js'); ?>"></script>

<script type="text/javascript">
     var transactions = <?php echo json_encode($transactions); ?>;
     var branches = <?php echo json_encode($branches); ?>;
     var base_url = "<?php echo base_url(); ?>";
</script>

<script type="text/javascript">
     $(function() {
          initSelect2();
          getBranches(branches);
          // getTransactions(transactions, base_url);
          toggleFilters();
     });
</script>

<script type="text/javascript">
     $(window).on("load", function() {
          initDataTable();
          showTable();
          $("#transaction-table_filter").appendTo("#table-toolbar");
          $("#transaction-table_length").appendTo("#table-toolbar");
     });
</script>
