<div class="row no-gutters">
     <div class="col-lg-1">
          <h4 class='font-weight-bold mb-3'>Retails</h4>
     </div>     
     <div class="col-lg-11 text-right animated fadeIn delay-1s">
          <div class="d-inline-block mb-2 ml-2" id="table-length"></div>
          <div class="d-inline-block mb-2 ml-2 text-uppercase" id="table-filter"></div>
     </div>
</div>

<div class="row no-gutters">
     <div class="col-12">
          <?php retail_tab('returned') ?>
     </div>
</div>

<div class="row">
     <div class="col-12 col-sm-12">
          <div class="card border-top-0">
               <div class="card-body pt-4">
                    <div class="table-responsive pb-0">
                         <table class="table animated fadeIn text-uppercase" id="product-table">
                              <thead>
                                   <tr class="text-uppercase">
                                        <th class="no-sort bg-img-none">#</th>
                                        <th class="text-left">Reference ID</th>
                                        <th class="text-left">Date</th>
                                        <?php if($role_fk == 'ADMS') { ?>
                                             <th class="text-left">Location</th>
                                        <?php } ?>
                                        <th class="text-left">Type</th>
                                        <th class="text-left">Product</th>
                                        <th class="text-left no-sort">Remarks</th>
                                        <th class="text-left">Status</th>
                                        
                                        <th class="no-sort">Refund</th>
                                        <th class='no-sort'></th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php foreach ($returns as $r => $return) { 
                                             // if($return['status'] !== 'Transferred') {
                                   ?>
                                             <tr>
                                                  <td> <?php ++$r ?> </td>
                                                  <td> 
                                                       <a href="<?php echo base_url('Transaction?search=') . $return['transaction_id']; ?>">
                                                            <?php echo $return['transaction_id']; ?>
                                                       </a>
                                                  </td>
                                                  <td> <?php echo date("d M Y" , strtotime($return['date'])); ?> </td>
                                                  <?php if($role_fk == 'ADMS') { ?>
                                                       <td> <?php echo $return['branch']; ?> </td>
                                                  <?php } ?>
                                                  <td> <?php echo $return['type']; ?> </td>
                                                  <td> <?php echo $return['product']; ?> </td>
                                                  <td> <?php echo $return['action'] . " - " . $return['remarks']; ?> </td>
                                                  <td> <?php echo $return['status']; ?> </td>
                                                  <td class="text-right"> <?php echo number_format($return['amount'], 2); ?> </td>
                                                  <td class="text-center">
                                                       <?php if($return['status'] !== 'For Transfer' && $return['status'] !== 'Transferred'){ ?>
                                                            <ul class="list-inline list-inline-barred m-0 text-small">
                                                                 <li class="list-inline-item">
                                                                      <a href="#" class="text-decoration-none primary font-11" data-toggle='modal' data-target='#status-modal' title="Update Status" data-id='<?php echo $return['retail_id']; ?>'>
                                                                           UPDATE STATUS
                                                                      </a>
                                                                 </li>
                                                                 <li class="list-inline-item">
                                                                      <a href="#" 
                                                                           class="transfer-btn text-decoration-none d-block font-11" 
                                                                           data-type="<?php echo $return['type']; ?>"
                                                                           data-type-fk="<?php echo $return['type_fk']; ?>"
                                                                           data-brand="<?php echo $return['brand']; ?>"
                                                                           data-brand-fk="<?php echo $return['brand_fk']; ?>"
                                                                           data-name="<?php echo $return['name']; ?>"
                                                                           data-remarks="<?php echo $return['remarks']; ?>"
                                                                           data-name-fk="<?php echo $return['name_fk']; ?>"
                                                                           data-unit-fk="<?php echo $return['unit_fk']; ?>"
                                                                           data-buy-price="<?php echo $return['buy']; ?>"
                                                                           data-retail-id="<?php echo $return['retail_id']; ?>"
                                                                           data-stock="<?php echo $return['quantity']; ?>"> 
                                                                                TRANSFER
                                                                      </a>
                                                                 </li>
                                                            </ul>
                                                       <?php } else if($return['status'] == 'For Transfer' ) { ?>
                                                            <ul class="list-inline list-inline-barred m-0 text-small">
                                                                 <li class="list-inline-item">
                                                                      <a href="<?php echo base_url('Retail/voidReturnTransfer/') . $return['shipment_id'] . '/' . $return['retail_id']; ?>" class="text-decoration-none primary font-11">
                                                                          VOID TRANSFER
                                                                      </a>
                                                                 </li>
                                                            </ul>
                                                       <?php } ?>
                                                  </td>
                                             </tr>
                                   <?php
                                             // } 
                                        } ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>

<!-- UDPATE STATUS MODAL -->
<form class="d-inline needs-validation" action="<?php echo base_url('Retail/updateReturnStatus'); ?>" method="post" novalidate autocomplete="off">
     <div class="modal fade form-modal" id="status-modal" tabindex="-1">
          <input type="hidden" name="id" value="">
          <div class="modal-dialog modal-sm">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-uppercase text-primary">Update Status</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="form-group mb-0">
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="Returned" required>
                                        Returned
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Sale" required>
                                        For Sale
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Repair" required>
                                        For Repair
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="For Disposal" required>
                                        For Disposal
                                   </label>
                              </div>
                              <div class="form-check form-check-primary">
                                   <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="Disposed" required>
                                        Disposed
                                   </label>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary" data-dismiss="modal">CANCEL</button>
                         <button type="button" class="col btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#confirm-status">UPDATE</button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="confirm-status">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to update status?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#status-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm" name="update_status" value="update">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- UDPATE STATUS MODAL end -->

<!-- TRANSFER MODAL -->
<form class="d-inline needs-validation" action="<?php echo base_url("Retail/createShipping"); ?>" method="post" novalidate autocomplete="off">
     <input id="transfer-retail-id" name="retail_id" type="hidden">
     <input id="transfer-type-fk" name="transfer_type_fk" type="hidden">
     <input id="transfer-brand-fk" name="transfer_brand_fk" type="hidden">
     <input id="transfer-name-fk" name="transfer_name_fk" type="hidden">
     <input id="input-quantity" name="transfer_quantity" type="hidden" min="1" step="1" class="form-control h-auto" placeholder="Quantity" required>
     <input id="transfer-unit-fk" name="transfer_unit_fk" type="hidden">
     <input id="input-buy" type="hidden" class="form-control" name="transfer_buy_price" placeholder="Buy Price" required maxlength="11" >
     <div class="modal fade form-modal" id="transfer-modal" tabindex="-1">
          <div class="modal-dialog modal-lg">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-uppercase text-info">
                              Transfer Item
                         </h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <div class="row">
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label>Date</label>
                                        <input type="text" name="transfer_date" class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date('F d, Y'); ?>" required>
                                   </div>
                              </div>
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label>Origin</label>
                                        <?php if($this->session->role_fk == "ADMS"){ ?>
                                             <select class="form-control" name="transfer_origin" required>
                                                  <?php
                                                       foreach ($branches as $branch) {
                                                            echo "<option value='" . $branch['id'] . "'  " . ($branch['id'] == $this->session->userdata('branch_fk') ? 'selected' : '') . "> " .
                                                                      $branch['name'] . " (" . $branch['id'] . ")
                                                                 </option>";
                                                       }
                                                  ?>
                                             </select>
                                        <?php } else { ?>
                                             <select class="form-control" name="transfer_origin" required>
                                                  <?php
                                                       foreach ($branches as $branch) {
                                                            if($branch['id'] == $this->session->userdata('branch_fk'))
                                                            echo "<option value='" . $branch['id'] . "'> " . $branch['name'] . " (" . $branch['id'] . ") </option>";
                                                       }
                                                  ?>
                                             </select>

                                        <?php } ?>
                                   </div>
                              </div>
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label>Destination</label>
                                        <?php if($this->session->role_fk == "ADMS"){ ?>
                                             <select class="form-control" name="transfer_destination" required>
                                                  <option value="" selected disabled>SELECT</option>
                                                  <?php
                                                       foreach ($branches as $branch) {
                                                            echo "<option value='" . $branch['id'] . "' > " .
                                                                      $branch['name'] . " (" . $branch['id'] . ")
                                                                 </option>";
                                                       }
                                                  ?>
                                             </select>
                                        <?php } else { ?>
                                             <select class="form-control" name="transfer_destination" required>
                                                  <option value="" selected disabled>SELECT</option>
                                                  <?php
                                                       foreach ($branches as $branch) {
                                                            if($branch['id'] !== $this->session->userdata('branch_fk'))
                                                            echo "<option value='" . $branch['id'] . "'> " . $branch['name'] . " (" . $branch['id'] . ") </option>";
                                                       }
                                                  ?>
                                             </select>
                                        <?php } ?>
                                   </div>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label>Type</label>
                                        <input id="tansfer-type" type="text" class="form-control text-uppercase" disabled>
                                   </div>
                              </div>
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label>Brand</label>
                                        <input id="transfer-brand" type="text" class="form-control text-uppercase" disabled>
                                   </div>
                              </div>
                              <div class="col-md-4">
                                   <div class="form-group">
                                        <label>Name</label>
                                        <input id="transfer-name" type="text" class="form-control text-uppercase" disabled>
                                   </div>
                              </div>
                              
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Remarks / Reason of transfer</label>
                                        <input id="transfer-remarks" type="text" class="form-control" name="transfer_remarks" placeholder="Remarks / Reason of transfer" maxlength="255">
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal"> CANCEL </button>
                         <button type="button" class="btn btn-info" data-dismiss="modal" data-toggle="modal" data-target="#transfer-confirm" > TRANSFER </button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="transfer-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to transfer the item?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#transfer-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>
<!-- TRANSFER MODAL end -->


<!-- JAVA SCRIPT -->
<script type="text/javascript" src="<?php echo base_url('assets/js/retail.js'); ?>"></script>

<script type="text/javascript">
     $(document).ready( function() {
          initSelect2();
          initDataTable();
          initModal();
          validateForm2();          
          $("#product-table_length").appendTo("#table-length");
          $("#product-table_filter").appendTo("#table-filter");

          $(".transfer-btn").on("click", function() {
               var type = $(this).attr("data-type");
               var brand = $(this).attr("data-brand");
               var name = $(this).attr("data-name");
               var remarks = $(this).attr("data-remarks");

               var type_fk = $(this).attr("data-type-fk");
               var brand_fk = $(this).attr("data-brand-fk");
               var name_fk = $(this).attr("data-name-fk");
               var retail_id = $(this).attr("data-retail-id");

               var unit_fk = $(this).attr("data-unit-fk");
               var stock = $(this).attr("data-stock");
               var buy_price = $.number($(this).attr("data-buy-price"), 2);

               $("#tansfer-type").val(type);
               $("#transfer-brand").val(brand);
               $("#transfer-name").val(name);
               $("#transfer-remarks").val(remarks);               
               $("#transfer-type-fk").val(type_fk);
               $("#transfer-brand-fk").val(brand_fk);
               $("#transfer-name-fk").val(name_fk);
               $("#transfer-retail-id").val(retail_id);
               $("#transfer-unit-fk").val(unit_fk);
               $("#input-quantity").val(stock);;
               $("#input-buy").attr("value", buy_price);

               $("#transfer-modal").modal("show");
          });

     });
</script>