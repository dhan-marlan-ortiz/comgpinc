<?php 
     transfer_tab('inbound'); 
?> 
<input type="hidden" name="" value="<?php echo base_url(); ?>" id="base-url">
<div class="row">
     <div class="col-12 col-sm-12 grid-margin stretch-card">
          <div class="card border-top-0" id="transaction-records">
               <div class="card-body">
                    <div class="table-responsive">
                         <table id="transaction-table" class="table">
                              <thead class="text-uppercase">
                                   <tr>
                                        <th class='no-sort'>#</th>
                                        <th>Date</th>
                                        <th>Reference ID</th>
                                        <th>Category</th>
                                        <th>Item Type</th>
                                        <th>Item Name</th>
                                        <th class="no-sort text-center">Qty</th>
                                        <th class="no-sort">Remarks</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th class='no-sort text-center'>Action</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php
                                        if($transfers) {
                                             foreach ($transfers as $t => $transfer) {
                                                  $category_name = "";
                                                  $quantity = 1;
                                                  if($transfer['transaction_id'][5] == 'T') {
                                                       $category_name = "Pawned Item";
                                                  } else if($transfer['transaction_id'][5] == 'P') {
                                                       $category_name = "Purchased Item";
                                                  }
                                                  echo "<tr>";
                                                  echo      "<td></td>";
                                                  echo      "<td class='text-uppercase'>" . date("d M Y", strtotime($transfer['date_created'])) . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $transfer['transaction_id'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $category_name . "</td>";
                                                  echo      "<td class='text-uppercase'>" . 
                                                                 ($transfer['transaction_id'][5] == 'T' ? $transfer['transactions_type_name'] : $transfer['purchases_type_name']) .                                                                                                                                  
                                                            "</td>";
                                                  echo      "<td class='text-uppercase'>" . 
                                                                 ($transfer['transaction_id'][5] == 'T' ? $transfer['transactions_brand_name'] . " " . $transfer['transactions_item_name'] : $transfer['purchases_brand_name'] . " " . $transfer['purchases_item_name']) .                                                                 
                                                            "</td>";
                                                  echo      "<td class='text-uppercase text-center'>" . $quantity. "</td>";
                                                  echo      "<td class='text-uppercase'>" . $transfer['remarks'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $transfer['origin_name'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $transfer['destination_name'] . "</td>";
                                                  echo      "<td class='text-center'>
                                                                 <div class='input-group d-inline'>
                                                                      <button id='' class='accept-btn btn btn-primary btn-sm' type='button' data-toggle='modal' data-target='#accept-modal' 
                                                                           data-id='". $transfer['transaction_id'] ."' 
                                                                           data-destination='". $transfer['destination'] ."'
                                                                           data-action='". base_url('Transaction/acceptTransfer') ."'
                                                                      >
                                                                           <small>ACCEPT</small>
                                                                      </button>
                                                                 </div>
                                                            </td>";
                                                  echo "</tr>";
                                             }
                                        }
                                        if($shipments) {
                                             foreach ($shipments as $s => $shipment) {
                                                  echo "<tr>";
                                                  echo      "<td></td>";
                                                  echo      "<td class='text-uppercase'>" . date("d M Y", strtotime($shipment['date'])) . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['transaction_id'] . "</td>";
                                                  echo      "<td class='text-uppercase'>Retail Item</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['type'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['brand'] . " " . $shipment['name'] . "</td>";
                                                  echo      "<td class='text-uppercase text-center'>" . $shipment['quantity'] . " " . $shipment['unit'] .  "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['remarks'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['origin_name'] . "</td>";
                                                  echo      "<td class='text-uppercase'>" . $shipment['destination_name'] . "</td>";
                                                  echo      "<td class='text-center'>
                                                                 <div class='input-group d-inline'>
                                                                      <button id='' class='btn btn-primary btn-sm accept-btn' type='button' data-toggle='modal' data-target='#accept-modal' 
                                                                           data-id='".$shipment['transaction_id']."' 
                                                                           data-destination='".$shipment['destination']."'
                                                                           data-action='". base_url('Retail/acceptTransfer') ."'
                                                                      >
                                                                           <small>ACCEPT</small>
                                                                      </button>
                                                                 </div>
                                                            </td>";
                                                  echo "</tr>";
                                             }
                                        }
                                   ?>
                              </tbody>
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
<form class="d-inline needs-validation" id="accept-form" action="" method="post" novalidate autocomplete="off">     
     <div class="modal fade form-modal" id="accept-modal" tabindex="-1">
          <div class="modal-dialog modal-sm">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title text-uppercase text-info"> Accept Transfer </h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="modal-body">
                         <div class="form-group">
                              <label>Reference ID</label>
                              <input class="form-control bg-white" type="text" name="accept-id" required readonly="readonly">     
                              <input type="hidden" name="accept-destination" required>     
                         </div>                    
                         <div class="form-group">
                              <label>Date Received</label>
                              <input type="text" name="accept-date" class="datepicker form-control text-uppercase" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date('F d, Y'); ?>" required>                              
                         </div>                    
                         <div class="form-group mb-2">
                              <label>Set Status </label>
                              <select class="form-control close-on-click" name="accept-status" required>
                                   <option value="For Sale" selected="selected">FOR SALE</option>
                                   <?php if($role_fk == 'ADMS') { ?>
                                        <option value="For Repair">FOR REPAIR</option>
                                        <option value="For Disposal">FOR DISPOSAL</option>
                                   <?php } ?>
                              </select>
                         </div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary" data-dismiss="modal"> CANCEL </button>
                         <button type="button" class="col btn btn-info" data-dismiss="modal" data-toggle="modal" data-target="#accept-confirm" > ACCEPT </button>
                    </div>
               </div>
          </div>
     </div>
     <div class="modal fade confirm-modal" id="accept-confirm">
          <div class="modal-dialog modal-sm" role="document">
               <div class="modal-content">
                    <div class="modal-header">
                         <h5 class="modal-title">Confirmation</h5>
                         <button type="button" class="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p class="mb-0">Are you sure want to accept?</p>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#accept-modal">CANCEL</button>
                         <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                    </div>
               </div>
          </div>
     </div>
</form>

<style>
     .dropdown-menu {
          min-width: auto;
          padding: 5px 0;
     }
     .dropdown-item {
          font-size: 12px;
          padding: 10px 13px;
     }
     .dropdown-item + .dropdown-item {
          border-top: 1px solid #c9ccd7;
     }
</style>

<script type="text/javascript" src="<?php echo base_url('assets/js/transfer.js'); ?>"></script>

<script type="text/javascript">
     $(function() {
          initSelect2();
          initDataTable();
          validateForm();
          initDatePicker();
          $("#transaction-table_filter").appendTo("#table-toolbar");
          $("#transaction-table_length").appendTo("#table-toolbar");

          $(".accept-btn").on("click", function() {
               var form = $("#accept-form");
               form.attr('action', $(this).attr('data-action'));
               form.find("input[name='accept-id']").val($(this).attr('data-id'));
               form.find("input[name='accept-destination']").val($(this).attr('data-destination'));
          });
     });
</script>