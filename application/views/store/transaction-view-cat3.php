<?php
     // $status = $transaction['status'];
     $category = ($transaction['category'] == 1 ? 'Pawn' : ($transaction['category'] == 2 ? 'Purchase' :  ($transaction['category'] == 3 ? 'Retail' : '')  ) );
     $user = $transaction['first_name'] . " " . $transaction['last_name'];
     $transactionId = $transaction['transaction_id'];
     $status = $transaction['status'];
     $date = date("d M Y",strtotime($transaction['date']));
?>

<div class="card-body <?php echo ($status == 'Void' ? 'void' : ''); ?>">
     <div class="row">
          <div class="col-12 col-sm-6">
               <h4 class="card-title text-primary">INFORMATION</h4>
          </div>
     </div>
     <div class="row narrow-gutters text-nowrap">
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>Location</label>
                         <p> <?php echo $transaction['branch_name'] . " (" . $transaction['branch_fk'] . ")"; ?> </p>
                    </div>
               </div>
          </div>
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>Transaction ID</label>
                         <p> <?php echo $transactionId; ?> </p>
                    </div>
               </div>
          </div>
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>Transaction Date</label>
                         <p> <?php echo $date; ?> </p>
                    </div>
               </div>
          </div>
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>Status</label>
                         <p> <?php echo $status; ?> </p>
                    </div>
               </div>
          </div>
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>Category</label>
                         <p> <?php echo $category; ?> </p>
                    </div>
               </div>
          </div>
          <div class="col">
               <div class="form-group">
                    <div class="text-field d-block">
                         <label>User</label>
                         <p> <?php echo $user; ?> </p>
                    </div>
               </div>
          </div>
     </div>
</div>

<div class="card-body border-top">
     <div class="row">
          <div class="col-6">
               <p class="font-weight-semibold py-2"> <i class="ti-shopping-cart"></i>&emsp;SALES DETAILS </p>
          </div>
          <div class="col-6">
               <?php if( $status != 'Void') { ?>
                    <button type="button" class="float-right btn btn-warning btn-sm text-uppercase btn-icon-text text-white" data-toggle="modal" data-target="#return-modal">
                         <span class="d-none d-sm-inline-block">Return Item</span>
                         <small class="m-0 ml-sm-1 ti-reload btn-icon-append"></small>
                    </button>

                    <!-- RETURN ITEM MODAL -->
                    <form class="d-inline needs-validation" action="<?php echo base_url('Retail/return'); ?>" method="post" novalidate autocomplete="off">
                         <div class="modal fade form-modal" id="return-modal" tabindex="-1">
                              <div class="modal-dialog">
                                   <input type="hidden" name="reference_id" value="<?php echo $transactionId; ?>">
                                   <div class="modal-content">
                                        <div class="modal-header text-warning">
                                             <h5 class="modal-title text-uppercase">
                                                  Return Item
                                             </h5>
                                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                             </button>
                                        </div>
                                        <div class="modal-body">
                                             <div class="row">
                                                  <div class="col-sm-6 border-right-sm">
                                                       <div class="form-group">
                                                            <label>Date</label>
                                                            <input type="text" name="return_date" class="datepicker form-control" placeholder="Month day, Year" data-date-format="MM dd, yyyy" value="<?php echo date("F d, Y"); ?>"  required>
                                                            <div class="invalid-feedback"> Please provide a valid date. </div>
                                                       </div>
                                                       <div class="form-group">
                                                            <label>Product</label>
                                                            <select class="form-control" name="return_sales_id" required>
                                                                 <option value="" disabled selected>SELECT ACTION</option>
                                                                 <?php
                                                                 foreach ($retails as $r => $return_item) {
                                                                      echo "<option data-quantity='" . $return_item['quantity'] . "' value='" . $return_item['sales_id'] . "'>" . $return_item['brand'] . " " . $return_item['name']  . " - " . $return_item['type'] . "</option>";
                                                                 }
                                                                 ?>
                                                            </select>
                                                       </div>
                                                       <!-- <div class="form-group"> -->
                                                            <!-- <label>Quantity</label> -->
                                                            <input type="hidden" step="1" min="1"  name="return_quantity" class="form-control" placeholder="Quantity" value="1" required>
                                                            <!-- <div class="invalid-feedback"> Please provide a valid quantity. </div> -->
                                                       <!-- </div> -->
                                                  </div>
                                                  <div class="col-sm-6">
                                                       <div class="form-group">
                                                            <label>Action</label>
                                                            <select class="form-control" name="return_action" required>
                                                                 <option value="" disabled selected>SELECT ACTION</option>
                                                                 <option value="Item Replacement">ITEM REPLACEMENT</option>
                                                                 <option value="Cash Refund">CASH REFUND</option>
                                                            </select>
                                                       </div>

                                                       <div class="form-group" id="return-quantity" style="display: none">
                                                            <label>Refund Amount</label>
                                                            <input type="number" name="return_amount" class="form-control" placeholder="Refund Amount" required disabled>
                                                            <div class="invalid-feedback"> Please provide a valid refund amount. </div>
                                                       </div>
                                                       <div class="form-group mb-3">
                                                            <label>Remarks</label>
                                                            <textarea name="return_remarks" class="form-control" rows="4" spellcheck="false" placeholder="Remarks" maxlength="255"></textarea>
                                                       </div>
                                                  </div>
                                             </div>                                             
                                        </div>
                                        <div class="modal-footer">
                                             <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
                                             <button type="button" class="btn btn-warning" data-dismiss="modal" data-toggle="modal" data-target="#return-modal-confirm" > RETURN </button>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div class="modal fade confirm-modal" id="return-modal-confirm" tabindex="-1">
                              <div class="modal-dialog modal-sm" role="document">
                                   <div class="modal-content">
                                        <div class="modal-header">
                                             <h5 class="modal-title">Confirmation</h5>
                                             <button type="button" class="close" data-dismiss="modal">
                                                  <span aria-hidden="true">&times;</span>
                                             </button>
                                        </div>
                                        <div class="modal-body">
                                             <p class="mb-0">Are you sure want to return the item?</p>
                                        </div>
                                        <div class="modal-footer">
                                             <button type="button" class="col btn btn-secondary cancel" data-dismiss="modal" data-toggle="modal" data-target="#return-modal">CANCEL</button>
                                             <button type="submit" class="col btn btn-success confirm">CONFIRM</button>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </form>
                    <!-- RETURN ITEM MODAL end -->
               <?php } ?>
          </div>
     </div>
     <div class="table-responsive">
          <table class="table">
               <thead class="border-bottom-dark-2">
                    <tr>
                         <th>#</th>
                         <th>TYPE</th>
                         <th>PRODUCT</th>
                         <th class="text-center">QUANTITY</th>
                         <th class="text-center">UNIT</th>
                         <th class="text-right">AMOUNT</th>
                         <th class="text-right">SUB TOTAL</th>
                    </tr>
               </thead>
               <tbody>
                    <?php
                         $total = 0;
                         $count = 0;
                         foreach ($retails as $r => $retail) {
                              $product = $retail['brand'] . " " . $retail['name'];
                              $quantity = $retail['quantity'];
                              $amount = $retail['selling_price'];
                              $subtotal = $quantity * $amount;
                              $total += $subtotal;
                              $count += $quantity;

                              echo "<tr>" .
                                        "<td>" . ++$r . "</td>" .
                                        "<td>" . $retail['type'] . "</td>" .
                                        "<td>" . $product . "</td>" .
                                        "<td class='text-center'>" . number_format($quantity) . "</td>" .
                                        "<td class='text-center'>" . $retail['unit'] . "</td>" .
                                        "<td class='text-right'>" . number_format($amount, 2) . "</td>" .
                                        "<td class='text-right'>" . number_format($subtotal, 2) . "</td>" .
                                   "</tr>";
                         }
                    ?>
               </tbody>
               <tfoot class="border-top-double">
                    <tr>
                         <th></th>
                         <th></th>
                         <th></th>
                         <th class="text-center">
                              <?php echo number_format($count); ?>
                         </th>
                         <th></th>
                         <th></th>
                         <th class="text-right">
                              <?php echo "&#8369; " . number_format($total, 2); ?>
                         </th>
                    </tr>
               </tfoot>
          </table>
     </div>
     <div class="row">
          <div class="col">
               <?php if($this->session->userdata('role_fk') == 'ADMS' && $status != 'Void' && !$retail_returns) { ?>
                    <button type="button" class="btn btn-dark btn-sm text-uppercase btn-icon-text mt-2 mr-1 text-white" data-toggle="modal" data-target="#void-modal">
                         Void<i class="ti-trash btn-icon-append"></i>
                    </button>

                    <!-- VOID MODAL -->
                    <div class="modal fade confirm-modal" id="void-modal">
                         <div class="modal-dialog modal-sm" role="document">
                              <div class="modal-content">
                                   <div class="modal-header">
                                        <h5 class="modal-title">Confirmation</h5>
                                        <button type="button" class="close" data-dismiss="modal">
                                             <span aria-hidden="true">&times;</span>
                                        </button>
                                   </div>
                                   <div class="modal-body">
                                        <p class="mb-0">Are you sure want to void transaction?</p>
                                   </div>
                                   <div class="modal-footer">
                                        <a href="" class="col btn btn-secondary cancel" data-dismiss="modal">CANCEL</a>
                                        <a href="<?php echo base_url('Retail/void/') . $transactionId; ?>" class="col btn btn-danger confirm">CONFIRM</a>
                                   </div>
                              </div>
                         </div>
                    </div>
               <?php } ?>

          </div>
     </div>
</div>

<?php if($retail_returns){ ?>
<div class="card-body border-top">
     <div class="row">
          <div class="col-12">
               <p class="font-weight-semibold py-2"> <i class="ti-reload"></i>&emsp;RETURNED ITEMS </p>
          </div>
     </div>
     <div class="table-responsive" id="retail-returns-table">
          <table class="table text-uppercase">
               <thead class="border-bottom-dark-2">
                    <tr>
                         <th>#</th>
                         <th>DATE</th>
                         <th>PRODUCT</th>
                         <th>QUANTITY</th>
                         <th>REMARKS</th>
                         <th class="text-right">AMOUNT</th>
                         <th></th>
                    </tr>
               </thead>
               <tbody>
                    <?php
                    $return_total = 0;
                    $return_count = 0;

                    foreach ($retail_returns as $rr => $return) {
                         $return_total += $return['amount'];
                         $return_count += $return['quantity'];
                         echo "<tr>" .
                                   "<td class='align-top'>" . ++$rr . "</td>" .
                                   "<td class='align-top'>" . date("d M Y", strtotime($return['date'])) . "</td>" .
                                   "<td class='align-top'>" .
                                        "<p class='mb-0'>" .
                                             $return['product'] .
                                        "</p>" .
                                        "<ul class='list-inline text-small text-muted mb-0'>
                                             <li class='list-inline-item'>
                                                  <i class='ti-package'></i> " . $return['type'] . "
                                             </li>
                                             <li class='list-inline-item'>
                                                  <i class='ti-location-pin'></i> " . $return['branch'] . "
                                             </li>
                                             <li class='list-inline-item'>
                                                  <i class='ti-user'></i> " . $return['first_name'] . " " . $return['last_name'] . "
                                             </li>
                                        </ul>" .
                                   "</td>" .
                                   "<td class='align-top'>" .
                                        $return['quantity'] .  " <span class='text-muted'>" . $return['unit'] . "<span>" .
                                   "</td>" .
                                   "<td class='align-top'>" .
                                        $return['remarks'] .
                                   "</td>" .
                                   "<td class='align-top text-right'>" .
                                        number_format($return['amount'], 2) .
                                   "</td>" .
                                   "<td class='align-top text-right'>" .
                                        "<a href='#' class='btn btn-danger btn-sm text-decoration-none " . ($this->session->userdata('role_fk') != 'ADMS' ? 'd-none' : '')  . "' data-toggle='modal' data-target='#void-return'>VOID</a>

                                        <div class='modal fade confirm-modal' id='void-return'>
                                             <div class='modal-dialog modal-sm' role='document'>
                                                  <div class='modal-content'>
                                                       <div class='modal-header'>
                                                            <h5 class='modal-title'>Confirmation</h5>
                                                            <button type='button' class='close' data-dismiss='modal'>
                                                                 <span aria-hidden='true'>&times;</span>
                                                            </button>
                                                       </div>
                                                       <div class='modal-body'>
                                                            <p class='mb-0'>Are you sure want to void return?</p>
                                                       </div>
                                                       <div class='modal-footer'>
                                                            <a href='#' class='col btn btn-secondary cancel' data-dismiss='modal'>CANCEL</button>
                                                            <a href='" . base_url('Retail/voidReturn/') . $transactionId . "/" . $return['id'] . "/" . $return['retail_id'] . "' class='col btn btn-danger confirm'>CONFIRM</a>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>

                                   </td>" .
                              "</tr>";
                    }
                    ?>
               </tbody>
               <tfoot class="border-top-double">
                    <tr>
                         <th></th>
                         <th></th>
                         <th></th>
                         <th>
                              <?php echo number_format($return_count); ?>
                         </th>
                         <th></th>
                         <th class="text-right">
                              <?php echo "&#8369; " . number_format($return_total, 2); ?>
                         </th>
                         <th></th>
                    </tr>
               </tfoot>
          </table>
     </div>
</div>
<?php } ?>
