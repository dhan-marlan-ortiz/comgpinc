          </div>
     </div>
</div>
<footer class="footer">
     <div class="d-sm-flex justify-content-center justify-content-sm-between">
          <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">Comunity Gadget Phoneshop</a>. All rights reserved.</span>
          <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Information System <i class="ti-desktop text-primary ml-1"></i></span>
     </div>
</footer>

</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="<?php echo base_url('assets/') ?>vendors/base/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="<?php echo base_url('assets/') ?>vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?php echo base_url('assets/') ?>js/off-canvas.js"></script>
<script src="<?php echo base_url('assets/') ?>js/hoverable-collapse.js"></script>
<script src="<?php echo base_url('assets/') ?>js/template.js"></script>
<script src="<?php echo base_url('assets/') ?>js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="<?php echo base_url('assets/') ?>js/dashboard.js"></script>

<!-- End custom js for this page-->

<script src="<?php echo base_url('assets/') ?>js/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/') ?>js/main.js"></script>

</body>

</html>
