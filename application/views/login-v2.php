<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>CGP | Login</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/') ?>images/favicon.ico" />

	<!-- plugins:css -->
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>vendors/ti-icons/css/themify-icons.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>vendors/base/vendor.bundle.base.css">
	<!-- endinject -->

	<!-- plugin css for this page -->
	<!-- End plugin css for this page -->

	<!-- inject:css -->
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/style.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/main.css">
	<!-- endinject -->

	<style media="screen">
		.auth .brand-logo img {

		}
		.brand-img {
			width: 100%;
			max-width: 230px;
		}
		.form-group {
			position: relative;
		}
		input.error {
			margin-bottom: 35px;
			border-color: rgba(204, 0, 0, 0.5);
		}
		label.error {
			position: absolute;
			color: rgba(204, 0, 0, 0.8);
			bottom: -35px;
			font-size: 13px !important;
		}
		.errors-authentication {
			font-size: 13px;
			color: rgba(204, 0, 0, 0.8);
		}
	</style>
</head>

<body>
	<div class="container-scroller">
		<div class="container-fluid page-body-wrapper full-page-wrapper">
			<div class="content-wrapper d-flex align-items-center auth lock-full-bg">
				<div class="row w-100">
					<div class="col-lg-4 mx-auto">
						<div class="auth-form-transparent text-left p-5 text-center">
							<img class="brand-img" src="<?php echo base_url('assets/images/logo.png'); ?>" alt="logo">
							<form class="pt-5">
								<div class="row narrow-gutters" >
									<div class="col-12 col-sm-6">              			
										<div class="form-group">                 
											<input type="text" class="form-control text-center" placeholder="Username">
										</div>
									</div>	
									<div class="col-12 col-sm-6">              			
										<div class="form-group">
											<input type="password" class="form-control text-center" placeholder="Password">
										</div>
									</div>	
								</div>
								<div class="mt-5">
									<a class="btn btn-block btn-success btn-lg font-weight-medium" href="../../index.html">Sign In</a>
								</div>
								<div class="mt-3 text-center d-none">
									<a href="#" class="auth-link text-white">Sign in using a different account</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- content-wrapper ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->
	<!-- plugins:js -->
	<script src="<?php echo base_url('assets/') ?>vendors/base/vendor.bundle.base.js"></script>
	<!-- endinject -->
	<!-- Plugin js for this page-->
	<script src="<?php echo base_url('assets/') ?>vendors/chart.js/Chart.min.js"></script>
	<!-- End plugin js for this page-->
	<!-- inject:js -->
	<script src="<?php echo base_url('assets/') ?>js/off-canvas.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/hoverable-collapse.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/template.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/todolist.js"></script>
	<!-- endinject -->
	<!-- Custom js for this page-->
	<script src="<?php echo base_url('assets/') ?>js/dashboard.js"></script>

	<!-- End custom js for this page-->

	<script src="<?php echo base_url('assets/') ?>js/jquery-3.4.1.min.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/jquery.validate.min.js"></script>
	<script src="<?php echo base_url('assets/') ?>js/main.js"></script>

</body>

</html>